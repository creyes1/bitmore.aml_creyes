// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskManagerService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The TaskManagerService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using Bitmore.Aml.DataAccess.Repositories;

    #endregion

    /// <summary>
    /// The TaskManagerService interface.
    /// </summary>
    public interface ITaskManagerService
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create back up and delete log.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CreateBackUpAndDeleteLog();

        #endregion
    }

    /// <summary>
    /// The task manager service.
    /// </summary>
    public class TaskManagerService : ITaskManagerService
    {
        #region Fields

        /// <summary>
        /// The task manager repository.
        /// </summary>
        private readonly ITaskManagerRepository taskManagerRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskManagerService"/> class.
        /// </summary>
        /// <param name="taskManagerRepository">
        /// The task manager repository.
        /// </param>
        public TaskManagerService(ITaskManagerRepository taskManagerRepository)
        {
            this.taskManagerRepository = taskManagerRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create back up and delete log.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CreateBackUpAndDeleteLog()
        {
            return this.taskManagerRepository.CreateBackUpAndDeleteLog();
        }

        #endregion
    }
}