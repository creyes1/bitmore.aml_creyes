// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserStatusService.cs" company="">
//   
// </copyright>
// <summary>
//   The UserStatusService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The UserStatusService interface.
    /// </summary>
    public interface IUserStatusService : IServiceBase<UserStatus>
    {
    }

    /// <summary>
    /// The user status service.
    /// </summary>
    public class UserStatusService : ServiceBase<UserStatus>, IUserStatusService
    {
        #region Fields

        /// <summary>
        /// The user status repository.
        /// </summary>
        private readonly IUserStatusRepository userStatusRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStatusService"/> class.
        /// </summary>
        /// <param name="userStatusRepository">
        /// The user status repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper"></param>
        public UserStatusService(
            IUserStatusRepository userStatusRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
            : base(userStatusRepository, unitOfWork, mapper)
        {
            this.userStatusRepository = userStatusRepository;
        }

        #endregion
    }
}