// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExportToXlsxService.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The ExportToXlsxService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using AutoMapper;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Resources.Language;

    using ClosedXML.Excel;

    #endregion

    /// <summary>
    /// The ExportToXlsxService interface.
    /// </summary>
    public interface IExportToXlsxService
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create xlsx to bytes.
        /// </summary>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <param name="sheetName">
        /// The sheet name.
        /// </param>
        /// <param name="reportTitle">
        /// The report title.
        /// </param>
        /// <param name="backgroundHtmlColor">
        /// The background html color.
        /// </param>
        /// <param name="fontHtmlColor">
        /// The font html color.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see>
        ///         <cref>byte[]</cref>
        ///     </see>
        ///     .
        /// </returns>
        byte[] CreateXlsxToBytes<T>(
            IEnumerable<T> enumerable,
            string sheetName = null,
            string reportTitle = null,
            string backgroundHtmlColor = null,
            string fontHtmlColor = null);

        /// <summary>
        /// The create xlsx to file.
        /// </summary>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <param name="fileFullPath">
        /// The file full path.
        /// </param>
        /// <param name="sheetName">
        /// The sheet name.
        /// </param>
        /// <param name="reportTitle">
        /// The report title.
        /// </param>
        /// <param name="backgroundHtmlColor">
        /// The background html color.
        /// </param>
        /// <param name="fontHtmlColor">
        /// The font html color.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string CreateXlsxToFile<T>(
            IEnumerable<T> enumerable,
            string fileFullPath,
            string sheetName = null,
            string reportTitle = null,
            string backgroundHtmlColor = null,
            string fontHtmlColor = null);

        /// <summary>
        /// The export to xlsx.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        string ExportToXlsx<T>(IEnumerable<T> list);

        #endregion
    }

    /// <summary>
    ///     The export to xlsx service.
    /// </summary>
    public class ExportToXlsxService : IExportToXlsxService
    {
        /// <summary>
        /// The action name.
        /// </summary>
        private const string ExtXlsx = @"xlsx";

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The app configuration.
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExportToXlsxService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="appConfiguration"></param>
        public ExportToXlsxService(
            IMapper mapper,
            IAppConfiguration appConfiguration)
        {
            this.mapper = mapper;
            this.appConfiguration = appConfiguration;
        }

        #region Constants

        /// <summary>
        ///     The column title.
        /// </summary>
        private const string ColumnTitle = "ColumnTitle";

        /// <summary>
        ///     The sheet name.
        /// </summary>
        private const string SheetName = "Sheet1";

        /// <summary>
        ///     The title.
        /// </summary>
        private const string Title = "Title";

        /// <summary>
        ///     The xlsx file extension.
        /// </summary>
        private const string XlsxFileExtension = ".xlsx";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create xlsx to bytes.
        /// </summary>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <param name="sheetName">
        /// The sheet name.
        /// </param>
        /// <param name="reportTitle">
        /// The report title.
        /// </param>
        /// <param name="backgroundHtmlColor">
        /// The background html color.
        /// </param>
        /// <param name="fontHtmlColor">
        /// The font html color.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>byte[]</cref>
        ///     </see>
        ///     .
        /// </returns>
        public byte[] CreateXlsxToBytes<T>(
            IEnumerable<T> enumerable,
            string sheetName = null,
            string reportTitle = null,
            string backgroundHtmlColor = null,
            string fontHtmlColor = null)
        {
            using (var workbook = new XLWorkbook())
            {
                // Get type
                var properties = this.GetProperties<T>();
                var data = this.GetDataTable(enumerable, properties);

                // Add worksheet
                var worksheet = workbook.Worksheets.Add(string.IsNullOrEmpty(sheetName) ? SheetName : sheetName);

                var c = 1;

                // Column names
                foreach (var property in properties)
                {
                    worksheet.Cell(3, c++).Value = property.Item1;
                }

                // Set Data 
                for (var i = 0; i < data.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < data.Columns.Count; j++)
                    {
                        worksheet.Cell(i + 4, j + 1).Value = data.Rows[i][j];
                    }
                }

                // Get styles
                var titleStyle = GetTitleStyle(workbook, backgroundHtmlColor, fontHtmlColor);

                // Ranges
                worksheet.Range(1, 1, 1, c - 1).Merge().Value = string.IsNullOrEmpty(reportTitle) ? Global.ViewExportToXlsxReportTitle : reportTitle;
                worksheet.Range(1, 1, 1, c - 1).AddToNamed(Title);

                worksheet.Range(2, 1, 2, c - 1).AddToNamed(ColumnTitle);
                worksheet.Range(3, 1, 3, c - 1).AddToNamed(ColumnTitle);

                // Format
                workbook.NamedRanges.NamedRange(ColumnTitle).Ranges.Style = titleStyle;
                workbook.NamedRanges.NamedRange(Title).Ranges.Style = titleStyle;
                workbook.NamedRanges.NamedRange(Title).Ranges.Style.Font.FontSize = 16;
                worksheet.Columns().AdjustToContents();

                // Get stream
                using (var memoryStream = new MemoryStream())
                {
                    // Save to memory stream
                    workbook.SaveAs(memoryStream);

                    return memoryStream.ToArray();
                }
            }
        }

        /// <summary>
        /// The get properties.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public Tuple<string, PropertyInfo>[] GetProperties<T>()
        {
            // Get type
            var type = typeof(T);
            var properties = type.GetProperties();
            return properties.Select(
                property =>
                {
                    var xmlattr = Attribute.GetCustomAttribute(property, typeof(XlsxElementAttribute)) as XlsxElementAttribute;
                    if (xmlattr == null)
                    {
                        return new Tuple<string, PropertyInfo>(property.Name, property);
                    }

                    return xmlattr.Ignore ? null : new Tuple<string, PropertyInfo>(xmlattr.ElementName, property);
                }).Where(x => x != null).ToArray();
        }

        /// <summary>
        /// The get data.
        /// </summary>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public DataTable GetDataTable<T>(IEnumerable<T> enumerable, Tuple<string, PropertyInfo>[] properties)
        {
            var t = new DataTable("Exported");
            foreach (var prop in properties)
            {
                t.Columns.Add(new DataColumn(prop.Item1, Nullable.GetUnderlyingType(prop.Item2.PropertyType) ?? prop.Item2.PropertyType));
            }

            enumerable.ForEach(
                x =>
                {
                    var data = t.NewRow();
                    properties.ForEach(
                        p =>
                        {
                            var value = p.Item2.GetValue(x);
                            if (value != null)
                            {
                                data[p.Item1] = value;
                            }
                        });
                    t.Rows.Add(data);
                });

            return t;
        }

        /// <summary>
        /// The create xlsx to file.
        /// </summary>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <param name="fileFullPath">
        /// The file full path.
        /// </param>
        /// <param name="sheetName">
        /// The sheet name.
        /// </param>
        /// <param name="reportTitle">
        /// The report title.
        /// </param>
        /// <param name="backgroundHtmlColor">
        /// The background html color.
        /// </param>
        /// <param name="fontHtmlColor">
        /// The font html color.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateXlsxToFile<T>(
            IEnumerable<T> enumerable,
            string fileFullPath,
            string sheetName = null,
            string reportTitle = null,
            string backgroundHtmlColor = null,
            string fontHtmlColor = null)
        {
            // Get bytes
            var bytes = this.CreateXlsxToBytes(
                enumerable,
                sheetName,
                reportTitle,
                backgroundHtmlColor,
                fontHtmlColor);

            using (var memoryStream = new MemoryStream(bytes))
            {
                // Get path and file name to validate extension
                var path = Path.GetDirectoryName(fileFullPath);
                var fileName = ValidateXlsxFileExtension(Path.GetFileName(fileFullPath));

                if (path != null)
                {
                    fileFullPath = Path.Combine(path, fileName);
                }

                // Write file
                using (var file = new FileStream(fileFullPath, FileMode.Create, FileAccess.Write))
                {
                    memoryStream.WriteTo(file);
                    file.Close();

                    return fileFullPath;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get title style.
        /// </summary>
        /// <param name="workbook">
        /// The workbook.
        /// </param>
        /// <param name="backgroundHtmlColor">
        /// The background html color.
        /// </param>
        /// <param name="fontHtmlColor">
        /// The font html color.
        /// </param>
        /// <returns>
        /// The <see cref="IXLStyle"/>.
        /// </returns>
        private static IXLStyle GetTitleStyle(XLWorkbook workbook, string backgroundHtmlColor, string fontHtmlColor)
        {
            var titleStyle = workbook.Style;
            titleStyle.Font.Bold = true;
            titleStyle.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            try
            {
                titleStyle.Fill.BackgroundColor = string.IsNullOrEmpty(backgroundHtmlColor)
                                                      ? XLColor.Black
                                                      : XLColor.FromHtml(backgroundHtmlColor);
            }
            catch
            {
                titleStyle.Fill.BackgroundColor = XLColor.Black;
            }

            try
            {
                titleStyle.Font.FontColor = string.IsNullOrEmpty(fontHtmlColor)
                                                ? XLColor.White
                                                : XLColor.FromHtml(fontHtmlColor);
            }
            catch
            {
                titleStyle.Font.FontColor = XLColor.White;
            }

            return titleStyle;
        }

        /// <summary>
        /// The validate xlsx file extension.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string ValidateXlsxFileExtension(string fileName)
        {
            if (fileName.Contains(XlsxFileExtension))
            {
                return fileName;
            }

            return fileName + XlsxFileExtension;
        }

        /// <summary>
        /// The export to xlsx.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ExportToXlsx<T>(IEnumerable<T> list)
        {
            // Get data
            var data = this.mapper.Map<T[]>(list);

            // Create xlsx
            var bytes = this.CreateXlsxToBytes(data);
            return this.WriteFile(bytes, ExtXlsx);
        }

        /// <summary>
        /// The write file method
        /// </summary>
        /// <param name="file"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public string WriteFile(byte[] file, string extension)
        {
            // Create the file
            var newName = Guid.NewGuid();
            var filename = $"{newName}.{extension}";
            var path = this.GetRealTempPath(filename);
            File.WriteAllBytes(path, file);
            return filename;
        }

        /// <summary>
        /// The get real temp path method
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// </returns>
        public string GetRealTempPath(string filename)
        {
            var realpath = $@"{this.appConfiguration.AppEnvironment.ApplicationBasePath}\{this.appConfiguration.AppEnvironment.TemporaryFolderPath}";

            // ReSharper disable once AssignNullToNotNullAttribute
            return Path.Combine(realpath, filename);
        }

        #endregion
    }

    /// <summary>
    /// The xlsx element attribute.
    /// </summary>
    public class XlsxElementAttribute : Attribute
    {
        /// <summary>
        /// The resource name.
        /// </summary>
        private readonly string resourceName;

        /// <summary>
        /// Initializes a new instance of the <see cref="XlsxElementAttribute"/> class.
        /// </summary>
        /// <param name="resourceName">
        /// The resource name.
        /// </param>
        public XlsxElementAttribute(string resourceName)
        {
            this.resourceName = resourceName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XlsxElementAttribute"/> class.
        /// </summary>
        /// <param name="ignore">
        /// The ignore.
        /// </param>
        public XlsxElementAttribute(bool ignore)
        {
            this.Ignore = ignore;
        }

        /// <summary>
        /// Gets or sets the element name.
        /// </summary>
        public string ElementName => Global.ResourceManager.GetString(this.resourceName);

        /// <summary>
        /// Gets or sets a value indicating whether ignore.
        /// </summary>
        public bool Ignore { get; set; }
    }
}