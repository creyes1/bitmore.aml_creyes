// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumnCollection.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The column collection.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    #region

    using System.Configuration;

    #endregion

    /// <summary>
    /// The column collection.
    /// </summary>
    [ConfigurationCollection(typeof(ColumnElement), AddItemName = "Column", 
        CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class ColumnCollection : ConfigurationElementCollection
    {
        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="nameOrFileColumnIndex">
        /// The name or file column index.
        /// </param>
        /// <returns>
        /// The <see cref="ColumnElement"/>.
        /// </returns>
        public new ColumnElement this[string nameOrFileColumnIndex]
        {
            get
            {
                return (ColumnElement)BaseGet(nameOrFileColumnIndex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create new element.
        /// </summary>
        /// <returns>
        /// The <see cref="ConfigurationElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ColumnElement();
        }

        /// <summary>
        /// The get element key.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            var column = (ColumnElement)element;
            return column.GetColumnKey();
        }

        #endregion
    }
}