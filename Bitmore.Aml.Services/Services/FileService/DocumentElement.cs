// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentElement.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The document element.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    #region

    using System.Configuration;

    #endregion

    /// <summary>
    /// The document element.
    /// </summary>
    public class DocumentElement : ConfigurationElement
    {
        #region Constants

        /// <summary>
        /// The column header index string.
        /// </summary>
        private const string ColumnHeaderIndexString = "ColumnHeaderIndex";

        /// <summary>
        /// The columns string.
        /// </summary>
        private const string ColumnsString = "Columns";

        /// <summary>
        /// The key string.
        /// </summary>
        private const string KeyString = "Key";

        // private const string ExcludeEmptyRowsString = "ExcludeEmptyRows";

        /// <summary>
        /// The min tolerance column empty string.
        /// </summary>
        private const string MinToleranceColumnEmptyString = "MinToleranceColumnEmpty";

        /// <summary>
        /// The path string.
        /// </summary>
        private const string PathString = "Path";

        /// <summary>
        /// The sheet name string.
        /// </summary>
        private const string SheetNameString = "SheetName";

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the column header index.
        /// </summary>
        [ConfigurationProperty(ColumnHeaderIndexString, DefaultValue = 1, IsRequired = false)]
        public int ColumnHeaderIndex
        {
            get
            {
                return (int)this[ColumnHeaderIndexString];
            }

            set
            {
                this[ColumnHeaderIndexString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the columns.
        /// </summary>
        [ConfigurationProperty(ColumnsString, IsRequired = false)]
        public ColumnCollection Columns
        {
            get
            {
                return (ColumnCollection)this[ColumnsString];
            }

            set
            {
                this[ColumnsString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [ConfigurationProperty(KeyString, IsRequired = true)]
        public string Key
        {
            get
            {
                return (string)this[KeyString];
            }

            set
            {
                this[KeyString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the min tolerance column empty. Example, if we have 2 (MinToleranceColumnEmpty="2") or more empty columns, the entire row will be empty 
        /// </summary>
        [ConfigurationProperty(MinToleranceColumnEmptyString, DefaultValue = null, IsRequired = false)]
        public byte? MinToleranceColumnEmpty
        {
            get
            {
                return (byte?)this[MinToleranceColumnEmptyString];
            }

            set
            {
                this[MinToleranceColumnEmptyString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        [ConfigurationProperty(PathString, IsRequired = false)]
        public string Path
        {
            get
            {
                return (string)this[PathString];
            }

            set
            {
                this[PathString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the sheet name.
        /// </summary>
        [ConfigurationProperty(SheetNameString, IsRequired = false)]
        public string SheetName
        {
            get
            {
                return (string)this[SheetNameString];
            }

            set
            {
                this[SheetNameString] = value;
            }
        }

        #endregion
    }
}