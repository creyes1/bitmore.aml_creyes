// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumnElement.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The column element.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    #region

    using System.Configuration;

    #endregion

    /// <summary>
    /// The column element.
    /// </summary>
    public class ColumnElement : ConfigurationElement
    {
        #region Constants

        /// <summary>
        /// The allow nulls string.
        /// </summary>
        private const string AllowNullsString = "AllowNulls";

        /// <summary>
        /// The column index name format.
        /// </summary>
        private const string ColumnIndexNameFormat = "Column{0}";

        /// <summary>
        /// The file column index string.
        /// </summary>
        private const string FileColumnIndexString = "FileColumnIndex";

        /// <summary>
        /// The max length string.
        /// </summary>
        private const string MaxLengthString = "MaxLength";

        /// <summary>
        /// The name string.
        /// </summary>
        private const string NameString = "Name";

        /// <summary>
        /// The net type string.
        /// </summary>
        private const string NetTypeString = "NetType";

        /// <summary>
        /// The regex to validate string.
        /// </summary>
        private const string RegexToValidateString = "RegexToValidate";

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether allow nulls.
        /// </summary>
        [ConfigurationProperty(AllowNullsString, DefaultValue = false, IsRequired = false)]
        public bool AllowNulls
        {
            get
            {
                return (bool)this[AllowNullsString];
            }

            set
            {
                this[AllowNullsString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the file column index.
        /// </summary>
        [ConfigurationProperty(FileColumnIndexString, DefaultValue = null, IsRequired = false)]
        public string FileColumnIndex
        {
            get
            {
                return (string)this[FileColumnIndexString];
            }

            set
            {
                this[FileColumnIndexString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the max length.
        /// </summary>
        [ConfigurationProperty(MaxLengthString, DefaultValue = null, IsRequired = false)]
        public int? MaxLength
        {
            get
            {
                return (int?)this[MaxLengthString];
            }

            set
            {
                this[MaxLengthString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [ConfigurationProperty(NameString, DefaultValue = null, IsRequired = false)]
        public string Name
        {
            get
            {
                return (string)this[NameString];
            }

            set
            {
                this[NameString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the net type.
        /// </summary>
        [ConfigurationProperty(NetTypeString, IsRequired = true)]
        public string NetType
        {
            get
            {
                return (string)this[NetTypeString];
            }

            set
            {
                this[NetTypeString] = value;
            }
        }

        /// <summary>
        /// Gets or sets the regex to validate.
        /// </summary>
        [ConfigurationProperty(RegexToValidateString, DefaultValue = null, IsRequired = false)]
        public string RegexToValidate
        {
            get
            {
                return (string)this[RegexToValidateString];
            }

            set
            {
                this[RegexToValidateString] = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get column key.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetColumnKey()
        {
            return string.IsNullOrEmpty(this.Name)
                       ? string.Format(ColumnIndexNameFormat, this.FileColumnIndex)
                       : this.Name;
        }

        #endregion
    }
}