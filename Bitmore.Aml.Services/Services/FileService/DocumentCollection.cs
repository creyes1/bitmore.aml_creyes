// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentCollection.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The document collection.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    #region

    using System.Configuration;

    #endregion

    /// <summary>
    /// The document collection.
    /// </summary>
    [ConfigurationCollection(typeof(DocumentElement), AddItemName = "Document", 
        CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class DocumentCollection : ConfigurationElementCollection
    {
        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="DocumentElement"/>.
        /// </returns>
        public new DocumentElement this[string name]
        {
            get
            {
                return (DocumentElement)BaseGet(name);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create new element.
        /// </summary>
        /// <returns>
        /// The <see cref="ConfigurationElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new DocumentElement();
        }

        /// <summary>
        /// The get element key.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DocumentElement)element).Key;
        }

        #endregion
    }
}