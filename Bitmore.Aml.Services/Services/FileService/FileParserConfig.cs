// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileParserConfig.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The file parser config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    #region

    using System.Configuration;

    #endregion

    /// <summary>
    /// The file parser config.
    /// </summary>
    public class FileParserConfig : ConfigurationSection
    {
        #region Constants

        /// <summary>
        /// The document element string.
        /// </summary>
        private const string DocumentElementString = "Documents";

        /// <summary>
        /// The section name.
        /// </summary>
        private const string SectionName = "FileParserConfig";

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the documents.
        /// </summary>
        [ConfigurationProperty(DocumentElementString, IsRequired = true)]
        public DocumentCollection Documents
        {
            get
            {
                return (DocumentCollection)this[DocumentElementString];
            }

            set
            {
                this[DocumentElementString] = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get section.
        /// </summary>
        /// <returns>
        /// The <see cref="FileParserConfig"/>.
        /// </returns>
        public static FileParserConfig GetSection()
        {
            return (FileParserConfig)ConfigurationManager.GetSection(SectionName);
        }

        #endregion
    }
}