// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CellErrorInfo.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The cell error info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    /// <summary>
    /// The cell error info.
    /// </summary>
    internal class CellErrorInfo
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the column key.
        /// </summary>
        public string ColumnKey { get; set; }

        /// <summary>
        /// Gets or sets the error type.
        /// </summary>
        public CellErrorType ErrorType { get; set; }

        /// <summary>
        /// Gets or sets the row index.
        /// </summary>
        public uint RowIndex { get; set; }

        #endregion
    }

    /// <summary>
    /// The cell error type.
    /// </summary>
    internal enum CellErrorType
    {
        /// <summary>
        /// The none.
        /// </summary>
        None, 

        /// <summary>
        /// The error convertion type.
        /// </summary>
        ErrorConvertionType, 

        /// <summary>
        /// The error allow nulls.
        /// </summary>
        ErrorAllowNulls, 

        /// <summary>
        /// The error regular expression.
        /// </summary>
        ErrorRegularExpression, 

        /// <summary>
        /// The error max length.
        /// </summary>
        ErrorMaxLength
    }
}