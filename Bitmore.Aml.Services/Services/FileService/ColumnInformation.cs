// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumnInformation.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The column information.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.FileService
{
    /// <summary>
    /// The column information.
    /// </summary>
    public class ColumnInformation
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the column element.
        /// </summary>
        public ColumnElement ColumnElement { get; set; }

        /// <summary>
        /// Gets or sets the column position index.
        /// </summary>
        public byte ColumnPositionIndex { get; set; }

        #endregion
    }
}