// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The AppModuleService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.DataObject;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Domain.Utils;

    using CacheManager.Core;

    #endregion

    /// <summary>
    ///     The AppModuleService interface.
    /// </summary>
    public interface IAppModuleService : IServiceBase<AppModule>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get root modules.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        IEnumerable<AppModule> GetRootModules();

        /// <summary>
        /// The get web modules by user id.
        /// </summary>
        /// <param name="currentRoleId">
        /// The current Role Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<AppModuleMenu> GetWebModulesByRoleId(Guid currentRoleId);

        /// <summary>
        ///     The reload modules by role.
        /// </summary>
        void ClearModulesByRole();

        /// <summary>
        /// The get all web modules.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<AppModule> GetAllWebModules();

        /// <summary>
        /// The get actions names by module type or url.
        /// </summary>
        /// <param name="typeOrUrl">
        /// The type or url.
        /// </param>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<string> GetActionsNamesByModuleTypeOrUrl(string typeOrUrl, Guid roleId);

        /// <summary>
        /// The has module action.
        /// </summary>
        /// <param name="moduleAction">
        /// The module action.
        /// </param>
        /// <param name="typeOrUrl">
        /// The type or url.
        /// </param>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool HasModuleAction(string moduleAction, string typeOrUrl, Guid roleId);

        #endregion
    }

    /// <summary>
    ///     The app module service.
    /// </summary>
    public class AppModuleService : ServiceBase<AppModule>, IAppModuleService
    {
        /// <summary>
        /// The region cache modules.
        /// </summary>
        const string RegionCacheModules = "ModulesRoleId";

        #region Fields

        /// <summary>
        ///     The app module action repository.
        /// </summary>
        private readonly IAppModuleActionRepository appModuleActionRepository;

        /// <summary>
        ///     The app moduler repository.
        /// </summary>
        private readonly IAppModuleRepository appModulerRepository;
        
        /// <summary>
        /// The app module type service
        /// </summary>
        private readonly IAppModuleTypeService appModuleTypeService;

        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The cache modules.
        /// </summary>
        private readonly ICacheManager<IEnumerable<AppModuleMenu>> cacheModules;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleService"/> class.
        /// </summary>
        /// <param name="appModulerRepository">
        /// The app moduler repository.
        /// </param>
        /// <param name="appModuleActionRepository">
        /// The app module action repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="appModuleTypeService"></param>
        /// <param name="mapper"></param>
        /// <param name="cacheModules"></param>
        public AppModuleService(
            IAppModuleRepository appModulerRepository,
            IAppModuleActionRepository appModuleActionRepository,
            IUnitOfWork unitOfWork,
            IAppModuleTypeService appModuleTypeService,
            IMapper mapper,
            ICacheManager<IEnumerable<AppModuleMenu>> cacheModules)
            : base(appModulerRepository, unitOfWork, mapper)
        {
            this.appModulerRepository = appModulerRepository;
            this.appModuleActionRepository = appModuleActionRepository;
            this.appModuleTypeService = appModuleTypeService;
            this.mapper = mapper;
            this.cacheModules = cacheModules;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get root modules.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public IEnumerable<AppModule> GetRootModules()
        {
            return
                this.GetQuery()
                    .IncludeMultiple(ap => ap.AppModuleType, ap => ap.ChildrenAppModules)
                    .Where(ap => ap.ParentAppModuleId == null)
                    .ToList();
        }

        /// <summary>
        /// The get web modules by user id.
        /// </summary>
        /// <param name="currentRoleId"></param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<AppModuleMenu> GetWebModulesByRoleId(Guid currentRoleId)
        {
            Func<string, string, IEnumerable<AppModuleMenu>> getData = (k, r) =>
            {
                // Get the module's ids
                var ids = (from action in this.appModuleActionRepository.GetQuery()
                           from role in action.Roles
                           where role.RoleId == currentRoleId
                           select action.AppModule.AppModuleId).ToList();

                // We use "GetAllWebModules" because we want to reduce the calls to the data base, because
                // this method is one of the most recurrent.
                var allModules = this.GetAllWebModules();
                var appModules = allModules as AppModule[] ?? allModules.ToArray();
                var modulesIds = appModules
                                        .Where(x => ids.Contains(x.AppModuleId))
                                        .Select(x => new { x.ParentAppModuleId, x.AppModuleId })
                                        .ToList();

                // Get the ids that are roots
                var rootIds = modulesIds.Select(x => x.ParentAppModuleId ?? x.AppModuleId).Distinct();
                var list = new List<AppModule>();

                // Get the properties only one time. We use these properties to clone the objects
                var properties = typeof(AppModule).GetNotVirtualProperties();
                appModules.Where(x => rootIds.Contains(x.AppModuleId)).ToList().ForEach(
                    ap =>
                    {
                        var module = ap.Clone(properties);
                        var children = appModules.Where(x => x.ParentAppModuleId == ap.AppModuleId && ids.Contains(x.AppModuleId))
                                                 .Select(x => x.Clone(properties));
                        module.ChildrenAppModules = new List<AppModule>(children);
                        list.Add(module);
                    });

                // The final list is a clone of AppModule
                return this.mapper.Map<IEnumerable<AppModuleMenu>>(list);
            };

            return this.cacheModules.GetOrAdd($"ModulesRoleId{ currentRoleId.ToString().Replace("-", string.Empty) }", RegionCacheModules, getData);
        }

        /// <summary>
        ///     The reload modules by role.
        /// </summary>
        public void ClearModulesByRole()
        {
            this.cacheModules.ClearRegion(RegionCacheModules);
        }

        /// <summary>
        /// The get all web modules.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<AppModule> GetAllWebModules()
        {
            var webId = this.appModuleTypeService.GetWebId();
            return this.GetAllFromCache(x => x.AppModuleTypeId == webId);
        }

        /// <summary>
        /// The get actions names by module type or url.
        /// </summary>
        /// <param name="typeOrUrl">
        /// The type or url.
        /// </param>
        /// <param name="roleId">
        /// The role Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<string> GetActionsNamesByModuleTypeOrUrl(string typeOrUrl, Guid roleId)
        {
            var query = from m in this.GetQuery()
                        from a in m.AppModuleActions
                        from r in a.Roles
                        where m.TypeOrUrl == typeOrUrl && r.RoleId == roleId
                        select a.Name;
            return this.Repository.GetAllFromCache(query);
        }

        /// <summary>
        /// The has module action.
        /// </summary>
        /// <param name="moduleAction">
        /// The module action.
        /// </param>
        /// <param name="typeOrUrl">
        /// The type Or Url.
        /// </param>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasModuleAction(string moduleAction, string typeOrUrl, Guid roleId)
        {
            var result = this.GetActionsNamesByModuleTypeOrUrl(typeOrUrl, roleId);
            return result.Contains(moduleAction);
        }

        #endregion
    }
}