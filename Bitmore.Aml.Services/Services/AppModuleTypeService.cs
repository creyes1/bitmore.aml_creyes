// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleTypeService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The AppModuleTypeService interface.
    /// </summary>
    public interface IAppModuleTypeService : IServiceBase<AppModuleType>
    {
        /// <summary>
        /// The get web id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Guid GetWebId();
    }

    /// <summary>
    /// The app module type service.
    /// </summary>
    public class AppModuleTypeService : ServiceBase<AppModuleType>, IAppModuleTypeService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleTypeService"/> class.
        /// </summary>
        /// <param name="appModuleTypeRepository">
        /// The app module type repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AppModuleTypeService(IAppModuleTypeRepository appModuleTypeRepository, IUnitOfWork unitOfWork, IMapper mapper)
            : base(appModuleTypeRepository, unitOfWork, mapper)
        {
        }

        /// <summary>
        /// The get web id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        public Guid GetWebId()
        {
            const string WebType = "Web";
            return this.GetFromCache(x => x.Name == WebType).AppModuleTypeId;
        }
    }
}