// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguageService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The LanguageService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Resources.Language;

    using CacheManager.Core;

    #endregion

    /// <summary>
    /// The LanguageService interface.
    /// </summary>
    public interface ILanguageService
    {
        /// <summary>
        /// The get cultures.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<CultureInfo> GetCultures();

        /// <summary>
        /// The get default culture.
        /// </summary>
        /// <param name="langueges">
        /// The langueges.
        /// </param>
        /// <returns>
        /// The <see cref="CultureInfo"/>.
        /// </returns>
        CultureInfo GetDefaultCulture(string[] langueges = null);
    }

    /// <summary>
    /// The language service.
    /// </summary>
    public class LanguageService : ILanguageService
    {
        /// <summary>
        /// The default cultrue.
        /// </summary>
        private const string DefaultCultrue = "es-MX";

        /// <summary>
        /// The cache culture info.
        /// </summary>
        private readonly ICacheManager<IEnumerable<CultureInfo>> cacheCultureInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageService"/> class.
        /// </summary>
        /// <param name="cacheCultureInfo">
        /// The cache Culture Info.
        /// </param>
        public LanguageService(ICacheManager<IEnumerable<CultureInfo>> cacheCultureInfo)
        {
            this.cacheCultureInfo = cacheCultureInfo;
        }

        /// <summary>
        /// The get cultures.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<CultureInfo> GetCultures()
        {
            const string ConcurrentCultures = "ConcurrentCultures";
            Func<string, IEnumerable<CultureInfo>> getData = (k) =>
                {
                    var cultures = new ConcurrentBag<CultureInfo> { new CultureInfo(DefaultCultrue) };
                    foreach (var cui in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
                    {
                        try
                        {
                            var resource = Global.ResourceManager.GetResourceSet(cui, true, false);
                            if (resource == null || string.IsNullOrEmpty(cui.Name)
                                || cultures.Any(c => c.Name == cui.Name))
                            {
                                continue;
                            }

                            if (cultures.All(x => x.Name != cui.Name))
                            {
                                cultures.Add(cui);
                            }
                        }
                        catch (FileNotFoundException)
                        {
                        }
                    }

                    return cultures;
                };

            return this.cacheCultureInfo.GetOrAdd(ConcurrentCultures, getData);
        }

        /// <summary>
        /// The get default culture.
        /// </summary>
        /// <param name="langueges">
        /// The langueges.
        /// </param>
        /// <returns>
        /// The <see cref="CultureInfo"/>.
        /// </returns>
        public CultureInfo GetDefaultCulture(string[] langueges = null)
        {
            var cultures = this.GetCultures();
            if (langueges == null || !langueges.Any())
            {
                return cultures.FirstOrDefault();
            }

            var enumerationCulture = cultures as CultureInfo[] ?? cultures.ToArray();
            var specificCulture = enumerationCulture.FirstOrDefault(c => langueges.Contains(c.Name));

            if (specificCulture != null)
            {
                return specificCulture;
            }

            return langueges.ToList().Any(
                l =>
                    {
                        var tmp = l.Substring(0, 2);
                        specificCulture = enumerationCulture.FirstOrDefault(c => c.Name.StartsWith(tmp));
                        return specificCulture != null;
                    })
                       ? specificCulture
                       : enumerationCulture.FirstOrDefault();
        }
    }
}