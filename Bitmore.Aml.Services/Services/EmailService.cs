// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The EmailService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Threading.Tasks;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Services.Services.Email;

    using SmartFormat;

    #endregion

    /// <summary>
    /// The EmailService interface.
    /// </summary>
    public interface IEmailService
    {
        #region Public Methods and Operators

        /// <summary>
        /// The send a email.
        /// </summary>
        /// <param name="parameters"></param>
        void SendEMail(EmailBodyParameters parameters);

        #endregion
    }

    /// <summary>
    /// The email service.
    /// </summary>
    public class EmailService : IEmailService
    {
        #region Constants

        /// <summary>
        /// The email config.
        /// </summary>
        private const string EmailConfig = "EmailConfig";

        /// <summary>
        /// The email template.
        /// </summary>
        private const string EmailTemplate = "Template.html";

        /// <summary>
        /// The email css file.
        /// </summary>
        private const string EmailCss = "ink.css";

        /// <summary>
        /// The email logo.
        /// </summary>
        private const string EmailLogo = "logo-email.png";

        #endregion

        #region Fields

        /// <summary>
        /// The global attribute service.
        /// </summary>
        private readonly IGlobalAttributeService globalAttributeService;

        /// <summary>
        /// The Log engine
        /// </summary>
        private readonly ILogEngine logEngine;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        public EmailService(
            IGlobalAttributeService globalAttributeService,
            IAppConfiguration appConfiguration,
            ILogEngine logEngine)
        {
            this.globalAttributeService = globalAttributeService;
            this.logEngine = logEngine;
            this.EmailFolderPath = Path.Combine(appConfiguration.AppEnvironment.ApplicationBasePath, appConfiguration.AppEnvironment.EmailFolderPath);
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="parameters"></param>
        public void SendEMail(EmailBodyParameters parameters)
        {
            var keyEmail = this.globalAttributeService.Get(att => att.Name == EmailConfig);
            var email = keyEmail.Value.DeserializeFromXml<Mail>();
            parameters.InkCss = File.ReadAllText(Path.Combine(this.EmailFolderPath, EmailCss));
            var emailBody = File.ReadAllText(Path.Combine(this.EmailFolderPath, EmailTemplate));
            email.Body = Smart.Format(emailBody, parameters);
            email.To = parameters.Addresses;
            email.Subject = parameters.Subject;
            this.SendMail(email, parameters.AttachmentFiles);
        }

        /// <summary>
        /// Gets or sets the logo path.
        /// </summary>
        public string EmailFolderPath { get; set; }

        /// <summary>
        /// The send mail.
        /// </summary>
        /// <param name="mail">
        /// The p mail.
        /// </param>
        /// <param name="attachmentFiles">
        /// The attachment files.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        public void SendMail(Mail mail, IEnumerable<AttachmentFile> attachmentFiles = null)
        {
            var mailMessage = new MailMessage { From = new MailAddress(mail.From) };
            if (mail.To != null && mail.To.Any())
            {
                mail.To.ToList().ForEach(mailMessage.To.Add);
            }

            if (mail.Cc != null && mail.Cc.Any())
            {
                mail.Cc.ToList().ForEach(mailMessage.CC.Add);
            }

            if (mail.Bcc != null && mail.Bcc.Any())
            {
                mail.Bcc.ToList().ForEach(mailMessage.Bcc.Add);
            }

            mailMessage.Subject = mail.Subject;
            mailMessage.Body = mail.Body;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;

            attachmentFiles?.ToList().ForEach(
                at =>
                    {
                        var attach = new Attachment(at.Stream, at.ContentTypes) { Name = at.FileName };
                        mailMessage.Attachments.Add(attach);
                    });

            this.AddLogo(mailMessage, mailMessage.Body);
            var smtp = new SmtpClient
                           {
                               EnableSsl = mail.EnableSsl,
                               Host = mail.Host,
                               Port = mail.Port,
                               Credentials = new NetworkCredential(mail.UserName, mail.Password)
                           };

            Task.Factory.StartNew(
                () =>
                    {
                        // Create new stopwatch.
                        var stopwatch = new Stopwatch();
                        try
                        {
                            stopwatch.Start();

                            // Sen async email
                            smtp.Send(mailMessage);

                            stopwatch.Stop();
                        }
                        finally
                        {
                            this.logEngine.WriteInfo("Smtp process finished", string.Concat("Total Elapsed Time : ", stopwatch.Elapsed));

                            // Code to close all files attached in open state
                            if (mailMessage.Attachments.Any())
                            {
                                foreach (var attachment in mailMessage.Attachments)
                                {
                                    attachment.ContentStream.Dispose();
                                }
                            }
                        }
                    });
        }

        /// <summary>
        /// The add logo.
        /// </summary>
        /// <param name="mail">
        /// The mail.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        public void AddLogo(MailMessage mail, string body)
        {
            var alternateview = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            var logoPath = Path.Combine(this.EmailFolderPath, EmailLogo);
            var inline = new LinkedResource(logoPath)
                             {
                                 ContentId = "logo",
                                 ContentType = new ContentType("image/png")
                             };
            alternateview.LinkedResources.Add(inline);
            mail.AlternateViews.Add(alternateview);
        }
    }

}