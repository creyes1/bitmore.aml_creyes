// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLogService.cs" company="Bitmore Technologies">
//   Copyright">Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The AppLogService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The AppLogService interface.
    /// </summary>
    public interface IAppLogService : IServiceBase<AppLog>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The clear app log errors.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult ClearAppLogErrors();

        #endregion
    }

    /// <summary>
    /// The app log service.
    /// </summary>
    public class AppLogService : ServiceBase<AppLog>, IAppLogService
    {
        #region Fields

        /// <summary>
        /// The app log repository.
        /// </summary>
        private readonly IAppLogRepository appLogRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogService"/> class.
        /// </summary>
        /// <param name="appLogRepository">
        /// The app log repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AppLogService(IAppLogRepository appLogRepository, IUnitOfWork unitOfWork, IMapper mapper)
            : base(appLogRepository, unitOfWork, mapper)
        {
            this.appLogRepository = appLogRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The clear app log errors.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult ClearAppLogErrors()
        {
            this.appLogRepository.ClearAppLogErrors();
            return new ServiceResult { Success = true, Message = Global.ViewAccountUsersMsgErrorMessagesDeleted };
        }

        #endregion
    }
}