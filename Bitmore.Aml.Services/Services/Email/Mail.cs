// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mail.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   Class used to fill email information
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.Email
{
    /// <summary>
    ///     Class used to fill email information
    /// </summary>
    public class Mail
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the bcc.
        /// </summary>
        public string[] Bcc { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the cc.
        /// </summary>
        public string[] Cc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether enable ssl.
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether enable tls.
        /// </summary>
        public bool EnableTls { get; set; }

        /// <summary>
        /// Gets or sets the from.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the to.
        /// </summary>
        public string[] To { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        #endregion
    }
}