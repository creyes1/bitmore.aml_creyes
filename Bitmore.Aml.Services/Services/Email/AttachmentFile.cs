// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttachmentFile.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The attachment file.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.Email
{
    #region

    using System.IO;
    using System.Net.Mime;

    #endregion

    /// <summary>
    /// The attachment file.
    /// </summary>
    public class AttachmentFile
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the content types.
        /// </summary>
        public ContentType ContentTypes { get; set; }

        /// <summary>
        /// Gets or sets the file names.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file names.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the stream.
        /// </summary>
        public Stream Stream => new FileStream(this.FilePath, FileMode.Open);

        #endregion
    }
}