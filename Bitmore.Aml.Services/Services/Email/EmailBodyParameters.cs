// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailBodyParameters.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   Defines the EmailBodyParameters type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services.Email
{
    using System.Collections.Generic;

    /// <summary>
    /// The email body parameters.
    /// </summary>
    public class EmailBodyParameters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailBodyParameters"/> class.
        /// </summary>
        public EmailBodyParameters()
        {
            this.TitleBand = "SeedBit";
        }

        /// <summary>
        /// Gets or sets the addresses.
        /// </summary>
        public string[] Addresses { get; set; }

        /// <summary>
        /// Gets or sets the content body.
        /// </summary>
        public string ContentBody { get; set; }

        /// <summary>
        /// Gets or sets the ink css.
        /// </summary>
        public string InkCss { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the sub title body.
        /// </summary>
        public string SubTitleBody { get; set; }

        /// <summary>
        /// Gets or sets the text bottom.
        /// </summary>
        public string TextBottom { get; set; }

        /// <summary>
        /// Gets or sets the text shared bottom.
        /// </summary>
        public string TextSharedBottom { get; set; }

        /// <summary>
        /// Gets or sets the title band.
        /// </summary>
        public string TitleBand { get; set; }

        /// <summary>
        /// Gets or sets the title body.
        /// </summary>
        public string TitleBody { get; set; }

        /// <summary>
        /// Gets or sets the attachment files.
        /// </summary>
        public IEnumerable<AttachmentFile> AttachmentFiles { get; set; }
    }
}