// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccessTokenService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Resources.Language;

    using CacheManager.Core;

    #endregion

    /// <summary>
    /// The AccessTokenService interface.
    /// </summary>
    public interface IAccessTokenService : IServiceBase<AccessToken>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The is black list.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsBlackList(Guid id);

        /// <summary>
        /// The reload black list.
        /// </summary>
        void ReloadBlackList();

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="accessToken">
        /// The access token.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult Update(AccessToken accessToken);

        #endregion
    }

    /// <summary>
    /// The access token service.
    /// </summary>
    public class AccessTokenService : ServiceBase<AccessToken>, IAccessTokenService
    {
        #region Fields

        /// <summary>
        /// The access token repository.
        /// </summary>
        private readonly IAccessTokenRepository accessTokenRepository;

        /// <summary>
        /// The cache blacklist.
        /// </summary>
        private readonly ICacheManager<IEnumerable<Guid>> cacheBlackList;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessTokenService"/> class.
        /// </summary>
        /// <param name="accessTokenRepository">
        /// The access token repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="cacheBlackList">
        /// The cache Black List.
        /// </param>
        public AccessTokenService(
            IAccessTokenRepository accessTokenRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ICacheManager<IEnumerable<Guid>> cacheBlackList)
            : base(accessTokenRepository, unitOfWork, mapper)
        {
            this.accessTokenRepository = accessTokenRepository;
            this.cacheBlackList = cacheBlackList;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The is black list.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsBlackList(Guid id)
        {
            const string BlackListKey = "JwtBlackList";
            var idsRevoked = this.cacheBlackList.GetOrAdd(
                BlackListKey,
                s => { return this.GetQuery().Where(a => a.IsRevoked).Select(a => a.AccessTokenId).ToArray(); });
            return idsRevoked.Contains(id);
        }

        /// <summary>
        /// The reload black list.
        /// </summary>
        public void ReloadBlackList()
        {
            this.cacheBlackList.Clear();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="accessToken">
        /// The access token.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult Update(AccessToken accessToken)
        {
            try
            {
                this.accessTokenRepository.Update(accessToken);
                this.UnitOfWork.Commit();
                this.ReloadBlackList();
                return new ServiceResult { Success = true, Message = Global.AccountAccessTokenSaved };
            }
            catch (Exception)
            {
                return new ServiceResult { Success = false, Message = Global.AccountAccessTokenNotSaved };
            }
        }

        #endregion
    }
}