// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The UserService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Dynamic;
    using System.Linq;
    using System.Security.Principal;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services.Email;
    using Bitmore.Aml.Services.Utils;

    using SmartFormat;

    #endregion

    /// <summary>
    /// The UserService interface.
    /// </summary>
    public interface IUserService : IServiceBase<User>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The assign password.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="newPassword">
        /// The new password.
        /// </param>
        /// <param name="repeatNewPassword">
        /// The repeat new password.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult AssignPassword(Guid userId, string newPassword, string repeatNewPassword);

        /// <summary>
        /// The change password.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="currentPassword">
        /// The current password.
        /// </param>
        /// <param name="newPassword">
        /// The new password.
        /// </param>
        /// <param name="repeatNewPassword">
        /// The repeat new password.
        /// </param>
        /// <param name="rowVersion">
        /// The row Version.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult ChangePassword(
            User user, 
            string currentPassword, 
            string newPassword, 
            string repeatNewPassword, 
            byte[] rowVersion);

        /// <summary>
        /// The hex to string and decrypt.
        /// </summary>
        /// <param name="hexEncriptedText">
        /// The hex encripted text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string HexToStringAndDecrypt(string hexEncriptedText);

        /// <summary>
        /// The password assigment.
        /// </summary>
        /// <param name="hexEncryptedUserId">
        /// The hex encrypted user id.
        /// </param>
        /// <param name="hexEncryptedPasswordUpdatedOnDate">
        /// The hex encrypted password updated on date.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        bool PasswordAssigment(string hexEncryptedUserId, string hexEncryptedPasswordUpdatedOnDate);

        /// <summary>
        /// The password expired.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="expirationDays">
        /// The expiration Days.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool PasswordExpired(object userId, int expirationDays);

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult Save(User user);

        /// <summary>
        /// The send email link.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="appPath">
        /// The app path.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult SendEmailLink(Guid userId, string appPath);

        /// <summary>
        /// The send password.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="appPath">
        /// The app path.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult SendPassword(string userName, string appPath);

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="overrideActiveSession">
        /// The override active session.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult Validate(string login, string password, bool overrideActiveSession);

        /// <summary>
        /// The is first log in.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsFirstLogIn(object userId);

        /// <summary>
        /// The get notify before days.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        Tuple<bool, int> GetNotifyBeforeDays(Guid userId);

        /// <summary>
        /// The SignOut has active session.
        /// </summary>
        /// <param name="userName">
        /// The user id.
        /// </param>
        void SignOut(string userName);

        /// <summary>
        /// The get all users id with has active session.
        /// </summary>
        void ClearHasActiveSession();

        /// <summary>
        /// The send email with new token.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="appPath">
        /// The app path.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult SendEmailWithNewToken(string userName, string appPath, string token);

        #endregion
    }

    /// <summary>
    /// The user service.
    /// </summary>
    public class UserService : ServiceBase<User>, IUserService
    {
        #region Constants

        /// <summary>
        /// The format date.
        /// </summary>
        private const string FormatDate = "{0:s}";

        /// <summary>
        /// The null string.
        /// </summary>
        private const string NullString = "null";

        /// <summary>
        /// The disabled string.
        /// </summary>
        private const string DisabledString = "Disabled";

        /// <summary>
        /// The salt length.
        /// </summary>
        private const int SaltLength = 6;

        #endregion

        #region Fields

        /// <summary>
        /// The email service.
        /// </summary>
        private readonly IEmailService emailService;

        /// <summary>
        /// The role repository.
        /// </summary>
        private readonly IRoleRepository roleRepository;

        /// <summary>
        /// The user repository.
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// The user status service.
        /// </summary>
        private readonly IUserStatusService userStatusService;

        /// <summary>
        /// The app configuration.
        /// </summary>
        private readonly IAppConfiguration appConfiguration;
        
        /// <summary>
        /// The Log engine
        /// </summary>
        private readonly ILogEngine logEngine;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="userRepository">
        /// The user repository.
        /// </param>
        /// <param name="roleRepository">
        /// The role repository.
        /// </param>
        /// <param name="emailService">
        /// The email service.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="userStatusService"></param>
        /// <param name="appConfiguration"></param>
        /// <param name="mapper"></param>
        /// <param name="logEngine"></param>
        public UserService(
            IUserRepository userRepository, 
            IRoleRepository roleRepository,
            IEmailService emailService, 
            IUnitOfWork unitOfWork,
            IUserStatusService userStatusService,
            IAppConfiguration appConfiguration,
            IMapper mapper,
            ILogEngine logEngine)
            : base(userRepository, unitOfWork, mapper)
        {
            this.roleRepository = roleRepository;
            this.emailService = emailService;
            this.userStatusService = userStatusService;
            this.appConfiguration = appConfiguration;
            this.logEngine = logEngine;
            this.userRepository = userRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The assign password.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="newPassword">
        /// The new password.
        /// </param>
        /// <param name="repeatNewPassword">
        /// The repeat new password.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult AssignPassword(Guid userId, string newPassword, string repeatNewPassword)
        {
            if (newPassword != repeatNewPassword)
            {
                return new ServiceResult { Success = false, Message = Global.ViewAccountUsersMsgPassDiffRep };
            }

            var user = this.Get(userId);
            user.Password = GetPasswordInHash(newPassword, user);
            user.PasswordUpdatedOnDate = DateTime.Now;
            this.Update(user);
            return new ServiceResult { Success = true, Message = Global.ViewAccountUsersMsgPassUpdated };
        }

        /// <summary>
        /// The build and send email link.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="appPath">
        /// The app path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool BuildAndSendEmailLink(Guid userId, string appPath)
        {
            try
            {
                var user = this.Get(userId);
                if (user == null)
                {
                    return true;
                }

                var hexEncriptedId = Encryptor.EncryptAndConvertToHex(user.UserId.ToString());
                var hexEncriptedUpdateOnDate =
                    Encryptor.EncryptAndConvertToHex(
                        user.PasswordUpdatedOnDate == null
                            ? NullString
                            : string.Format(FormatDate, user.PasswordUpdatedOnDate));
                const string EmailLinkCreatePasswrod = "{0}/Account/PasswordAssigment";
                var link = string.Format(EmailLinkCreatePasswrod, appPath);
                var body = Smart.Format(
                    Global.ViewAccountEmailBodyCreatePassword,
                    new
                    {
                        Link = link,
                        UserID = hexEncriptedId,
                        PasswordUpdatedOnDate = hexEncriptedUpdateOnDate,
                        user.UserName
                    });
                var subject = Smart.Format(Global.ViewAccountEmailSubjectCreatePassword, new { user.DisplayName });
                this.emailService.SendEMail(
                    new EmailBodyParameters()
                    {
                        Addresses = new[] { user.Email },
                        ContentBody = body,
                        TitleBody = Global.ViewAccountEmailCreatePasswordTitleBody,
                        SubTitleBody = user.DisplayName,
                        Subject = subject,
                        TextBottom = string.Empty,
                        TextSharedBottom = Global.ViewAccountEmailSharedTextBottom
                    });

                return true;
            }
            catch (Exception e)
            {
                this.logEngine.WriteError(e);
                return false;
            }
        }

        /// <summary>
        /// The change password.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="currentPassword">
        /// The current password.
        /// </param>
        /// <param name="newPassword">
        /// The new password.
        /// </param>
        /// <param name="repeatNewPassword">
        /// The repeat new password.
        /// </param>
        /// <param name="rowVersion">
        /// The row Version.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult ChangePassword(
            User user, 
            string currentPassword, 
            string newPassword, 
            string repeatNewPassword, 
            byte[] rowVersion)
        {
            if (newPassword != repeatNewPassword)
            {
                return new ServiceResult { Success = false, Message = Global.ViewAccountUsersMsgPassDiffRep };
            }

            var currentEncrypted = GetPasswordInHash(currentPassword, user);
            if (user == null || currentEncrypted != user.Password)
            {
                return new ServiceResult { Success = false, Message = Global.ViewAccountUsersMsgPassDiff };
            }

            user.Password = GetPasswordInHash(newPassword, user);
            user.PasswordUpdatedOnDate = DateTime.Now;
            user.RowVersion = rowVersion;
            return this.Update(user, new ServiceResult { Success = true, Message = Global.ViewAccountUsersMsgPassUpdated });
        }

        /// <summary>
        /// The hex to string and decrypt.
        /// </summary>
        /// <param name="hexEncriptedText">
        /// The hex encripted text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string HexToStringAndDecrypt(string hexEncriptedText)
        {
            return Encryptor.HexToStringAndDecrypt(hexEncriptedText);
        }

        /// <summary>
        /// The password assigment.
        /// </summary>
        /// <param name="hexEncryptedUserId">
        /// The hex encrypted user id.
        /// </param>
        /// <param name="hexEncryptedPasswordUpdatedOnDate">
        /// The hex encrypted password updated on date.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public bool PasswordAssigment(string hexEncryptedUserId, string hexEncryptedPasswordUpdatedOnDate)
        {
            string decryptedUserId;
            string decryptedPasswordUpdatedOnDate;

            try
            {
                decryptedUserId = Encryptor.HexToStringAndDecrypt(hexEncryptedUserId);
                decryptedPasswordUpdatedOnDate = Encryptor.HexToStringAndDecrypt(hexEncryptedPasswordUpdatedOnDate);
            }
            catch
            {
                return false;
            }

            var user = this.Get(Guid.Parse(decryptedUserId));
            if (user != null && user.PasswordUpdatedOnDate == null)
            {
                return true;
            }

            if (user == null || decryptedPasswordUpdatedOnDate == NullString)
            {
                return false;
            }

            return decryptedPasswordUpdatedOnDate == string.Format(FormatDate, user.PasswordUpdatedOnDate);
        }

        /// <summary>
        /// The password expired.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="expirationDays">
        /// The expiration Days.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool PasswordExpired(object userId, int expirationDays)
        {
            var validate = expirationDays > 0;
            if (!validate)
            {
                return false;
            }

            var id = (Guid)userId;
            var passwordUpdatedOnDate =
                this.GetQuery().Where(x => x.UserId == id).Select(x => x.PasswordUpdatedOnDate).FirstOrDefault();
            return passwordUpdatedOnDate == null || (DateTimeOffset.Now - passwordUpdatedOnDate).Value.TotalDays > expirationDays;
        }

        /// <summary>
        /// The is first log in.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsFirstLogIn(object userId)
        {
            var usr = this.Get(userId);
            return usr.LastLogIn == null;
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult Save(User user)
        {
            const string ProfileImage = "Avatars-25.svg";
            var exists = this.Get(u => u.UserName == user.UserName, u => u.Roles);
            if (exists != null && ((exists.UserId != user.UserId) || user.UserId == Guid.Empty))
            {
                return new ServiceResult { Success = false, Message = Global.ViewAccountUsersMsgExistingUser };
            }

            var array = user.Roles.Select(rt => rt.RoleId);
            user.Roles = this.roleRepository.GetMany(r => array.Contains(r.RoleId)).ToList();
            var strResult = string.Empty;
            var result = new ServiceResult { Success = true, Message = Global.ViewAccountUsersMsgSavedUser + strResult };
            if (user.UserId != Guid.Empty)
            {
                var invalidPasswordAttempt = this.appConfiguration.InvalidPasswordAttempt;
                if (!invalidPasswordAttempt.Activated)
                {
                    return this.Update(user, result);
                }

                var currentStatusId =
                    this.GetQuery().Where(u => u.UserId == user.UserId).Select(u => u.UserStatusId).FirstOrDefault();
                if (user.UserStatusId != currentStatusId && user.PasswordAttempt >= invalidPasswordAttempt.Attempt)
                {
                    user.PasswordAttempt = 0;
                }

                return this.Update(user, result);
            }

            var password = GeneratePassword(user);
            user.ProfileImage = ProfileImage;
            this.Add(user);
            this.emailService.SendEMail(
                new EmailBodyParameters()
                    {
                        Addresses = new[] { user.Email },
                        ContentBody = Smart.Format(Global.ViewAccountEmailNewUserContentBody, new { user.UserName, Password = password, Url = string.Empty }),
                        TitleBody = Smart.Format(Global.ViewAccountEmailNewUserTitleBody, new { user.DisplayName }),
                        SubTitleBody = Global.ViewAccountEmailNewUserSubTitleBody,
                        Subject = Global.ViewAccountEmailNewUserSubject,
                        TextBottom = string.Empty,
                        TextSharedBottom = Global.ViewAccountEmailSharedTextBottom
                });

            return result;
        }

        /// <summary>
        /// The send email password.
        /// </summary>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="appPath">
        /// The app path.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult SendEmailLink(Guid userId, string appPath)
        {
            return this.BuildAndSendEmailLink(userId, appPath)
                       ? new ServiceResult { Success = true, Message = Global.ViewAccountSentMailSuccessMsj }
                       : new ServiceResult { Success = false, Message = Global.ViewAccountSentMailErrorMsj };
        }

        /// <summary>
        /// The send password.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="appPath"></param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult SendPassword(string userName, string appPath)
        {
            var userId = this.GetQuery().Where(u => u.UserName == userName).Select(x => x.UserId).FirstOrDefault();
            if (userId == Guid.Empty)
            {
                return new ServiceResult { Success = false, Message = Global.ViewAccountUsersMsgUserNotExist };
            }

            this.BuildAndSendEmailLink(userId, appPath);
            return new ServiceResult { Success = true, Message = Global.ViewAccountUsersMsgSendEmailToUser };
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="overrideActiveSession">
        /// The override active session.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult Validate(string login, string password, bool overrideActiveSession)
        {
            var result = new ServiceResult();

            var user = this.Get(u => u.UserName == login);
            if (user == null)
            {
                result.Success = false;
                result.Message = Global.ViewAccountUsersMsgErrorLoginIncorrectUser;
                return result;
            }

            var invalidPasswordAttempt = this.appConfiguration.InvalidPasswordAttempt;
            if (invalidPasswordAttempt.Activated && (user.PasswordAttempt >= invalidPasswordAttempt.Attempt || user.UserStatus.Name == DisabledString))
            {
                result.Success = false;
                result.Message = Global.ViewAccountUsersMsgErrorLoginBlocked;
                return result;
            }

            var passEncrypted = GetPasswordInHash(password, user);
            if (user.Password != passEncrypted)
            {
                result.Success = false;
                result.Message = Global.ViewAccountUsersMsgErrorLoginIncorrectPassword;
                if (invalidPasswordAttempt.Activated)
                {
                    this.SetPasswordAttempt(user, invalidPasswordAttempt.Attempt);
                }

                return result;
            }

            if (!overrideActiveSession && user.HasActiveSession)
            {
                result.Success = false;
                result.Message = Global.ViewAccountUsersMsgErrorLoginActiveSession;
                dynamic objectResult = new ExpandoObject();
                objectResult.HasActiveSession = true;
                result.Object = objectResult;
                return result;
            }

            user.PasswordAttempt = 0;
            user.SessionToken = Guid.NewGuid();
            user.LastLogIn = user.CurrentLogIn;
            user.CurrentLogIn = DateTime.Now;
            user.HasActiveSession = true;
            result.Success = true;
            result.Object = user;

            // Set the genericIdentity only to save the "UserName" in AppLog Table
            var genericIdentity = new GenericIdentity(login);
            var threadCurrentPrincipal = new GenericPrincipal(genericIdentity, null);
            System.Threading.Thread.CurrentPrincipal = threadCurrentPrincipal;

            this.Update(user);
            return result;
        }

        /// <summary>
        /// The change has active session.
        /// </summary>
        /// <param name="userName">
        /// The user id.
        /// </param>
        public void SignOut(string userName)
        {
            this.userRepository.SignOut(userName);
        }

        /// <summary>
        /// The get all users id with has active session.
        /// </summary>
        public void ClearHasActiveSession()
        {
            this.userRepository.ClearHasActiveSession();
        }

        /// <summary>
        /// The set password attempt.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="maxPasswordAttempt"></param>
        public void SetPasswordAttempt(User user, int maxPasswordAttempt)
        {
            // This method is necesary to message log
            user.PasswordAttempt++;
            if (user.PasswordAttempt == maxPasswordAttempt)
            {
                // DisabledString
                user.UserStatus = this.userStatusService.Get(s => s.Name == DisabledString);
            }

            this.Update(user);
        }

        /// <summary>
        /// The send email with new token.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="appPath">
        /// The app path.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult SendEmailWithNewToken(string userName, string appPath, string token)
        {
            try
            {
                var user = this.Get(x => x.UserName == userName);
                if (user == null)
                {
                    return new ServiceResult { Success = false, Message = Global.ViewAccountSentMailErrorMsj };
                }
                
                var body = Smart.Format(
                    Global.ViewAccountEmailBodyCreateJwt,
                    new
                    {
                        HostName = appPath,
                        Token = token,
                        user.UserName
                    });
                this.emailService.SendEMail(
                    new EmailBodyParameters()
                    {
                        Addresses = new[] { user.Email },
                        ContentBody = body,
                        TitleBody = Global.ViewAccountEmailSubjectCreateJwt,
                        SubTitleBody = user.DisplayName,
                        Subject = Global.ViewAccountEmailSubjectCreateJwt,
                        TextBottom = string.Empty,
                        TextSharedBottom = Global.ViewAccountEmailSharedTextBottom
                    });

                return new ServiceResult { Success = true, Message = Global.ViewAccountSentMailSuccessMsj };
            }
            catch (Exception e)
            {
                this.logEngine.WriteError(e);
                return new ServiceResult { Success = false, Message = Global.ViewAccountSentMailErrorMsj };
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The generate password.
        /// </summary>
        /// <param name="usr">
        /// The usr.
        /// </param>
        /// <param name="maxLength">
        /// The max length.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GeneratePassword(User usr, int maxLength = 5)
        {
            var salt = Encryptor.GenerateRandomText(SaltLength);
            usr.Salt = Encryptor.Encrypt(salt);
            var password = Encryptor.GenerateRandomText(maxLength);
            usr.Password = Encryptor.HashSha512(password + salt);
            return password;
        }

        /// <summary>
        /// The get password in hash.
        /// </summary>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetPasswordInHash(string password, User user)
        {
            var salt = Encryptor.Decrypt(user.Salt);
            return Encryptor.HashSha512(password + salt);
        }

        /// <summary>
        /// The get notify before days.
        /// </summary>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        public Tuple<bool, int> GetNotifyBeforeDays(Guid userId)
        {
            var settings = this.appConfiguration.ExpirationPassword;
            var hasExpiration = settings.Days > 0;
            if (!settings.Activated || !hasExpiration)
            {
                return new Tuple<bool, int>(false, 0);
            }

            var usr = this.Get(userId);

            if (usr.PasswordUpdatedOnDate == null)
            {
                return new Tuple<bool, int>(false, 0);
            }

            var passedDays = DateTimeOffset.Now - usr.PasswordUpdatedOnDate;
            var remainDays = (int)(settings.Days - settings.NotifyBeforeDays);
            var total = remainDays - passedDays.Value.Days;
            return total <= 0 ? new Tuple<bool, int>(true, settings.NotifyBeforeDays - Math.Abs(total)) : new Tuple<bool, int>(false, 0);
        }

        #endregion
    }
}