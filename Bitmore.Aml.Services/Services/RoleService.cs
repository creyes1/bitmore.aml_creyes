// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The RoleService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Linq;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Resources.Language;

    using CacheManager.Core;

    #endregion

    /// <summary>
    /// The RoleService interface.
    /// </summary>
    public interface IRoleService : IServiceBase<Role>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        ServiceResult Save(Role role);

        /// <summary>
        /// The is valid role by module.
        /// </summary>
        /// <param name="currentRoleId">
        /// The current Role Id.
        /// </param>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsValidRoleByModule(Guid currentRoleId, string module);

        /// <summary>
        /// The clear cached roles.
        /// </summary>
        void ClearCachedRoles();

        #endregion
    }

    /// <summary>
    /// The role service.
    /// </summary>
    public class RoleService : ServiceBase<Role>, IRoleService
    {
        /// <summary>
        ///     The app module type web.
        /// </summary>
        private const string AppModuleTypeWeb = "Web";

        /// <summary>
        ///     The view module action.
        /// </summary>
        private const string ViewModuleAction = "ViewModule";

        /// <summary>
        /// The region valid role by module.
        /// </summary>
        private const string RegionValidRoleByModule = "ValidRoleByModule";

        #region Fields

        /// <summary>
        ///     The role repository.
        /// </summary>
        private readonly IRoleRepository roleRepository;

        /// <summary>
        ///     The app module repository.
        /// </summary>
        private readonly IAppModuleRepository appModuleRepository;

        /// <summary>
        /// The cache modules.
        /// </summary>
        private readonly ICacheManager<Tuple<Guid, string, bool>> cachePermissions;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleService"/> class.
        /// </summary>
        /// <param name="roleRepository">
        /// The role repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="appModuleRepository">
        /// </param>
        /// <param name="mapper">
        /// </param>
        /// <param name="cachePermissions"></param>
        public RoleService(
            IRoleRepository roleRepository,
            IUnitOfWork unitOfWork,
            IAppModuleRepository appModuleRepository,
            IMapper mapper,
            ICacheManager<Tuple<Guid, string, bool>> cachePermissions)
            : base(roleRepository, unitOfWork, mapper)
        {
            this.roleRepository = roleRepository;
            this.appModuleRepository = appModuleRepository;
            this.cachePermissions = cachePermissions;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public ServiceResult Save(Role role)
        {
            var roleId = this.GetQuery().Where(r => r.Name == role.Name).Select(x => x.RoleId).FirstOrDefault();
            if (roleId != Guid.Empty && roleId != role.RoleId)
            {
                return new ServiceResult { Success = false, Message = Global.ViewAccountUsersMsgExistingRole };
            }

            this.PurgeCache();
            if (role.RoleId != Guid.Empty)
            {
                return this.Update(role, new ServiceResult { Success = true, Message = Global.ViewAccountRolesMsgModified });
            }

            this.Add(role);
            return new ServiceResult { Success = true, Message = Global.ViewAccountRolesMsgSaved };
        }

        /// <summary>
        /// The is valid role by module.
        /// </summary>
        /// <param name="currentRoleId">
        /// The current Role Id.
        /// </param>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsValidRoleByModule(Guid currentRoleId, string module)
        {
            var format = $"{currentRoleId} - {module}";
            var cachedPermissions = this.cachePermissions.GetOrAdd(
                format,
                RegionValidRoleByModule,
                (k, r) =>
                    {
                        var result = (from apm in this.appModuleRepository.GetQuery()
                                      from action in apm.AppModuleActions
                                      from role in action.Roles
                                      where
                                          apm.TypeOrUrl == module
                                          && apm.AppModuleType.Name == AppModuleTypeWeb
                                          && action.Name == ViewModuleAction
                                      select new { apm.AppModuleId, role.RoleId }).ToList();


                        var isValid = !result.Any() || result.Any(x => x.RoleId == currentRoleId);
                        return new Tuple<Guid, string, bool>(currentRoleId, module, isValid);
                    });

            return cachedPermissions.Item3;
        }

        /// <summary>
        /// The clear cached roles.
        /// </summary>
        public void ClearCachedRoles()
        {
            this.cachePermissions.ClearRegion(RegionValidRoleByModule);
        }

        #endregion
    }
}