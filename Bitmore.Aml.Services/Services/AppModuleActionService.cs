// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleActionService.cs" company="Bitmore Technologies">
//   Copyright">Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The AppModuleActionService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The AppModuleActionService interface.
    /// </summary>
    public interface IAppModuleActionService : IServiceBase<AppModuleAction>
    {
    }

    /// <summary>
    /// The app module action service.
    /// </summary>
    public class AppModuleActionService : ServiceBase<AppModuleAction>, IAppModuleActionService
    {
        #region Fields

        /// <summary>
        /// The app module action repository.
        /// </summary>
        private readonly IAppModuleActionRepository appModuleActionRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleActionService"/> class.
        /// </summary>
        /// <param name="appModuleActionRepository">
        /// The app module action repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AppModuleActionService(
            IAppModuleActionRepository appModuleActionRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
            : base(appModuleActionRepository, unitOfWork, mapper)
        {
            this.appModuleActionRepository = appModuleActionRepository;
        }

        #endregion
    }
}