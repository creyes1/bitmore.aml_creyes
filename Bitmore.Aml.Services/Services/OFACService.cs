﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OFACService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos;
    using Bitmore.Aml.Domain.Utils;

    using Newtonsoft.Json;

    using RestSharp;

    #endregion

    /// <summary>
    /// The OFACService interface.
    /// </summary>
    public interface IOfacService
    {
        #region Public Methods and Operators

        /// <summary>
        /// The search by name and sources.
        /// </summary>
        /// <param name="firstName">
        /// The first Name.
        /// </param>
        /// <param name="lastNameFirst">
        /// The last Name First.
        /// </param>
        /// <param name="lastNameSecond">
        /// The last Name Second.
        /// </param>
        /// <param name="sourcesDelimitedByComma">
        /// The sources delimited by comma.
        /// </param>
        /// <param name="useFuzzyName">
        /// The use fuzzy name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        ConsolidatedScreenDTO SearchByNameAndSources(
            string firstName,
            string lastNameFirst,
            string lastNameSecond,
            string sourcesDelimitedByComma,
            bool useFuzzyName = false);

        #endregion
    }

    /// <summary>
    /// The ofac service.
    /// </summary>
    public class OfacService : IOfacService
    {
        #region Constants

        /// <summary>
        /// The ofac key.
        /// </summary>
        private const string OFACKey = "OFACKey";

        /// <summary>
        /// The source consolidate screening list.
        /// Example https://api.trade.gov/consolidated_screening_list/search?sources=ISN,SDN
        /// </summary>
        private const string SourceConsolidateScreeningList =
            "https://api.trade.gov/consolidated_screening_list/search?api_key={0}";

        /// <summary>
        /// The space key.
        /// </summary>
        private const string SpaceKey = " ";

        /// <summary>
        /// The url segment adress.
        /// </summary>
        private const string UrlSegmentAdress = "&address={0}";

        /// <summary>
        /// The url segment countries.
        /// </summary>
        private const string UrlSegmentCountries = "&countries={0}";

        /// <summary>
        /// The url segment end date.
        /// </summary>
        private const string UrlSegmentEndDate = "&end_date={0} TO {1}";

        /// <summary>
        /// The url segment expiration date.
        /// </summary>
        private const string UrlSegmentExpirationDate = "&expiration_date={0} TO {1}";

        /// <summary>
        /// The url segment fuzzy name.
        /// </summary>
        private const string UrlSegmentFuzzyName = "&fuzzy_name=true";

        /// <summary>
        /// The url segment issue date.
        /// </summary>
        private const string UrlSegmentIssueDate = "&issue_date={0} TO {1}";

        /// <summary>
        /// The url segmentkey word.
        /// </summary>
        private const string UrlSegmentkeyWord = "&q={0}";

        /// <summary>
        /// The url segment name.
        /// </summary>
        private const string UrlSegmentName = "&name={0}";

        /// <summary>
        /// The url segment size plus off set.
        /// </summary>
        private const string UrlSegmentSizePlusOffSet = "&size=1 to {0}&offset=1 to {1}";

        /// <summary>
        /// The url segment sources.
        /// </summary>
        private const string UrlSegmentSources = "&sources={0}";

        /// <summary>
        /// The url segment start date.
        /// </summary>
        private const string UrlSegmentStartDate = "&start_date={0} TO {1}";

        /// <summary>
        /// The url segment type.
        /// </summary>
        private const string UrlSegmentType = "&type={0}";

        /// <summary>
        /// The error getting data key.
        /// </summary>
        private const string ErrorGettingDataKey = "ErrorGettingData";

        #endregion

        #region Fields

        /// <summary>
        /// The global attribute service.
        /// </summary>
        private readonly IGlobalAttributeService globalAttributeService;

        /// <summary>
        /// The log engine.
        /// </summary>
        private readonly ILogEngine logEngine;
        
        /// <summary>
        /// The company repository.
        /// </summary>
        private readonly ICompanyRepository companyRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OfacService"/> class.
        /// </summary>
        /// <param name="logEngine">
        /// The log engine.
        /// </param>
        /// <param name="globalAttributeService">
        /// The global Attribute Service.
        /// </param>
        /// <param name="companyRepository">
        /// The company Repository.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public OfacService(
            ILogEngine logEngine, 
            IGlobalAttributeService globalAttributeService, 
            ICompanyRepository companyRepository)
        {
            this.logEngine = logEngine;
            this.globalAttributeService = globalAttributeService;
            this.companyRepository = companyRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The search by name and sources.
        /// </summary>
        /// <param name="firstName">
        /// The first Name.
        /// </param>
        /// <param name="lastNameFirst">
        /// The last Name First.
        /// </param>
        /// <param name="lastNameSecond">
        /// The last Name Second.
        /// </param>
        /// <param name="sourcesDelimitedByComma">
        /// The sources delimited by comma.
        /// </param>
        /// <param name="useFuzzyName">
        /// The use fuzzy name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public ConsolidatedScreenDTO SearchByNameAndSources(
            string firstName, 
            string lastNameFirst, 
            string lastNameSecond,
            string sourcesDelimitedByComma,
            bool useFuzzyName = false)
        {
            firstName = string.IsNullOrEmpty(firstName) ? " " : firstName;
            lastNameFirst = string.IsNullOrEmpty(lastNameFirst) ? " " : lastNameFirst;
            lastNameSecond = string.IsNullOrEmpty(lastNameSecond) ? " " : lastNameSecond;
            var fullName = (lastNameFirst + " " + lastNameSecond + " " + firstName).Trim();
            var url = this.GetUrlBase() + this.GetSegmentSearchByName(fullName, useFuzzyName)
                      + this.GetSegmentSearchBySources(sourcesDelimitedByComma);
            var result = this.ProcessRequest(url);

            if (!string.IsNullOrEmpty(result))
            {
                if (result == ErrorGettingDataKey)
                {
                    return this.GetLocalOfacSearchDetails(firstName, lastNameFirst, lastNameSecond);
                }
                else
                {
                    var dto = JsonConvert.DeserializeObject<ConsolidatedScreenDTO>(result);
                    return dto;
                }
            }
            else
            {
                return new ConsolidatedScreenDTO();
            }
            
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get local ofac search details.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastNameFirst">
        /// The last name first.
        /// </param>
        /// <param name="lastNameSecond">
        /// The last name second.
        /// </param>
        /// <returns>
        /// The <see cref="ConsolidatedScreenDTO"/>.
        /// </returns>
        private ConsolidatedScreenDTO GetLocalOfacSearchDetails(string firstName, string lastNameFirst, string lastNameSecond)
        {
            var consolidatedScreenDto = new ConsolidatedScreenDTO();
            var localOdacSearchDetails = this.companyRepository.GetLocalOfacSearchDetails(firstName, lastNameFirst, lastNameSecond);
            if (localOdacSearchDetails != null && localOdacSearchDetails.Any())
            {
                foreach (var localOdacSearchDetail in localOdacSearchDetails)
                {
                    var detail = new ResultDTO
                                     {
                                         name = localOdacSearchDetail.sdn_name,
                                         remarks = localOdacSearchDetail.sdn_remarks,
                                         type = localOdacSearchDetail.alt_type,
                                         addresses = new List<AddressDTO>(),
                                         alt_names = new List<string>()
                                     };
                    detail.addresses.Add(new AddressDTO
                                              {
                                                  address = localOdacSearchDetail.add_address,
                                                  city = localOdacSearchDetail.add_city_name,
                                                  country = localOdacSearchDetail.add_country
                                              });

                    detail.alt_names.Add(localOdacSearchDetail.alt_name);
                    consolidatedScreenDto.results.Add(detail);
                }
                consolidatedScreenDto.total = localOdacSearchDetails.Count;
            }
            
            return consolidatedScreenDto;
        }

        /// <summary>
        /// The get key.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetKey()
        {
            var ofacKey = this.globalAttributeService.GetQuery().FirstOrDefault(x => x.Name == OFACKey);
            return ofacKey != null ? ofacKey.Value : SpaceKey;
        }

        // Searches against fields in the addresses array.
        /// <summary>
        /// The get segment search by address.
        /// </summary>
        /// <param name="address">
        /// The address.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByAddress(string address)
        {
            return string.Format(UrlSegmentAdress, address);
        }

        /// <summary>
        /// The get segment search by countries.
        /// Searches only entities whose country, nationalities, or citizenships fields match the country code 
        /// based on ISO alpha-2 country codes 
        /// http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements.htm
        /// The country fields are found in the addresses and ids arrays.
        /// This method allows you to search for multiple countries (plural) separated by commas but will 
        /// only return one country(singular) per entity.
        /// </summary>
        /// <param name="countriesDelimitedByCommas">
        /// The countries delimited by commas.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByCountries(string countriesDelimitedByCommas)
        {
            return string.Format(UrlSegmentCountries, countriesDelimitedByCommas);
        }

        /// <summary>
        /// The get segment search by end date.
        /// Returns entries based on their end date. Dates are filtered by comparing them against an 
        /// inclusive range, which must be entered with the following format: YYYY-mm-dd TO YYYY-mm-dd. 
        /// Searching on a single date can be done by entering the same value for the start and end of the range.
        /// </summary>
        /// <param name="rangeDateStartString">
        /// The range date start string.
        /// </param>
        /// <param name="rangeDateEndString">
        /// The range date end string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByEndDate(string rangeDateStartString, string rangeDateEndString)
        {
            return string.Format(UrlSegmentEndDate, rangeDateStartString, rangeDateEndString);
        }

        /// <summary>
        /// The get segment search by expiration date.
        /// Returns entries based on the expiration dates of the ids array. Dates are filtered by comparing 
        /// them against an inclusive range, which must be entered with the following format: YYYY-mm-dd TO YYYY-mm-dd. 
        /// Searching on a single date can be done by entering the same value for the start and end of the range.
        /// </summary>
        /// <param name="rangeDateStartString">
        /// The range date start string.
        /// </param>
        /// <param name="rangeDateEndString">
        /// The range date end string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByExpirationDate(string rangeDateStartString, string rangeDateEndString)
        {
            return string.Format(UrlSegmentExpirationDate, rangeDateStartString, rangeDateEndString);
        }

        /// <summary>
        /// The get segment search by issue date.
        /// Returns entries based on the issue dates of the ids array. Dates are filtered by comparing them 
        /// against an inclusive range, which must be entered with the following format: YYYY-mm-dd TO YYYY-mm-dd. 
        /// Searching on a single date can be done by entering the same value for the start and end of the range.
        /// </summary>
        /// <param name="rangeDateStartString">
        /// The range date start string.
        /// </param>
        /// <param name="rangeDateEndString">
        /// The range date end string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByIssueDate(string rangeDateStartString, string rangeDateEndString)
        {
            return string.Format(UrlSegmentIssueDate, rangeDateStartString, rangeDateEndString);
        }

        /// <summary>
        /// The get segment search by keyword.
        /// Searches for a match within the name, alt_names, remarks, and title fields from all eleven lists.
        /// </summary>
        /// <param name="keyword">
        /// The keyword.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByKeyword(string keyword)
        {
            return string.Format(UrlSegmentkeyWord, keyword);
        }

        /// <summary>
        /// The get segment search by name.
        /// Searches against the name and alt_names fields.
        /// Note: The fuzzy_name parameter only works in tandem with name. 
        /// Set fuzzy_name=true to utilize fuzzy name matching when searching against the name and alt_names fields. 
        /// The default setting for fuzzy_name is false, which means the endpoint will only return exact matches.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="useFuzzyName">
        /// The use fuzzy name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByName(string name, bool useFuzzyName = false)
        {
            var urlSerchByName = string.Format(UrlSegmentName, name);
            if (useFuzzyName)
            {
                urlSerchByName += UrlSegmentFuzzyName;
            }

            return urlSerchByName;
        }

        /// <summary>
        /// The get segment search by size plus off set.
        /// The size parameter allows you to configure the number of results to be returned up to a maximum of 100. 
        /// The offset parameter defines the offset from the first result you want to fetch.Unless specified the 
        /// API returns 10 results at a time.
        /// </summary>
        /// <param name="size">
        /// The size.
        /// </param>
        /// <param name="offSet">
        /// The off set.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchBySizePlusOffSet(int size, int offSet)
        {
            if (size > 100)
            {
                size = 100;
            }
            else
            {
                if (size < 1)
                {
                    size = 1;
                }
            }

            if (offSet < 1)
            {
                offSet = 1;
            }

            return string.Format(UrlSegmentSizePlusOffSet, size, offSet);
        }

        /// <summary>
        /// The get segment search by sources.
        /// Searches only the lists specified by the Source Abbreviation.
        /// </summary>
        /// <param name="sourcesDelimitedByComma">
        /// The sources delimited by comma.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchBySources(string sourcesDelimitedByComma)
        {
            return string.Format(UrlSegmentSources, sourcesDelimitedByComma);
        }

        /// <summary>
        /// The get segment search by start date.
        /// Returns entries based on their start date. Dates are filtered by comparing them against an 
        /// inclusive range, which must be entered with the following format: YYYY-mm-dd TO YYYY-mm-dd. 
        /// Searching on a single date can be done by entering the same value for the start and end of the range.
        /// </summary>
        /// <param name="rangeDateStartString">
        /// The range date start string.
        /// </param>
        /// <param name="rangeDateEndString">
        /// The range date end string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByStartDate(string rangeDateStartString, string rangeDateEndString)
        {
            return string.Format(UrlSegmentStartDate, rangeDateStartString, rangeDateEndString);
        }

        /// <summary>
        /// The get segment search by type.
        /// Searches based on the type of the entry(e.g, Individual, Entity, Vessel).
        /// </summary>
        /// <param name="entryType">
        /// The entry type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSegmentSearchByType(string entryType)
        {
            return string.Format(UrlSegmentType, entryType);
        }

        /// <summary>
        /// The get url base.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetUrlBase()
        {
            return string.Format(SourceConsolidateScreeningList, this.GetKey());
        }

        /// <summary>
        /// The process request.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ProcessRequest(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.BadRequest || 
                response.StatusCode == HttpStatusCode.InternalServerError ||
                response.StatusCode == HttpStatusCode.ExpectationFailed ||
                response.StatusCode == HttpStatusCode.BadGateway)
            {
                return ErrorGettingDataKey;
            }

            return response.Content;
        }

        #endregion
    }

    /// <summary>
    /// The source abreviations.
    /// </summary>
    public class SourceAbreviations
    {
        #region Constants

        #region Department of State – Directorate of Defense Trade Controls

        /// <summary>
        /// The ITAR Debarred = DTC.
        /// </summary>
        public const string DTC = "DTC";

        #endregion

        #region Department of State – Bureau of International Security and Non-Proliferation

        /// <summary>
        /// The Nonproliferation Sanctions = ISN.
        /// </summary>
        public const string ISN = "ISN";

        #endregion

        #endregion

        #region Source Abbreviations Department of Commerce – Bureau of Industry and Security (BIS)

        /// <summary>
        /// The Denied Persons List = DPL.
        /// </summary>
        public const string DPL = "DPL";

        /// <summary>
        /// The Entity List = EL.
        /// </summary>
        public const string EL = "EL";

        /// <summary>
        /// The Unverified List = UVL.
        /// </summary>
        public const string UVL = "UVL";

        #endregion

        #region Department of Treasury - Office of Foreign Assets Control

        /// <summary>
        /// The Foreign Sanctions Evaders = FSE.
        /// </summary>
        public const string FSE = "FSE";

        /// <summary>
        /// The Non-SDN Iranian Sanctions Act List = ISA.
        /// </summary>
        public const string ISA = "ISA";

        /// <summary>
        /// The Palestinian Legislative Council List = PLC.
        /// </summary>
        public const string PLC = "PLC";

        /// <summary>
        /// The Part 561 List = 561.
        /// </summary>
        public const string FiveSixOne561 = "561";

        /// <summary>
        /// The Sectoral Sanctions Identifications List = SSI.
        /// </summary>
        public const string SSI = "SSI";

        /// <summary>
        /// The Specially Designated Nationals = SDN.
        /// </summary>
        public const string SDN = "SDN";

        #endregion
    }
}