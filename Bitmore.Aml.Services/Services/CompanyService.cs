namespace Bitmore.Aml.Services.Services
{
    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;

    public interface ICompanyService : IServiceBase<Company>
    {
    }

    public class CompanyService : ServiceBase<Company>, ICompanyService
    {

        private readonly ICompanyRepository companyRepository;

        public CompanyService(ICompanyRepository companyRepository, IUnitOfWork unitOfWork, IMapper mapper)
            : base(companyRepository, unitOfWork, mapper)
        {
            this.companyRepository = companyRepository;
        }
		
    }
}
