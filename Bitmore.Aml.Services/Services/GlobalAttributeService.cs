// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAttributeService.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The GlobalAttributeService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Services.Utils;

    #endregion

    /// <summary>
    /// The GlobalAttributeService interface.
    /// </summary>
    public interface IGlobalAttributeService : IServiceBase<GlobalAttribute>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The generate random text.
        /// </summary>
        /// <param name="maxLength">
        /// The max length.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GenerateRandomText(int maxLength = 5);

        #endregion
    }

    /// <summary>
    /// The global attribute service.
    /// </summary>
    public class GlobalAttributeService : ServiceBase<GlobalAttribute>, IGlobalAttributeService
    {
        #region Fields

        /// <summary>
        /// The global attribute repository.
        /// </summary>
        private readonly IGlobalAttributeRepository globalAttributeRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalAttributeService"/> class.
        /// </summary>
        /// <param name="globalAttributeRepository">
        /// The global attribute repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// </param>
        public GlobalAttributeService(
            IGlobalAttributeRepository globalAttributeRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
            : base(globalAttributeRepository, unitOfWork, mapper)
        {
            this.globalAttributeRepository = globalAttributeRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The generate random text.
        /// </summary>
        /// <param name="maxLength">
        /// The max length.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateRandomText(int maxLength = 5)
        {
            return Encryptor.GenerateRandomText(maxLength).ToUpper();
        }

        #endregion
    }
}