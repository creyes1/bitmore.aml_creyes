// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceBase.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The service base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AutoMapper;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The service base.
    /// </summary>
    /// <typeparam name="TEntity">
    /// </typeparam>
    public abstract class ServiceBase<TEntity> : IServiceBase<TEntity>
        where TEntity : class
    {
        #region Fields

        /// <summary>
        /// The repository.
        /// </summary>
        protected readonly IRepository<TEntity> Repository;

        /// <summary>
        /// The unit of work.
        /// </summary>
        protected readonly IUnitOfWork UnitOfWork;

        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceBase{TEntity}"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper"></param>
        protected ServiceBase(
            IRepository<TEntity> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.Repository = repository;
            this.UnitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Add(TEntity entity)
        {
            this.Repository.Add(entity);
            this.UnitOfWork.Commit();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Delete(TEntity entity)
        {
            this.Repository.Delete(entity);
            this.UnitOfWork.Commit();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            this.Repository.Delete(where);
            this.UnitOfWork.Commit();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        public virtual void Delete(params object[] primaryKeys)
        {
            this.Repository.Delete(primaryKeys);
            this.UnitOfWork.Commit();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity Get(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.Get(where);
        }

        /// <summary>
        /// The get from cache.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity GetFromCache(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.GetFromCache(where);
        }

        /// <summary>
        /// The get dynamic query.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="sort">
        /// The sort.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<T> GetDynamicQuery<T>(
            IQueryable<T> query,
            string sort,
            string where = null)
        {
            return this.Repository.GetDynamicQuery(
                query,
                sort,
                where);
        }

        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Any(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.Any(where);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity Get(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Repository.Get(where, includes);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary Keys.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity Get(params object[] primaryKeys)
        {
            return this.Repository.Get(primaryKeys);
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<TEntity> GetAsync(params object[] primaryKeys)
        {
            return this.Repository.GetAsync(primaryKeys);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.Repository.GetAll();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Repository.GetAll(includes);
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<List<TEntity>> GetAllAsync()
        {
            return this.Repository.GetAllAsync();
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<List<TEntity>> GetAllAsync(params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Repository.GetAllAsync(includes);
        }

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAllFromCache()
        {
            return this.Repository.GetAllFromCache();
        }

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAllFromCache(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.GetAllFromCache(where);
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.GetAsync(where);
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<TEntity> GetAsync(
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Repository.GetAsync(predicate, includes);
        }

        /// <summary>
        /// The get many.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.GetMany(where);
        }

        /// <summary>
        /// The get many.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetMany(
            Expression<Func<TEntity, bool>> where,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Repository.GetMany(where, includes);
        }

        /// <summary>
        /// The get many async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> where)
        {
            return this.Repository.GetManyAsync(where);
        }

        /// <summary>
        /// The get many async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<List<TEntity>> GetManyAsync(
            Expression<Func<TEntity, bool>> where,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Repository.GetManyAsync(where, includes);
        }

        /// <summary>
        /// The get paged query.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="sort">
        /// The sort.
        /// </param>
        /// <param name="maximumRows">
        /// The maximum rows.
        /// </param>
        /// <param name="startRowIndex">
        /// The start row index.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<T> GetPagedQuery<T>(
            IQueryable<T> query,
            string sort,
            int maximumRows,
            int startRowIndex,
            string where = null)
        {
            return this.Repository.GetPagedQuery(
                query,
                sort,
                maximumRows,
                startRowIndex,
                where);
        }

        /// <summary>
        /// The get query.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<TEntity> GetQuery()
        {
            return this.Repository.GetQuery();
        }

        /// <summary>
        /// The get query as no tracking.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<TEntity> GetQueryAsNoTracking()
        {
            return this.Repository.GetQueryAsNoTracking();
        }

        /// <summary>
        /// The map to save.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <typeparam name="TSource">
        /// </typeparam>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity MapToSave<TSource>(TSource source) where TSource : class
        {
            TEntity objectToSave;
            var keys = this.Repository.MapKeys(source);
            if (keys == null || keys.Any(k => k == null || k.Equals(k.GetDefaultValue())))
            {
                objectToSave = this.mapper.Map<TEntity>(source);
            }
            else
            {
                objectToSave = this.Get(keys);
                this.mapper.Map(source, objectToSave);
            }

            return objectToSave;
        }

        /// <summary>
        /// The map to save list.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <typeparam name="TSource">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> MapToSaveList<TSource>(IEnumerable<TSource> source) where TSource : class
        {
            if (source == null)
            {
                return new List<TEntity>();
            }

            var enumerable = source as TSource[] ?? source.ToArray();
            if (!enumerable.Any())
            {
                return new List<TEntity>();
            }

            var listOfobjects = enumerable.ToList().Select(s => new { Keys = this.Repository.MapKeys(s), Value = s }).ToArray();
            var currentObjects = this.Repository.GetCollection(listOfobjects.Select(x => x.Keys).ToArray());

            // Map the new objects
            var newobjects =
                listOfobjects.Where(o => o.Keys.All(k => k.Equals(k.GetDefaultValue())))
                    .Select(o => this.mapper.Map<TEntity>(o.Value)).ToList();

            // Map the existing objects
            var updatedObjects =
                listOfobjects.Where(o => !o.Keys.All(k => k.Equals(k.GetDefaultValue()))).Select(
                    ex =>
                    {
                        var objectToMap = currentObjects.FirstOrDefault(
                            co =>
                            {
                                var keysMapped = this.Repository.MapKeys(co);
                                return ex.Keys.All(k => keysMapped.Contains(k));
                            });

                        // Updated objects
                        return this.mapper.Map(ex.Value, objectToMap);
                    }).ToList();

            return newobjects.Concat(updatedObjects);
        }

        /// <summary>
        /// The map to save list.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="dest">
        /// The dest.
        /// </param>
        /// <param name="deleteObsolete">
        /// The delete obsolete.
        /// </param>
        /// <typeparam name="TSource">
        /// </typeparam>
        public virtual void MapToSaveList<TSource>(IEnumerable<TSource> source, ICollection<TEntity> dest, bool deleteObsolete = false) where TSource : class
        {
            var listOfobjects = this.MapToSaveList(source).ToArray();

            if (deleteObsolete)
            {
                // Get deleted objects
                var deletedObjects =
                       dest.Where(
                       x =>
                       {
                           var destkeys = this.Repository.MapKeys(x);
                           return !listOfobjects.Any(
                                      l =>
                                      {
                                          var keys = this.Repository.MapKeys(l);
                                          return keys.All(k => destkeys.Contains(k));
                                      });
                       }).ToList();

                this.Repository.Delete(deletedObjects);
            }

            dest.Clear();
            dest.AddRangeUnique(listOfobjects);
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="correctServiceResult">
        /// The correct service result.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public virtual ServiceResult Save(TEntity entity, ServiceResult correctServiceResult)
        {
            var keys = this.Repository.MapKeys(entity);
            if (keys == null || keys.Any(k => k.Equals(k.GetDefaultValue())))
            {
                // New entity
                this.Repository.Add(entity);
            }
            else
            {
                // Update entity
                this.Repository.Update(entity);
            }

            return !this.UnitOfWork.Commit()
                       ? new ServiceResult { Success = false, Message = Global.ViewSharedMsgConcurrencyException }
                       : correctServiceResult;
        }

        /// <summary>
        /// The purge cache.
        /// </summary>
        public void PurgeCache()
        {
            this.Repository.PurgeCache();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Update(TEntity entity)
        {
            this.Repository.Update(entity);
            this.UnitOfWork.Commit();
        }

        /// <summary>
        /// The update. WithOptimisticConcurrency
        /// </summary>
        /// <param name="entity">
        /// </param>
        /// <param name="correctServiceResult">
        /// </param>
        /// <param name="checkOptimisticConcurrency">
        /// The check Optimistic Concurrency.
        /// </param>
        /// <returns>
        /// The <see cref="ServiceResult"/>.
        /// </returns>
        public virtual ServiceResult Update(TEntity entity, ServiceResult correctServiceResult, bool checkOptimisticConcurrency = false)
        {
            this.Repository.Update(entity, checkOptimisticConcurrency);
            return !this.UnitOfWork.Commit()
                       ? new ServiceResult { Success = false, Message = Global.ViewSharedMsgConcurrencyException }
                       : correctServiceResult;
        }

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<int> UpdateAsync(TEntity entity)
        {
            this.Repository.Update(entity);
            return this.UnitOfWork.CommitAsync();
        }

        #endregion
    }
}