// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Encryptor.cs" company="Bitmore Technologies">
//   
// </copyright>
// <summary>
//   The encryptor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Services.Utils
{
    #region

    using System;
    using System.Globalization;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    #endregion

    /// <summary>
    /// The encryptor.
    /// </summary>
    internal class Encryptor
    {
        #region Constants

        /// <summary>
        /// Thestring  that contains the Abc.
        /// </summary>
        private const string Abc = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

        /// <summary>
        /// The encriptor message.
        /// </summary>
        private const string EncriptorMessage = "El string necesita ser diferente de null para poder ser Encriptado/Desencriptado";

        /// <summary>
        /// The format hash md 5.
        /// </summary>
        private const string FormatHex = "{0:x2}";

        #endregion

        #region Static Fields

        /// <summary>
        /// The y key.
        /// </summary>
        private static readonly byte[] YKey = Encoding.UTF8.GetBytes("BiTmOre#&=2015");

        /// <summary>
        /// The y vector.
        /// </summary>
        private static readonly byte[] YVector = Encoding.UTF8.GetBytes("685002799229157032");

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The convert hex to string.
        /// </summary>
        /// <param name="hexValue">
        /// The hex value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ConvertHexToString(string hexValue)
        {
            var strValue = string.Empty;
            while (hexValue.Length > 0)
            {
                strValue += Convert.ToChar(Convert.ToUInt32(hexValue.Substring(0, 2), 16)).ToString();
                hexValue = hexValue.Substring(2, hexValue.Length - 2);
            }

            return strValue;
        }

        /// <summary>
        /// The convert string to hex.
        /// </summary>
        /// <param name="asciiString">
        /// The ascii string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ConvertStringToHex(string asciiString)
        {
            var hex = string.Empty;
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var c in asciiString)
            {
                int tmp = c;
                hex += string.Format(FormatHex, Convert.ToUInt32(tmp.ToString(CultureInfo.InvariantCulture)));
            }

            return hex;
        }

        /// <summary>
        /// The decrypt.
        /// </summary>
        /// <param name="encryptedText">
        /// The texto encriptado.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public static string Decrypt(string encryptedText)
        {
            if (string.IsNullOrEmpty(encryptedText))
            {
                throw new ArgumentNullException(EncriptorMessage);
            }

            using (var cryptoProvider = Rijndael.Create())
            {
                using (var memoryStream = new MemoryStream(Convert.FromBase64String(encryptedText)))
                {
                    using (
                        var cryptoStream = new CryptoStream(
                            memoryStream, 
                            cryptoProvider.CreateDecryptor(YKey, YVector), 
                            CryptoStreamMode.Read))
                    {
                        using (var reader = new StreamReader(cryptoStream))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The encrypt.
        /// </summary>
        /// <param name="originalText">
        /// The texto original.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public static string Encrypt(string originalText)
        {
            if (string.IsNullOrEmpty(originalText))
            {
                throw new ArgumentNullException(EncriptorMessage);
            }

            using (var cryptoProvider = Rijndael.Create())
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (
                        var cryptoStream = new CryptoStream(
                            memoryStream,
                            cryptoProvider.CreateEncryptor(YKey, YVector),
                            CryptoStreamMode.Write))
                    {
                        using (var writer = new StreamWriter(cryptoStream))
                        {
                            writer.Write(originalText);
                            writer.Flush();
                            cryptoStream.FlushFinalBlock();
                            writer.Flush();
                            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The encrypt to url parameter.
        /// </summary>
        /// <param name="originalText">
        /// The original text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string EncryptAndConvertToHex(string originalText)
        {
            var encriptedText = Encrypt(originalText);
            var hexEncriptedText = ConvertStringToHex(encriptedText);
            return hexEncriptedText;
        }

        /// <summary>
        /// The generate random number.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int GenerateRandomNumber()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var buffer = new byte[4];
                rng.GetBytes(buffer);
                return BitConverter.ToInt32(buffer, 0);
            }
        }

        /// <summary>
        /// The generate random text.
        /// </summary>
        /// <param name="maxLength">
        /// The max length.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GenerateRandomText(int maxLength = 5)
        {
            var generatePassword = new StringBuilder(maxLength);
            var ran = new Random(GenerateRandomNumber());
            for (var i = 0; i < maxLength; i++)
            {
                generatePassword.Append(Abc[ran.Next(0, Abc.Length)]);
            }

            return generatePassword.ToString();
        }

        /// <summary>
        /// The hash m d 5.
        /// </summary>
        /// <param name="bytesToHash">
        /// The bytes to hash.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string HashMd5(byte[] bytesToHash)
        {
            using (var md5 = MD5.Create())
            {
                var sb = new StringBuilder();
                var stream = md5.ComputeHash(bytesToHash);
                foreach (var t in stream)
                {
                    sb.AppendFormat(FormatHex, t);
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// The hash m d 5.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string HashMd5(string path)
        {
            string md5;
            if (!File.Exists(path))
            {
                return null;
            }

            using (var file = File.Open(path, FileMode.Open))
            {
                var buffer = new byte[file.Length];
                file.Read(buffer, 0, (int)file.Length);
                md5 = HashMd5(buffer);
            }

            return md5;
        }

        /// <summary>
        /// The hash sha 512.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string HashSha512(string data)
        {
            var bytesToHash = Encoding.UTF8.GetBytes(data);
            using (var sha = new SHA512Managed())
            {
                var hash = sha.ComputeHash(bytesToHash);
                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// The decrypt from url parameter.
        /// </summary>
        /// <param name="hexEncriptedText">
        /// The hex encripted text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string HexToStringAndDecrypt(string hexEncriptedText)
        {
            var encriptedText = ConvertHexToString(hexEncriptedText);
            var originalText = Decrypt(encriptedText);
            return originalText;
        }

        #endregion
    }
}