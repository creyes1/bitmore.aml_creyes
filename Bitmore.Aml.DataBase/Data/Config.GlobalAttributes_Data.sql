INSERT INTO [Config].[GlobalAttributes] ([Name], [Value], [Description], [IsEditable], [CreatedOnDate]) VALUES (N'EmailBody', N'<html>
	<body> 
		<div>
			<div class="header" 
				style="width=100%;
				background-color:#ffffff;
				color: white;
				font-size: 28;
				font-family: sans-serif;">
				<img src="cid:logo" style="margin: 5px 5px 5px 20px;">
			</div>
			<div class="header-line" 
				style="width=100%;
				background-color: #d6b233;
				height: 3px;"></div>
			<br/>
			<div class="subject" 
				style="font-weight:bold;
				font-size:larger;
				margin-top:10px;"> {Subject} </div> 
			<hr class="content-line" size="2" 
				style="color: #2a323b;
				width: 100%;" > 
			<div class="content" style="margin-top:20px;"> {Content} </div>
		</div>
	</body> 
</html>', N'Template del correo que se va a enviar', 1, '2014-07-11 09:48:37.843')
INSERT INTO [Config].[GlobalAttributes] ([Name], [Value], [Description], [IsEditable], [CreatedOnDate]) VALUES (N'EmailConfig', N'<?xml version="1.0" encoding="utf-16"?>
<Mail xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <From>noreply@bitmoretechnologies.com</From>
    <To />
    <CC />
    <Bcc />
    <Subject />
    <Body />
    <Host>smtp.gmail.com</Host>
    <Port>587</Port>
    <UserName>noreply@bitmoretechnologies.com</UserName>
    <Password>noreplay</Password>
    <EnableSsl>true</EnableSsl>
    <EnableTls>false</EnableTls>
</Mail>', N'Template de la configuración de correo que se va a enviar', 1, '2014-07-11 09:48:37.843')
INSERT INTO [Config].[GlobalAttributes] ([Name], [Value], [Description], [IsEditable], [CreatedOnDate]) VALUES (N'MainWebPageId', N'1CC973BB-9D3E-47AD-8DFC-416DAD6200EF', N'Id de la página que se usará como página inicial de todos los roles', 0, '2016-07-19 16:35:52.490')
INSERT INTO [Config].[GlobalAttributes] ([Name], [Value], [Description], [IsEditable], [CreatedOnDate]) VALUES (N'UrlBackupBitSeed', N'D:\databases\Backup_databases\BackupBitSeedTest\', N'Ruta donde se almacenaran los Backup automaticos', 0, '2015-02-12 10:14:38.087')
