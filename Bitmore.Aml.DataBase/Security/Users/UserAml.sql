IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'UserBitSeed')
CREATE LOGIN [UserBitSeed] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [UserBitSeed] FOR LOGIN [UserBitSeed]
GO
GRANT IMPERSONATE ON USER:: [UserBitSeed] TO [UserBitSeedBackup]
GO
GRANT EXECUTE TO [UserBitSeed]
GRANT VIEW DEFINITION TO [UserBitSeed]
