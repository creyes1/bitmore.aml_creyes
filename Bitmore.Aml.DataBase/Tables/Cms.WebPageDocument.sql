CREATE TABLE [Cms].[WebPageDocument]
(
[WebPageID] [uniqueidentifier] NOT NULL,
[stream_id] [uniqueidentifier] NOT NULL,
[OriginalFileName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Cms].[WebPageDocument] ADD CONSTRAINT [PK_WebPageDocument] PRIMARY KEY CLUSTERED  ([WebPageID], [stream_id]) ON [PRIMARY]
GO
ALTER TABLE [Cms].[WebPageDocument] ADD CONSTRAINT [FK_WebPageDocument_Document] FOREIGN KEY ([stream_id]) REFERENCES [FileObject].[Document] ([stream_id]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [Cms].[WebPageDocument] ADD CONSTRAINT [FK_WebPageDocument_WebPage] FOREIGN KEY ([WebPageID]) REFERENCES [Cms].[WebPage] ([WebPageID])
GO
