CREATE TABLE [Person].[UserRole]
(
[UserID] [uniqueidentifier] NOT NULL,
[RoleID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Person].[UserRole] ADD CONSTRAINT [PK_Person.UserRole] PRIMARY KEY CLUSTERED  ([RoleID], [UserID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_RoleID] ON [Person].[UserRole] ([RoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_UserID] ON [Person].[UserRole] ([UserID]) ON [PRIMARY]
GO
ALTER TABLE [Person].[UserRole] ADD CONSTRAINT [FK_Person.UserRole_Person.Role_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [Person].[Role] ([RoleID]) ON DELETE CASCADE
GO
ALTER TABLE [Person].[UserRole] ADD CONSTRAINT [FK_Person.UserRole_Person.User_UserID] FOREIGN KEY ([UserID]) REFERENCES [Person].[User] ([UserID]) ON DELETE CASCADE
GO
