CREATE TABLE [App].[AppLog]
(
[AppLogID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AppLog__AppLogID__108B795B] DEFAULT (newid()),
[Date] [datetime] NOT NULL,
[Level] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Logger] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Thread] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AppLogType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [App].[AppLog] ADD CONSTRAINT [PK_App.AppLog] PRIMARY KEY CLUSTERED  ([AppLogID]) ON [PRIMARY]
GO
