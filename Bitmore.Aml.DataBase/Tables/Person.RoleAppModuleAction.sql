CREATE TABLE [Person].[RoleAppModuleAction]
(
[RoleID] [uniqueidentifier] NOT NULL,
[AppModuleActionID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Person].[RoleAppModuleAction] ADD CONSTRAINT [PK_Person.RoleAppModuleAction] PRIMARY KEY CLUSTERED  ([AppModuleActionID], [RoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AppModuleActionID] ON [Person].[RoleAppModuleAction] ([AppModuleActionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_RoleID] ON [Person].[RoleAppModuleAction] ([RoleID]) ON [PRIMARY]
GO
ALTER TABLE [Person].[RoleAppModuleAction] ADD CONSTRAINT [FK_Person.RoleAppModuleAction_App.AppModuleAction_AppModuleActionID] FOREIGN KEY ([AppModuleActionID]) REFERENCES [App].[AppModuleAction] ([AppModuleActionID]) ON DELETE CASCADE
GO
ALTER TABLE [Person].[RoleAppModuleAction] ADD CONSTRAINT [FK_Person.RoleAppModuleAction_Person.Role_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [Person].[Role] ([RoleID]) ON DELETE CASCADE
GO
