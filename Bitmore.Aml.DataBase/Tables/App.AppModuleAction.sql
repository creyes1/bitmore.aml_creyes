CREATE TABLE [App].[AppModuleAction]
(
[AppModuleActionID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AppModule__AppMo__1920BF5C] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AppModuleID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [App].[AppModuleAction] ADD CONSTRAINT [PK_App.AppModuleAction] PRIMARY KEY CLUSTERED  ([AppModuleActionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AppModuleID] ON [App].[AppModuleAction] ([AppModuleID]) ON [PRIMARY]
GO
ALTER TABLE [App].[AppModuleAction] ADD CONSTRAINT [FK_App.AppModuleAction_App.AppModule_AppModuleID] FOREIGN KEY ([AppModuleID]) REFERENCES [App].[AppModule] ([AppModuleID])
GO
