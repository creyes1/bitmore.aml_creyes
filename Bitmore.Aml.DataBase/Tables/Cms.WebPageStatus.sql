CREATE TABLE [Cms].[WebPageStatus]
(
[WebPageStatusID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__WebPageSt__WebPa__4316F928] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Color] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsVisible] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Cms].[WebPageStatus] ADD CONSTRAINT [PK_Cms.WebPageStatus] PRIMARY KEY CLUSTERED  ([WebPageStatusID]) ON [PRIMARY]
GO
