CREATE TABLE [App].[AppModule]
(
[AppModuleID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AppModule__AppMo__164452B1] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeOrUrl] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppModuleTypeID] [uniqueidentifier] NOT NULL,
[CssOrDesktopIcon] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentAppModuleID] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [App].[AppModule] ADD CONSTRAINT [PK_App.AppModule] PRIMARY KEY CLUSTERED  ([AppModuleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AppModuleTypeID] ON [App].[AppModule] ([AppModuleTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ParentAppModuleID] ON [App].[AppModule] ([ParentAppModuleID]) ON [PRIMARY]
GO
ALTER TABLE [App].[AppModule] ADD CONSTRAINT [FK_App.AppModule_App.AppModule_ParentAppModuleID] FOREIGN KEY ([ParentAppModuleID]) REFERENCES [App].[AppModule] ([AppModuleID])
GO
ALTER TABLE [App].[AppModule] ADD CONSTRAINT [FK_App.AppModule_App.AppModuleType_AppModuleTypeID] FOREIGN KEY ([AppModuleTypeID]) REFERENCES [App].[AppModuleType] ([AppModuleTypeID])
GO
