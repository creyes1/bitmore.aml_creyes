CREATE TABLE [App].[AppLogMessage]
(
[AppLogMessageID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AppLogMes__AppLo__1367E606] DEFAULT (newid()),
[IsRegex] [bit] NOT NULL,
[RegexStackTraceOrClass] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [App].[AppLogMessage] ADD CONSTRAINT [PK_App.AppLogMessage] PRIMARY KEY CLUSTERED  ([AppLogMessageID]) ON [PRIMARY]
GO
