CREATE TABLE [Person].[Role]
(
[RoleID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Role__RoleID__21B6055D] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOnDate] [datetime] NOT NULL CONSTRAINT [DF__Role__CreatedOnD__22AA2996] DEFAULT (getdate()),
[RowVersion] [timestamp] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Person].[Role] ADD CONSTRAINT [PK_Person.Role] PRIMARY KEY CLUSTERED  ([RoleID]) ON [PRIMARY]
GO
