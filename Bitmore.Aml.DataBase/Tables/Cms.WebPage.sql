CREATE TABLE [Cms].[WebPage]
(
[WebPageID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__WebPage__WebPage__48CFD27E] DEFAULT (newid()),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Title] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UrlFriendly] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UrlUnique] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Html] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HtmlPending] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebPageStatusID] [uniqueidentifier] NOT NULL,
[HasPendingChanges] AS (isnull(case  when [Html] IS NULL AND [HtmlPending] IS NOT NULL OR [HtmlPending]<>[Html] AND [HtmlPending] IS NOT NULL then CONVERT([bit],(1),(0)) end,CONVERT([bit],(0),(0)))) PERSISTED NOT NULL,
[CreatedOnDate] [datetime] NOT NULL CONSTRAINT [DF__WebPage__Created__49C3F6B7] DEFAULT (getdate()),
[CreatedBy] [uniqueidentifier] NOT NULL,
[Observations] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Cms].[WebPage] ADD CONSTRAINT [PK_Cms.WebPage] PRIMARY KEY CLUSTERED  ([WebPageID]) ON [PRIMARY]
GO
ALTER TABLE [Cms].[WebPage] ADD CONSTRAINT [FK_WebPage_User] FOREIGN KEY ([CreatedBy]) REFERENCES [Person].[User] ([UserID])
GO
ALTER TABLE [Cms].[WebPage] ADD CONSTRAINT [FK_WebPage_WebPageStatus] FOREIGN KEY ([WebPageStatusID]) REFERENCES [Cms].[WebPageStatus] ([WebPageStatusID])
GO
