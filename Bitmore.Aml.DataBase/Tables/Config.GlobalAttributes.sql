CREATE TABLE [Config].[GlobalAttributes]
(
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsEditable] [bit] NOT NULL,
[CreatedOnDate] [datetime] NOT NULL CONSTRAINT [DF__GlobalAtt__Creat__1ED998B2] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Config].[GlobalAttributes] ADD CONSTRAINT [PK_Config.GlobalAttributes] PRIMARY KEY CLUSTERED  ([Name]) ON [PRIMARY]
GO
