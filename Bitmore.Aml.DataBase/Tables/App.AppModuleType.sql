CREATE TABLE [App].[AppModuleType]
(
[AppModuleTypeID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AppModule__AppMo__1BFD2C07] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [App].[AppModuleType] ADD CONSTRAINT [PK_App.AppModuleType] PRIMARY KEY CLUSTERED  ([AppModuleTypeID]) ON [PRIMARY]
GO
