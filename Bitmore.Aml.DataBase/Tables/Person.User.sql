CREATE TABLE [Person].[User]
(
[UserID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__User__UserID__276EDEB3] DEFAULT (newid()),
[CreatedOnDate] [datetime] NOT NULL CONSTRAINT [DF__User__CreatedOnD__286302EC] DEFAULT (getdate()),
[CurrentRoleID] [uniqueidentifier] NOT NULL,
[Email] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastLogIn] [datetime] NULL,
[CurrentLogIn] [datetime] NULL,
[MaternalLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PasswordUpdatedOnDate] [datetime] NULL,
[PasswordAttempt] [tinyint] NOT NULL,
[PaternalLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowVersion] [timestamp] NOT NULL,
[SessionToken] [uniqueidentifier] NOT NULL,
[UserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserStatusID] [uniqueidentifier] NOT NULL,
[DisplayName] AS (rtrim((((rtrim(ltrim([Name]))+' ')+rtrim(ltrim([PaternalLastName])))+' ')+rtrim(ltrim(isnull([MaternalLastName],''))))) PERSISTED,
[DefaultLanguage] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Salt] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HasActiveSession] [bit] NOT NULL CONSTRAINT [DF_User_HasActiveSession] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Person].[User] ADD CONSTRAINT [PK_Person.User] PRIMARY KEY CLUSTERED  ([UserID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CurrentRoleID] ON [Person].[User] ([CurrentRoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_UserStatusID] ON [Person].[User] ([UserStatusID]) ON [PRIMARY]
GO
ALTER TABLE [Person].[User] ADD CONSTRAINT [FK_Person.User_Person.Role_CurrentRoleID] FOREIGN KEY ([CurrentRoleID]) REFERENCES [Person].[Role] ([RoleID])
GO
ALTER TABLE [Person].[User] ADD CONSTRAINT [FK_Person.User_Person.UserStatus_UserStatusID] FOREIGN KEY ([UserStatusID]) REFERENCES [Person].[UserStatus] ([UserStatusID])
GO
