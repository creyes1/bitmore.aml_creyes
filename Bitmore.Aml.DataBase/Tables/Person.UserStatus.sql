CREATE TABLE [Person].[UserStatus]
(
[UserStatusID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__UserStatu__UserS__2E1BDC42] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Person].[UserStatus] ADD CONSTRAINT [PK_Person.UserStatus] PRIMARY KEY CLUSTERED  ([UserStatusID]) ON [PRIMARY]
GO
