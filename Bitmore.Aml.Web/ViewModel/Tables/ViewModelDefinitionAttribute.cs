// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewModelDefinitionAttribute.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables
{
    #region

    using System;
    using System.Reflection;

    using Bitmore.Aml.Web.Infrastructure.Config;

    #endregion

    /// <summary>
    /// The view model definition attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ViewModelDefinitionAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelDefinitionAttribute"/> class.
        /// </summary>
        /// <param name="serviceType">
        /// The service type.
        /// </param>
        public ViewModelDefinitionAttribute(Type serviceType)
        {
            this.ServiceType = serviceType;
        }

        /// <summary>
        /// Gets or set the service type.
        /// </summary>
        public Type ServiceType { get; }

        /// <summary>
        /// Gets or sets the method get all.
        /// </summary>
        public MethodInfo MethodGetAll { get; set; }

        /// <summary>
        /// Gets the service
        /// </summary>
        public dynamic Service => UnityConfig.Instance.Resolve(this.ServiceType);
    }
}