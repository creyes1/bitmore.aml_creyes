// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionDateTimeOffset.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition date time offset.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The linq dynamic condition date time offset.
    /// </summary>
    public class LinqDynamicConditionDateTimeOffset : LinqDynamicConditionDateTime
    {
        #region Constants

        /// <summary>
        /// The datetimeoffset format.
        /// </summary>
        private const string DatetimeoffsetFormat = "{0}, TimeSpan.Zero";

        #endregion

        #region Methods

        /// <summary>
        /// The get formatted data.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected override string GetFormattedData(Type type, string value)
        {
            var dtos = DateTimeOffset.ParseExact(value, this.CurrentDateFormat, null);
            value = string.Format(DatetimeoffsetFormat, dtos.Ticks);
            return value;
        }

        #endregion
    }
}