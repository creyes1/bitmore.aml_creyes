// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionDefault.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition default.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The linq dynamic condition default.
    /// </summary>
    public class LinqDynamicConditionDefault : LinqDynamicConditionBase
    {
        #region Methods

        /// <summary>
        /// The get formatted data.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected override string GetFormattedData(Type type, string value)
        {
            return value;
        }

        #endregion
    }
}