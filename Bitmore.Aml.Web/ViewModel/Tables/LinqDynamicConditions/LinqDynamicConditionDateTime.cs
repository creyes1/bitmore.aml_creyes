// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionDateTime.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition date time.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Threading;

    using Bitmore.Aml.Domain.Utils;

    #endregion

    /// <summary>
    /// The linq dynamic condition date time.
    /// </summary>
    public class LinqDynamicConditionDateTime : LinqDynamicConditionBase
    {
        #region Constants

        /// <summary>
        /// The current date format.
        /// </summary>
        protected readonly string CurrentDateFormat;

        /// <summary>
        /// The date time between format.
        /// </summary>
        private const string DateTimeBetweenFormat = "{0} && {1}";

        /// <summary>
        /// The date time format.
        /// </summary>
        private const string DateTimeFormat = "{0},{1},{2}";

        /// <summary>
        /// The date time string.
        /// </summary>
        private const string DateTimeString = "datetime";

        /// <summary>
        /// The datetimeoffset string.
        /// </summary>
        private const string DatetimeoffsetString = "datetimeoffset";

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LinqDynamicConditionDateTime"/> class.
        /// </summary>
        public LinqDynamicConditionDateTime()
        {
            this.CurrentDateFormat = Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern;
        }

        #region Public Methods and Operators

        /// <summary>
        /// The get condition.
        /// </summary>
        /// <param name="ruleOp">
        /// The rule op.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="originalType">
        /// The original type.
        /// </param>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="propertyPath"></param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetCondition(string ruleOp, string data, Type originalType, string propertyName, string propertyPath)
        {
            var propTypeName = originalType.GetUnderlyingType().Name.ToLower();
            var allowed = new List<string>() { this.OpEqual, this.OpBetween };
            if (!allowed.Contains(ruleOp) || (propTypeName != DateTimeString && propTypeName != DatetimeoffsetString))
            {
                return null;
            }

            try
            {
                var startdate = string.Empty;
                var endDate = string.Empty;
                if (ruleOp == this.OpEqual)
                {
                    startdate = Convert.ToDateTime(data).ToString(this.CurrentDateFormat);
                    endDate = Convert.ToDateTime(data).AddDays(1).ToString(this.CurrentDateFormat);
                }
                else
                {
                    if (ruleOp == this.OpBetween)
                    {
                        var arrData = data.Split(" - ");
                        startdate = Convert.ToDateTime(arrData[0]).ToString(this.CurrentDateFormat);
                        endDate = Convert.ToDateTime(arrData[1]).AddDays(1).ToString(this.CurrentDateFormat);
                    }
                }

                var formatLeft = base.GetCondition(this.OpGreaterOrEqual, startdate, originalType, propertyName, propertyPath);
                var formatRigth = base.GetCondition(this.OpLessThan, endDate, originalType, propertyName, propertyPath);
                var expression = string.Format(DateTimeBetweenFormat, formatLeft, formatRigth);
                return expression;
            }
            catch
            {
            }

            return null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get formatted data.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected override string GetFormattedData(Type type, string value)
        {
            var dt = DateTime.ParseExact(value, this.CurrentDateFormat, null);
            value = string.Format(DateTimeFormat, dt.Year, dt.Month, dt.Day);
            return value;
        }

        #endregion
    }
}