// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionGuid.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition guid.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions
{
    #region

    using System;

    using Bitmore.Aml.Domain.Utils;

    #endregion

    /// <summary>
    /// The linq dynamic condition guid.
    /// </summary>
    public class LinqDynamicConditionGuid : LinqDynamicConditionBase
    {
        #region Constants

        /// <summary>
        /// The guid format.
        /// </summary>
        private const string GuidFormat = "\"{0}\"";

        /// <summary>
        /// The guid null format.
        /// </summary>
        private const string GuidNullFormat = @"{0} = null";

        /// <summary>
        /// The null string.
        /// </summary>
        private const string NullString = "null";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get condition.
        /// </summary>
        /// <param name="ruleOp">
        /// The rule op.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="originalType">
        /// The original type.
        /// </param>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="propertyPath"></param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetCondition(string ruleOp, string data, Type originalType, string propertyName, string propertyPath)
        {
            if (!IsNull(originalType, data))
            {
                return base.GetCondition(ruleOp, data, originalType, propertyName, propertyPath);
            }

            return ruleOp == this.OpEqual
                       ? string.Format(GuidNullFormat, propertyName)
                       : base.GetCondition(ruleOp, data, originalType, propertyName, propertyPath);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get formatted data.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected override string GetFormattedData(Type type, string value)
        {
            return IsNull(type, value) ? NullString : string.Format(GuidFormat, value);
        }

        /// <summary>
        /// The is null.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsNull(Type type, string value)
        {
            return type.IsNullable() && (string.IsNullOrEmpty(value) || value.ToLower() == NullString);
        }

        #endregion
    }
}