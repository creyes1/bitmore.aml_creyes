// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionString.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition string.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The linq dynamic condition string.
    /// </summary>
    public class LinqDynamicConditionString : LinqDynamicConditionBase
    {
        #region Constants

        /// <summary>
        /// The string format.
        /// </summary>
        private const string StringFormat = @"""{0}""";

        #endregion

        #region Methods

        /// <summary>
        /// The get formatted data.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected override string GetFormattedData(Type type, string value)
        {
            return string.Format(StringFormat, value);
        }

        #endregion
    }
}