// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionBase.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions
{
    #region

    using System;
    using System.Collections.Generic;

    using Bitmore.Aml.Domain.Utils;

    #endregion

    /// <summary>
    /// The linq dynamic condition base.
    /// </summary>
    public abstract class LinqDynamicConditionBase
    {
        #region Static Fields

        /// <summary>
        /// The where operation.
        /// </summary>
        public static readonly Dictionary<string, string> WhereOperation = new Dictionary<string, string>
                                                                               {
                                                                                   {
                                                                                       "eq", 
                                                                                       "{1} =  {2}({0})"
                                                                                   }, 
                                                                                   {
                                                                                       "ne", 
                                                                                       "{1} != {2}({0})"
                                                                                   }, 
                                                                                   {
                                                                                       "lt", 
                                                                                       "{1} <  {2}({0})"
                                                                                   }, 
                                                                                   {
                                                                                       "le", 
                                                                                       "{1} <= {2}({0})"
                                                                                   }, 
                                                                                   {
                                                                                       "gt", 
                                                                                       "{1} >  {2}({0})"
                                                                                   }, 
                                                                                   {
                                                                                       "ge", 
                                                                                       "{1} >= {2}({0})"
                                                                                   }, 
                                                                                   {
                                                                                       "bw", 
                                                                                       "{1}.StartsWith({2}({0}))"
                                                                                   }, 
                                                                                   {
                                                                                       "bn", 
                                                                                       "!{1}.StartsWith({2}({0}))"
                                                                                   }, 
                                                                                   {
                                                                                       "in", 
                                                                                       string
                                                                                       .Empty
                                                                                   }, 
                                                                                   {
                                                                                       "ni", 
                                                                                       string
                                                                                       .Empty
                                                                                   }, 
                                                                                   {
                                                                                       "ew", 
                                                                                       "{1}.EndsWith({2}({0}))"
                                                                                   }, 
                                                                                   {
                                                                                       "en", 
                                                                                       "!{1}.EndsWith({2}({0}))"
                                                                                   }, 
                                                                                   {
                                                                                       "cn", 
                                                                                       "{1}.Contains({2}({0}))"
                                                                                   }, 
                                                                                   {
                                                                                       "nc", 
                                                                                       "!{1}.Contains({2}({0}))"
                                                                                   }, 
                                                                                   {
                                                                                       "nu", 
                                                                                       "{1} == null"
                                                                                   }, 
                                                                                   {
                                                                                       "nn", 
                                                                                       "{1} != null"
                                                                                   }
                                                                               };

        #endregion

        #region Fields

        /// <summary>
        /// The op equal.
        /// </summary>
        protected readonly string OpEqual = "eq";

        /// <summary>
        /// The op between.
        /// </summary>
        protected readonly string OpBetween = "bw";

        /// <summary>
        /// The op greater or equal.
        /// </summary>
        protected readonly string OpGreaterOrEqual = "ge";

        /// <summary>
        /// The op less than.
        /// </summary>
        protected readonly string OpLessThan = "lt";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get format.
        /// </summary>
        /// <param name="ruleOp">
        /// The rule op.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="originalType">
        /// The original type.
        /// </param>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="propertyPath">
        /// The property Path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public virtual string GetCondition(string ruleOp, string data, Type originalType, string propertyName, string propertyPath)
        {
            var propTypeUnwraped = originalType.GetUnderlyingType();
            return string.Format(
                WhereOperation[ruleOp],
                this.GetFormattedData(originalType, data),
                propertyPath,
                propTypeUnwraped.Name);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get formatted data.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected virtual string GetFormattedData(Type type, string value)
        {
            return value;
        }

        #endregion
    }
}