// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqDynamicConditionHelper.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The linq dynamic condition helper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Tables
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Web.ViewModel.Tables.LinqDynamicConditions;

    #endregion

    /// <summary>
    /// The linq dynamic condition helper.
    /// </summary>
    public static class LinqDynamicConditionHelper
    {
        #region Constants

        /// <summary>
        /// The enumerable format.
        /// </summary>
        private const string EnumerableFormat = "{0}.Any({1})";

        /// <summary>
        /// The field split format.
        /// </summary>
        private const char FieldSplitFormat = '.';

        /// <summary>
        /// The incorrect value.
        /// </summary>
        private const string IncorrectValue = "%";

        /// <summary>
        /// The operator format.
        /// </summary>
        private const string OperatorFormat = "{0}:";

        #endregion

        #region Static Fields

        /// <summary>
        /// The valid operators.
        /// </summary>
        public static readonly Dictionary<string, string> ValidOperators = new Dictionary<string, string>
                                                                               {
                                                                                   {
                                                                                       "Object", 
                                                                                       string
                                                                                       .Empty
                                                                                   }, 
                                                                                   {
                                                                                       "Boolean", 
                                                                                       "eq:ne:"
                                                                                   }, 
                                                                                   {
                                                                                       "Char", 
                                                                                       string
                                                                                       .Empty
                                                                                   }, 
                                                                                   {
                                                                                       "String", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:bn:cn:nc:"
                                                                                   }, 
                                                                                   {
                                                                                       "SByte", 
                                                                                       "bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "Byte", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "Int16", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "UInt16", 
                                                                                       "bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "Int32", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "UInt32", 
                                                                                       "bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "Int64", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "UInt64", 
                                                                                       "bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "Decimal", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "Single", 
                                                                                       "eq:ne:lt:le:gt:ge:"
                                                                                   }, 
                                                                                   {
                                                                                       "Double", 
                                                                                       "eq:ne:lt:le:gt:ge:bw:ew:cn:"
                                                                                   }, 
                                                                                   {
                                                                                       "DateTime",
                                                                                       "eq:ne:lt:le:gt:ge:bw:"
                                                                                   }, 
                                                                                   {
                                                                                       "DateTimeOffset",
                                                                                       "eq:ne:lt:le:gt:ge:bw:"
                                                                                   }, 
                                                                                   {
                                                                                       "TimeSpan", 
                                                                                       string
                                                                                       .Empty
                                                                                   }, 
                                                                                   {
                                                                                       "Guid", 
                                                                                       "eq:"
                                                                                   }
                                                                               };

        /// <summary>
        /// The types allow.
        /// </summary>
        private static readonly IDictionary<Type, LinqDynamicConditionBase> SpecialTypes =
            new Dictionary<Type, LinqDynamicConditionBase>
                {
                    { typeof(Guid), new LinqDynamicConditionGuid() }, 
                    { typeof(String), new LinqDynamicConditionString() }, 
                    { typeof(DateTime), new LinqDynamicConditionDateTime() }, 
                    {
                        typeof(DateTimeOffset), 
                        new LinqDynamicConditionDateTimeOffset()
                    }, 
                };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get condition.
        /// </summary>
        /// <param name="rule">
        /// The rule.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetCondition<T>(IRuleViewModel rule)
        {
            if (rule.data == IncorrectValue)
            {
                // Returns an empty string when the data is ‘%’  
                return string.Empty;
            }

            // Initializing variables
            var stringProperty = rule.field.Split(FieldSplitFormat);
            var propertyTree = new List<PropertyInfo>();
            for (var i = 0; i < stringProperty.Length; i++)
            {
                var type = (i == 0) ? typeof(T) : propertyTree[i - 1].PropertyType;
                var propertyInfo = type.IsIEnumerable()
                                       ? type.GenericTypeArguments[0].GetProperty(stringProperty[i])
                                       : type.GetProperty(stringProperty[i]);
                propertyTree.Add(propertyInfo);
            }

            if (propertyTree.Count <= 0)
            {
                return string.Empty;
            }

            // Creating the condition expression
            var finalTypeName = propertyTree[propertyTree.Count - 1].PropertyType.GetUnderlyingType().Name;
            if (!ValidOperators[finalTypeName].Contains(string.Format(OperatorFormat, rule.op)))
            {
                return string.Empty;
            }

            var expression = string.Empty;
            var propInfo = propertyTree.Last();
            var propTypeUnwraped = propInfo.PropertyType.GetUnderlyingType();
            var propType = propInfo.PropertyType;
            var propName = propInfo.Name;
            var propertyPath = propertyTree.Select(x => x.Name).Aggregate((i, j) => i + "." + j);

            // If we have a special case, we parser the condition with a LinqDynamicCondition
            if (SpecialTypes.ContainsKey(propTypeUnwraped))
            {
                var parser = SpecialTypes[propTypeUnwraped];
                expression = parser.GetCondition(rule.op, rule.data, propType, propName, propertyPath);
            }
            else
            {
                // If is IEnumerable we need to find the element with "Any"
                if (propTypeUnwraped.IsIEnumerable())
                {
                    expression = string.Format(EnumerableFormat, propTypeUnwraped.Name, expression);
                }
                else
                {
                    var parser = new LinqDynamicConditionDefault();
                    expression = parser.GetCondition(rule.op, rule.data, propType, propName, propertyPath);
                }
            }

            return expression;
        }

        #endregion
    }

    /// <summary>
    /// The RuleViewModel interface.
    /// </summary>
    public interface IRuleViewModel
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        string data { get; set; }

        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        string field { get; set; }

        /// <summary>
        /// Gets or sets the op.
        /// </summary>
        string op { get; set; }
    }
}