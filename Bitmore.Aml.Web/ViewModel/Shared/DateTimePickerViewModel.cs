// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DateTimePickerViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Shared
{
    /// <summary>
    /// The date time picker view model.
    /// </summary>
    public class DateTimePickerViewModel
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="DateTimePickerViewModel"/> class from being created. 
        /// </summary>
        private DateTimePickerViewModel()
        {
        }

        /// <summary>
        /// Gets the search field.
        /// </summary>
        public string SearchField { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is range.
        /// </summary>
        public bool IsRange { get; private set; }

        /// <summary>
        /// The create search field.
        /// </summary>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <param name="isRange"></param>
        /// <returns>
        /// The <see cref="DateTimePickerViewModel"/>.
        /// </returns>
        public static DateTimePickerViewModel CreateTableField(string field, bool isRange)
        {
            return new DateTimePickerViewModel { SearchField = field, IsRange = isRange };
        }

        /// <summary>
        /// The create input field.
        /// </summary>
        /// <returns>
        /// The <see cref="DateTimePickerViewModel"/>.
        /// </returns>
        public static DateTimePickerViewModel CreateInputField(string ngModel)
        {
            return new DateTimePickerViewModel { NgModel = ngModel, IsRange = false };
        }

        /// <summary>
        /// The create input range field.
        /// </summary>
        /// <param name="ngModelStrat">
        /// The ng model strat.
        /// </param>
        /// <param name="ngModelEnd">
        /// The ng model end.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimePickerViewModel"/>.
        /// </returns>
        public static DateTimePickerViewModel CreateInputRangeField(string ngModelStrat, string ngModelEnd)
        {
            return new DateTimePickerViewModel { NgModelStart = ngModelStrat, NgModelEnd = ngModelEnd, IsRange = true };
        }

        public void Build()
        {
            const string FilterFormat = "st-search-date-time-picker='{0}' st-search-filter-op='eq' st-search-filter-op-field='{0}'";
            const string FilterRangeFormat = "st-search-date-time-picker-range='{0}' st-search-filter-op='bw' st-search-filter-op-field='{0}'";
            if (!string.IsNullOrEmpty(this.SearchField))
            {
                if (this.IsRange)
                {
                    this.AttrDateRange = string.Format(FilterRangeFormat, this.SearchField);
                }
                else
                {
                    this.AttrFirst = string.Format(FilterFormat, this.SearchField);
                }
            }
            else
            {
                if (!this.IsRange && !string.IsNullOrEmpty(this.NgModel))
                {
                    this.AttrFirst = $"ng-model='{ this.NgModel }'";
                }
                else
                {
                    if (!this.IsRange || string.IsNullOrEmpty(this.NgModelStart)
                        || string.IsNullOrEmpty(this.NgModelEnd))
                    {
                        return;
                    }

                    this.AttrFirst = $"ng-model='{ this.NgModelStart }'";
                    this.AttrSecond = $"ng-model='{ this.NgModelEnd }'";
                }
            }
        }

        /// <summary>
        /// Gets or sets the ng model.
        /// </summary>
        public string NgModel { get; set; }

        /// <summary>
        /// Gets or sets the ng model start.
        /// </summary>
        public string NgModelStart { get; set; }

        /// <summary>
        /// Gets or sets the ng model end.
        /// </summary>
        public string NgModelEnd { get; set; }

        /// <summary>
        /// Gets or sets the date range.
        /// </summary>
        public string AttrDateRange { get; set; }

        /// <summary>
        /// Gets or sets the first attr.
        /// </summary>
        public string AttrFirst { get; set; }

        /// <summary>
        /// Gets or sets the second attr.
        /// </summary>
        public string AttrSecond { get; set; }
    }
}