// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModuleHeaderViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Shared
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The module header view model.
    /// </summary>
    public class ModuleHeaderViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleHeaderViewModel"/> class.
        /// </summary>
        public ModuleHeaderViewModel()
        {
            this.GridActions = new List<Tuple<string, string>>();
        }

        /// <summary>
        /// Gets or sets the title icon.
        /// </summary>
        public string TitleIcon { get; set; }

        /// <summary>
        /// Gets or sets the title message.
        /// </summary>
        public string TitleMessage { get; set; }

        /// <summary>
        /// Gets or sets the title message.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the title top.
        /// </summary>
        public string TitleTop { get; set; }

        /// <summary>
        /// The grid actions.
        /// </summary>
        public List<Tuple<string, string>> GridActions { get; set; }

        /// <summary>
        /// The add grid action.
        /// </summary>
        /// <param name="onclick">
        /// The onclick.
        /// </param>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <returns>
        /// The <see cref="ModuleHeaderViewModel"/>.
        /// </returns>
        public ModuleHeaderViewModel AddGridAction(string onclick, string text)
        {
            this.GridActions.Add(new Tuple<string, string>(onclick, text));
            return this;
        }
    }
}