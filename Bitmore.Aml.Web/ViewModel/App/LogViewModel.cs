// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.App
{
    #region

    using System.Collections.Generic;
    using System.Dynamic;

    #endregion

    /// <summary>
    /// The log view model.
    /// </summary>
    public class LogViewModel
    {
        /// <summary>
        /// Gets or sets the app types.
        /// </summary>
        public IEnumerable<ExpandoObject> CurrentAppTypes { get; set; }

        /// <summary>
        /// Gets or sets the message types.
        /// </summary>
        public IEnumerable<ExpandoObject> CurrentLoggerTypes { get; set; }

        /// <summary>
        /// Gets or sets the log msg confirm.
        /// </summary>
        public string LogMsgConfirm { get; set; }

        /// <summary>
        /// Gets or sets the log msg success.
        /// </summary>
        public string LogMsgSuccess { get; set; }
    }
}