// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLogGridViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app log grid view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.App
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The app log grid view model.
    /// </summary>
    public class AppLogGridViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the app log id.
        /// </summary>
        public Guid AppLogId { get; set; }

        /// <summary>
        /// Gets or sets the app log type.
        /// </summary>
        public string AppLogType { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        public string Logger { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        #endregion
    }
}