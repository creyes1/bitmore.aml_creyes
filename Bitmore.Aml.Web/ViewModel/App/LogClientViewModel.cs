// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogClientViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.App
{
    /// <summary>
    /// The log client view model.
    /// </summary>
    public class LogClientViewModel
    {
        /// <summary>
        /// Gets or sets the col.
        /// </summary>
        public string Col { get; set; }

        /// <summary>
        /// Gets or sets the href.
        /// </summary>
        public string Href { get; set; }

        /// <summary>
        /// Gets or sets the line.
        /// </summary>
        public string Line { get; set; }

        /// <summary>
        /// Gets or sets the msg.
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }
    }
}