// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAttributeViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.App
{
    #region

    using System;
    using System.Data.SqlClient;

    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Extensions.Configuration;

    #endregion

    /// <summary>
    /// The global attribute view model.
    /// </summary>
    public class GlobalAttributeViewModel
    {
        /// <summary>
        /// The builder.
        /// </summary>
        private readonly SqlConnectionStringBuilder builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalAttributeViewModel"/> class.
        /// </summary>
        public GlobalAttributeViewModel()
        {
            var config = UnityConfig.Instance.Resolve<IConfiguration>();
            var connectionString = config.GetConnectionString("DefaultConnection");
            this.builder = new SqlConnectionStringBuilder(connectionString);
        }

        /// <summary>
        /// The app machine name.
        /// </summary>
        public string AppMachineName => Environment.MachineName;

        /// <summary>
        /// The data base machine name.
        /// </summary>
        public string DataBaseMachineName => this.builder.DataSource;

        /// <summary>
        /// The initial catalog.
        /// </summary>
        public string InitialCatalog => this.builder.InitialCatalog;

        /// <summary>
        /// The msg confirm purge.
        /// </summary>
        public string MsgConfirmPurge => Global.ViewAppGlobalAttributesMsgConfirmPurge;

        /// <summary>
        /// The msg success purge.
        /// </summary>
        public string MsgSuccessPurge => Global.ViewAppGlobalAttributesMsgSuccessPurge;

        /// <summary>
        /// The version
        /// </summary>
        public string Version => HtmlExtensions.GetVersion();
    }
}