// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmartTableColumnSearch.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.SmartTable
{
    using Bitmore.Aml.Web.ViewModel.Tables;

    /// <summary>
    /// The smart table column search.
    /// </summary>
    public class SmartTableColumnSearch : IRuleViewModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string field { get; set; }

        /// <summary>
        /// Gets or sets the op.
        /// </summary>
        public string op { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string data { get; set; }
    }
}