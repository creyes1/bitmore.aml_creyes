// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmartTableResult.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.SmartTable
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The smart table result.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class SmartTableResult<T>
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        /// Gets or sets the records filtered.
        /// </summary>
        public int NumberOfPages { get; set; }

        /// <summary>
        /// Gets or sets the records total.
        /// </summary>
        public int TotalItemCount { get; set; }
    }
}