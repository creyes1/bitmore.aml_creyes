// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmartTableViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.SmartTable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Infrastructure.Config;
    using Bitmore.Aml.Web.ViewModel.Tables;

    /// <summary>
    /// The smart table view model.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class SmartTableViewModel<T>
    {
        /// <summary>
        /// The format sort property name.
        /// </summary>
        private const string FormatSortPropertyName = "{0} {1}";

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the start.
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Gets or sets the sort.
        /// </summary>
        public SmartTableSort Sort { get; set; }

        /// <summary>
        /// Gets or sets the columns.
        /// </summary>
        public List<SmartTableColumnSearch> Columns { get; set; }

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmartTableViewModel{T}"/> class.
        /// </summary>
        public SmartTableViewModel()
        {
            this.mapper = UnityConfig.Instance.Resolve<IMapperGrid>().GetMapper();
        }

        /// <summary>
        /// The get result.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <param name="selector">
        /// The selector.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="TEntity">
        /// </typeparam>
        /// <returns>
        /// The <see cref="SmartTableResult"/>.
        /// </returns>
        public SmartTableResult<T> GetResult<TEntity>(
            IServiceBase<TEntity> service,
            Expression<Func<TEntity, object>> selector,
            Expression<Func<TEntity, bool>> where) where TEntity : class
        {
            var orderName = this.GetOrderName();
            var orderDir = this.GetOrderDir();
            var sort = orderName != null ? string.Format(FormatSortPropertyName, orderName, orderDir) : null;

            var filterWhere = this.GetWhere<T>();
            var query = service.GetQuery();
            if (where != null)
            {
                query = query.Where(where);
            }

            var result = selector != null ?
                              query.ProjectTo<T>(this.mapper.ConfigurationProvider, selector)
                              : query.ProjectTo<T>(this.mapper.ConfigurationProvider);
            var records = service.GetPagedQuery(result, sort, this.Number, this.Start, filterWhere).ToList();
            var totalRecords = service.GetDynamicQuery(result, sort, filterWhere).Count();
            return this.GetResult(records, totalRecords, this.Number);
        }

        /// <summary>
        /// The get result.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="TEntity">
        /// </typeparam>
        /// <returns>
        /// The <see cref="SmartTableResult"/>.
        /// </returns>
        public SmartTableResult<T> GetResult<TEntity>(
            IServiceBase<TEntity> service,
            Expression<Func<TEntity, bool>> where) where TEntity : class
        {
            return this.GetResult(service, null, where);
        }

        /// <summary>
        /// The get result.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <typeparam name="TEntity">
        /// </typeparam>
        /// <returns>
        /// The <see cref="SmartTableResult"/>.
        /// </returns>
        public SmartTableResult<T> GetResult<TEntity>(
            IServiceBase<TEntity> service) where TEntity : class
        {
            return this.GetResult(service, null, null);
        }

        /// <summary>
        /// The get result.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <param name="totalRecords">
        /// The total records.
        /// </param>
        /// <param name="pageSize">
        /// The page Size.
        /// </param>
        /// <returns>
        /// The <see cref="SmartTableResult"/>.
        /// </returns>
        public SmartTableResult<T> GetResult(IEnumerable<T> result, int totalRecords, int pageSize)
        {
            var dataresult = new SmartTableResult<T>
            {
                TotalItemCount = totalRecords,
                NumberOfPages = (int)Math.Ceiling((double)totalRecords / pageSize),
                Data = result
            };
            return dataresult;
        }

        /// <summary>
        /// The get result.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <returns>
        /// The <see cref="SmartTableResult"/>.
        /// </returns>
        public SmartTableResult<T> GetResult(IEnumerable<T> result)
        {
            var enumerable = result as T[] ?? result.ToArray();
            var items = enumerable.Skip(this.Start).Take(this.Number);
            var totalRecords = enumerable.Count();
            var dataresult = new SmartTableResult<T>
            {
                TotalItemCount = enumerable.Count(),
                NumberOfPages = (int)Math.Ceiling((double)totalRecords / this.Number),
                Data = items
            };
            return dataresult;
        }

        /// <summary>
        /// The get order name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetOrderName()
        {
            return !string.IsNullOrEmpty(this.Sort?.Predicate) ? this.Sort.Predicate : null;
        }

        /// <summary>
        /// The get order dir.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetOrderDir()
        {
            if (this.Sort != null)
            {
                return this.Sort.Reverse ? "desc" : "asc";
            }

            return null;
        }

        /// <summary>
        /// The get where.
        /// </summary>
        /// <typeparam name="TEntity">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetWhere<TEntity>()
        {
            var sb = new StringBuilder();
            if (this.Columns == null || this.Columns.All(x => string.IsNullOrEmpty(x.data)))
            {
                return sb.ToString();
            }

            var filters = this.Columns.Where(x => !string.IsNullOrEmpty(x.data));
            var numberRules = 0;
            var rules = filters as SmartTableColumnSearch[] ?? filters.ToArray();
            foreach (var rule in rules)
            {
                var conditionRule = LinqDynamicConditionHelper.GetCondition<TEntity>(rule);
                if (conditionRule == null || conditionRule.Length <= 0)
                {
                    continue;
                }

                sb.Append(conditionRule);
                numberRules++;
                if (numberRules < rules.Count())
                {
                    // We can use && or ||, by default we always use && 
                    sb.Append(" && ");
                }
            }

            return sb.ToString();
        }
    }
}