// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmartTableSort.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.SmartTable
{
    /// <summary>
    /// The smart table sort.
    /// </summary>
    public class SmartTableSort
    {
        /// <summary>
        /// Gets or sets the default order.
        /// </summary>
        public string Predicate { get; set; }

        /// <summary>
        /// Gets or sets the default order dir.
        /// </summary>
        public bool Reverse { get; set; }
    }
}