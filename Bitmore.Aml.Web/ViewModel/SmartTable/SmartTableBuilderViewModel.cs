// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmartTableBuilderViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.SmartTable
{
    #region

    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Web;

    using AutoMapper.QueryableExtensions;

    using Bitmore.Aml.Web.Infrastructure.Config;
    using Bitmore.Aml.Web.ViewModel.Tables;

    using Newtonsoft.Json;

    using Assembly = System.Reflection.Assembly;

    #endregion

    /// <summary>
    /// The smart table builder view model.
    /// </summary>
    public static class SmartTableBuilderViewModel
    {
        /// <summary>
        ///     The viewModelTypes field
        /// </summary>
        private static ConcurrentBag<Type> viewModelTypes;

        /// <summary>
        /// Create a new instance of SmartTableResult based on view model name
        /// </summary>
        /// <param name="viewModelName">
        /// </param>
        /// <param name="request">
        /// </param>
        /// <returns>
        /// The <see cref="SmartTableResult"/>.
        /// </returns>
        public static dynamic GetResult(string viewModelName, HttpRequestBase request)
        {
            var typeViewModel = GetViewModelByName(viewModelName);
            var type = typeof(SmartTableViewModel<>);
            var typeArgs = new[] { typeViewModel };
            var makeme = type.MakeGenericType(typeArgs);
            using (var req = request.InputStream)
            {
                req.Seek(0, SeekOrigin.Begin);
                using (var sr = new StreamReader(req))
                {
                    var json = sr.ReadToEnd();
                    dynamic viewModel = !string.IsNullOrEmpty(json)
                                            ? JsonConvert.DeserializeObject(json, makeme)
                                            : Activator.CreateInstance(makeme);

                    var attr = GetViewModelDefinition(typeViewModel);
                    return viewModel.GetResult(attr.Service);
                }
            }
        }

        /// <summary>
        /// The get result.
        /// </summary>
        /// <param name="viewModelName">
        /// The view model name.
        /// </param>
        /// <returns>
        /// The <see cref="dynamic"/>.
        /// </returns>
        public static dynamic GetResult(string viewModelName)
        {
            var typeViewModel = GetViewModelByName(viewModelName);
            var attr = GetViewModelDefinition(typeViewModel);
            var query = attr.Service.GetQuery() as IQueryable;
            if (attr.MethodGetAll == null)
            {
                attr.MethodGetAll =
                    typeof(SmartTableBuilderViewModel).GetMethod(
                        "GetProjection",
                        BindingFlags.NonPublic | BindingFlags.Static).MakeGenericMethod(typeViewModel);
            }

            var result = attr.MethodGetAll.Invoke(null, new object[] { query });
            return result;
        }

        /// <summary>
        /// The get view model by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/>.
        /// </returns>
        public static Type GetViewModelByName(string name)
        {
            const string ViewModelSiffix = "ViewModel";
            if (viewModelTypes == null)
            {
                viewModelTypes = new ConcurrentBag<Type>();
                var viewModels =
                    Assembly.GetExecutingAssembly()
                        .GetTypes()
                        .Where(t => t.IsClass && t.Name.EndsWith(ViewModelSiffix))
                        .ToList();
                viewModels.ForEach(x => viewModelTypes.Add(x));
            }

            name = string.Concat(name, ViewModelSiffix);
            return viewModelTypes?.FirstOrDefault(x => x.Name == name);
        }

        /// <summary>
        /// The get view model definition.
        /// </summary>
        /// <param name="typeViewModel">
        /// The type View Model.
        /// </param>
        /// <returns>
        /// The <see cref="ViewModelDefinitionAttribute"/>.
        /// </returns>
        public static ViewModelDefinitionAttribute GetViewModelDefinition(Type typeViewModel)
        {
            return Attribute.GetCustomAttribute(typeViewModel, typeof(ViewModelDefinitionAttribute)) as ViewModelDefinitionAttribute;
        }

        // ReSharper disable once UnusedMember.Local

        /// <summary>
        /// The get projection.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="dynamic"/>.
        /// </returns>
        private static dynamic GetProjection<T>(IQueryable query)
        {
            var mapper = UnityConfig.Instance.Resolve<IMapperGrid>().GetMapper();
            return query.ProjectTo<T>(mapper.ConfigurationProvider).ToList();
        }
    }
}