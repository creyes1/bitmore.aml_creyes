// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleGridViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The role grid view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The role grid view model.
    /// </summary>
    public class RoleGridViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public Guid RoleId { get; set; }

        #endregion
    }
}