// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsWebViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The settings web view model.
    /// </summary>
    public class SettingsWebViewModel : SettingsEditViewModel
    {
        /// <summary>
        ///     Gets or sets the created on date.
        /// </summary>
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        ///     Gets or sets the current log in.
        /// </summary>
        public DateTime? CurrentLogIn { get; set; }

        /// <summary>
        ///     Gets the display name.
        /// </summary>
        public string DisplayName { get; internal set; }

        /// <summary>
        ///     Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets the last log in.
        /// </summary>
        public DateTime? LastLogIn { get; set; }

        /// <summary>
        ///     Gets or sets the maternal last name.
        /// </summary>
        public string MaternalLastName { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the paternal last name.
        /// </summary>
        public string PaternalLastName { get; set; }

        /// <summary>
        ///     Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the profile image.
        /// </summary>
        public string ProfileImage { get; set; }

        /// <summary>
        /// Gets or sets the avatars.
        /// </summary>
        public SettingsAvatarViewModel[] Avatars { get; set; }
    }
}