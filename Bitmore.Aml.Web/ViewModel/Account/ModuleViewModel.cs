// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModuleViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Bitmore.Aml.Services.Services;

    #endregion

    /// <summary>
    /// The role page view model.
    /// </summary>
    public class ModuleViewModel
    {
        /// <summary>
        /// The app module action service.
        /// </summary>
        protected readonly IAppModuleActionService AppModuleActionService;

        /// <summary>
        /// The app module service.
        /// </summary>
        protected readonly IAppModuleService AppModuleService;

        /// <summary>
        /// The mapper.
        /// </summary>
        protected readonly IMapper Mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleViewModel"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="appModuleActionService">
        /// The app module action service.
        /// </param>
        /// <param name="appModuleService">
        /// The app module service.
        /// </param>
        public ModuleViewModel(
            IMapper mapper,
            IAppModuleActionService appModuleActionService,
            IAppModuleService appModuleService)
        {
            this.Mapper = mapper;
            this.AppModuleActionService = appModuleActionService;
            this.AppModuleService = appModuleService;
        }

        /// <summary>
        /// The get private modules.
        /// </summary>
        /// <param name="includeModuleActions">
        /// The include module actions.
        /// </param>
        /// <param name="onlyParents">
        /// The only parents.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TreeNode> GetPrivateModules(bool includeModuleActions = true, bool onlyParents = false)
        {
            return this.GetModules(true, includeModuleActions, onlyParents);
        }

        /// <summary>
        /// The get public modules.
        /// </summary>
        /// <param name="includeModuleActions">
        /// The include module actions.
        /// </param>
        /// <param name="onlyParents">
        /// The only parents.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TreeNode> GetPublicModules(bool includeModuleActions = true, bool onlyParents = false)
        {
            return this.GetModules(false, includeModuleActions, onlyParents);
        }

        /// <summary>
        /// The get nodes.
        /// </summary>
        /// <param name="modules">
        /// The modules.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private static IEnumerable<TreeNode> GetNodes(ICollection<TreeNode> modules)
        {
            var list = new List<TreeNode>(modules);
            list.ForEach(
                x =>
                {
                    var children = modules.Where(m => m.Parent == x.Id.ToString()).ToList();
                    if (children.Count != 1)
                    {
                        return;
                    }

                    var child = children.ElementAt(0);
                    if (child.Icon != null)
                    {
                        return;
                    }

                    modules.Remove(child);
                    x.Text = $"{x.Text} - {child.Text}";
                    x.Id = child.Id;
                });
            return modules;
        }

        /// <summary>
        /// The get modules.
        /// </summary>
        /// <param name="isPrivate">
        /// The is private.
        /// </param>
        /// <param name="includeModuleActions">
        /// The include module actions.
        /// </param>
        /// <param name="onlyParents">
        /// The only parents.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<TreeNode> GetModules(bool isPrivate, bool includeModuleActions, bool onlyParents)
        {
            var modules =
                this.Mapper.Map<ICollection<TreeNode>>(
                    this.AppModuleService.GetAllWebModules()
                        .Where(
                            x =>
                                x.IsPrivate == isPrivate
                                && ((onlyParents && x.ParentAppModuleId == null) || !onlyParents)));
            if (!includeModuleActions)
            {
                return GetNodes(modules);
            }

            var actions = this.Mapper.Map<ICollection<TreeNode>>(this.AppModuleActionService.GetAllFromCache());
            modules = modules.Concat(actions).ToList();
            return GetNodes(modules);
        }
    }
}