// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestPasswordViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The request password view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    /// <summary>
    /// The request password view model.
    /// </summary>
    public class RequestPasswordViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether is valid.
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        #endregion
    }
}