﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTokenGridViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.ViewModel.Tables;

    #endregion

    /// <summary>
    /// The user token grid view model.
    /// </summary>
    [ViewModelDefinition(typeof(IAccessTokenService))]
    public class UserTokenGridViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the access token id.
        /// </summary>
        [XlsxElement(true)]
        public Guid AccessTokenId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [XlsxElement(true)]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the created on date string.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUserTokensGridCreatedOnDate))]
        public string CreatedOnDateString { get; set; }

        /// <summary>
        /// Gets or sets the expiration on date string.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUserTokensGridExpirationDate))]
        public string ExpirationOnDateString { get; set; }

        /// <summary>
        /// Gets or sets the signature.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUserTokensGridSignature))]
        public string Signature { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUserTokensGridDescription))]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        [XlsxElement(true)]
        public DateTimeOffset CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the expiration on date.
        /// </summary>
        [XlsxElement(true)]
        public DateTimeOffset ExpirationOnDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is revoked.
        /// </summary>
        [XlsxElement(true)]
        public bool IsRevoked { get; set; }

        #endregion
    }
}