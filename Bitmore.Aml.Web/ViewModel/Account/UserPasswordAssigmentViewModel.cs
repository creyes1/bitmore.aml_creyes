// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserPasswordAssigmentViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The user password assigment view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The user password assigment view model.
    /// </summary>
    public class UserPasswordAssigmentViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the password updated on date.
        /// </summary>
        public string PasswordUpdatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the repeat new password.
        /// </summary>
        public string RepeatNewPassword { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        #endregion
    }
}