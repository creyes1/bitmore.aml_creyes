// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleTypeViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app module type view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The app module type view model.
    /// </summary>
    public class AppModuleTypeViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the app module type id.
        /// </summary>
        public Guid AppModuleTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}