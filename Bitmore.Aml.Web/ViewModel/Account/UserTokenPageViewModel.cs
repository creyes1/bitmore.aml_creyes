﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTokenPageViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    /// <summary>
    /// The user token page view model.
    /// </summary>
    public class UserTokenPageViewModel
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserTokenPageViewModel"/> class.
        /// </summary>
        public UserTokenPageViewModel()
        {
            this.Catalogs = new UserTokenPageCatalogsViewModel();
            this.Messages = new UserTokenPageMessagesViewModel();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the catalogs.
        /// </summary>
        public UserTokenPageCatalogsViewModel Catalogs { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        public UserTokenPageMessagesViewModel Messages { get; set; }

        #endregion
    }

    /// <summary>
    /// The user token page messages view model.
    /// </summary>
    public class UserTokenPageMessagesViewModel
    {
        /// <summary>
        /// Gets or sets the confirm revoke token msg.
        /// </summary>
        public string ConfirmRevokeTokenMsg { get; set; }

        /// <summary>
        /// Gets or sets the confirm generate token msg.
        /// </summary>
        public string ConfirmGenerateTokenMsg { get; set; }
    }

    /// <summary>
    /// The user token page catalogs view model.
    /// </summary>
    public class UserTokenPageCatalogsViewModel
    {
    }
}