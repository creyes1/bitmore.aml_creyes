// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAttributesGridViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The global attributes grid view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The global attributes grid view model.
    /// </summary>
    public class GlobalAttributesGridViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets the created on date.
        /// </summary>
        public DateTime CreatedOnDate { get; internal set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }

        #endregion
    }
}