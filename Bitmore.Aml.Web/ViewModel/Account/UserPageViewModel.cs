// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserPageViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The user page view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     The user page view model.
    /// </summary>
    public class UserPageViewModel
    {
        public UserPageViewModel()
        {
            this.Catalogs = new UserPageCatalogsViewModel();
            this.Messages = new UserPageMessagesViewModel();
        }

        /// <summary>
        /// Gets or sets the catalogs.
        /// </summary>
        public UserPageCatalogsViewModel Catalogs { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        public UserPageMessagesViewModel Messages { get; set; }
    }

    /// <summary>
    /// The user page messages view model.
    /// </summary>
    public class UserPageMessagesViewModel
    {
        /// <summary>
        ///     Gets or sets the lbl msg table chng pass.
        /// </summary>
        public string LblMsgTableChngPass { get; set; }

        /// <summary>
        /// Gets or sets the lbl view account users company.
        /// </summary>
        public string lblViewAccountUsersCompany { get; set; }
    }

    /// <summary>
    /// The user page catalogs view model.
    /// </summary>
    public class UserPageCatalogsViewModel
    {
        /// <summary>
        /// Gets or sets the current languages.
        /// </summary>
        public IEnumerable<object> CurrentLanguage { get; set; }

        /// <summary>
        /// Gets or sets the current roles.
        /// </summary>
        public IEnumerable<RoleViewModel> CurrentRole { get; set; }

        /// <summary>
        /// Gets or sets the current user status.
        /// </summary>
        public IEnumerable<UserStatusViewModel> CurrentUserStatus { get; set; }
    }
}