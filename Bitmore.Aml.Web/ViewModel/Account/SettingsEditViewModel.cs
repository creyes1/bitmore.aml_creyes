// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsEditViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The user password edit view model.
    /// </summary>
    public class SettingsEditViewModel
    {
        /// <summary>
        /// Gets or sets the current password.
        /// </summary>
        public string CurrentPassword { get; set; }
        
        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the repeat new password.
        /// </summary>
        public string RepeatNewPassword { get; set; }

        /// <summary>
        /// Gets the row version.
        /// </summary>
        public string RowVersion { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }
    }
}