// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    /// <summary>
    /// The main view model.
    /// </summary>
    public class MainViewModel
    {
        /// <summary>
        /// Gets or sets the html.
        /// </summary>
        public string Html { get; set; }
    }
}