// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangePasswordExpiredViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The change password expired view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    /// <summary>
    /// The change password expired view model.
    /// </summary>
    public class ChangePasswordExpiredViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the repeat new password.
        /// </summary>
        public string RepeatNewPassword { get; set; }

        /// <summary>
        /// Gets the row version.
        /// </summary>
        public string RowVersion { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is first log in.
        /// </summary>
        public bool IsFirstLogIn { get; set; }

        #endregion
    }
}