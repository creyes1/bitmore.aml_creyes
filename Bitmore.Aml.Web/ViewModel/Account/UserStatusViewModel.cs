// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserStatusViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The user status view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The user status view model.
    /// </summary>
    public class UserStatusViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the user status id.
        /// </summary>
        public Guid UserStatusId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string NameTranslated => Global.ResourceManager.GetString($"ViewAccountUserStatus{this.Name}");

        #endregion
    }
}