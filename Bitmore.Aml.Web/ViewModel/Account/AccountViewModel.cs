// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The account view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System.ComponentModel.DataAnnotations;

    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The account view model.
    /// </summary>
    public class AccountViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the return url.
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether remember me.
        /// </summary>
        public bool RememberMe { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has active session.
        /// </summary>
        public bool HasActiveSession { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether override session.
        /// </summary>
        public bool OverrideActiveSession { get; set; }

        #endregion
    }
}