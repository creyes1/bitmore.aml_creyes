// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UsersGridViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The users grid view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.ViewModel.Tables;

    #endregion

    /// <summary>
    /// The users grid view model.
    /// </summary>
    [ViewModelDefinition(typeof(IUserService))]
    public class UsersGridViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUsersGridCreatedOnDate))]
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUsersGridDisplayName))]
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUsersGridEmail))]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the last log in.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUsersGridLastLogIn))]
        public DateTime? LastLogIn { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [XlsxElement(true)]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        [XlsxElement(nameof(Global.ViewAccountUsersGridUserName))]
        public string UserName { get; set; }
        
        #endregion
    }
}