// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserEditViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The user edit view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Globalization;

    #endregion

    /// <summary>
    /// The user edit view model.
    /// </summary>
    public class UserEditViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the current role id.
        /// </summary>
        public Guid CurrentRoleId { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the maternal last name.
        /// </summary>
        public string MaternalLastName { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the paternal last name.
        /// </summary>
        public string PaternalLastName { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        public string RowVersion { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user status id.
        /// </summary>
        public Guid UserStatusId { get; set; }

        /// <summary>
        /// Gets or sets the default language.
        /// </summary>
        public string DefaultLanguage { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        public IEnumerable<RoleViewModel> Roles { get; set; }

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        public Guid? CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the company selectize.
        /// </summary>
        public List<CompanyOptionViewModel> CompanySelectize { get; set; }

        #endregion
    }
}