// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleActionRoleViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app module action role view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The app module action role view model.
    /// </summary>
    public class AppModuleActionRoleViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the app module action id.
        /// </summary>
        public Guid AppModuleActionId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}