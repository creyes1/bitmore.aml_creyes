// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TreeNode.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The tree node.
    /// </summary>
    public class TreeNode
    {
        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        public string Parent { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// The state.
        /// </summary>
        public readonly object State = new { Opened = true };
    }
}