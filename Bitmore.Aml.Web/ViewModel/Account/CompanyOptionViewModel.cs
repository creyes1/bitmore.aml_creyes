﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompanyOptionViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The company option view model.
    /// </summary>
    public class CompanyOptionViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}