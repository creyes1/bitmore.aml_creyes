// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleEditViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The role edit view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The role edit view model.
    /// </summary>
    public class RoleEditViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the app module actions.
        /// </summary>
        public ICollection<AppModuleActionRoleViewModel> AppModuleActions { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        public string RowVersion { get; set; }

        #endregion
    }
}