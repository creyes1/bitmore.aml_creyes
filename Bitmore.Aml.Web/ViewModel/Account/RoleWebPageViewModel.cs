// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleWebPageViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The role web page view model.
    /// </summary>
    public class RoleWebPageViewModel
    {
        /// <summary>
        /// Gets or sets the modules.
        /// </summary>
        public IEnumerable<TreeNode> Modules { get; set; }
    }
}