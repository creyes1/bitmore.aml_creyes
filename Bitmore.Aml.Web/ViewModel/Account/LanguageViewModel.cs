// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguageViewModel.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The language view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.ViewModel.Account
{
    #region

    using System.Collections.Generic;
    using System.Globalization;

    #endregion

    /// <summary>
    /// The language view model.
    /// </summary>
    public class LanguageViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the languages.
        /// </summary>
        public IEnumerable<CultureInfo> Cultures { get; set; }

        /// <summary>
        /// Gets or sets the selected.
        /// </summary>
        public string Selected { get; set; }

        #endregion
    }
}