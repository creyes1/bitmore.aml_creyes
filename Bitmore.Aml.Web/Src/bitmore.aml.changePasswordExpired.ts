﻿/// <reference types="angular" />
﻿﻿/// <reference path="./bitmore.aml.controllers.ts" />

module ChangePasswordExpired {

    // ***************
    // * Interfaces  *
    // ***************

    export interface IChangePasswordConfiguration {
        rowVersion: string;
    }

    // ***************
    // * Controllers *
    // ***************
    
    export class ChangePassModalController extends Controllers.DetailModalController {
    
        protected onSaved(json: IJsonResult): void {
            if (json.success) {
                window.location.href = this.commonService.getUrl("/Account/Main");
            }
        }
    }

    export class ChangePasswordExpiredController {

        private vm: any;

        // To solve the minification problem
        static $inject = ["modalEngineService"];

        constructor(private modalEngineService: Common.IModalEngineService) {
            this.modalEngineService = modalEngineService;
        }

        init(config: IChangePasswordConfiguration): void {
            this.vm = {};
            this.vm.config = config;
            this.showModal();
        }
        
        showModal(): void {
            const modal = {
                templateUrl: "modalChangePasswordExpired.html",
                controller: "modalInstanceCtrl as ctrl",
                resolve: {
                    config: () => this.vm.config,
                    data: null,
                    urlSave: () => { return "/Account/ChangeCurrentPasswordExpired"; }
                }
            } as angular.ui.bootstrap.IModalSettings;
            this.modalEngineService.createAndOpen(modal);
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "toastr" , "signlRMessages"])
        .controller("changePasswordExpiredController", ChangePasswordExpiredController)
        .controller("modalInstanceCtrl", ChangePassModalController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("modalEngineService", Common.ModalService)
        .service("commonService", Common.CommonService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}