/// <reference path="./bitmore.aml.controllers.ts" />

module GlobalAttributes {

    // ***************
    // * Interfaces  *
    // ***************

    export interface IGlobalAttributesConfiguration {
        msgConfirmPurge: string;
        msgSuccessPurge: string;
    }

    export interface IGlobalAttributesRowViewModel {
        name: string;
    }

    // ***************
    // * Controllers *
    // ***************

    export class GlobalAttributesController extends Controllers.ControllerBase {

        protected vm: IViewModel<IGlobalAttributesConfiguration, IGlobalAttributesRowViewModel>;
        protected getUrlEdit(): string { return "/App/GetGlobalAttribute" };
        protected getUrlGrid(): string { return "/App/GetGlobalAttributes" };
        protected getUrlSave(): string { return "/App/SaveGlobalAttribute" };

        protected gridPostParam(row: IGlobalAttributesRowViewModel) {
            return { 'name': row.name };
        }

        purgeCache(): void {
            this.confirmAndPost(this.vm.config.msgConfirmPurge, "/App/PurgeCache", this.vm.config.msgSuccessPurge);
        }

        showSystemInfo(): void {
            const modal = {
                windowClass: "right fade modal-sidebar-sm",
                templateUrl: "modalSystemInfo.html",
                backdrop: true,
                controller: ["$scope", "$uibModalInstance", ($scope: any, $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance) => {
                    $scope.onClose = () => {
                        $uibModalInstance.close();
                    };
                }]
            } as angular.ui.bootstrap.IModalSettings;
            this.modalEngineService.createAndOpen(modal);
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "smart-table", "datetimepicker", "toastr", "signlRMessages"])
        .controller("globalAttributesController", GlobalAttributesController)
        .controller("detailInstanceCtrl", Controllers.DetailModalController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("smartGridService", SmartGrid.SmartGridService)
        .service("modalEngineService", Common.ModalService)
        .service("commonService", Common.CommonService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}