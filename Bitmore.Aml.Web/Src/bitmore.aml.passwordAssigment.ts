module PasswordAssigment {

    // ***************
    // * Controllers *
    // ***************

    export class PasswordAssigmentController {

        private $http: ng.IHttpService;
        private commonService: Common.ICommonService;
        private modalEngineService: Common.IModalEngineService;
        private vm: IDataViewModel<any, any>;

        // To solve the minification problem
        static $inject = ["$http", "commonService", "modalEngineService"];

        constructor(
            $http: ng.IHttpService,
            commonService: Common.ICommonService,
            modalEngineService: Common.IModalEngineService) {
            this.$http = $http;
            this.commonService = commonService;
            this.modalEngineService = modalEngineService;
        }

        init(config: any): void {
            this.vm = {} as any;
            this.vm.config = config;
            angular.merge(this.vm.data = {}, this.vm.config);
        }

        onPasswordAssigment(): void {
            this.modalEngineService.showProcess(() => {
                return this.$http
                    .post(this.commonService.getUrl("/Account/AssignPassword"), JSON.stringify(this.vm.data))
                    .success(this.setValues);
            });
        }

        setValues = (json: IJsonResult, status: any): void => {
            if (json && json.success) {
                window.location.replace(this.commonService.getUrl("/Account/Main"));
            } else {
                this.modalEngineService.warning(json.message);
            }
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "toastr"])
        .controller("passwordAssigmentController", PasswordAssigmentController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("modalEngineService", Common.ModalService)
        .service("commonService", Common.CommonService);
}