module Main {

    // ***************
    // * Interfaces  *
    // ***************

    export interface IConfiguration {
        show: boolean;
        title: string;
        text: string;
    };

    // ***************
    // * Controllers *
    // ***************

    export class MainController {

        private modalEngineService: Common.IModalEngineService;

        // To solve the minification problem
        static $inject = ["modalEngineService"];

        constructor(modalEngineService: Common.IModalEngineService) {
            this.modalEngineService = modalEngineService;
        }

        init(config: IConfiguration): void {
            if (config.show) {
                this.modalEngineService.success(config.text, config.title);
            }
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["ngAnimate", "ui.bootstrap", "toastr", "signlRMessages"])
        .service("modalEngineService", Common.ModalService)
        .controller("mainController", MainController)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}