module Common {

    declare var globalOptions: IGlobalOptions;

    // ***************
    // * Interfaces  *
    // ***************

    export interface IGlobalOptions {
        url: string;
        currentCulture: string;
        waitMessage: string;
    }

    export interface ICommonService {
        getUrl(url: string): string;
        getEmprtyGuid();
        isValidGuid(value: string);
        generateRandomString(length?: number, chars?: string);
        getValueByString(o: any, s: string): any;
    }

    // ***************
    // *  Services   *
    // ***************

    export class CommonService implements ICommonService {

        private $uibModal: angular.ui.bootstrap.IModalService;
        private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance;

        // To solve the minification problem
        static $inject = ["$uibModal"];

        constructor($uibModal: angular.ui.bootstrap.IModalService) {
            this.$uibModal = $uibModal;
        }

        getUrl(url: string): string {
            return globalOptions.url + url;
        }

        static getUrl(url: string): string {
            return globalOptions.url + url;
        }

        getEmprtyGuid(): string {
            return "00000000-0000-0000-0000-000000000000";
        }

        isValidGuid(value: string): boolean {
            if (!value) {
                return false;
            }

            const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;
            return regex.test(value);
        }

        generateRandomString(length?: number, chars?: string): string {
            let result = "";
            if (!chars) {
                chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }

            if (!length) {
                length = 6;
            }

            for (let i = length; i > 0; --i) {
                result += chars[Math.floor(Math.random() * chars.length)];
            }

            return result;
        }

        getValueByString(o: any, s: string): any {
            s = s.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
            s = s.replace(/^\./, "");           // strip a leading dot
            const a = s.split(".");
            for (var i = 0, n = a.length; i < n; ++i) {
                const k = a[i];
                if (k in o) {
                    o = o[k];
                } else {
                    return;
                }
            }
            return o;
        }
    }

    // *********************
    // *  Global Functions *
    // *********************

    export function getOpt() {
        return globalOptions;
    }

    export function getCulture() {
        return getOpt().currentCulture.substring(0, 2);
    }

    export function defaultErrorMessageResolver(resolver: any) {
        resolver.setI18nFileRootPath(CommonService.getUrl("/Content/angular-auto-validate/lang"));
        resolver.setCulture(Common.getOpt().currentCulture);
    }

    function fixModalStyle() {
        // Fix scroll background
        const $modal = $("div[modal-render='true']") as any;
        var $html = $("html");
        if (typeof $modal.livequery === "undefined") {
            return;
        }

        $modal.livequery(() => {
            $html.addClass("modal-open");
        }, () => {
            $html.removeClass("modal-open");
        });


        // Dirty fix for jumping scrollbar when modal opens
        const removeBodyStyleAttr = e => {
            if ($(window).width() > 768) {
                $("body").removeAttr("style");
            }
        };
        $(document).on("hide.bs.modal", ".modal", removeBodyStyleAttr);
        $(document).on("show.bs.modal", removeBodyStyleAttr);
    }

    function slimScroll() {
        $(".sidenav-scroll").slimScroll({
            height: "100%",
            wheelStep: 5,
            touchScrollStep: 50
        });
    }

    $(document).ready(() => {
        fixModalStyle();
        slimScroll();
    });
}