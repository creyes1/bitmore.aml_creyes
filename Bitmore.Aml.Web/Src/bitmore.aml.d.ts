// ***************
// *   JsTree    *
// ***************

interface ITreeInfo {
    treeConfig: JSTreeStaticDefaults;
    treeInstance: JSTree;
    treeData: Array<ITreeNode>;
    treeReady: boolean;
}

interface ITreeNode {
    id: string;
    parent: string;
    text: string;
    state: any;
    icon: string;
}

// ************************
// * SignalR Declarations *
// ************************

interface IMessageHubConnection extends SignalR.Hub.Connection {
    // Hubs Client functions: 
    client: {
        addMessageSessionFinished: (message: string) => void;
    };

    // Hubs Server function: 
    server: {
        send(message: string): any;
    };
}

interface SignalR {
    messageHub: IMessageHubConnection;
}

// ***************
// *    Shared   *
// ***************

interface IViewModel<TConfig, TRow> {
    config: TConfig;
    rowCollection: Array<TRow>;
}

interface IDataViewModel<TConfig, TRow> {
    config: TConfig;
    data: TRow;
}

interface IFillCatalogViewModel {
    catalog: string;
    property: string;
    propertyOfCatalog?: string;
}

interface IJsonResult {
    success: boolean;
    message: string;
    object: any;
}