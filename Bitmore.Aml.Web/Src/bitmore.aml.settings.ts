/// <reference path="./bitmore.aml.controllers.ts" />

module Settings {

    // ***************
    // * Interfaces  *
    // ***************

    export interface IDataSettings {
        profileImage: string;
    }

    // ***************
    // * Controllers *
    // ***************

    export class ModalAvatarController extends Controllers.DetailModalController {

        protected vm: IDataViewModel<any, IDataSettings>;

        protected onSaved(json: IJsonResult): void {
            if (json.success) {
                this.vm.data.profileImage = json.object;
                $("img.img-avatar").prop("src", this.vm.data.profileImage);
            }
        }
    }

    export class ChangePasswordModalController extends Controllers.DetailModalController {

        protected vm: IDataViewModel<any, SettingsChangePasswordViewModel>;

        init(config?: any): void {
            angular.merge(this.vm.data = new SettingsChangePasswordViewModel(), config);
        }

    }

    export class SettingsController {

        private $http: ng.IHttpService;
        private modalEngineService: Common.IModalEngineService;
        protected vm: IDataViewModel<any, any>;

        // To solve the minification problem
        static $inject = ["modalEngineService"];

        constructor(
            modalEngineService: Common.IModalEngineService) {
            this.modalEngineService = modalEngineService;
            this.vm = {} as any;
        }

        init(data: any): void {
            if (data) {
                this.vm.data = {};
                angular.merge(this.vm.data, data);
            }
        }

        changePassword(): void {
            const modal = {
                templateUrl: "modalChangePassword.html",
                controller: "modalInstanceCtrl as ctrl",
                resolve: {
                    data: null,
                    urlSave: () => { return "/Account/ChangeCurrentPassword"; }
                }
            } as angular.ui.bootstrap.IModalSettings;
            this.modalEngineService.createAndOpen(modal);
        }

        changeAvatar(): void {
            const modal = {
                windowClass: "right fade modal-sidebar-md",
                templateUrl: "modalAvatar.html",
                controller: "modalAvatarCtrl as ctrl",
                backdrop: true,
                resolve: {
                    data: this.vm.data,
                    urlSave: () => { return "/Account/ChangeAvatar"; }
                }
            } as angular.ui.bootstrap.IModalSettings;
            this.modalEngineService.createAndOpen(modal);
        }
    }

    // ***************
    // * ViewModels  *
    // ***************

    class SettingsChangePasswordViewModel {
        currentPassword: string;
        newPassword: string;
        repeatNewPassword: string;
        enableButton(): boolean {
            return this.currentPassword != null
                && this.newPassword != null
                && this.repeatNewPassword != null;
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "toastr", "signlRMessages"])
        .controller("settingsController", SettingsController)
        .controller("modalInstanceCtrl", ChangePasswordModalController)
        .controller("modalAvatarCtrl", ModalAvatarController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("modalEngineService", Common.ModalService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}