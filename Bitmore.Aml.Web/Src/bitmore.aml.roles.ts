module Roles {

    import ControllerBase = Controllers.ControllerBase;

    // ***************
    // * Interfaces  *
    // ***************

    export interface IAppModuleAction {
        appModuleActionId: string;
    }

    export interface IRoleConfiguration {
        modules: Array<ITreeNode>
    }

    export interface IRoleDataViewModel {
        tree: ITreeInfo;
        data: IRoleRowViewModel;
    }

    export interface IRoleRowViewModel {
        roleId: string;
        appModuleActions: Array<IAppModuleAction>;
    }

    // ***************
    // * Controllers *
    // ***************

    export class RolesDetailModalController extends Controllers.DetailModalController {

        protected vm: IRoleDataViewModel;

        initRoles(config: IRoleConfiguration): void {
            if (!this.vm.tree) {
                this.vm.tree = ({} as ITreeInfo);
            }

            this.vm.tree.treeData = config.modules;
            this.vm.tree.treeConfig = {
                core: {
                    multiple: true,
                    animation: true,
                    check_callback: true,
                    worker: true,
                    themes: {
                        name: "proton",
                        responsive: true
                    }
                },
                checkbox: {
                    tie_selection: false
                },
                version: 1,
                plugins: ["types", "checkbox", "wholerow"]
            } as any;
        }

        onTreeReady = (): void => {
            this.vm.tree.treeReady = true;
            if (!this.vm.data.appModuleActions) {
                this.vm.data.appModuleActions = new Array<IAppModuleAction>();
                return;
            }

            var ids = this.vm.data.appModuleActions.map((x) => { return x.appModuleActionId; });
            var tree = this.vm.tree.treeInstance;
            tree.jstree("check_node", ids);
        }

        onCheckNode = (e, data): void => {
            this.updateSelectedModules(data, true);
        }

        onUnCheckNode = (e, data): void => {
            this.updateSelectedModules(data, false);
        }

        updateSelectedModules = (data, insert: boolean): void => {
            var task = (id) => {
                var modules = this.vm.data.appModuleActions;
                const elements = this.$filter("filter")(modules, { appModuleActionId: id });
                if (insert) {
                    // If not exists, then add the new node. This is in case that is the first time
                    if (elements != null && elements.length === 0) {
                        const actions = this.vm.tree.treeData;
                        const nodetoAdd = this.$filter("filter")(actions, { id: id })[0] as ITreeNode;
                        modules.push({ appModuleActionId: nodetoAdd.id, name: nodetoAdd.text } as IAppModuleAction);
                    }
                } else {
                    const nodetoDelete = elements[0] as IAppModuleAction;
                    const index = modules.indexOf(nodetoDelete);
                    if (index > -1) {
                        modules.splice(index, 1);
                    }
                }
            };

            // Ignore not leaf nodes
            if (data.node.children.length > 0) {
                data.node.children.forEach((x) => task(x));
            } else {
                task(data.node.id);
            }
        }

    }

    export class RoleController extends ControllerBase {

        protected vm: IViewModel<IRoleRowViewModel, any>;
        protected getUrlEdit(): string { return "/Account/RoleById" };
        protected getUrlGrid(): string { return "/Account/GetRoles" };
        protected getUrlSave(): string { return "/Account/SaveRole" };

        protected gridPostParam(row: IRoleRowViewModel) {
            return { roleId: row.roleId };
        }

        protected parseModalSettings(settings: angular.ui.bootstrap.IModalSettings): void {
            settings.windowClass = "modal-wide";
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "smart-table", "datetimepicker", "ngJsTree", "toastr", "signlRMessages"])
        .controller("roleController", RoleController)
        .controller("detailInstanceCtrl", RolesDetailModalController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("smartGridService", SmartGrid.SmartGridService)
        .service("modalEngineService", Common.ModalService)
        .service("commonService", Common.CommonService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}