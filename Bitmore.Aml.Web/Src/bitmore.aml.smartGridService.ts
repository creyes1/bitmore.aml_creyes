module SmartGrid {
    import Promise = angular.IPromise;

    // ***************
    // * Interfaces  *
    // ***************

    export interface ISmartGridService {
        getGridData<T>(tableState: any, url: string, viewmodel: any, param?: ISmartGridParam): Promise<any>;
        exportToXlsx(url: string): void;
        exportFilteredToXlsx(url: string, tableState: any): void;
    }

    export interface ISmartGridParam {
        collectionName: string;
        loadingName: string;
    }

    // ***************
    // *  Services   *
    // ***************

    export class SmartGridService {
        private $q: ng.IQService;
        private $filter: ng.IFilterService;
        private $http: ng.IHttpService;
        private $scope: ng.IScope;
        private commonService: Common.ICommonService;

        // To solve the minification problem
        static $inject = ["$q", "$http", "commonService"];

        constructor($q: ng.IQService, $http: ng.IHttpService, commonService: Common.ICommonService) {
            this.$q = $q;
            this.$http = $http;
            this.commonService = commonService;
        }

        getGridData<T>(tableState: any, url: string, viewmodel: any, param?: ISmartGridParam): Promise<any> {
            param = angular.extend({
                collectionName: "rowCollection",
                loadingName: "isLoading"
            }, param);

            var deferred = this.$q.defer();
            const postData = this.getPostData(tableState);
            if (!postData.sort.predicate) {
                return deferred.promise;
            }

            viewmodel[param.loadingName] = true;
            this.$http.post(this.commonService.getUrl(url), JSON.stringify(postData)).success((data: any, status: any) => {
                tableState.pagination.numberOfPages = data.numberOfPages; // Set the number of pages so the pagination can update
                tableState.pagination.totalItemCount = data.totalItemCount;

                viewmodel[param.collectionName] = angular.fromJson(data.data);
                viewmodel[param.loadingName] = false;
                deferred.resolve({
                    data: data.data,
                    numberOfPages: data.numberOfPages
                });
            });
            return deferred.promise;
        }

        exportToXlsx(url: string): void {
            this.export(url, null);
        }

        exportFilteredToXlsx(url: string, tableState: any): void {
            const data = this.getPostData(tableState);
            this.export(url, data);
        }

        private export(url, datarequest): void {
            this.$http.post(this.commonService.getUrl(url), datarequest ? JSON.stringify(datarequest) : null).success((data: any, status: any) => {
                if (data) {
                    window.open(this.commonService.getUrl(`/App/GetFile?filename=${data}`));
                }
            });
        }

        private getPostData(tableState: any) {
            const pagination = tableState.pagination;
            const columns = new Array();
            const filter = tableState.search.predicateObject;
            if (filter) {
                const properties = new Array<string>();
                this.getProperties(filter, "", properties);
                properties.forEach(p => {
                    const value = this.commonService.getValueByString(filter, p);
                    let op = "cn";
                    if (tableState.stSearchFilterOp) {
                        op = tableState.stSearchFilterOp[p];
                    }

                    columns.push({ data: value, field: p, op: op });
                });
            }

            return {
                start: pagination.start || 0, // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                number: pagination.number || 10, // Number of entries showed per page.
                columns: columns,
                sort: tableState.sort
            }
        }

        private getProperties(obj: any, stack: string, list: Array<string>) {
            for (let property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (typeof obj[property] == "object") {
                        this.getProperties(obj[property], stack + "." + property, list);
                    } else {
                        const result = stack + "." + property;
                        list.push(result.substring(1, result.length));
                    }
                }
            }
        }

    }
}