/// <reference path="../bitmore.aml.controllers.ts" />

module UserTokens {
    import ControllerBase = Controllers.ControllerBase;

    // ***************
    // * Interfaces  *
    // ***************

    interface IUserTokensDataViewModel extends IViewModel<IUserTokensConfiguration, UserTokenRowViewModel> {
        data: UserTokenRowViewModel;
    }

    export interface IUserTokensConfiguration {
        confirmRevokeTokenMsg: string;
        confirmGenerateTokenMsg: string;
    }
    
    // ***************
    // * Controllers *
    // ***************

    export class UserTokensController extends ControllerBase {

        // To solve the minification problem
        static $inject = ["$http", "$filter", "smartGridService", "commonService", "modalEngineService", "data", "$uibModalInstance"];

        constructor(
            $http: ng.IHttpService,
            $filter: ng.IFilterService,
            smartGridService: SmartGrid.ISmartGridService,
            commonService: Common.ICommonService,
            modalEngineService: Common.IModalEngineService,
            data: any,
            public $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance) {

            super($http, $filter, smartGridService, commonService, modalEngineService);
            this.vm.data = data;
            this.$uibModalInstance = $uibModalInstance;
        }

        protected vm: IUserTokensDataViewModel;
        protected getUrlEdit(): string { return "/Account/GetUserToken" };
        protected getUrlGrid(): string { return `/Account/GetUserTokens?userId=${this.vm.data.userId}` };
        protected getUrlSave(): string { return "/Account/SaveUserToken" };
        protected getUrlExportToXlsx(): string { return "/App/ExportToXlsx?name=UserTokenGrid" };

        protected gridPostParam(row: UserTokenRowViewModel) {
            return { AccessTokenId: row.accessTokenId };
        }

        protected parseModalSettings(settings: angular.ui.bootstrap.IModalSettings): void {
            settings.windowClass = "modal-wide";
        }

        onRevokeToken(index: number, row: UserTokenRowViewModel): void {
            this.modalEngineService.confirm(this.vm.config.confirmRevokeTokenMsg)
                .result.then((result) => {
                    this.$http.post(this.commonService.getUrl("/Account/RevokeToken"), JSON.stringify({ accessTokenId: row.accessTokenId }))
                        .success((jsonResult: IJsonResult) => {
                            if (jsonResult.success) {
                                this.modalEngineService.success(jsonResult.message as string);
                                this.reloadGrid();
                            } else {
                                this.modalEngineService.alert(jsonResult.message as string);
                            }
                        });
                });
        }

        onCreateToken(): void {
            var userId = this.vm.data.userId;
            this.modalEngineService.confirm(this.vm.config.confirmGenerateTokenMsg)
                .result.then((result) => {
                    this.$http.post(this.commonService.getUrl("/Account/GenerateToken"), JSON.stringify({ userId: userId}))
                        .success((jsonResult: IJsonResult) => {
                            if (jsonResult.success) {
                                this.modalEngineService.success(jsonResult.message as string);
                                this.reloadGrid();
                            } else {
                                this.modalEngineService.alert(jsonResult.message as string);
                            }
                        });
                });
        }

        onCloseTokenModal(): void {
            this.$uibModalInstance.close();
        }

        static addModalTokens(modalEngineService: Common.IModalEngineService, data: any): angular.ui.bootstrap.IModalServiceInstance{
            const modal = {
                windowClass: "modal-wide",
                templateUrl: "modalTokensEdit.html",
                controller: "userTokensController as ctrl",
                resolve: {
                    data: () => {
                        return data;
                    }
                }
            } as angular.ui.bootstrap.IModalSettings;
            return modalEngineService.createAndOpen(modal);
        }
    }

    // ***************
    // * ViewModels  *
    // ***************

    export class UserTokenRowViewModel {
        accessTokenId: string;
        userId: string;    
    }
}