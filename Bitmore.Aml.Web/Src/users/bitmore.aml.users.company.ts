/// <reference path="../bitmore.aml.controllers.ts" />

module UserCompany {

    // ***************
    // * Controllers *
    // ***************

    export class ModalCompanyAssignamentController extends Controllers.DetailModalController {

        init(config?: any): void {
            super.init(config);

            this.vm.config.companySelectizeConfig = {
                maxItems: 1,
                valueField: 'companyId',
                labelField: 'name',
                searchField: 'name',
                placeholder: this.vm.config.lblViewAccountUsersCompany,
                load: (query, callback) => {
                    if (!query.length || query.length < 3) {
                        return callback();
                    }
                    this.$http.post(this.commonService.getUrl("/Account/GetCompaniesByFilter"), JSON.stringify({ term: query }))
                        .success((jsonResult: IJsonResult) => {
                            callback(jsonResult);
                        });

                    return callback();
                },
                onItemAdd: (value, $item) => {
                    this.vm.data.companyName = $item["0"].innerText;
                },
                onItemRemove: (value) => {
                    this.vm.data.companyId = null;
                    this.vm.data.companyName = null;
                }
            }

        }

        onClose(): void {
            this.$uibModalInstance.close();
        }

        onSaveCompanySelected(): void {
            this.$uibModalInstance.close(this.vm.data);
        }

        static addModalCompany(modalEngineService: Common.IModalEngineService, data: any): angular.ui.bootstrap.IModalServiceInstance {
            const modal = {
                windowClass: "right fade modal-sidebar-xs",
                templateUrl: "modalCompanyAssignament.html",
                controller: "modalCompanyAssignamentCtrl as ctrl",
                backdrop: true,
                resolve: {
                    data: angular.copy(data, {}),
                    urlSave: () => { return " "; }
                }
            } as angular.ui.bootstrap.IModalSettings;
            return modalEngineService.createAndOpen(modal);
        }

    }
}