/// <reference path="../bitmore.aml.controllers.ts" />

module Users {
    import ControllerBase = Controllers.ControllerBase;
    import UserTokensController = UserTokens.UserTokensController;

    // ***************
    // * Interfaces  *
    // ***************

    export interface IUserConfiguration {
        lblMsgTableChngPass: string;
    }

    export interface IUserDetailConfiguration {
        currentUserStatus: Array<any>;
        currentRole: Array<any>;
        currentLanguage: Array<any>;
    }

    export class UserDetailModalController extends Controllers.DetailModalController {

        protected vm: IDataViewModel<IUserDetailConfiguration, UserRowViewModel>;

        init(config?: any): void {
            super.init(config);
            if (this.vm.data && this.vm.data.userId) {
                this.fillCatalogByStr("currentUserStatus", "userStatusId");
                this.fillCatalogByStr("currentLanguage", "defaultLanguage", "name");
                this.fillCatalogByStr("currentRole", "currentRoleId", "roleId");
            } else {
                if (!this.vm.data.defaultLanguage && this.vm.config.currentLanguage.length === 1) {
                    this.vm.data.defaultLanguage = this.vm.config.currentLanguage[0].name;
                }
            }
        }

        onLinkCompany(): void {
            UserCompany.ModalCompanyAssignamentController.addModalCompany(this.modalEngineService, this.vm.data)
                .result.then((result) => {
                    if (result) {
                        angular.merge(this.vm.data, result);
                    }
                });
        }

    }

    export class UserController extends ControllerBase {

        protected vm: IViewModel<IUserConfiguration, UserRowViewModel>;
        protected getUrlEdit(): string { return "/Account/GetUser" };
        protected getUrlGrid(): string { return "/Account/GetUsers" };
        protected getUrlSave(): string { return "/Account/Save" };
        protected getUrlExportToXlsx(): string { return "/App/ExportToXlsx?name=UsersGrid" };

        protected gridPostParam(row: UserRowViewModel) {
            return { UserId: row.userId };
        }

        protected parseModalSettings(settings: angular.ui.bootstrap.IModalSettings): void {
            settings.windowClass = "modal-wide";
        }

        onSendPass(index: number, row: UserRowViewModel): void {
            this.confirmAndPost(this.vm.config.lblMsgTableChngPass, "/Account/SendPasswordById", null, JSON.stringify({ userId: row.userId }));
        }

        onEditTokens(index: number, row: UserRowViewModel): void {
            this.showTokensModal(row);
        }

        showTokensModal(data: any): angular.IPromise<any> {
            return UserTokensController.addModalTokens(this.modalEngineService, data).result.then((result) => {
                if (result) {
                    // Reload data
                    this.loadData(this.tableState);
                }
            });
        }
    }

    // ***************
    // * ViewModels  *
    // ***************

    export class UserRowViewModel {
        defaultLanguage: string;
        userId: string;
        roles = new Array<any>();

        // Extra
        companyId: string;
        companyName: string;
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "smart-table", "datetimepicker", "checklist-model", "toastr", "signlRMessages", "selectize"])
        .controller("userTokensController", UserTokens.UserTokensController)
        .controller("userController", UserController)
        .controller("detailInstanceCtrl", UserDetailModalController)
        .controller("modalCompanyAssignamentCtrl", UserCompany.ModalCompanyAssignamentController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("smartGridService", SmartGrid.SmartGridService)
        .service("modalEngineService", Common.ModalService)
        .service("commonService", Common.CommonService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}