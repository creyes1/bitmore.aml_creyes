module Controllers {

    // ***************
    // * Controllers *
    // ***************

    export class DetailModalController {
    
        protected vm: any;

        // To solve the minification problem
        static $inject = ["$scope", "$http", "$uibModalInstance", "commonService", "modalEngineService", "$filter", "urlSave", "data"];

        constructor(
            protected $scope: ng.IScope,
            protected $http: ng.IHttpService,
            protected $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
            protected commonService: Common.ICommonService,
            protected modalEngineService: Common.IModalEngineService,
            protected $filter: ng.IFilterService,
            protected urlSave: string,
            data: any) {
            this.$scope = $scope;
            this.$http = $http;
            this.$uibModalInstance = $uibModalInstance;
            this.commonService = commonService;
            this.modalEngineService = modalEngineService;
            this.$filter = $filter;
            this.vm = { data: data };
            this.urlSave = urlSave;
            this.onCreated();
        }

        onClose(): void {
            this.$uibModalInstance.close();
        }

        onSave(): void {
            if (!this.urlSave) {
                return;
            }

            this.modalEngineService.showProcess(() => {
                return this.$http.post(this.commonService.getUrl(this.urlSave), this.vm.data)
                    .success(this.saved);
            });
        }

        saved = (json: IJsonResult, status: any): void => {
            if (json && json.success) {
                this.$uibModalInstance.close(true);
                this.modalEngineService.success(json.message);
            } else {
                this.modalEngineService.warning(json.message);
            }

            this.onSaved(json);
        }

        protected onSaved(json: IJsonResult): void { }

        protected onCreated(): void { }

        protected fillCatalog(args: (IFillCatalogViewModel | IFillCatalogViewModel[])): void {
            if (typeof args === "object") {
                const arg = args as IFillCatalogViewModel;
                arg.propertyOfCatalog = arg.propertyOfCatalog ? arg.propertyOfCatalog : arg.property;
                let catalogparsed = arg.catalog;
                const valuefilter = this.$scope.$eval(`ctrl.vm.data.${arg.property}`);
                const listCatalog = this.$scope.$eval(`ctrl.vm.config.${arg.catalog}`);
                let data = this.vm.data;

                if (catalogparsed.indexOf(".") > 0) {
                    const arrCat = catalogparsed.split(".");
                    catalogparsed = arrCat[arrCat.length - 1];
                }

                if (arg.propertyOfCatalog.indexOf(".") > 0) {
                    const arr = arg.propertyOfCatalog.split(".");
                    arg.property = arr[arr.length - 1];
                    data = this.$scope.$eval(`ctrl.vm.data.${arr.slice(0, arr.length - 1).join(".")}`);
                    arg.propertyOfCatalog = arg.property;
                }

                const predicate = {};
                predicate[arg.propertyOfCatalog] = valuefilter;
                const result = this.$filter("filter")(listCatalog, predicate)[0];
                data[`${catalogparsed}Selected`] = result;
            } else {
                for (let a of args as IFillCatalogViewModel[]) {
                    this.fillCatalog(a);
                }
            }
        }

        protected fillCatalogByStr(catalog: string, property: string, propertyOfCatalog?: string): void {
            const arg = {
                catalog: catalog,
                property: property,
                propertyOfCatalog: propertyOfCatalog
            } as IFillCatalogViewModel;
            this.fillCatalog(arg);
        }

        init(config?: any): void {
            if (!config) {
                return;
            }

            angular.merge(this.vm.config = {}, config);
        }
    }

    export abstract class ControllerBase {

        protected tableState: any;
        protected vm: IViewModel<any, any>;
        protected modalEdit = "modalEdit.html";

        // To solve the minification problem
        static $inject = ["$http", "$filter", "smartGridService", "commonService", "modalEngineService"];

        constructor(
            protected $http: ng.IHttpService,
            protected $filter: ng.IFilterService,
            protected smartGridService: SmartGrid.ISmartGridService,
            protected commonService: Common.ICommonService,
            protected modalEngineService: Common.IModalEngineService) {
            this.$http = $http;
            this.$filter = $filter;
            this.vm = {} as any;
            this.smartGridService = smartGridService;
            this.commonService = commonService;
            this.modalEngineService = modalEngineService;
        }

        init(config: any): void {
            this.vm.config = {};
            if (config) {
                angular.merge(this.vm.config, config);
            }
        }

        onEdit(index: number, row: any): void {
            this.modalEngineService.showProcess(() => {
                const param = this.gridPostParam(row);
                return this.$http
                    .post(this.commonService.getUrl(this.getUrlEdit()), JSON.stringify(param))
                    .success(this.setValues);
            });
        }

        loadData = (tableState): void => {
            this.tableState = tableState;
            this.smartGridService.getGridData(tableState, this.commonService.getUrl(this.getUrlGrid()), this.vm, {} as SmartGrid.ISmartGridParam).then((result) => { });
        }

        setValues = (json: any, status: any): void => {
            var data = this.parseJsonResult(json);
            this.showModal(data);
        }

        showModal(data: any): angular.IPromise<any> {
            const modal = {
                templateUrl: this.modalEdit,
                controller: "detailInstanceCtrl as ctrl",
                resolve: {
                    urlSave: () => this.getUrlSave(),
                    data: () => { return data; }
                }
            } as angular.ui.bootstrap.IModalSettings;
            this.parseModalSettings(modal);
            return this.modalEngineService.createAndOpen(modal).result.then((result) => {
                if (result) {
                    // Reload data
                    this.loadData(this.tableState);
                }
            });
        }

        confirmAndPost<T>(messageConfirm: string, url: string, messageSuccess?: string, data?: any, successCallback?: angular.IHttpPromiseCallback<T>): void {
            this.modalEngineService.confirm(messageConfirm).result.then((result) => {
                if (result) {
                    if (!successCallback) {
                        successCallback = (json) => {
                            this.modalEngineService.success(messageSuccess);
                        };
                    }

                    this.$http.post(this.commonService.getUrl(url), data).success(successCallback);
                }
            });
        }

        addNew() {
            this.showModal({});
        }

        reset(): void {
            this.loadData(this.tableState);
        }

        reloadGrid(): void {
            this.loadData(this.tableState);
        }

        exportToXlsx(): void {
            if (!this.getUrlExportToXlsx()) {
                return;
            }

            this.smartGridService.exportToXlsx(this.getUrlExportToXlsx());
        }

        exportFilteredToXlsx(): void {
            if (!this.getUrlExportToXlsx()) {
                return;
            }

            this.smartGridService.exportFilteredToXlsx(this.getUrlExportToXlsx(), this.tableState);
        }

        protected getUrlExportToXlsx(): string { return null }

        protected parseJsonResult(json: any): any { return json; }

        protected parseModalSettings(settings: angular.ui.bootstrap.IModalSettings): void { }

        protected getUrlSave(): string { return null }

        protected abstract gridPostParam(row: any): any;

        protected abstract getUrlEdit(): string;

        protected abstract getUrlGrid(): string;
    }
}