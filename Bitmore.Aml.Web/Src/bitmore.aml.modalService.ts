module Common {

    declare var globalOptions: IGlobalOptions;

    // ***************
    // * Interfaces  *
    // ***************

    interface ILanguage {
        ok: string;
        cancel: string;
        lan: string;
        waitMessage: string;
        successMsg: string;
        inputMsg: string;
        inputWord: string;
    }

    export interface IModalEngineService {
        showProcess(method: () => ng.IHttpPromise<{}>): void;
        createAndOpen(settings: angular.ui.bootstrap.IModalSettings): angular.ui.bootstrap.IModalServiceInstance;
        confirm(message: string): angular.ui.bootstrap.IModalServiceInstance;
        confirmWithInput(message?: string, inputWord?: string): angular.ui.bootstrap.IModalServiceInstance;
        alert(message: string): angular.ui.bootstrap.IModalServiceInstance;
        success(message?: string, title?: string, options?: angular.toastr.IToastOptions): angular.toastr.IToast;
        warning(message: string, title?: string, options?: angular.toastr.IToastOptions): angular.toastr.IToast;
    }

    // ***************
    // *  Services   *
    // ***************

    export class ModalService implements IModalEngineService {

        private $uibModal: ng.ui.bootstrap.IModalService;
        private toastr: angular.toastr.IToastrService;
        private currentLan: ILanguage;

        // To solve the minification problem
        static $inject = ["$uibModal", "toastr"];

        constructor($uibModal: ng.ui.bootstrap.IModalService, toastr: angular.toastr.IToastrService) {
            this.$uibModal = $uibModal;
            this.toastr = toastr;
            this.currentLan = this.getCurrentLanguage();
        }

        private build(opt: angular.ui.bootstrap.IModalSettings): angular.ui.bootstrap.IModalServiceInstance {
            const defaultModal = {
                openedClass: "modal-open",
                backdrop: "static",
                keyboard: false
            } as angular.ui.bootstrap.IModalSettings;
            angular.extend(defaultModal, opt);
            return this.$uibModal.open(defaultModal);
        }

        showProcess(method: () => ng.IHttpPromise<{}>): void {
            const opt = {
                template:
                `<div class='modal-body'>
                    	${ this.currentLan.waitMessage}
                    	<div class='progress progress-striped active'>
                    		<div class='progress-bar' role='progressbar' style='width: 100%'></div>
                    	</div>
                    </div>`
            } as angular.ui.bootstrap.IModalSettings;
            var modalInstance = this.build(opt);
            method().success(() => modalInstance.close());
        }

        confirm(message: string): angular.ui.bootstrap.IModalServiceInstance {
            const opt = {
                template:
                `<div class='modal-body'>${message}</div>
                     <div class='modal-footer'>
                        <button class='btn btn-default' ng-click='onCancel()'>${ this.currentLan.cancel}</button>
                        <button class='btn btn-primary' ng-click='onOk()'>${ this.currentLan.ok}</button>
                    </div>`,
                controller: ["$scope", "$uibModalInstance", this.controller]
            } as angular.ui.bootstrap.IModalSettings;
            return this.build(opt);
        }

        confirmWithInput(message: string, inputMsg?: string, inputWord?: string): angular.ui.bootstrap.IModalServiceInstance {
            if (!inputMsg) {
                inputMsg = this.currentLan.inputMsg;
            }

            if (!inputWord) {
                inputWord = this.currentLan.inputWord;
            }

            const opt = {
                template:
                `<div class='modal-body'>
                        <div class="alert alert-danger">
                            ${message}
                        </div>
                        <div>${inputMsg}</div>
                        <div class="form-group">
                            <input type="text" class="form-control" ng-model="inputWord" required />
                        </div>
                     </div>
                     <div class='modal-footer'>
                        <button class='btn btn-default' ng-click='onCancel()'>${ this.currentLan.cancel}</button>
                        <button class='btn btn-primary' ng-disabled="inputWord !== '${ inputWord}'" ng-click='onOk()'>${this.currentLan.ok}</button>
                    </div>`,
                controller: ["$scope", "$uibModalInstance", this.controller]
            } as angular.ui.bootstrap.IModalSettings;
            return this.build(opt);
        }

        alert(message: string): angular.ui.bootstrap.IModalServiceInstance {
            const opt = {
                template:
                `<div class='modal-body'>${message}</div>
                     <div class='modal-footer'>
                        <button class='btn btn-primary' ng-click='onOk()'>${ this.currentLan.ok}</button>
                     </div>`,
                controller: ["$scope", "$uibModalInstance", this.controller]
            } as angular.ui.bootstrap.IModalSettings;
            return this.build(opt);
        }

        success(message?: string, title?: string, options?: angular.toastr.IToastOptions): angular.toastr.IToast {
            if (!message) {
                message = this.currentLan.successMsg;
            }

            return this.toastr.success(message, title, options);
        }

        warning(message: string, title?: string, options?: angular.toastr.IToastOptions): angular.toastr.IToast {
            return this.toastr.warning(message, title, options);
        }

        createAndOpen(settings: angular.ui.bootstrap.IModalSettings): angular.ui.bootstrap.IModalServiceInstance {
            return this.build(settings);
        }

        controller($scope: any, $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance): void {
            $scope.onOk = () => {
                $uibModalInstance.close(true);
            };
            $scope.onCancel = () => {
                $uibModalInstance.dismiss("cancel");
            };
        }

        getCurrentLanguage() {
            const languages = [
                { lan: "en", ok: "Ok", cancel: "Cancel", waitMessage: "Please wait ...", successMsg: "Information saved", inputMsg: "Enter the text <strong>DELETED</strong> to confirm the action", inputWord: "DELETED" },
                { lan: "es", ok: "Aceptar", cancel: "Cancelar", waitMessage: "Por favor espere ...", successMsg: "Información guardada correctamente", inputMsg: "Introduce el texto <strong>ELIMINAR</strong> para confirmar la acción", inputWord: "ELIMINAR" }
            ] as Array<ILanguage>;
            var cu = getCulture();
            const current = languages.find((value, index) => { return value.lan === cu });
            return current ? current : languages[0];
        }
    }

    // *********************
    // *  Global Functions *
    // *********************

    export function toastrConfig(opt: any) {
        angular.extend(opt, {
            progressBar: true,
            timeOut: "5000",
            closeButton: true
        });
    }
}