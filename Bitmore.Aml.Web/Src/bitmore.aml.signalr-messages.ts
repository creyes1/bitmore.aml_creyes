module SignlRMessages {

    // ***************
    // * Controllers *
    // ***************

    export class SignlRMessagesController {
    
        private modalEngineService: Common.IModalEngineService;
        private commonService: Common.ICommonService;
        private isSessionFinished: boolean;

        // To solve the minification problem
        static $inject = ["commonService", "modalEngineService"];

        constructor(
            commonService: Common.ICommonService,
            modalEngineService: Common.IModalEngineService) {
            this.modalEngineService = modalEngineService;
            this.commonService = commonService;
            this.initialize();
        }

        initialize(): void {
            const messageHub = $.connection.messageHub;
            messageHub.client.addMessageSessionFinished = (message) => this.addMessageSessionFinished(message);
            $.connection.hub.start().done(() => { }).fail(() => { });
        }

        addMessageSessionFinished(message: string): void {
            if (this.isSessionFinished) {
                return;
            }

            this.isSessionFinished = true;
            this.modalEngineService.alert(message)
                .result.then(() => window.location.href = this.commonService.getUrl("/account/login"));
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("signlRMessages", ["ui.bootstrap", "toastr"])
        .controller("signlRMessagesController", SignlRMessagesController)
        .service("commonService", Common.CommonService)
        .service("modalEngineService", Common.ModalService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);
}