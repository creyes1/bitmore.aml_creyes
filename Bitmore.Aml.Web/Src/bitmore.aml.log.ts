/// <reference path="./bitmore.aml.controllers.ts" />

module Log {
    import ControllerBase = Controllers.ControllerBase;

    // ***************
    // * Interfaces  *
    // ***************

    export interface ILogConfiguration {
        logMsgConfirm: string;
        logMsgSuccess: string;
    }

    export interface ILogRowViewModel {
        appLogId: string;
    }

    // ***************
    // * Controllers *
    // ***************

    export class LogController extends ControllerBase {

        protected vm: IViewModel<ILogConfiguration, ILogRowViewModel>;
        protected getUrlEdit(): string { return "/App/GetLogDetail" };
        protected getUrlGrid(): string { return "/App/GetLog" };

        protected parseJsonResult(json: any) {
            if (json && json.data) {
                json.data = json.data.replace(/(?:\r\n|\r|\n)/g, "<br/>");
            }

            return json;
        }

        protected gridPostParam(row: ILogRowViewModel) {
            return { appLogId: row.appLogId };
        }

        protected parseModalSettings(settings: angular.ui.bootstrap.IModalSettings): void {
            settings.windowClass = "modal-wide";
        }

        onClickClearLog(): void {
            this.confirmAndPost(this.vm.config.logMsgConfirm, "/App/ClearAppLogErrors", this.vm.config.logMsgSuccess);
        }
    }

    // ***************
    // *  Register   *
    // ***************

    angular
        .module("app", ["smart-table", "ui.bootstrap", "ngSanitize", "datetimepicker", "toastr", "signlRMessages"])
        .controller("logController", LogController)
        .controller("detailInstanceCtrl", Controllers.DetailModalController)
        .service("smartGridService", SmartGrid.SmartGridService)
        .service("commonService", Common.CommonService)
        .service("modalEngineService", Common.ModalService)
        .config(["toastrConfig", toastrConfig => Common.toastrConfig(toastrConfig)]);

}