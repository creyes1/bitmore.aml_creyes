// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Startup.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#region

using Bitmore.Aml.Web;

using Microsoft.Owin;

#endregion

[assembly: OwinStartup(typeof(Startup))]

namespace Bitmore.Aml.Web
{
    using System.Collections.Generic;
    using System.Web.Hosting;
    using System.Web.Mvc;

    using Bitmore.Aml.Web.Infrastructure;
    using Bitmore.Aml.Web.Infrastructure.Auth.Cookie;
    using Bitmore.Aml.Web.Infrastructure.Auth.Jwt;
    using Bitmore.Aml.Web.Infrastructure.Binders;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Extensions.Configuration;

    using Owin;

    /// <summary>
    /// The startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// The configuration.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public void Configuration(IAppBuilder app)
        {
            var overrideConfiguration = new Dictionary<string, string>
            {
                {
                    "AppConfiguration:AppEnvironment:ApplicationBasePath",
                    HostingEnvironment.MapPath("~/")
                }
            };

            var builder =
                new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", true, true)
                    .AddInMemoryCollection(overrideConfiguration);
            builder.AddEnvironmentVariables();
            this.Config = builder.Build();
            this.Configure(app);
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        public IConfigurationRoot Config { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public void Configure(IAppBuilder app)
        {
            app.ConfigureDependencyInjectionConfg(this.Config);
            app.ConfigureAutoMapper();
            app.ConfigureCookieAuthentication();
            app.ConfigureJwtAuthentication();
            app.ConfigureBundles();
            app.ConfigureRoutes()
               .ConfigureWebApi();
            app.MapSignalR();

            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            JobLauncher.SchedulerFactoryStart();
        }
    }
}