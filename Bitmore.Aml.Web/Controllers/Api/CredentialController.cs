﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountController.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Controllers.Api
{
    #region

    using System.Threading.Tasks;
    using System.Web.Http;

    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Dto.Account;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure;
    using Bitmore.Aml.Web.Infrastructure.Auth.Jwt;

    #endregion

    /// <summary>
    /// The account controller.
    /// </summary>
    [Authorize]
    public class CredentialController : ApiController
    {
        // Example: http://localhost:51561/api/credential/token
        //      x-www-form-urlencoded
        //      param = { UserName:"admin", Password:"admin" }


        // Example: http://localhost:51561/api/credential/testauth
        //  Headers:
        //      Authorization: Bearer eyJ0eXAiOiJKV1 ...
        //      Content-Type: application/json


        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The authentication.
        /// </summary>
        private readonly IAuthentication authentication;

        /// <summary>
        /// The jwt builder.
        /// </summary>
        private readonly IJwtBuilder jwtBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="CredentialController"/> class.
        /// </summary>
        /// <param name="userService">
        /// The user service.
        /// </param>
        /// <param name="authentication">
        /// </param>
        /// <param name="jwtBuilder">
        /// The jwt Builder.
        /// </param>
        public CredentialController(IUserService userService, IAuthentication authentication, IJwtBuilder jwtBuilder)
        {
            this.userService = userService;
            this.authentication = authentication;
            this.jwtBuilder = jwtBuilder;
        }

        /// <summary>
        /// The token.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<AuthTokenResponseDto> Token([FromBody]AuthTokenRequestDto model)
        {
            if (!this.ModelState.IsValid)
            {
                return new AuthTokenResponseDto { Success = false, Message = this.ModelState.GetErrorsAsString() };
            }

            var result = this.userService.Validate(model.UserName, model.Password, false);
            if (result == null || !result.Success)
            {
                return new AuthTokenResponseDto { Success = false, Message = Global.ApiAccountInvelidParameters };
            }

            var user = await this.userService.GetAsync(x => x.UserName == model.UserName);
            if (user == null)
            {
                return new AuthTokenResponseDto { Success = false, Message = Global.ApiAccountInvelidParameters };
            }

            var token = this.jwtBuilder.GenerateTokenAndSave(model.UserName);
            return new AuthTokenResponseDto
            {
                Success = true,
                AuthorizationToken = token,
                Message = Global.ApiAccountTokenSuccess
            };
        }

        /// <summary>
        /// The test auth.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [HttpGet]
        public string TestAuth()
        {
            return this.authentication.GetCurrentUserName();
        }
    }
}