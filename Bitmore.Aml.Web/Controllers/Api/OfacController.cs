﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OfacController.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Controllers.Api
{
    #region

    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos;
    using Bitmore.Aml.Services.Services;

    #endregion

    /// <summary>
    /// The ofac controller.
    /// </summary>
    [Authorize]
    public class OfacController : ApiController
    {
        /// <summary>
        /// The ofac service.
        /// </summary>
        private readonly IOfacService ofacService;

        /// <summary>
        /// Initializes a new instance of the <see cref="OfacController"/> class.
        /// </summary>
        /// <param name="ofacService">
        /// The ofac service.
        /// </param>
        public OfacController(IOfacService ofacService)
        {
            this.ofacService = ofacService;
        }

        /// <summary>
        /// The search by name and sources.
        /// </summary>
        /// <param name="firstName">
        /// The first Name.
        /// </param>
        /// <param name="lastNameFirst">
        /// The last Name First.
        /// </param>
        /// <param name="lastNameSecond">
        /// The last Name Second.
        /// </param>
        /// <param name="sourcesDelimitedByComma">
        /// The sources delimited by comma.
        /// </param>
        /// <param name="useFuzzyName">
        /// The use fuzzy name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [HttpGet]
        public ConsolidatedScreenDTO SearchByNameAndSources(
            string firstName,
            string lastNameFirst,
            string lastNameSecond, 
            string sourcesDelimitedByComma, 
            bool useFuzzyName = false)
        {
            return this.ofacService.SearchByNameAndSources(firstName, lastNameFirst, lastNameSecond, sourcesDelimitedByComma, useFuzzyName);
        }
    }
}