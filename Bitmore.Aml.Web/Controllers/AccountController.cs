// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountController.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The account controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Controllers
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using AutoMapper;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure;
    using Bitmore.Aml.Web.Infrastructure.Auth.Jwt;
    using Bitmore.Aml.Web.Infrastructure.Config;
    using Bitmore.Aml.Web.Infrastructure.Filters;
    using Bitmore.Aml.Web.ViewModel.Account;
    using Bitmore.Aml.Web.ViewModel.SmartTable;

    using BotDetect.Web.Mvc;

    #endregion

    /// <summary>
    /// The account controller.
    /// </summary>
    [ModuleAuthorizeFilter]
    [UnhandledExceptionFilter]
    [SessionFilter]
    [ExpirationPasswordFilter]
    [InternationalizationFilter]
    public class AccountController : Controller
    {
        #region Constants

        /// <summary>
        /// The avatar path.
        /// </summary>
        public const string AvatarPath = @"Content\images\avatars";

        /// <summary>
        /// The password updated on date.
        /// </summary>
        public const string PasswordUpdatedOnDate = "PasswordUpdatedOnDate";

        /// <summary>
        /// The user id.
        /// </summary>
        public const string UserId = "UserId";
        
        /// <summary>
        /// The return url.
        /// </summary>
        private const string ReturnUrl = "ReturnUrl";

        #endregion

        #region Fields

        /// <summary>
        /// The app module action service
        /// </summary>
        private readonly IAppModuleActionService appModuleActionService;

        /// <summary>
        /// The app module service.
        /// </summary>
        private readonly IAppModuleService appModuleService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly IRoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The user status service.
        /// </summary>
        private readonly IUserStatusService userStatusService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly ILanguageService languageService;

        /// <summary>
        /// The authentication service.
        /// </summary>
        private readonly IAuthentication authentication;

        /// <summary>
        /// The http helper 
        /// </summary>
        private readonly IHttpHelper httpHelper;

        /// <summary>
        /// The controller helper
        /// </summary>
        private readonly IControllerHelper controllerHelper;

        /// <summary>
        /// The app configuration
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Log Engine
        /// </summary>
        private readonly ILogEngine logEngine;

        /// <summary>
        /// The access token service.
        /// </summary>
        private readonly IAccessTokenService accessTokenService;

        /// <summary>
        /// The jwt builder.
        /// </summary>
        private readonly IJwtBuilder jwtBuilder;

        /// <summary>
        /// The company service.
        /// </summary>
        private readonly ICompanyService companyService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="userService">
        /// The user service.
        /// </param>
        /// <param name="userStatusService">
        /// The user status service.
        /// </param>
        /// <param name="roleService">
        /// The role service.
        /// </param>
        /// <param name="appModuleService">
        /// The app module service.
        /// </param>
        /// <param name="languageService">
        /// The language service.
        /// </param>
        /// <param name="authentication">
        /// </param>
        /// <param name="httpHelper">
        /// </param>
        /// <param name="controllerHelper">
        /// </param>
        /// <param name="appConfiguration">
        /// </param>
        /// <param name="appModuleActionService">
        /// </param>
        /// <param name="mapper">
        /// </param>
        /// <param name="logEngine">
        /// </param>
        /// <param name="accessTokenService">
        /// The access Token Service.
        /// </param>
        /// <param name="jwtBuilder">
        /// The jwt Builder.
        /// </param>
        /// <param name="companyService">
        /// The company Service.
        /// </param>
        public AccountController(
            IUserService userService, 
            IUserStatusService userStatusService, 
            IRoleService roleService,
            IAppModuleService appModuleService,
            ILanguageService languageService,
            IAuthentication authentication,
            IHttpHelper httpHelper,
            IControllerHelper controllerHelper,
            IAppConfiguration appConfiguration,
            IAppModuleActionService appModuleActionService,
            IMapper mapper,
            ILogEngine logEngine, 
            IAccessTokenService accessTokenService, 
            IJwtBuilder jwtBuilder, 
            ICompanyService companyService)
        {
            this.userService = userService;
            this.userStatusService = userStatusService;
            this.roleService = roleService;
            this.appModuleService = appModuleService;
            this.languageService = languageService;
            this.authentication = authentication;
            this.httpHelper = httpHelper;
            this.controllerHelper = controllerHelper;
            this.appConfiguration = appConfiguration;
            this.appModuleActionService = appModuleActionService;
            this.mapper = mapper;
            this.logEngine = logEngine;
            this.accessTokenService = accessTokenService;
            this.jwtBuilder = jwtBuilder;
            this.companyService = companyService;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The assign password.
        /// </summary>
        /// <param name="viewmodel">
        /// The user password assigment view model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult AssignPassword(UserPasswordAssigmentViewModel viewmodel)
        {
            var result = this.userService.AssignPassword(
                viewmodel.UserId, 
                viewmodel.NewPassword, 
                viewmodel.RepeatNewPassword);
            return this.JsonNet(result);
        }

        /// <summary>
        /// The change culture.
        /// </summary>
        /// <param name="lan">
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult ChangeCulture(string lan)
        {
            this.authentication.AddCurrentCultureInfo(new[] { lan });
            var user = this.authentication.GetCurrentUser();
            var url = this.GetUrlToRedirect();

            if (user == null)
            {
                return this.Redirect(url);
            }

            user.DefaultLanguage = lan;
            this.userService.Update(user);
            return this.Redirect(url);
        }

        /// <summary>
        /// The get url to redirect
        /// </summary>
        /// <returns></returns>
        public string GetUrlToRedirect()
        {
            // Get the Url to redirect
            var queryString = this.HttpContext.Request.QueryString.ToString();
            var match = Regex.Match(queryString, "(?<=lan=.*&url=)(?<Url>.*)");
            var urlraw = match.Groups["Url"].Value;
            return HttpUtility.UrlDecode(urlraw);
        }

        /// <summary>
        /// The change password expired.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ChangePasswordExpired()
        {
            var viewModel = new ChangePasswordExpiredViewModel
            {
                IsFirstLogIn = this.userService.IsFirstLogIn(this.authentication.GetCurrentUserId()),
                RowVersion = Convert.ToBase64String(this.authentication.GetCurrentUser().RowVersion)
            };

            return this.View(viewModel);
        }

        /// <summary>
        /// The cjange current password expired
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ActionResult ChangeCurrentPasswordExpired(ChangePasswordExpiredViewModel viewModel)
        {
            var result = this.userService.AssignPassword(
                this.authentication.GetCurrentUserId(),
                viewModel.NewPassword,
                viewModel.RepeatNewPassword);
            return this.JsonNet(result);
        }

        /// <summary>
        /// The change current password.
        /// </summary>
        /// <param name="userPassword">
        /// The user password.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult ChangeCurrentPassword(SettingsEditViewModel userPassword)
        {
            var result = this.userService.ChangePassword(
                this.authentication.GetCurrentUser(),
                userPassword.CurrentPassword, 
                userPassword.NewPassword, 
                userPassword.RepeatNewPassword,
                Convert.FromBase64String(userPassword.RowVersion));
            return this.JsonNet(result);
        }

        /// <summary>
        /// The change password.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Settings()
        {
            var userViewModel = this.mapper.Map<SettingsWebViewModel>(this.authentication.GetCurrentUser());
            var dir = Path.Combine(this.appConfiguration.AppEnvironment.ApplicationBasePath, AvatarPath);
            userViewModel.ProfileImage = this.Url.Content($@"~\{AvatarPath}\{userViewModel.ProfileImage}");
            userViewModel.Avatars = Directory.GetFiles(dir, "*.svg", SearchOption.AllDirectories).Select(
                x =>
                {
                    var filename = Path.GetFileName(x);
                    var path = this.Url.Content($@"~\{AvatarPath}\{Path.GetFileName(x)}");
                    return new SettingsAvatarViewModel()
                    {
                        Path = path,
                        FileName = filename
                    };
                }).ToArray();
            return this.View(userViewModel);
        }

        /// <summary>
        /// The change avatar.
        /// </summary>
        /// <param name="profileImageTmp">
        /// The profile Image Tmp.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult ChangeAvatar(string profileImageTmp)
        {
            var user = this.authentication.GetCurrentUser();
            user.ProfileImage = profileImageTmp;
            this.userService.Update(user);

            profileImageTmp = this.Url.Content($@"~\{AvatarPath}\{ profileImageTmp }");
            return this.JsonNet(new ServiceResult { Success = true, Object = profileImageTmp });
        }

        /// <summary>
        /// The change role.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ChangeRole(Guid roleId)
        {
            // Update the data base with the current role
            var user = this.authentication.GetCurrentUser();
            user.CurrentRoleId = roleId;
            this.userService.Update(user);

            // Update the client's store
            var userAuth = this.authentication.GetCurrentUserAuth();
            userAuth.CurrentRoleId = roleId;
            this.authentication.UpdateClaim(userAuth);

            var urlReferrer = this.ControllerContext.RequestContext.HttpContext.Request.UrlReferrer;
            if (urlReferrer != null)
            {
                return this.Redirect(urlReferrer.PathAndQuery);
            }

            return this.RedirectToRoute(this.controllerHelper.GetRouteValues<AppController>(c => c.PageNotFound()));
        }

        /// <summary>
        /// The change role.
        /// </summary>
        /// <param name="viewmodel">
        /// The viewmodel.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public async Task<ActionResult> RoleById(RoleViewModel viewmodel)
        {
            var role = await this.roleService.GetAsync(viewmodel.RoleId);
            var roleViewModel = this.mapper.Map<RoleEditViewModel>(role);
            return this.JsonNet(roleViewModel);
        }

        /// <summary>
        /// The get roles.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult GetRoles(SmartTableViewModel<RoleGridViewModel> param)
        {
            return this.JsonNet(param.GetResult(this.roleService));
        }

        /// <summary>
        /// The change menu visible.
        /// </summary>
        /// <param name="isMenuCollapsed">
        /// The is menu collapsed.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult ChangeMenuVisible(bool isMenuCollapsed)
        {
            var currentUsrer = this.authentication.GetCurrentUserAuth();
            currentUsrer.IsMenuCollapsed = isMenuCollapsed;
            this.authentication.UpdateClaim(currentUsrer);
            return this.JsonNet(true);
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="viewmodel"></param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public async Task<ActionResult> GetUser(UserEditViewModel viewmodel)
        {
            var usr = await this.userService.GetAsync(viewmodel.UserId);
            var usrViewModel = this.mapper.Map<UserEditViewModel>(usr);

            if (usrViewModel.CompanyId != null)
            {
                usrViewModel.CompanySelectize = new List<CompanyOptionViewModel> { new CompanyOptionViewModel { CompanyId = usrViewModel.CompanyId.Value, Name = usrViewModel.CompanyName } };
            }

            return this.JsonNet(usrViewModel);
        }

        /// <summary>
        /// The get users.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetUsers(SmartTableViewModel<UsersGridViewModel> param)
        {
            return this.JsonNet(param.GetResult(this.userService));
        }

        /// <summary>
        /// The language.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult Language()
        {
            var currentCultureInfo = this.authentication.GetCurrentCultureInfo() ?? this.languageService.GetDefaultCulture();
            var viewModel = new LanguageViewModel
                                {
                                    Selected = currentCultureInfo.Name,
                                    Cultures = this.languageService.GetCultures()
                                };
            return this.PartialView(viewModel);
        }

        /// <summary>
        /// The log in.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            if (this.HttpContext.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction(this.GetMemberName(c => c.Main()));
            }

            if (this.httpHelper.IsAjaxRequest())
            {
                this.Response.StatusCode = 401;
            }
            
            var returnUrl = this.Request.QueryString[ReturnUrl];
            var viewmodel = this.authentication.GetRememberMe();
            viewmodel.ReturnUrl = returnUrl ?? string.Empty;
            return this.View(viewmodel);
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult Index()
        {
            return this.RedirectToAction("LogIn", "Account");
        }

        /// <summary>
        /// The log in.
        /// </summary>
        /// <param name="account">
        /// The account.
        /// </param>
        /// <param name="returnUrl">
        /// The return url.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(AccountViewModel account, string returnUrl)
        {
            try
            {
                if (!this.HttpContext.User.Identity.IsAuthenticated)
                {
                    var result = this.userService.Validate(account.UserName, account.Password, account.OverrideActiveSession);
                    if (result.Success)
                    {
                        this.authentication.SignIn(account.UserName, account.Password, account.RememberMe);
                        var user = result.Object as User;
                        if (user != null)
                        {
                            this.authentication.AddCurrentCultureInfo(new[] { user.DefaultLanguage });
                        }

                        if (!string.IsNullOrEmpty(returnUrl))
                        {
                            return this.Redirect(returnUrl);
                        }

                        return this.RedirectToAction(this.GetMemberName(c => c.Main()));
                    }

                    if (this.appConfiguration.SessionParameters.SingleSessionActivated && this.appConfiguration.SessionParameters.SingleSessionShowAlert)
                    {
                        var resultObject = result.Object as ExpandoObject;
                        if (resultObject != null)
                        {
                            dynamic obj = resultObject;
                            account.HasActiveSession = obj.HasActiveSession;
                        }
                    }

                    account.Message = result.Message;
                }
                else
                {
                    return this.RedirectToAction(this.GetMemberName(c => c.Main()));
                }
            }
            catch (Exception e)
            {
                this.logEngine.WriteError(e);
                account.Message = Global.ViewAccountLogInError;
            }

            return this.View(account);
        }

        /// <summary>
        /// The log off.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult LogOff()
        {
            this.userService.SignOut(this.authentication.GetCurrentUserName()); // Change the HasActiveSession property
            this.SignOut(); // Delete the authentication ticket and sign out.
            return this.RedirectToRoute(this.controllerHelper.GetRouteValues<AccountController>(c => this.LogIn()));
        }

        /// <summary>
        /// The sign out.
        /// </summary>
        public void SignOut()
        {
            var httpSessionStateBase = this.ControllerContext.HttpContext.Session;
            httpSessionStateBase?.Clear();

            var userName = this.authentication.GetCurrentUserName();
            this.ControllerContext.HttpContext.Cache.Remove(userName);

            // Delete the user details from cache.
            this.authentication.SignOut(); // Delete the authentication ticket and sign out.
        }

        /// <summary>
        /// The main.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Main()
        {
            return this.View();
        }

        /// <summary>
        /// The menu.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult Menu()
        {
            var user = this.authentication.GetCurrentUser();
            var result = this.appModuleService.GetWebModulesByRoleId(user.CurrentRoleId).OrderBy(x => x.NameTranslated).ToList();
            return this.PartialView(result);
        }

        /// <summary>
        /// The password assigment.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult PasswordAssigment()
        {
            var hexEncryptedUserId = this.Request.QueryString[UserId];
            var hexEncryptedPasswordUpdatedOnDate = this.Request.QueryString[PasswordUpdatedOnDate];

            var result = this.userService.PasswordAssigment(hexEncryptedUserId, hexEncryptedPasswordUpdatedOnDate);
            if (!result)
            {
                return this.RedirectToAction(this.GetMemberName(c => c.Main()));
            }

            var userPasswordAssigmentViewModel = new UserPasswordAssigmentViewModel
                                                     {
                                                         UserId = Guid.Parse(this.userService.HexToStringAndDecrypt(hexEncryptedUserId))
                                                     };

            return this.View(userPasswordAssigmentViewModel);
        }

        /// <summary>
        /// The request password.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult RequestPassword()
        {
            return this.View();
        }

        /// <summary>
        /// The request password.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [CaptchaValidation("CaptchaCode", "PasswordCaptcha")]
        [AllowAnonymous]
        public ActionResult RequestPassword(RequestPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var userId = this.userService.GetQuery()
                                .Where(x => x.UserName == model.UserName)
                                .Select(x => x.UserId)
                                .FirstOrDefault();
                if (userId == Guid.Empty)
                {
                    return this.View(model);
                }

                var appPath = this.httpHelper.GetAppPath();
                var result = this.userService.SendEmailLink(userId, appPath);
                model.IsValid = result.Success;
                model.Message = result.Message;
                MvcCaptcha.ResetCaptcha("PasswordCaptcha");
            }
            else
            {
                model.Message = Global.ViewAccountRequestPasswordCaptchaNotValid;
            }

            return this.View(model);
        }

        /// <summary>
        /// The roles.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Roles()
        {
            var module = UnityConfig.Instance.Resolve<ModuleViewModel>();
            return this.View(new RoleWebPageViewModel { Modules = module.GetPrivateModules() });
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Save(UserEditViewModel viewmodel)
        {
            var userToSave = this.userService.MapToSave(viewmodel);
            var result = this.userService.Save(userToSave);
            return this.JsonNet(result);
        }

        /// <summary>
        /// The save role.
        /// </summary>
        /// <param name="viewmodel"></param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SaveRole(RoleEditViewModel viewmodel)
        {
            var roleToSave = this.roleService.MapToSave(viewmodel);
            this.appModuleActionService.MapToSaveList(viewmodel.AppModuleActions, roleToSave.AppModuleActions);
            var result = this.roleService.Save(roleToSave);
            return this.JsonNet(result);
        }

        /// <summary>
        /// The send password by id.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SendPasswordById(UserEditViewModel viewModel)
        {
            var appPath = this.httpHelper.GetAppPath();
            return this.JsonNet(this.userService.SendEmailLink(viewModel.UserId, appPath));
        }

        /// <summary>
        /// The users.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Users()
        {
            var viewmodel = new UserPageViewModel()
            {
                Catalogs =
                {
                    CurrentUserStatus = this.mapper.Map<IEnumerable<UserStatusViewModel>>(this.userStatusService.GetAllFromCache()),
                    CurrentRole = this.mapper.Map<IEnumerable<RoleViewModel>>(this.roleService.GetAllFromCache()),
                    CurrentLanguage = this.languageService.GetCultures().Select(x => new { x.DisplayName, x.Name }).ToList()
                },
                Messages =
                {
                    LblMsgTableChngPass = Global.ViewAccountUsersMsgTableSendPass,
                    lblViewAccountUsersCompany = Global.ViewAccountUsersCompanyName
                }
            };
            return this.View(viewmodel);
        }

        public ActionResult GetUserTokens(SmartTableViewModel<UserTokenGridViewModel> param, Guid userId)
        {
            return this.JsonNet(param.GetResult(this.accessTokenService, x => x.UserId == userId));
        }

        public ActionResult GenerateToken([Bind(Include = "UserId")] UserTokenGridViewModel viewModel)
        {
            var user = this.userService.Get(viewModel.UserId);
            var token = this.jwtBuilder.GenerateTokenAndSave(user.UserName);
            var result = this.userService.SendEmailWithNewToken(user.UserName, this.httpHelper.GetAppPath(), token);
            return this.JsonNet(result);
        }

        public ActionResult RevokeToken([Bind(Include = "AccessTokenId")] UserTokenGridViewModel viewModel)
        {
            var accesssTokenToSave = this.accessTokenService.Get(viewModel.AccessTokenId);
            accesssTokenToSave.IsRevoked = true;
            var result = this.accessTokenService.Update(accesssTokenToSave);
            return this.JsonNet(result);
        }

        public ActionResult GetCompaniesByFilter(string term)
        {
            var companies = this.companyService.GetQuery().Where(x => x.Name.Contains(term)).ToList();
            var companiesViewModel = this.mapper.Map<IEnumerable<CompanyOptionViewModel>>(companies);
            return this.JsonNet(companiesViewModel);
        }

        #endregion

    }
}