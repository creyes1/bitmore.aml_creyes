// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppController.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Controllers
{
    #region

    using System.Linq;
    using System.Web.Mvc;

    using AutoMapper;

    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure;
    using Bitmore.Aml.Web.Infrastructure.Cache;
    using Bitmore.Aml.Web.Infrastructure.Filters;
    using Bitmore.Aml.Web.ViewModel.Account;
    using Bitmore.Aml.Web.ViewModel.App;
    using Bitmore.Aml.Web.ViewModel.SmartTable;

    using CacheManager.Core;

    using Newtonsoft.Json;

    #endregion

    /// <summary>
    /// The app controller.
    /// </summary>
    [ModuleAuthorizeFilter]
    [UnhandledExceptionFilter]
    [SessionFilter]
    [ExpirationPasswordFilter]
    [InternationalizationFilter]
    public class AppController : Controller
    {
        /// <summary>
        /// The app log service.
        /// </summary>
        private readonly IAppLogService appLogService;

        /// <summary>
        /// The controller helper.
        /// </summary>
        private readonly IControllerHelper controllerHelper;

        /// <summary>
        /// The global attribute service.
        /// </summary>
        private readonly IGlobalAttributeService globalAttributeService;

        /// <summary>
        /// Log Engine
        /// </summary>
        private readonly ILogEngine logEngine;

        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The export to xlsx service.
        /// </summary>
        private readonly IExportToXlsxService exportToXlsxService;

        /// <summary>
        /// The authentication.
        /// </summary>
        private readonly IAuthentication authentication;

        /// <summary>
        /// The cache factory manager.
        /// </summary>
        private readonly ICacheFactoryManager cacheFactoryManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppController"/> class.
        /// </summary>
        /// <param name="appLogService">
        /// The app log service.
        /// </param>
        /// <param name="globalAttributeService">
        /// </param>
        /// <param name="controllerHelper">
        /// </param>
        /// <param name="mapper"></param>
        /// <param name="logEngine"></param>
        /// <param name="exportToXlsxService"></param>
        /// <param name="authentication"></param>
        /// <param name="cacheFactoryManager"></param>
        public AppController(
            IAppLogService appLogService, 
            IGlobalAttributeService globalAttributeService, 
            IControllerHelper controllerHelper,
            IMapper mapper,
            ILogEngine logEngine,
            IExportToXlsxService exportToXlsxService,
            IAuthentication authentication,
            ICacheFactoryManager cacheFactoryManager)
        {
            this.appLogService = appLogService;
            this.globalAttributeService = globalAttributeService;
            this.controllerHelper = controllerHelper;
            this.mapper = mapper;
            this.logEngine = logEngine;
            this.exportToXlsxService = exportToXlsxService;
            this.authentication = authentication;
            this.cacheFactoryManager = cacheFactoryManager;
        }

        /// <summary>
        /// The clear app log errors.
        /// </summary>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult ClearAppLogErrors()
        {
            var result = this.appLogService.ClearAppLogErrors();
            return this.Json(result);
        }

        /// <summary>
        /// The get global attribute.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult GetGlobalAttribute(string name)
        {
            var result = this.globalAttributeService.Get(g => g.Name == name);
            return this.JsonNet(this.mapper.Map<GlobalAttributeEditViewModel>(result));
        }

        /// <summary>
        /// The get global attributes.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult GetGlobalAttributes(SmartTableViewModel<GlobalAttributesGridViewModel> param)
        {
            return this.JsonNet(param.GetResult(this.globalAttributeService, x => x.IsEditable));
        }

        /// <summary>
        /// The get log.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult GetLog(SmartTableViewModel<AppLogGridViewModel> param)
        {
            return this.JsonNet(param.GetResult(this.appLogService));
        }

        /// <summary>
        /// The get log detail.
        /// </summary>
        /// <param name="viewmodel">
        /// The viewmodel.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult GetLogDetail(AppLog viewmodel)
        {
            var id = viewmodel.AppLogId;
            var result =
                this.appLogService.GetQuery()
                    .Where(ap => ap.AppLogId == id)
                    .Select(ap => new { ap.Data })
                    .FirstOrDefault();
            return this.JsonNet(result);
        }

        /// <summary>
        /// The global attributes.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GlobalAttributes()
        {
            var viewmodel = new GlobalAttributeViewModel();
            return this.View(viewmodel);
        }

        /// <summary>
        /// The purge cache.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult PurgeCache()
        {
            // Purge queries
            this.appLogService.PurgeCache();

            // Purge other caches
            this.cacheFactoryManager.ClearAll();
            return this.JsonNet(true);
        }

        /// <summary>
        /// The log.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Log()
        {
            var viewModel = new LogViewModel
                {
                    CurrentAppTypes = this.controllerHelper.CreateFilterToGrid(this.logEngine.AppTypes),
                    CurrentLoggerTypes = this.controllerHelper.CreateFilterToGrid(this.logEngine.MessageTypes),
                    LogMsgConfirm = Global.ViewAppLogMsgConfirm,
                    LogMsgSuccess = Global.ViewAppLogLogMsgSuccess,
                };
            return this.View(viewModel);
        }

        /// <summary>
        /// The page not found.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [AllowAnonymous]
        public ActionResult PageNotFound()
        {
            return this.View();
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="attributeEdit">
        /// The attribute.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public JsonResult SaveGlobalAttribute(GlobalAttributeEditViewModel attributeEdit)
        {
            var attr = this.globalAttributeService.MapToSave(attributeEdit);
            var result = this.globalAttributeService.Save(
                attr, 
                new ServiceResult { Message = Global.ViewAppGlobalAttributesSaved, Success = true });
            return this.JsonNet(result);
        }

        /// <summary>
        /// The log client events.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        [AllowAnonymous]
        public JsonResult LogClientEvents(LogClientViewModel viewModel)
        {
            var data = JsonConvert.SerializeObject(viewModel, Formatting.Indented);
            if (viewModel.Type != "Error")
            {
                return this.JsonNet(true);
            }

            var username = this.authentication.GetCurrentUserName();
            this.logEngine.WriteError($"JavaScript Error - { viewModel.Msg }", data, username);
            return this.JsonNet(true);
        }

        /// <summary>
        /// The get file.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// The <see cref="FileContentResult"/>.
        /// </returns>
        public FileContentResult GetFile(string filename)
        {
            return this.controllerHelper.GetTemporaryFile(filename, Global.ViewSharedGridExportToXlsxDefaultFileName);
        }

        /// <summary>
        /// The export to xlsx.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ExportToXlsx(string name)
        {
            string fileName;
            if (this.Request.InputStream.Length > 0)
            {
                var result = SmartTableBuilderViewModel.GetResult(name, this.Request);
                fileName = this.exportToXlsxService.ExportToXlsx(result.Data);
            }
            else
            {
                var list = SmartTableBuilderViewModel.GetResult(name);
                fileName = this.exportToXlsxService.ExportToXlsx(list);
            }
            
            return this.JsonNet(fileName);
        }
    }
}