// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   Specifies the Unity configuration for the main container.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Config
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure;
    using Bitmore.Aml.Web.Infrastructure.Auth.Jwt;
    using Bitmore.Aml.Web.Infrastructure.Cache;
    using Bitmore.Aml.Web.Infrastructure.Hub;

    using CacheManager.Core;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Practices.Unity;

    using Owin;
    using DataAccess.Infrastructure.Log;

    #endregion

    /// <summary>
    /// The factory io c.
    /// </summary>
    public class UnityConfig
    {
        #region Static Fields

        /// <summary>
        ///     The instance.
        /// </summary>
        private static UnityConfig instance;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="UnityConfig" /> class from being created.
        /// </summary>
        private UnityConfig()
        {
            this.Container = new UnityContainer();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static UnityConfig Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                instance = new UnityConfig();
                instance.InitContainer();
                return instance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the container.
        /// </summary>
        public IUnityContainer Container { get; }

        /// <summary>
        /// The create child container.
        /// </summary>
        /// <returns>
        /// The <see cref="IUnityContainer"/>.
        /// </returns>
        public IUnityContainer CreateChildContainer()
        {
            return this.Container.CreateChildContainer();
        }

        /// <summary>
        /// The get container.
        /// </summary>
        /// <returns>
        /// The <see cref="IUnityContainer"/>.
        /// </returns>
        public IUnityContainer GetConfiguredContainer()
        {
            return this.Container;
        }

        /// <summary>
        ///     The resolve.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        ///     The <see cref="T" />.
        /// </returns>
        public T Resolve<T>()
        {
            return this.Container.Resolve<T>();
        }

        /// <summary>
        /// The resolve.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Resolve(Type type)
        {
            return this.Container.Resolve(type);
        }

        /// <summary>
        /// The resolve.
        /// </summary>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T Resolve<T>(params KeyValuePair<string, object>[] parameters)
        {
            var args = new List<ResolverOverride>(parameters.Count());
            parameters.ToList().ForEach(p => args.Add(new ParameterOverride(p.Key, p.Value)));
            return this.Container.Resolve<T>(args.ToArray());
        }

        /// <summary>
        /// The resolve.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        public T Resolve<T>(object[] values)
        {
            var param = GetConstructorParameters(typeof(T));
            var parameterInfos = param as ParameterInfo[] ?? param.ToArray();
            if (parameterInfos.Count() != values.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            var result = parameterInfos.Select((p, index) => new KeyValuePair<string, object>(p.Name, values[index])).ToArray();
            return this.Resolve<T>(result);
        }

        /// <summary>
        /// The get constructor parameters.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private static IEnumerable<ParameterInfo> GetConstructorParameters(Type type)
        {
            var ctors = type.GetConstructors();

            // Assuming class A has only one constructor
            var ctor = ctors[0];
            return ctor.GetParameters();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The init container.
        /// </summary>
        private void InitContainer()
        {
            this.Container.RegisterType<IUnitOfWork, UnitOfWork>(new PerRequestLifetimeManager());
            this.Container.RegisterType<IDatabaseLog, DatabaseLog>();
            this.Container.RegisterType<IDatabaseFactory, DatabaseFactory>(new PerRequestLifetimeManager());

            this.Container.RegisterType<IMessageHub, MessageHub>(new TransientLifetimeManager());

            this.Container.RegisterType<IAuthentication, Authentication>(new PerRequestLifetimeManager());
            this.Container.RegisterType<IHttpHelper, HttpHelper>();
            this.Container.RegisterType<IHtmlHelper, HtmlHelper>();
            this.Container.RegisterType<IControllerHelper, ControllerHelper>();
            this.Container.RegisterType<ILogEngine, Log>(new ContainerControlledLifetimeManager());

            this.Container.RegisterType<IUserService, UserService>();
            this.Container.RegisterType<IUserRepository, UserRepository>();

            this.Container.RegisterType<IUserStatusService, UserStatusService>();
            this.Container.RegisterType<IUserStatusRepository, UserStatusRepositoy>();

            this.Container.RegisterType<IRoleService, RoleService>();
            this.Container.RegisterType<IRoleRepository, RoleRepository>();

            this.Container.RegisterType<IAppModuleService, AppModuleService>();
            this.Container.RegisterType<IAppModuleRepository, AppModuleRepository>();

            this.Container.RegisterType<IAppModuleTypeService, AppModuleTypeService>();
            this.Container.RegisterType<IAppModuleTypeRepository, AppModuleTypeRepository>();

            this.Container.RegisterType<IAppModuleActionService, AppModuleActionService>();
            this.Container.RegisterType<IAppModuleActionRepository, AppModuleActionRepository>();

            this.Container.RegisterType<IAppLogService, AppLogService>();
            this.Container.RegisterType<IAppLogRepository, AppLogRepository>();

            this.Container.RegisterType<IGlobalAttributeService, GlobalAttributeService>();
            this.Container.RegisterType<IGlobalAttributeRepository, GlobalAttributeRepository>();

            this.Container.RegisterType<ILanguageService, LanguageService>();

            this.Container.RegisterType<IEmailService, EmailService>();

            this.Container.RegisterType<ITaskManagerService, TaskManagerService>();
            this.Container.RegisterType<ITaskManagerRepository, TaskManagerRepository>();

            this.Container.RegisterType<IExportToXlsxService, ExportToXlsxService>();

            this.Container.RegisterInstance<ICacheFactoryManager>(new CacheFactoryManager());

            this.Container.RegisterType<IOfacService, OfacService>();

            var cacheConfig = CacheManager.Core.ConfigurationBuilder.BuildConfiguration(
                "default",
                settings =>
                    {
                        settings.WithSystemRuntimeCacheHandle()
                            .WithExpiration(ExpirationMode.Sliding, TimeSpan.FromMinutes(15));
                    });

            this.Container.RegisterType(
                typeof(ICacheManager<>),
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(
                    (c, t, n) =>
                        {
                            var cacheFactoryManager = this.Container.Resolve<ICacheFactoryManager>();
                            var cachemanager = CacheFactory.FromConfiguration(t.GetGenericArguments()[0], cacheConfig);
                            cacheFactoryManager.Add(cachemanager);
                            return cachemanager;
                        }));
            this.Container.RegisterType<IJwtBuilder, JwtBuilder>();

            this.Container.RegisterType<IAccessTokenRepository, AccessTokenRepository>();
            this.Container.RegisterType<IAccessTokenService, AccessTokenService>();

            this.Container.RegisterType<ICompanyRepository, CompanyRepository>();
            this.Container.RegisterType<ICompanyService, CompanyService>();
        }

        /// <summary>
        /// The register instance.
        /// </summary>
        /// <param name="intance">
        /// The intance.
        /// </param>
        /// <typeparam name="TInterface"></typeparam>
        public void RegisterInstance<TInterface>(TInterface intance)
        {
            this.Container.RegisterInstance(intance);
        }

        #endregion
    }

    /// <summary>
    /// The unity config extensions.
    /// </summary>
    public static class UnityConfigExtensions
    {
        /// <summary>
        /// The add dependency injection confg.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        /// <param name="configurationRoot">
        /// The configuration root.
        /// </param>
        public static void ConfigureDependencyInjectionConfg(this IAppBuilder app, IConfigurationRoot configurationRoot)
        {
            var container = UnityConfig.Instance.Container;
            container.RegisterInstance(typeof(IConfigurationRoot), configurationRoot);
            container.RegisterInstance(typeof(IConfiguration), configurationRoot);
            container.RegisterType<IAppConfiguration>(
                new PerRequestLifetimeManager(),
                GetAppConfigurationFactory(configurationRoot));
        }

        /// <summary>
        /// The get app configuration factory.
        /// </summary>
        /// <param name="configurationRoot">
        /// The configuration root.
        /// </param>
        /// <returns>
        /// The <see cref="InjectionFactory"/>.
        /// </returns>
        public static InjectionFactory GetAppConfigurationFactory(IConfigurationRoot configurationRoot)
        {
            return 
                new InjectionFactory(
                c =>
                {
                    var appConfig = new AppConfiguration();
                    configurationRoot.GetSection(nameof(AppConfiguration)).Bind(appConfig);
                    return appConfig;
                });
        }

        /// <summary>
        /// The get app configuration.
        /// </summary>
        /// <param name="appBuilder"></param>
        /// <returns>
        /// The <see cref="AppConfiguration"/>.
        /// </returns>
        public static IAppConfiguration GetAppConfiguration(this IAppBuilder appBuilder)
        {
            return UnityConfig.Instance.Container.Resolve<IAppConfiguration>();
        }
    }
}