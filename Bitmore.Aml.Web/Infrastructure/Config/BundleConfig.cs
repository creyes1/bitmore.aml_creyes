// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BundleConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The bundle config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace Bitmore.Aml.Web.Infrastructure.Config
{
    #region

    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Optimization;

    using Owin;

    #endregion

    /// <summary>
    /// The bundle config.
    /// </summary>
    public static class BundleConfig
    {
        #region Constants

        /// <summary>
        /// The path content.
        /// </summary>
        private const string PathContent = "~/Content/{0}";

        /// <summary>
        /// The path assets.
        /// </summary>
        private const string PathAssets = "~/Assets/{0}";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The configure bundles.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public static void ConfigureBundles(this IAppBuilder app)
        {
            RegisterBundles(BundleTable.Bundles);
            RegisterDynamicBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// The register bundles.
        /// </summary>
        /// <param name="bundles">
        /// The bundles.
        /// </param>
        private static void RegisterBundles(BundleCollection bundles)
        {
            RegisterVendorBundles(bundles);

            // Site styles
            bundles.Add(
                new StyleBundle("~/Assets/site")
                    .Include("css/dist/site.css".Build(), new CssRewriteUrlTransform()));
            
            // Bitmore Common
            bundles.Add(
                new ScriptBundle("~/Assets/common/js")
                    .Include("dist/bitmore.aml.common.js".Build())
                    .Include("dist/bitmore.aml.modalService.js".Build()));
            
            // Main
            bundles.Add(
                new ScriptBundle("~/Assets/app/main/js")
                    .Include("dist/bitmore.aml.main.js".Build()));
            
            // RequestPassword styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/requestPassword/js")
                    .Include("dist/bitmore.requestPassword.js".Build()));

            // Settings styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/settings/js")
                    .Include("dist/bitmore.aml.controllers.js".Build())
                    .Include("dist/bitmore.aml.settings.js".Build()));

            // PasswordAssigment styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/passwordAssigment/js")
                    .Include("dist/bitmore.aml.passwordAssigment.js".Build()));

            // Log styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/log/js")
                    .Include("dist/bitmore.aml.controllers.js".Build())
                    .Include("dist/bitmore.aml.log.js".Build()));

            // Users styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/users/js")
                    .Include("dist/bitmore.aml.controllers.js".Build())
                    .Include("dist/users/bitmore.aml.users.company.js".Build())
                    .Include("dist/users/bitmore.aml.users.tokens.js".Build())
                    .Include("dist/users/bitmore.aml.users.js".Build()));
            
            // Roles styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/roles/js")
                    .Include("dist/bitmore.aml.controllers.js".Build())
                    .Include("dist/bitmore.aml.roles.js".Build()));

            // Global Attributes styles and scripts
            bundles.Add(
                new ScriptBundle("~/Assets/app/globalAttributes/js")
                    .Include("dist/bitmore.aml.controllers.js".Build())
                    .Include("dist/bitmore.aml.globalAttributes.js".Build()));

            // ChangePasswordExpired
            bundles.Add(
                new ScriptBundle("~/Assets/app/changePasswordExpired/js")
                    .Include("dist/bitmore.aml.controllers.js".Build())
                    .Include("dist/bitmore.aml.changePasswordExpired.js".Build()));
            
            // SignalR
            bundles.Add(
                new ScriptBundle("~/Assets/signalr-messages/js")
                    .Include("dist/bitmore.aml.signalr-messages.js".Build()));

            bundles.Add(
                new StyleBundle("~/Assets/login/css")
                    .Include("css/dist/site.login.css".Build(), new CssRewriteUrlTransform()));
        }

        /// <summary>
        /// The register vendor bundles.
        /// </summary>
        /// <param name="bundles">
        /// The bundles.
        /// </param>
        public static void RegisterVendorBundles(BundleCollection bundles)
        {
            // Bootstrap styles and script
            bundles.Add(
                new StyleBundle("~/Assets/bootstrap")
                    .Include("bootstrap/css/bootstrap.css".Build(), new RewritePathTransform(@"/Content/bootstrap"))
                    .Include("bootstrap/css/bootstrap-theme.css".Build()));
            bundles.Add(
                new ScriptBundle("~/Assets/bootstrap/js")
                    .Include("bootstrap/js/bootstrap.js".Build()));

            // Fontello styles
            bundles.Add(
                new StyleBundle("~/Assets/fontello")
                    .Include("fontello/css/fontello.css".Build(), new CssRewriteUrlTransform()));

            // Fontawesome styles
            bundles.Add(
                new StyleBundle("~/Assets/fontawesome")
                    .Include("fontawesome/css/font-awesome.css".Build(), new CssRewriteUrlTransform()));

            // jQuery
            bundles.Add(
                new ScriptBundle("~/Assets/jquery/js")
                    .Include("jquery/jquery-1.11.1.min.js".Build()));
            
            // bootstrap-datetimepicker
            bundles.Add(
                new ScriptBundle("~/Assets/bootstrap-datetimepicker/js")
                    .Include("moment/moment.min.js".Build())
                    .Include("bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js".Build())
                    .Include("bootstrap-datetimepicker/js/angular-bootstrap-datetimepicker-directive.js".Build())
                    .Include("bootstrap-datetimepicker/js/angular-bootstrap-datetimepicker-smart-table-directive.js".Build())
                    .Include("bootstrap-datetimepicker/js/angular-bootstrap-datetimepicker-smart-table-range-directive.js".Build()));
            bundles.Add(
                new StyleBundle("~/Assets/bootstrap-datetimepicker/css")
                    .Include("bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css".Build()));

            // Angular-toastr
            bundles.Add(
                new ScriptBundle("~/Assets/angular-toastr/js")
                    .Include("angular-toastr/angular-toastr.tpls.min.js".Build()));
            bundles.Add(
                new StyleBundle("~/Assets/angular-toastr/css")
                    .Include("angular-toastr/angular-toastr.css".Build()));

            // JsTree
            bundles.Add(
                new ScriptBundle("~/Assets/jstree/js")
                    .Include("jstree/jstree.min.js".Build())
                    .Include("jstree/ngJsTree.min.js".Build()));
            bundles.Add(
                new StyleBundle("~/Assets/jstree/css")
                    .Include("jstree/themes/proton/style.css".Build(), new CssRewriteUrlTransform()));

            // angular-checklist-model
            bundles.Add(
                new ScriptBundle("~/Assets/angular-checklist-model/js")
                    .Include("angular-checklist-model/checklist-model.js".Build()));

            // angular-auto-validate
            bundles.Add(
                new ScriptBundle("~/Assets/angular-auto-validate/js")
                    .Include("angular-auto-validate/jcs-auto-validate.min.js".Build()));

            // Angular
            bundles.Add(
                new ScriptBundle("~/Assets/angular/js")
                    .Include("es6-shim/es6-shim.min.js".Build())
                    .Include("angular/angular.min.js".Build())
                    .Include("angular/angular-sanitize.min.js".Build())
                    .Include("angular/angular-animate.min.js".Build())
                    .Include("angular/angular-resource.min.js".Build())
                    .Include("angular-ui-bootstrap/ui-bootstrap-tpls-2.1.4.min.js".Build()));

            // Angular Smart-Table
            bundles.Add(
                new ScriptBundle("~/Assets/angular-smart-table/js")
                    .Include("angular-smart-table/smart-table.js".Build())
                    .Include("angular-smart-table/smart-table.directives.js".Build())
                    .Include("dist/bitmore.aml.smartGridService.js".Build()));

            bundles.Add(
                new StyleBundle("~/Assets/angular-smart-table/css")
                    .Include("angular-smart-table/smart-table.css".Build()));
            
            // Awesome-bootstrap-checkbox
            bundles.Add(
                new StyleBundle("~/Assets/awesome-bootstrap-checkbox/css")
                    .Include("awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css".Build()));

            // bootstrap-iconpicker
            bundles.Add(
                new ScriptBundle("~/Assets/bootstrap-iconpicker/js")
                    .Include("bootstrap-iconpicker/js/iconset/iconset-fontawesome-4.2.0.min.js".Build())
                    .Include("bootstrap-iconpicker/js/iconset/iconset-glyphicon.min.js".Build())
                    .Include("bootstrap-iconpicker/js/bootstrap-iconpicker.js".Build())
                    .Include("bootstrap-iconpicker/js/bootstrap-iconpicker-angularjs-directive.js".Build()));
            bundles.Add(
                new StyleBundle("~/Assets/bootstrap-iconpicker/css")
                    .Include("bootstrap-iconpicker/css/bootstrap-iconpicker.min.css".Build())
                    .Include("bootstrap-iconpicker/css/bootstrap-iconpicker-fix.css".Build()));

            // bootstrap-modal-sidebar
            bundles.Add(
                new StyleBundle("~/Assets/bootstrap-modal-sidebar/css")
                    .Include("bootstrap-modal-sidebar/bootstrap-modal-sidebar.css".Build()));

            // Simple-sidenav
            bundles.Add(
                new ScriptBundle("~/Assets/simple-sidenav/js")
                    .Include("simple-sidenav/simple-sidenav.js".Build()));
            bundles.Add(
                new StyleBundle("~/Assets/simple-sidenav/css")
                    .Include("simple-sidenav/simple-sidenav-style.css".Build()));

            // Slimscroll
            bundles.Add(
                new ScriptBundle("~/Assets/slimscroll/js")
                    .Include("jquery.slimscroll/jquery.slimscroll.min.js".Build()));

            // selectize
            bundles.Add(
                new ScriptBundle("~/Assets/selectize/js/standalone")
                    .Include("selectize/js/standalone/selectize.min.js".Build())
                    .Include("selectize/js/standalone/angular-selectize.js".Build()));
            bundles.Add(
                new StyleBundle("~/Assets/selectize/css")
                    .Include("selectize/css/selectize.default.css".Build()));
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string Build(this string path)
        {
            return string.Format(PathContent, path);
        }

        /// <summary>
        /// The register bundles.
        /// </summary>
        /// <param name="bundles">
        /// The bundles.
        /// </param>
        public static void RegisterDynamicBundles(BundleCollection bundles)
        {
            RegisterDynamicBundles(
                bundles,
                "jquery.globalization",
                string.Format(PathAssets, "jquery.globalization/{0}"),
                @"\w{2}(?=-)",
                new[] { "~/Content/jquery.globalization/globalize.js" });
        }

        #endregion

        #region Methods

        /// <summary>
        /// The register dynamic bundles.
        /// </summary>
        /// <param name="bundles">
        /// The bundles.
        /// </param>
        /// <param name="bundlePath">
        /// The bundle path.
        /// </param>
        /// <param name="assertPathFormat">
        /// The assert Path Format.
        /// </param>
        /// <param name="regexExtract">
        /// The regex extract.
        /// </param>
        /// <param name="extraFiles">
        /// The extra Files.
        /// </param>
        private static void RegisterDynamicBundles(
            BundleCollection bundles,
            string bundlePath,
            string assertPathFormat,
            string regexExtract,
            IEnumerable<string> extraFiles = null)
        {
            var virtualPath = string.Format(PathContent, bundlePath);
            var path = System.Web.Hosting.HostingEnvironment.MapPath(virtualPath);
            var files = Directory.GetFiles(path);
            files.ToList().ForEach(
                f =>
                {
                    var fileName = Path.GetFileName(f);
                    if (fileName == null)
                    {
                        return;
                    }

                    var lan = Regex.Match(fileName, regexExtract);
                    var resource = string.Format(assertPathFormat, lan);
                    if (bundles.Any(b => b.Path == resource))
                    {
                        return;
                    }

                    var bundle = new ScriptBundle(resource);
                    if (extraFiles != null)
                    {
                        var enumerable = extraFiles as string[] ?? extraFiles.ToArray();
                        if (enumerable.Any())
                        {
                            enumerable.ToList().ForEach(extf => bundle.Include(extf));
                        }
                    }

                    bundle.Include(string.Concat(virtualPath, "/", fileName));
                    bundles.Add(bundle);
                });
        }

        #endregion
    }
}