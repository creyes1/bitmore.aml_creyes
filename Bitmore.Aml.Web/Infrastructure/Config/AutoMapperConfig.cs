// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoMapperConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Config
{
    #region

    using AutoMapper;

    using Bitmore.Aml.Domain.DataObject;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Web.ViewModel.Account;
    using Bitmore.Aml.Web.ViewModel.App;

    using Owin;

    #endregion

    /// <summary>
    /// The default profile.
    /// </summary>
    public class DefaultProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultProfile"/> class. 
        /// The configure.
        /// </summary>
        public DefaultProfile()
        {
            this.CreateMap<AppModule, AppModuleMenu>();
            this.CreateMap<AppModule, TreeNode>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AppModuleId))
                .ForMember(
                    dest => dest.Parent, 
                    opt => opt.MapFrom(src => src.ParentAppModuleId == null ? "#" : src.ParentAppModuleId.ToString()))
                .ForMember(dest => dest.Icon, opt => opt.MapFrom(src => src.CssOrDesktopIcon))
                .AfterMap(
                    (src, dest) =>
                    {
                        if (dest == null)
                        {
                            return;
                        }

                        dest.Text = Global.ResourceManager.GetString($"ViewSharedMenu{src.Name}");
                    });
            this.CreateMap<AppModuleAction, TreeNode>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AppModuleActionId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => Global.ResourceManager.GetString($"ViewAccountAppModuleAction{src.Name}")))
                .ForMember(dest => dest.Parent, opt => opt.MapFrom(src => src.AppModuleId))
                .AfterMap(
                    (src, dest) =>
                    {
                        if (dest == null)
                        {
                            return;
                        }

                        dest.Text = Global.ResourceManager.GetString($"ViewAccountAppModuleAction{src.Name}");
                    });
            this.CreateMap<AppModuleAction, AppModuleActionRoleViewModel>();
            this.CreateMap<AppModuleActionRoleViewModel, AppModuleAction>()
                .ForMember(m => m.Name, opt => opt.Ignore());
            this.CreateMap<AppModuleType, AppModuleTypeViewModel>();
            this.CreateMap<AppLog, AppLogGridViewModel>();
            this.CreateMap<User, UsersGridViewModel>();
            this.CreateMap<User, UserEditViewModel>()
                .ForMember(at => at.CompanyName, opt => opt.MapFrom(src => src.Company != null ? src.Company.Name : null));
            this.CreateMap<UserEditViewModel, User>();
            this.CreateMap<User, SettingsWebViewModel>();
            this.CreateMap<SettingsWebViewModel, SettingsEditViewModel>();
            this.CreateMap<UserStatus, UserStatusViewModel>();
            this.CreateMap<RoleViewModel, Role>().ReverseMap();
            this.CreateMap<Role, RoleEditViewModel>();
            this.CreateMap<Role, RoleGridViewModel>();
            this.CreateMap<RoleEditViewModel, Role>()
                .ForMember(m => m.AppModuleActions, opt => opt.Ignore());
            this.CreateMap<GlobalAttribute, GlobalAttributeEditViewModel>().ReverseMap();
            this.CreateMap<AccessToken, UserTokenGridViewModel>()
                .ForMember(at => at.CreatedOnDateString, opt => opt.MapFrom(src => src.CreatedOnDate.ToString()))
                .ForMember(at => at.ExpirationOnDateString, opt => opt.MapFrom(src => src.ExpirationOnDate.ToString()));
            this.CreateMap<Company, CompanyOptionViewModel>();

            // When convert RowVersion
            this.CreateMap<string, byte[]>().ConvertUsing<FromBase64StringResolver>();
            this.CreateMap<byte[], string>().ConvertUsing<ToBase64StringResolver>();
        }
    }

    /// <summary>
    /// The grid default profile.
    /// </summary>
    public class GridDefaultProfile : DefaultProfile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridDefaultProfile"/> class. 
        /// The configure.
        /// </summary>
        public GridDefaultProfile()
        {
            this.CreateMissingTypeMaps = true;
        }

        /// <summary>
        /// The profile name.
        /// </summary>
        public override string ProfileName => nameof(GridDefaultProfile);
    }

    /// <summary>
    /// The auto mapper configuration.
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public static void ConfigureAutoMapper(this IAppBuilder app)
        {
            // Default profile
            var defaultConfig = new MapperConfiguration(
                cfg =>
                    {
                        cfg.AddProfile(new DefaultProfile());

                        // Bacause we use a service inside PathNodeViewModel to do a validation
                        cfg.ConstructServicesUsing(UnityConfig.Instance.Resolve);
                    });

            UnityConfig.Instance.RegisterInstance(defaultConfig.CreateMapper());
            UnityConfig.Instance.RegisterInstance<IMapperGrid>(new MapperGrid());
        }

    }

    /// <summary>
    /// The MapperGrid interface.
    /// </summary>
    public interface IMapperGrid
    {
        /// <summary>
        /// The get mapper.
        /// </summary>
        /// <returns>
        /// The <see cref="IMapper"/>.
        /// </returns>
        IMapper GetMapper();
    }

    /// <summary>
    /// The mapper grid.
    /// </summary>
    public class MapperGrid : IMapperGrid
    {
        /// <summary>
        /// The mapper grid.
        /// </summary>
        private readonly IMapper mapperGrid;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapperGrid"/> class.
        /// </summary>
        public MapperGrid()
        {
            // Default profile
            var gridConfig = new MapperConfiguration(
                cfg =>
                    {
                        cfg.AddProfile(new GridDefaultProfile());

                        // Bacause we use a service inside PathNodeViewModel to do a validation
                        cfg.ConstructServicesUsing(UnityConfig.Instance.Resolve);
                    });

            this.mapperGrid = gridConfig.CreateMapper();
        }

        /// <summary>
        /// The get mapper.
        /// </summary>
        /// <returns>
        /// The <see cref="IMapper"/>.
        /// </returns>
        public IMapper GetMapper()
        {
            return this.mapperGrid;
        }
    }

    /// <summary>
    /// The from base 64 string resolver.
    /// </summary>
    public class FromBase64StringResolver : ITypeConverter<string, byte[]>
    {
        /// <summary>
        /// The convert.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="destination">
        /// The destination.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] Convert(string source, byte[] destination, ResolutionContext context)
        {
            return source == null ? new byte[0] : System.Convert.FromBase64String(source);
        }
    }

    /// <summary>
    /// The to base 64 string resolver.
    /// </summary>
    public class ToBase64StringResolver : ITypeConverter<byte[], string>
    {
        /// <summary>
        /// The convert.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="destination">
        /// The destination.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Convert(byte[] source, string destination, ResolutionContext context)
        {
            return source == null ? null : System.Convert.ToBase64String(source);
        }
    }
}