// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityMvcActivatorConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   Provides the bootstrapping for integrating Unity with ASP.NET MVC.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#region

using Bitmore.Aml.Web.Infrastructure.Config;

using WebActivatorEx;

#endregion

[assembly: PreApplicationStartMethod(typeof(UnityActivatorConfig), "Start")]
[assembly: ApplicationShutdownMethod(typeof(UnityActivatorConfig), "Shutdown")]

namespace Bitmore.Aml.Web.Infrastructure.Config
{
    #region

    using System.Linq;
    using System.Web.Mvc;

    using Bitmore.Aml.Web.Infrastructure.Hub;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;
    using Microsoft.Practices.Unity.Mvc;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    #endregion

    /// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
    public static class UnityActivatorConfig
    {
        #region Public Methods and Operators

        /// <summary>Disposes the Unity container when the application is shut down.</summary>
        public static void Shutdown()
        {
            var container = UnityConfig.Instance.GetConfiguredContainer();
            container.Dispose();
        }

        /// <summary>Integrates Unity when the application starts.</summary>
        public static void Start()
        {
            var container = UnityConfig.Instance.GetConfiguredContainer();

            // Used for MVC Filters
            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

            // Used for MVC
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            // Used for SignalR
            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => new UnityHubActivator(container));
       
            DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
        }

        #endregion
    }
}