// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Config
{
    #region

    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Owin;

    #endregion

    /// <summary>
    /// The web api config.
    /// </summary>
    public static class RouteConfig
    {
        /// <summary>
        /// The configure web api.
        /// </summary>
        /// <param name="appBuilder">
        /// The app builder.
        /// </param>
        public static HttpConfiguration ConfigureRoutes(this IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();

            // Web Api, Route catches the GET, PUT, DELETE, etc. Typical REST interactions
            // config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
            config.MapHttpAttributeRoutes();
            config.EnableCors();

            // This allows the RPC Style methods
            config.Routes.MapHttpRoute(
              "DefaultApi",
              "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional });

            appBuilder.UseWebApi(config);

            // Currently ASP.NET MVC doesn't run on OWIN.
            // Web API will because it's been decoupled from System.Web, specifically HttpContext.
            // This is the main dependency that prevents MVC from running on OWIN as well.
            RegisterRoutes(RouteTable.Routes);
            return config;
        }

        /// <summary>
        /// The register routes.
        /// </summary>
        /// <param name="routes">
        /// The routes.
        /// </param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            // BotDetect requests must not be routed
            routes.IgnoreRoute("{*botdetect}", new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // MVC set default page (Account/LogIn)
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Account", action = "Index", id = UrlParameter.Optional });
        }
    }
}