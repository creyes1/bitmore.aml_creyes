﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Config
{
    #region

    using System.Web.Http;

    using Unity.WebApi;

    #endregion

    /// <summary>
    /// The web api config.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// The configure web api.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <returns>
        /// The <see cref="HttpConfiguration"/>.
        /// </returns>
        public static HttpConfiguration ConfigureWebApi(this HttpConfiguration config)
        {
            var container = UnityConfig.Instance.GetConfiguredContainer();

            // Used for WebAPI
            config.DependencyResolver = new UnityDependencyResolver(container);
            return config;
        }
    }
}