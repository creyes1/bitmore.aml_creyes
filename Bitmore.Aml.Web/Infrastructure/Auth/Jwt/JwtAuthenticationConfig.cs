﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JwtBearerAuthentication.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Auth.Jwt
{
    #region

    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Jwt;
    using Microsoft.Owin.Security.OAuth;

    using Owin;

    #endregion

    /// <summary>
    /// The jwt bearer authentication.
    /// </summary>
    public static class JwtAuthenticationConfig
    {
        /// <summary>
        /// The use jwt.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public static void ConfigureJwtAuthentication(this IAppBuilder app)
        {
            // Api controllers with an [Authorize] attribute will be validated with JWT
            var jwtOpt = new JwtOptions();
            var options = new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = jwtOpt.Audience,
                IssuerSecurityTokenProviders =
                    new IIssuerSecurityTokenProvider[]
                        {
                            new SymmetricKeyIssuerSecurityTokenProvider(jwtOpt.Issuer, JwtOptions.Secret)
                        },
                Provider =
                    new OAuthBearerAuthenticationProvider
                    {
                        OnRequestToken = JwtAuthenticationProvider.OnRequestToken
                    }
            };
            app.UseJwtBearerAuthentication(options);
        }
    }
}