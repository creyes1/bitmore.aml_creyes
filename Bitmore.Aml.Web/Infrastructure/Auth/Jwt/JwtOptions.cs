﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JwtOptions.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Auth.Jwt
{
    #region

    using System;
    using System.IdentityModel.Tokens;
    using System.Text;
    using System.Threading.Tasks;

    #endregion

    /// <summary>
    /// The jwt options.
    /// </summary>
    public class JwtOptions
    {
        /// <summary>
        /// The issuer.
        /// </summary>
        public const string DefaultIssuer = "Bitmore";

        /// <summary>
        /// The secret.
        /// </summary>
        public static readonly byte[] Secret = Encoding.ASCII.GetBytes("ZSCwjOe8ldFKIK0GkOdcMLjJmPIHaHm97l5H5yA2");

        /// <summary>
        /// The audience.
        /// </summary>
        public static readonly string[] DefaultAudience = { "http://www.bitmore.com", "http://localhost" };

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        public string[] Audience { get; set; } = DefaultAudience;

        /// <summary>
        /// The audience to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string AudienceToString()
        {
            return string.Join(",", this.Audience);
        }

        /// <summary>
        /// Gets or sets the expiration.
        /// </summary>
        public TimeSpan Expiration { get; set; } = TimeSpan.FromDays(365);

        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        public string Issuer { get; set; } = DefaultIssuer;

        /// <summary>
        /// The jti generator.
        /// </summary>
        public Func<Task<string>> JtiGenerator => () => Task.FromResult(Guid.NewGuid().ToString());

        /// <summary>
        /// Gets or sets the signing credentials.
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; } = new SigningCredentials(
                                                                            new InMemorySymmetricSecurityKey(Secret),
                                                                            SecurityAlgorithms.HmacSha256Signature,
                                                                            SecurityAlgorithms.Sha256Digest);
    }
}