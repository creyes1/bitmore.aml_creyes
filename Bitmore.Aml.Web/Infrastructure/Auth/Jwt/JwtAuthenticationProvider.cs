﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JwtAuthenticationProvider.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Auth.Jwt
{
    #region

    using System;
    using System.IdentityModel.Tokens;
    using System.Threading.Tasks;

    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Owin.Security.OAuth;

    #endregion

    /// <summary>
    /// The jwt authentication provider.
    /// </summary>
    public static class JwtAuthenticationProvider
    {
        /// <summary>
        /// The on request token.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task OnRequestToken(OAuthRequestTokenContext context)
        {
            var accessTokenService = UnityConfig.Instance.Resolve<IAccessTokenService>();
            var logEngine = UnityConfig.Instance.Resolve<ILogEngine>();
            try
            {
                var jwtSecurityToken = new JwtSecurityToken(context.Token);
                if (accessTokenService.IsBlackList(Guid.Parse(jwtSecurityToken.Id)))
                {
                    context.Token = string.Empty;
                }
            }
            catch (Exception e)
            {
                logEngine.WriteError(e);
            }

            return Task.FromResult<object>(null);
        }
    }
}