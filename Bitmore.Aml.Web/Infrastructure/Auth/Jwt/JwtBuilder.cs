﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JwtBuilder.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Auth.Jwt
{
    #region

    using System;
    using System.IdentityModel.Protocols.WSTrust;
    using System.IdentityModel.Tokens;
    using System.Linq;
    using System.Security.Claims;

    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Services.Services;

    #endregion

    /// <summary>
    /// The JwtBuilder interface.
    /// </summary>
    public interface IJwtBuilder
    {
        /// <summary>
        /// The generate token.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GenerateToken(string userName, string role = null);

        /// <summary>
        /// The generate token and save.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GenerateTokenAndSave(string userName, string role = null, string description = "DeveloperApi");

        /// <summary>
        /// The get jti.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetJti(string token);

        /// <summary>
        /// The get jwt security token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="JwtSecurityToken"/>.
        /// </returns>
        JwtSecurityToken GetJwtSecurityToken(string token);
    }

    /// <summary>
    /// The jwt builder.
    /// </summary>
    public class JwtBuilder : IJwtBuilder
    {
        /// <summary>
        /// The access token service.
        /// </summary>
        private readonly IAccessTokenService accessTokenService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="JwtBuilder"/> class.
        /// </summary>
        /// <param name="accessTokenService">
        /// The access token service.
        /// </param>
        /// <param name="userService"></param>
        public JwtBuilder(IAccessTokenService accessTokenService, IUserService userService)
        {
            this.accessTokenService = accessTokenService;
            this.userService = userService;
        }

        /// <summary>
        /// The generate token.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateToken(string userName, string role = null)
        {
            const string DefaultRole = "ApiViewer";
            if (string.IsNullOrEmpty(role))
            {
                role = DefaultRole;
            }

            var jwtOptions = new JwtOptions();
            var tokenHandler = new JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject =
                    new ClaimsIdentity(
                        new[]
                            {
                                new Claim(ClaimTypes.Name, userName),
                                new Claim(ClaimTypes.Role, role),
                                new Claim(JwtRegisteredClaimNames.Sub, userName),
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                new Claim(JwtRegisteredClaimNames.Iat, now.ToBinary().ToString(), ClaimValueTypes.Integer64)
                            }),
                TokenIssuerName = jwtOptions.Issuer,
                AppliesToAddress = jwtOptions.Audience[0],
                Lifetime = new Lifetime(now, now.AddYears(5)),
                SigningCredentials =
                    new SigningCredentials(
                        new InMemorySymmetricSecurityKey(JwtOptions.Secret),
                        SecurityAlgorithms.HmacSha256Signature,
                        SecurityAlgorithms.Sha256Digest)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        /// <summary>
        /// The generate token and save.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateTokenAndSave(string userName, string role = null, string description = "DeveloperApi")
        {
            var token = this.GenerateToken(userName, role);
            var securityToken = this.GetJwtSecurityToken(token);
            var userId = this.userService.Get(x => x.UserName == userName).UserId;
            var accessToken = new AccessToken()
            {
                AccessTokenId = Guid.Parse(securityToken.Id),
                CreatedOnDate = securityToken.ValidFrom,
                ExpirationOnDate = securityToken.ValidTo,
                Signature = securityToken.RawSignature,
                Description = description,
                UserId = userId
            };
            this.accessTokenService.Add(accessToken);
            return token;
        }

        /// <summary>
        /// The get jti.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetJti(string token)
        {
            var jwtToken = this.GetJwtSecurityToken(token);
            var jticlaim = jwtToken.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti);
            return jticlaim?.Value;
        }

        /// <summary>
        /// The get jwt security token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="JwtSecurityToken"/>.
        /// </returns>
        public JwtSecurityToken GetJwtSecurityToken(string token)
        {
            return new JwtSecurityToken(token);
        }
    }
}