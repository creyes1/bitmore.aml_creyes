// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthenticationConfig.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Auth.Cookie
{
    #region

    using System;
    using System.Web;

    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.AspNet.Identity;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;

    using Owin;

    #endregion

    /// <summary>
    /// The Authentication config.
    /// </summary>
    public static class CookieAuthenticationConfig
    {
        /// <summary>
        /// The configure Authentication.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public static void ConfigureCookieAuthentication(this IAppBuilder app)
        {
            var appConfiguration = app.GetAppConfiguration();
            app.UseCookieAuthentication(
                new CookieAuthenticationOptions
                    {
                        AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                        LoginPath = new PathString("/Account/LogIn"),
                        ExpireTimeSpan = TimeSpan.FromMinutes(appConfiguration.SessionParameters.ExpireTimeSpan),
                        Provider = new CookieAuthenticationProvider
                        {
                            OnValidateIdentity = CookieAuthProvider.ValidateIdentity,
                            OnApplyRedirect = CookieAuthProvider.OnApplyRedirect
                        }
                    });
        }
    }
}