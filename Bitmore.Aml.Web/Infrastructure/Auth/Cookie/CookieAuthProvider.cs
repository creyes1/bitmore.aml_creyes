﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CookieAuthProvider.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Auth.Cookie
{
    #region

    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Caching;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Resources.Language;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Infrastructure.Config;
    using Bitmore.Aml.Web.Infrastructure.Hub;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Practices.Unity;

    #endregion

    /// <summary>
    /// The cookie auth provider.
    /// </summary>
    public static class CookieAuthProvider
    {
        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="userAuth">
        /// </param>
        /// <param name="sessionTimeout">
        /// The session timeout.
        /// </param>
        public static void AddCache(UserAuth userAuth, TimeSpan sessionTimeout)
        {
            var tmp = HttpContext.Current.Cache.Get(userAuth.Name);
            if (tmp != null)
            {
                HttpContext.Current.Cache.Remove(userAuth.Name);
            }

            Debug.WriteLine("timeSpan -> " + sessionTimeout);

            // Add the object to the cache with the current session id, and set a cache removal callback method
            HttpContext.Current.Cache.Insert(
                userAuth.Name,
                userAuth,
                null,
                Cache.NoAbsoluteExpiration,
                sessionTimeout,
                CacheItemPriority.Default,
                CacheItemRemovedCallbackMethod);
        }

        /// <summary>
        /// The on apply redirect.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public static void OnApplyRedirect(CookieApplyRedirectContext context)
        {
            if (!IsApiRequest(context.Request))
            {
                context.Response.Redirect(context.RedirectUri);
            }
        }

        /// <summary>
        /// The is api request.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsApiRequest(IOwinRequest request)
        {
            var apiPath = VirtualPathUtility.ToAbsolute("~/api/");
            return request.Uri.LocalPath.StartsWith(apiPath);
        }

        /// <summary>
        /// The validate identity.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task ValidateIdentity(CookieValidateIdentityContext context)
        {
            if (context.Identity.IsAuthenticated == false || context.Properties.ExpiresUtc == null)
            {
                return Task.FromResult(0);
            }

            var timeSpan = context.Properties.ExpiresUtc.Value.Subtract(DateTimeOffset.UtcNow);
            if (timeSpan.Ticks <= 0)
            {
                return Task.FromResult(0);
            }

            var auth = UnityConfig.Instance.Resolve<IAuthentication>();
            var userAuth = auth.GetCurrentUserAuth(context.Identity.Claims);

            // Add the object to the cache with the current session id, and set a cache removal callback method
            AddCache(userAuth, timeSpan);
            return Task.FromResult(0);
        }

        /// <summary>
        /// The cache item removed callback method.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="reason">
        /// The reason.
        /// </param>
        private static void CacheItemRemovedCallbackMethod(string key, object value, CacheItemRemovedReason reason)
        {
            if (reason != CacheItemRemovedReason.Expired)
            {
                return;
            }

            Execute(
                userService =>
                    {
                        var userAuth = value as UserAuth;
                        if (userAuth == null)
                        {
                            return;
                        }

                        userService.SignOut(userAuth.Name);
                        var messageHub = UnityConfig.Instance.Resolve<IMessageHub>();
                        messageHub.AddMessageSessionFinished(
                            Global.ViewSharedSignalRSessionFinished,
                            userAuth.SessionToken);
                    });
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        public static void Execute(Action<IUserService> function)
        {
            if (function == null)
            {
                return;
            }

            // Important !!!
            // The framework makes no guarantees that a single instance of your filter attribute will only service one request at a time.
            // Given this, you cannot mutate attribute instance state from within the OnActionExecuting / OnActionExecuted methods.
            using (var container = UnityConfig.Instance.CreateChildContainer())
            {
                // We needs to create a new container and change the LifetimeManager because the "execute" method can run without a HttpContext
                container.RegisterType<IUnitOfWork, UnitOfWork>(new ContainerControlledLifetimeManager());
                container.RegisterType<IDatabaseFactory, DatabaseFactory>(new ContainerControlledLifetimeManager());
                var configurationRoot = container.Resolve<IConfigurationRoot>();
                container.RegisterType<IAppConfiguration>(
                    new ContainerControlledLifetimeManager(),
                    UnityConfigExtensions.GetAppConfigurationFactory(configurationRoot));

                var userService = container.Resolve<IUserService>();
                function(userService);
            }
        }
    }
}