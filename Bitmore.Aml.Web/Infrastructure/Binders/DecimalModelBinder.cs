// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DecimalModelBinder.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The decimal model binder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Binders
{
    #region

    using System;
    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// The decimal model binder.
    /// </summary>
    public class DecimalModelBinder : DefaultModelBinder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The bind model.
        /// </summary>
        /// <param name="controllerContext">
        /// The controller context.
        /// </param>
        /// <param name="bindingContext">
        /// The binding context.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (valueProviderResult.AttemptedValue.Equals("N.aN") || valueProviderResult.AttemptedValue.Equals("NaN")
                || valueProviderResult.AttemptedValue.Equals("Infini.ty")
                || valueProviderResult.AttemptedValue.Equals("Infinity")
                || string.IsNullOrEmpty(valueProviderResult.AttemptedValue))
            {
                return 0m;
            }

            return Convert.ToDecimal(valueProviderResult.AttemptedValue);
        }

        #endregion
    }
}