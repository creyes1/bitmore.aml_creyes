// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JobLauncher.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The job launcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure
{
    #region

    using System;
    using System.Threading.Tasks;

    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Practices.Unity;

    using Quartz;
    using Quartz.Impl;

    #endregion

    /// <summary>
    /// The job launcher.
    /// </summary>
    public class JobLauncher : IJob
    {
        /// <summary>
        /// The resolution type string.
        /// </summary>
        internal const string ResolutionTypeString = "Type";

        /// <summary>
        /// The resolution method string.
        /// </summary>
        internal const string ResolutionMethodString = "Method";

        #region Public Methods and Operators

        /// <summary>
        /// The scheduler factory start.
        /// </summary>
        public static void SchedulerFactoryStart()
        {
            var factory = new StdSchedulerFactory();
            var scheduler = factory.GetScheduler();
            scheduler.Start();
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async void Execute(IJobExecutionContext context)
        {
            object typeName, methodName;
            var jobDataMap = context.MergedJobDataMap;
            if (!jobDataMap.TryGetValue(ResolutionTypeString, out typeName) || !jobDataMap.TryGetValue(ResolutionMethodString, out methodName))
            {
                return;
            }

            var type = Type.GetType(typeName.ToString());
            if (type == null)
            {
                return;
            }

            using (var container = UnityConfig.Instance.CreateChildContainer())
            {
                var instance = container.Resolve(type);
                await Task.Run(
                    () =>
                        {
                            instance?.InvokeMethod(methodName.ToString());
                        });
            }
        }

        #endregion
    }
}