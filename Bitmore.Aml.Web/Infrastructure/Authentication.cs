// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Authentication.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.Security;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Controllers;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure.Auth.Cookie;
    using Bitmore.Aml.Web.Infrastructure.Filters;
    using Bitmore.Aml.Web.ViewModel.Account;

    using Microsoft.AspNet.Identity;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;

    using Newtonsoft.Json;

    #endregion

    /// <summary>
    /// The Authentication interface.
    /// </summary>
    public interface IAuthentication
    {
        /// <summary>
        /// The add current culture info.
        /// </summary>
        /// <param name="currentCulture">
        /// The current culture.
        /// </param>
        void AddCurrentCultureInfo(CultureInfo currentCulture);

        /// <summary>
        /// The add current culture info.
        /// </summary>
        /// <param name="langueges">
        /// The langueges.
        /// </param>
        void AddCurrentCultureInfo(string[] langueges);

        /// <summary>
        /// The get current culture info.
        /// </summary>
        /// <returns>
        /// The <see cref="CultureInfo"/>.
        /// </returns>
        CultureInfo GetCurrentCultureInfo();

        /// <summary>
        /// The get current language.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetCurrentLanguage();

        /// <summary>
        /// The get current user.
        /// </summary>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        User GetCurrentUser();

        /// <summary>
        /// The get current user auth
        /// </summary>
        /// <returns>
        /// The <see cref="UserAuth"/>.
        /// </returns>
        UserAuth GetCurrentUserAuth(IEnumerable<Claim> claims = null);

        /// <summary>
        /// The get current user id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Guid GetCurrentUserId();

        /// <summary>
        /// The get current user name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetCurrentUserName();

        /// <summary>
        /// The get user roles.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<string> GetCurrentUserRoles();

        /// <summary>
        /// The get http context.
        /// </summary>
        /// <returns>
        /// The <see cref="HttpContext"/>.
        /// </returns>
        HttpContext GetHttpContext();

        /// <summary>
        /// The get owin context.
        /// </summary>
        /// <returns>
        /// The <see cref="IOwinContext"/>.
        /// </returns>
        IOwinContext GetOwinContext();

        /// <summary>
        /// The get remember me.
        /// </summary>
        /// <returns>
        /// The <see cref="AccountViewModel"/>.
        /// </returns>
        AccountViewModel GetRememberMe();

        /// <summary>
        /// The get session token.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Guid GetSessionToken();

        /// <summary>
        /// Has multiple roles
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool HasMultipleRoles();

        /// <summary>
        /// The is authenticated.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsAuthenticated();

        /// <summary>
        /// The redirect to login page.
        /// </summary>
        void RedirectToLoginPage();

        /// <summary>
        /// The sign in.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="persistent">
        /// The persistent.
        /// </param>
        void SignIn(string userName, string password, bool persistent);

        /// <summary>
        /// The sign out.
        /// </summary>
        void SignOut();

        /// <summary>
        /// The update claim
        /// </summary>
        /// <param name="value"></param>
        void UpdateClaim(UserAuth value);

        /// <summary>
        /// The short date time format to upper
        /// </summary>
        /// <returns></returns>
        string ShortDateTimeFormatToUpper();

        /// <summary>
        /// The short date time format
        /// </summary>
        /// <returns></returns>
        string ShortDateTimeFormat();

        /// <summary>
        /// The full date time format
        /// </summary>
        /// <returns></returns>
        string FullDateTimeFormat();

        /// <summary>
        /// Set created by
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        void SetCreatedBy<TEntity>(TEntity entity);

        /// <summary>
        /// The set class.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string SetAuthClass();
    }

    /// <summary>
    /// The Authentication.
    /// </summary>
    public class Authentication : IAuthentication
    {
        /// <summary>
        /// The culture.
        /// </summary>
        private const string Culture = "Culture";

        /// <summary>
        /// The current user context string.
        /// </summary>
        private const string CurrentUserContextString = "CurrentUserContext";

        /// <summary>
        /// The owin cookie persistent string.
        /// </summary>
        private const string OwinCookiePersistentString = "OwinCookiePersistent";

        /// <summary>
        /// The app configuration.
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        /// <summary>
        /// The controller helper.
        /// </summary>
        private readonly IControllerHelper controllerHelper;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly ILanguageService languageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="Authentication"/> class.
        /// </summary>
        /// <param name="userService">
        /// The user service.
        /// </param>
        /// <param name="appConfiguration">
        /// The app configuration.
        /// </param>
        /// <param name="controllerHelper">
        /// </param>
        /// <param name="languageService"></param>
        public Authentication(
            IUserService userService,
            IAppConfiguration appConfiguration,
            IControllerHelper controllerHelper,
            ILanguageService languageService)
        {
            this.userService = userService;
            this.appConfiguration = appConfiguration;
            this.controllerHelper = controllerHelper;
            this.languageService = languageService;
        }

        /// <summary>
        /// The add current culture info.
        /// </summary>
        /// <param name="lan">
        /// The lan.
        /// </param>
        public void AddCurrentCultureInfo(string[] langueges)
        {
            var currentCulture = this.languageService.GetDefaultCulture(langueges);
            this.AddCurrentCultureInfo(currentCulture);
        }

        /// <summary>
        /// The add current culture info.
        /// </summary>
        /// <param name="currentCulture">
        /// </param>
        public void AddCurrentCultureInfo(CultureInfo currentCulture)
        {
            Thread.CurrentThread.CurrentUICulture = currentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currentCulture.Name);
            this.GetHttpContext().Session.Add(Culture, currentCulture.Name);
        }

        /// <summary>
        /// The get authentication.
        /// </summary>
        /// <returns>
        /// The <see cref="IAuthenticationManager"/>.
        /// </returns>
        public IAuthenticationManager GetAuthentication()
        {
            return this.GetOwinContext().Authentication;
        }

        /// <summary>
        /// The get claim.
        /// </summary>
        /// <param name="claimType">
        /// The claim type.
        /// </param>
        /// <returns>
        /// The <see cref="Claim"/>.
        /// </returns>
        public Claim GetClaim(string claimType)
        {
            return this.GetClaims(claimType).FirstOrDefault();
        }

        /// <summary>
        /// Get a list of claims
        /// </summary>
        /// <param name="claimType">
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Claim> GetClaims(string claimType)
        {
            var claims = this.GetClaims();
            return claims.Where(x => x.Type == claimType);
        }

        /// <summary>
        /// Get all claims
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Claim> GetClaims()
        {
            var identity = this.GetIdentity();
            return identity?.Claims;
        }

        /// <summary>
        /// The get current culture info.
        /// </summary>
        /// <returns>
        /// The <see cref="CultureInfo"/>.
        /// </returns>
        public CultureInfo GetCurrentCultureInfo()
        {
            var session = this.GetHttpContext().Session;
            var sessionCulture = session[Culture];
            return sessionCulture == null ? this.languageService.GetDefaultCulture() : new CultureInfo(sessionCulture.ToString());
        }

        /// <summary>
        /// The get current language.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetCurrentLanguage()
        {
            return Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
        }

        /// <summary>
        /// The get current user.
        /// </summary>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        public User GetCurrentUser()
        {
            return this.GetValueFromContext(
                CurrentUserContextString,
                () =>
                {
                    var userId = this.GetCurrentUserId();
                    return userId == Guid.Empty ? null : this.userService.Get(userId);
                });
        }

        /// <summary>
        /// The get current user auth
        /// </summary>
        /// <returns>
        /// The <see cref="UserAuth"/>.
        /// </returns>
        public UserAuth GetCurrentUserAuth(IEnumerable<Claim> claims = null)
        {
            if (claims == null)
            {
                claims = this.GetClaims();
            }

            return UserAuth.ConvertFromClaims(claims);
        }

        /// <summary>
        /// The get current user id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        public Guid GetCurrentUserId()
        {
            var userAuth = this.GetCurrentUserAuth();
            return userAuth?.UserId ?? Guid.Empty;
        }

        /// <summary>
        /// The get current user name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetCurrentUserName()
        {
            var nameClaim = this.GetClaim(ClaimTypes.Name);
            return nameClaim?.Value;
        }

        /// <summary>
        /// The get user roles.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<string> GetCurrentUserRoles()
        {
            return this.GetClaims(ClaimTypes.Role).Select(x => x.Value).ToList();
        }

        /// <summary>
        /// The get http context.
        /// </summary>
        /// <returns>
        /// The <see cref="HttpContext"/>.
        /// </returns>
        public HttpContext GetHttpContext()
        {
            return HttpContext.Current;
        }

        /// <summary>
        /// The get identity.
        /// </summary>
        /// <returns>
        /// The <see cref="ClaimsIdentity"/>.
        /// </returns>
        public ClaimsIdentity GetIdentity()
        {
            return this.GetHttpContext().User.Identity as ClaimsIdentity;
        }

        /// <summary>
        /// The get owin context.
        /// </summary>
        /// <returns>
        /// The <see cref="IOwinContext"/>.
        /// </returns>
        public IOwinContext GetOwinContext()
        {
            return HttpContext.Current.GetOwinContext();
        }

        /// <summary>
        /// The get remember me.
        /// </summary>
        /// <returns>
        /// The <see cref="AccountViewModel"/>.
        /// </returns>
        public AccountViewModel GetRememberMe()
        {
            KeyValuePair<string, string> cookie;
            if (!this.GetRememberMeCookie(out cookie))
            {
                return new AccountViewModel();
            }

            var bytes = HttpServerUtility.UrlTokenDecode(cookie.Value);
            var output = MachineKey.Unprotect(bytes);
            var result = Encoding.UTF8.GetString(output);
            return JsonConvert.DeserializeObject<AccountViewModel>(result);
        }

        /// <summary>
        /// The get remember me cookie.
        /// </summary>
        /// <param name="cookieref">
        /// The cookieref.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetRememberMeCookie(out KeyValuePair<string, string> cookieref)
        {
            var ctx = this.GetHttpContext();
            var cookies = ctx.Request.Cookies;
            var cookie =
                cookies.Cast<string>()
                    .Select(x => new KeyValuePair<string, string>(x, cookies[x].Value))
                    .FirstOrDefault(x => x.Key == OwinCookiePersistentString);
            cookieref = cookie;
            return !cookie.Equals(default(KeyValuePair<string, string>));
        }

        /// <summary>
        /// The get session token.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        public Guid GetSessionToken()
        {
            return this.GetCurrentUserAuth().SessionToken;
        }

        /// <summary>
        /// The has multiple roles.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasMultipleRoles()
        {
            var roles = this.GetCurrentUserRoles();
            return roles.Count() > 1;
        }

        /// <summary>
        /// The is authenticated.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsAuthenticated()
        {
            var ctx = this.GetHttpContext();
            var auth = this.GetAuthentication();
            return auth != null && ctx.User != null && ctx.User.Identity.IsAuthenticated
                   && this.GetCurrentUserId() != Guid.Empty;
        }

        /// <summary>
        /// The redirect to login page.
        /// </summary>
        public void RedirectToLoginPage()
        {
            this.GetHttpContext().Response.Redirect(this.controllerHelper.Action<AccountController>(a => a.LogIn()));
        }

        /// <summary>
        /// The set remember me.
        /// </summary>
        /// <param name="userName">
        /// The user Name.
        /// </param>
        /// <param name="password">
        /// </param>
        /// <param name="persistent">
        /// The persistent.
        /// </param>
        public void SetRememberMe(string userName, string password, bool persistent)
        {
            var ctx = this.GetOwinContext();
            if (!persistent)
            {
                KeyValuePair<string, string> cookie;
                if (this.GetRememberMeCookie(out cookie))
                {
                    ctx.Response.Cookies.Append(
                        OwinCookiePersistentString,
                        string.Empty,
                        new CookieOptions { Expires = DateTime.Now.AddYears(-1) });
                }

                return;
            }

            var options = new CookieOptions { Expires = DateTime.Now.AddDays(30), HttpOnly = true };
            var value = JsonConvert.SerializeObject(new { UserName = userName, Password = password, RememberMe = true });
            var cookieText = Encoding.UTF8.GetBytes(value);
            var encryptedValue = HttpServerUtility.UrlTokenEncode(MachineKey.Protect(cookieText));
            ctx.Response.Cookies.Append(OwinCookiePersistentString, encryptedValue, options);
        }

        /// <summary>
        /// The Authentication.
        /// </summary>
        /// <param name="userName">
        /// The user Name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="persistent">
        /// The persistent.
        /// </param>
        public void SignIn(string userName, string password, bool persistent)
        {
            var user = this.userService.Get(u => u.UserName == userName, x => x.Roles);

            // User data
            var userAuth = new UserAuth
            {
                UserId = user.UserId,
                SessionToken = user.SessionToken,
                CurrentRoleId = user.CurrentRoleId,
                Name = user.UserName,
                Roles = user.Roles.Select(x => x.Name).ToArray()
            };

            var identity = new ClaimsIdentity(userAuth.ConvertToClaims(), DefaultAuthenticationTypes.ApplicationCookie);
            var principal = new ClaimsPrincipal(identity);
            this.GetOwinContext()
                .Authentication.SignIn(new AuthenticationProperties { IsPersistent = persistent }, identity);

            Thread.CurrentPrincipal = principal;
            HttpContext.Current.User = principal;

            // In case of "Remember Me"
            this.SetRememberMe(userName, password, persistent);

            if (!this.appConfiguration.SessionParameters.SingleSessionActivated)
            {
                return;
            }

            // Set timeSpan and add Cache
            var timeSpan = DateTimeOffset.UtcNow.AddMinutes(this.appConfiguration.SessionParameters.ExpireTimeSpan).Subtract(DateTimeOffset.UtcNow);
            CookieAuthProvider.AddCache(userAuth, timeSpan);
        }

        /// <summary>
        /// The sign out.
        /// </summary>
        public void SignOut()
        {
            var auth = this.GetAuthentication();
            auth.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        /// <summary>
        /// The update claim
        /// </summary>
        /// <param name="value"></param>
        public void UpdateClaim(UserAuth value)
        {
            var authenticationManager = this.GetAuthentication();
            var identity = this.GetIdentity();
            var claims = value.ConvertToClaims();
            var claimsToDelete = identity.Claims.Where(c => claims.Any(x => x.Type == c.Type)).ToList();
            claimsToDelete.ForEach(identity.RemoveClaim);
            identity.AddClaims(claims);
            authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = true });
        }

        /// <summary>
        /// Get short date time format
        /// </summary>
        /// <returns></returns>
        public string ShortDateTimeFormat()
        {
            return this.GetCurrentCultureInfo().DateTimeFormat.ShortDatePattern;
        }

        /// <summary>
        /// Get short date time format to upper
        /// </summary>
        /// <returns></returns>
        public string ShortDateTimeFormatToUpper()
        {
            return this.GetCurrentCultureInfo().DateTimeFormat.ShortDatePattern.ToUpper();
        }

        /// <summary>
        /// Get fukk date time format
        /// </summary>
        /// <returns></returns>
        public string FullDateTimeFormat()
        {
            var datePattern = this.GetCurrentCultureInfo().DateTimeFormat.ShortDatePattern;
            return $"{datePattern} HH:mm:ss";
        }

        /// <summary>
        /// The set created by.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <typeparam name="TEntity">
        /// </typeparam>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void SetCreatedBy<TEntity>(TEntity entity)
        {
            const string CreatedByKey = "CreatedBy";
            var current = entity.GetPropertyValue<Guid>(CreatedByKey);
            if (current == Guid.Empty)
            {
                entity.SetPropertyValue(CreatedByKey, this.GetCurrentUserId());
            }
        }

        /// <summary>
        /// The get value from http context.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="func">
        /// The func.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private T GetValueFromContext<T>(string key, Func<T> func)
        {
            var context = this.GetOwinContext();
            if (context.Environment.ContainsKey(key))
            {
                return (T)context.Environment[key];
            }

            var obj = func();
            context.Environment.Add(key, obj);
            return obj;
        }

        /// <summary>
        /// The set auth class.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SetAuthClass()
        {
            return this.IsAuthenticated() ? "logged" : "not-logged";
        }
    }
}