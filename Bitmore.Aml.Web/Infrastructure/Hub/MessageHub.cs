// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageHub.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Hub
{
    #region

    using System;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading.Tasks;

    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Infrastructure;

    #endregion

    /// <summary>
    /// The MessageHub interface.
    /// </summary>
    public interface IMessageHub
    {
        /// <summary>
        /// The add message session finished.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="sessionId">
        /// The session id.
        /// </param>
        void AddMessageSessionFinished(string message, Guid sessionId);
    }

    /// <summary>
    /// The message hub.
    /// </summary>
    public class MessageHub : Hub, IMessageHub
    {
        /// <summary>
        /// The users.
        /// </summary>
        private static readonly ConcurrentDictionary<string, UserAuth> Users = new ConcurrentDictionary<string, UserAuth>();

        /// <summary>
        /// The add message session finished.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="sessionToken">
        /// The session Token.
        /// </param>
        public void AddMessageSessionFinished(string message, Guid sessionToken)
        {
            var userAuth = Users.FirstOrDefault(x => x.Value != null && x.Value.SessionToken == sessionToken);
            if (userAuth.Key == null)
            {
                return;
            }

            // If you want to send something to clients from outside of hub handler methods (ie not during handling message on server), you have to use GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            // Reason is that when the method is called to handle some client side message, hub instance is created by SignalR and Clients property is correctly initialized.This is not the case when you calling method yourself from server code (and probably creating hub instance yourself).
            var messageHub = GlobalHost.DependencyResolver.Resolve<IConnectionManager>().GetHubContext<MessageHub>();
            messageHub.Clients.Client(userAuth.Key).addMessageSessionFinished(message);
        }

        /// <summary>
        /// The broadcast message to all.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void BroadcastMessageToAll(string message)
        {
            this.Clients.All.newMessageReceived(message);
        }

        /// <summary>
        /// The broadcast to group.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="group">
        /// The group.
        /// </param>
        public void BroadcastToGroup(string message, string group)
        {
            this.Clients.Group(group).newMessageReceived(message);
        }

        /// <summary>
        /// The join a group.
        /// </summary>
        /// <param name="group">
        /// The group.
        /// </param>
        public void JoinAGroup(string group)
        {
            this.Groups.Add(this.Context.ConnectionId, group);
        }

        /// <summary>
        /// The on connected.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override Task OnConnected()
        {
            var authentication = UnityConfig.Instance.Resolve<IAuthentication>();
            var userAuth = authentication.GetCurrentUserAuth();
            Users.AddOrUpdate(this.Context.ConnectionId, userAuth, (k, v) => userAuth != null && v.SessionToken != userAuth.SessionToken ? userAuth : v);
            return base.OnConnected();
        }

        /// <summary>
        /// The on disconnected.
        /// </summary>
        /// <param name="stopCalled">
        /// The stop called.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            UserAuth garbage;
            Users.TryRemove(this.Context.ConnectionId, out garbage);
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// The remove from a group.
        /// </summary>
        /// <param name="group">
        /// The group.
        /// </param>
        public void RemoveFromAGroup(string group)
        {
            this.Groups.Remove(this.Context.ConnectionId, group);
        }
    }
}