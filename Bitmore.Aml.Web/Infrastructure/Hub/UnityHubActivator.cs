// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityHubActivator.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Hub
{
    #region

    using System;
    using System.Web.Mvc;

    using Microsoft.AspNet.SignalR.Hubs;
    using Microsoft.Practices.Unity;

    #endregion

    /// <summary>
    /// The signal r unity dependency resolver.
    /// </summary>
    public class UnityHubActivator : IHubActivator
    {
        /// <summary>
        /// The container.
        /// </summary>
        private readonly IUnityContainer container;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityHubActivator"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public UnityHubActivator(IUnityContainer container)
        {
            this.container = container;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="descriptor">
        /// The descriptor.
        /// </param>
        /// <returns>
        /// The <see cref="IHub"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public IHub Create(HubDescriptor descriptor)
        {
            if (descriptor == null)
            {
                throw new ArgumentNullException(nameof(descriptor));
            }

            if (descriptor.HubType == null)
            {
                return null;
            }

            var hub = this.container.Resolve(descriptor.HubType) ?? Activator.CreateInstance(descriptor.HubType);
            return hub as IHub;
        }
    }
}