﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheFactoryManager.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Cache
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The CacheFactoryManager interface.
    /// </summary>
    public interface ICacheFactoryManager
    {
        /// <summary>
        /// The clear all.
        /// </summary>
        void ClearAll();

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="cacheManager">
        /// The cache manager.
        /// </param>
        void Add(object cacheManager);
    }

    /// <summary>
    /// The cache factory manager.
    /// </summary>
    public class CacheFactoryManager : ICacheFactoryManager
    {
        /// <summary>
        /// The current cache managers.
        /// </summary>
        private readonly List<object> currentCacheManagers;

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheFactoryManager"/> class.
        /// </summary>
        public CacheFactoryManager()
        {
            this.currentCacheManagers = new List<object>();
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="cacheManager">
        /// The cache manager.
        /// </param>
        public void Add(object cacheManager)
        {
            this.currentCacheManagers.Add(cacheManager);
        }

        /// <summary>
        /// The clear all.
        /// </summary>
        public void ClearAll()
        {
            this.currentCacheManagers.ForEach(x => { x.InvokeMethod("Clear"); });
        }
    }
}