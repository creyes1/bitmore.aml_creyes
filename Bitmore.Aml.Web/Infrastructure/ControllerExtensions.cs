// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControllerExtensions.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure
{
    #region

    using System;
    using System.Net;
    using System.Web.Mvc;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    #endregion

    /// <summary>
    /// The controller extensions.
    /// </summary>
    public static class ControllerExtensions
    {
        /// <summary>
        /// The json net.
        /// </summary>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="behavior">
        /// The behavior.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        public static JsonResult JsonNet(this Controller controller, object data, JsonRequestBehavior behavior = JsonRequestBehavior.AllowGet)
        {
            return new JsonDotNetResult
                       {
                           Data = data,
                           JsonRequestBehavior = behavior
            };
        }
    }

    // Based on http://tostring.it/2012/07/18/customize-json-result-in-web-api/
    // Based on http://stackoverflow.com/questions/17244774/proper-json-serialization-in-mvc-4

    /// <summary>
    /// The json dot net result.
    /// </summary>
    public class JsonDotNetResult : JsonResult
    {
        /// <summary>
        /// The settings.
        /// </summary>
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(), 
                    DateFormatHandling = DateFormatHandling.IsoDateFormat
        };

        /// <summary>
        /// The execute result.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// </exception>
        public override void ExecuteResult(ControllerContext context)
        {
            if (this.JsonRequestBehavior == JsonRequestBehavior.DenyGet
                && string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("GET request not allowed");
            }

            var response = context.HttpContext.Response;
            if (response.StatusCode != (int)HttpStatusCode.OK)
            {
                return;
            }

            response.Clear();
            response.ClearContent();
            if (!response.IsRequestBeingRedirected)
            {
                response.ClearHeaders();
            }

            response.ContentType = !string.IsNullOrEmpty(this.ContentType) ? this.ContentType : "application/json";

            if (this.ContentEncoding != null)
            {
                response.ContentEncoding = this.ContentEncoding;
            }

            if (this.Data == null)
            {
                return;
            }

            response.Write(JsonConvert.SerializeObject(this.Data, Settings));
        }
    }
}