// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserAuth.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure
{
    #region

    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;
    using System.Xml.Serialization;

    using Bitmore.Aml.Domain.Utils;

    #endregion

    /// <summary>
    /// The user auth.
    /// </summary>
    public class UserAuth
    {
        /// <summary>
        /// Initializes static members of the <see cref="UserAuth"/> class.
        /// </summary>
        static UserAuth()
        {
            Properties = new ConcurrentDictionary<string, PropertyInfo>();
            var prop = typeof(UserAuth).GetProperties();
            prop.ForEach(
                p =>
                    {
                        var attr = Attribute.GetCustomAttributes(p, typeof(XmlElementAttribute));
                        var name = p.Name;
                        if (attr.Any())
                        {
                            var xmlatt = attr.FirstOrDefault() as XmlElementAttribute;
                            if (!string.IsNullOrEmpty(xmlatt?.ElementName))
                            {
                                name = xmlatt.ElementName;
                            }
                        }

                        Properties.TryAdd(name, p);
                    });
        }

        /// <summary>
        /// Gets or sets the session token.
        /// </summary>
        public Guid SessionToken { get; set; }

        /// <summary>
        /// Gets or sets the CurrentRoleId
        /// </summary>
        public Guid CurrentRoleId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElementAttribute(ElementName = ClaimTypes.Name)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is menu collapsed.
        /// </summary>
        public bool IsMenuCollapsed { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        [XmlElementAttribute(ElementName = ClaimTypes.Role)]
        public IEnumerable<string> Roles { get; set; }

        /// <summary>
        /// The convert to claims.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static UserAuth ConvertFromClaims(IEnumerable<Claim> claims)
        {
            var result = new UserAuth();
            Properties.ForEach(
                    p =>
                    {
                        var tmp = claims.Where(c => c.Type == p.Key).ToArray();
                        if (!tmp.Any())
                        {
                            return;
                        }

                        if (tmp.Length == 1)
                        {
                            var claim = tmp.FirstOrDefault();
                            if (claim?.Value == null)
                            {
                                return;
                            }

                            var value = TypeDescriptor.GetConverter(p.Value.PropertyType).ConvertFromInvariantString(claim.Value.ToString());
                            p.Value.SetValue(result, value);
                        }
                        else
                        {
                            var listType = typeof(List<>);
                            var genericArgs = p.Value.PropertyType.GetGenericArguments();
                            var concreteType = listType.MakeGenericType(genericArgs);
                            var newList = Activator.CreateInstance(concreteType) as dynamic;
                            foreach (var claim in tmp)
                            {
                                if (claim.Value == null)
                                {
                                    continue;
                                }

                                newList.Add(claim.Value);
                            }

                            p.Value.SetValue(result, newList);
                        }
                    });
            return result;
        }

        /// <summary>
        /// The convert to claims.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Claim> ConvertToClaims()
        {
            var result = new List<Claim>();
            Properties.ForEach(
                p =>
                    {
                        var value = p.Value.GetValue(this);
                        if (value == null)
                        {
                            return;
                        }

                        var enumerable = value as IEnumerable;
                        if (enumerable != null && enumerable.GetType() != typeof(string))
                        {
                            // ReSharper disable once LoopCanBeConvertedToQuery
                            foreach (var item in enumerable)
                            {
                                if (item == null)
                                {
                                    continue;
                                }

                                result.Add(new Claim(p.Key, item.ToString()));
                            }
                        }
                        else
                        {
                            result.Add(new Claim(p.Key, p.Value.GetValue(this).ToString()));
                        }
                    });
            return result;
        }

        /// <summary>
        /// The properties.
        /// </summary>
        private static readonly ConcurrentDictionary<string, PropertyInfo> Properties;
    }
}