// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnhandledExceptionFilter.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The unhandled exception filter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Bitmore.Aml.Web.Infrastructure.Filters
{
    #region

    using System.Web;
    using System.Web.Mvc;

    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Practices.Unity;

    #endregion

    /// <summary>
    /// The unhandled exception filter.
    /// </summary>
    public class UnhandledExceptionFilter : HandleErrorAttribute
    {
        #region Public Methods and Operators

        /// <summary>
        /// The on exception.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void OnException(ExceptionContext context)
        {
            // Important !!!
            // The framework makes no guarantees that a single instance of your filter attribute will only service one request at a time.
            // Given this, you cannot mutate attribute instance state from within the OnActionExecuting / OnActionExecuted methods.
            var container = UnityConfig.Instance.Container;
            var authentication = container.Resolve<IAuthentication>();
            var logEngine = container.Resolve<ILogEngine>();

            // Because some exceptions can be without userName
            var userName = authentication.GetCurrentUserName() ?? HttpContext.Current.User.Identity.Name;
            logEngine.WriteError(context.Exception, userName);
        }

        #endregion
    }
}