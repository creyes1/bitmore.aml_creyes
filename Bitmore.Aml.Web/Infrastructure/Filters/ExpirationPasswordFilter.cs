// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpirationPasswordFilter.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The expiration password filter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Filters
{
    #region

    using System.Web.Mvc;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Utils;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Controllers;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Practices.Unity;

    #endregion

    /// <summary>
    /// The expiration password filter.
    /// </summary>
    public class ExpirationPasswordFilter : AuthorizeAttribute
    {
        /// <summary>
        /// The single session string.
        /// </summary>
        public const string ExpirationPasswordString = "ExpirationPassword";

        /// <summary>
        /// The format action.
        /// </summary>
        private const string FormatAction = @"/{0}/{1}";

        /// <summary>
        /// The action password expired.
        /// </summary>
        private static string actionPasswordExpired;

        /// <summary>
        /// The change current password expired
        /// </summary>
        private static string changecurrentpasswordexpired;

        /// <summary>
        /// The create route.
        /// </summary>
        /// <param name="appConfiguration">
        /// The app configuration.
        /// </param>
        /// <param name="controllerHelper">
        /// The controller helper.
        /// </param>
        public void CreateRoute(IAppConfiguration appConfiguration, IControllerHelper controllerHelper)
        {
            if (appConfiguration.ExpirationPassword == null)
            {
                return;
            }

            if (actionPasswordExpired != null)
            {
                return;
            }

            var nameController = controllerHelper.GetControllerName<AccountController>();
            var actionChangePasswordExpired =
                ExtendedMethods.GetMemberName<AccountController>(c => c.ChangePasswordExpired());
            actionPasswordExpired = string.Format(FormatAction, nameController, actionChangePasswordExpired).ToLower();

            var nameAction =
                ExtendedMethods.GetMemberName<AccountController>(c => c.ChangeCurrentPasswordExpired(null));
            changecurrentpasswordexpired = string.Format(FormatAction, nameController, nameAction).ToLower();
        }

        /// <summary>
        /// The on authorization.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void OnAuthorization(AuthorizationContext context)
        {
            // Important !!!
            // There is no gurantee in the MVC that every single instance of the AuthorizeAttribute would serve single request.
            // This basically means that the same instance of the action filter can be reused for different actions and if you have stored instance state in it it will probably break.
            var container = UnityConfig.Instance.Container;
            var authentication = container.Resolve<IAuthentication>();
            var appConfiguration = container.Resolve<IAppConfiguration>();
            var controllerHelper = container.Resolve<IControllerHelper>();
            var userService = container.Resolve<IUserService>();

            // Check user's session token
            if (!authentication.IsAuthenticated())
            {
                return;
            }

            if (context.IsChildAction)
            {
                return;
            }

            if (!appConfiguration.ExpirationPassword.Activated)
            {
                return;
            }

            this.CreateRoute(appConfiguration, controllerHelper);
            var userId = authentication.GetCurrentUserId();
            var values = context.RouteData.Values;
            var url = string.Format(FormatAction, values["Controller"], values["Action"]).ToLower();
            if (appConfiguration.ExpirationPassword == null || appConfiguration.ExpirationPassword.Days <= 0)
            {
                if (url == actionPasswordExpired)
                {
                    context.HttpContext.Response.Redirect(controllerHelper.Action<AccountController>(a => a.Main()));
                }

                return;
            }

            if (url == actionPasswordExpired || url == changecurrentpasswordexpired)
            {
                return;
            }

            if (!userService.PasswordExpired(userId, appConfiguration.ExpirationPassword.Days))
            {
                return;
            }

            if (context.HttpContext.Response.IsRequestBeingRedirected)
            {
                return;
            }

            context.HttpContext.Response.Redirect(controllerHelper.Action<AccountController>(a => a.ChangePasswordExpired()), true);
        }
    }
}