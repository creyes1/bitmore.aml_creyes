// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModuleAuthorize.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The module authorize.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Filters
{
    #region

    using System.Web;
    using System.Web.Mvc;

    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Practices.Unity;

    #endregion

    /// <summary>
    /// The module authorize.
    /// </summary>
    public class ModuleAuthorizeFilter : AuthorizeAttribute
    {
        /// <summary>
        /// The url format.
        /// </summary>
        private const string UrlFormat = "/{0}/{1}";

        /// <summary>
        /// The http item module authorized.
        /// </summary>
        private const string HttpItemModuleAuthorized = "ModuleAuthorized";

        #region Public Methods and Operators

        /// <summary>
        /// The on authorization.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                                    || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(
                                        typeof(AllowAnonymousAttribute),
                                        true);

            if (skipAuthorization)
            {
                return;
            }

            // Important !!!
            // There is no gurantee in the MVC that every single instance of the AuthorizeAttribute would serve single request.
            // This basically means that the same instance of the action filter can be reused for different actions and if you have stored instance state in it it will probably break.
            var container = UnityConfig.Instance.Container;
            var authentication = container.Resolve<IAuthentication>();
            var roleService = container.Resolve<IRoleService>();
            if (!authentication.IsAuthenticated())
            {
                base.OnAuthorization(filterContext);
                return;
            }

            var descriptor = filterContext.ActionDescriptor;
            var user = authentication.GetCurrentUserAuth();
            var urlrequest = string.Format(UrlFormat, descriptor.ControllerDescriptor.ControllerName, descriptor.ActionName);
            var result = roleService.IsValidRoleByModule(user.CurrentRoleId, urlrequest);
            if (result)
            {
                filterContext.HttpContext.Items.Add(HttpItemModuleAuthorized, true);
                return;
            }

            base.OnAuthorization(filterContext);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The authorize core.
        /// </summary>
        /// <param name="httpContext">
        /// The http context.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var value = httpContext.Items[HttpItemModuleAuthorized];
            if (value == null)
            {
                return false;
            }

            bool outValue;
            return bool.TryParse(value.ToString(), out outValue) && outValue;
        }

        #endregion
    }
}