// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InternationalizationFilter.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The internationalization filter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Filters
{
    #region

    using System.Web;
    using System.Web.Mvc;

    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Practices.Unity;

    #endregion

    /// <summary>
    /// The internationalization filter.
    /// </summary>
    public class InternationalizationFilter : ActionFilterAttribute
    {
        #region Public Methods and Operators

        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Important !!!
            // The framework makes no guarantees that a single instance of your filter attribute will only service one request at a time.
            // Given this, you cannot mutate attribute instance state from within the OnActionExecuting / OnActionExecuted methods.
            var container = UnityConfig.Instance.Container;
            var authentication = container.Resolve<IAuthentication>();
            var currentCulture = authentication.GetCurrentCultureInfo();
            if (currentCulture != null)
            {
                authentication.AddCurrentCultureInfo(currentCulture);
                return;
            }

            var request = HttpContext.Current.Request;
            authentication.AddCurrentCultureInfo(request.UserLanguages);
        }

        #endregion
    }
}