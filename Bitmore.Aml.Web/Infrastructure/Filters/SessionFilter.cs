// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SingleSessionFilter.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The single session filter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Infrastructure.Filters
{
    #region

    using System;
    using System.Web.Mvc;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Controllers;
    using Bitmore.Aml.Web.Helpers;
    using Bitmore.Aml.Web.Infrastructure.Auth.Cookie;
    using Bitmore.Aml.Web.Infrastructure.Config;

    using Microsoft.Practices.Unity;

    #endregion
    
    /// <summary>
    /// The single session filter.
    /// </summary>
    public class SessionFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Initializes static members of the <see cref="SessionFilter"/> class.
        /// </summary>
        static SessionFilter()
        {
            // Get all the users that has active session
            CookieAuthProvider.Execute((userService) =>
            {
                userService.ClearHasActiveSession();
            });
        }

        /// <summary>
        /// The is single session.
        /// </summary>
        /// <param name="appConfiguration">
        /// The app Configuration.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsSingleSessionActivated(IAppConfiguration appConfiguration)
        {
            return appConfiguration.SessionParameters != null && appConfiguration.SessionParameters.SingleSessionActivated;
        }

        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Important !!!
            // The framework makes no guarantees that a single instance of your filter attribute will only service one request at a time.
            // Given this, you cannot mutate attribute instance state from within the OnActionExecuting / OnActionExecuted methods.
            var container = UnityConfig.Instance.Container;
            var authentication = container.Resolve<IAuthentication>();
            var appConfiguration = container.Resolve<IAppConfiguration>();
            var userService = container.Resolve<IUserService>();
            var httpHelper = container.Resolve<IHttpHelper>();
            var controllerHelper = container.Resolve<IControllerHelper>();

            if (!this.IsSingleSessionActivated(appConfiguration))
            {
                return;
            }

            // Check user's session token
            if (!authentication.IsAuthenticated())
            {
                return;
            }

            var sessionToken = authentication.GetSessionToken();
            if (sessionToken == Guid.Empty)
            {
                // No Authentication ticket found so logout this user
                // Should never hit this code
                authentication.SignOut();
                authentication.RedirectToLoginPage();
                return;
            }

            var currentUser = authentication.GetCurrentUser();

            // May want to add a conditional here so we only check
            // if the user needs to be checked. For instance, your business
            // rules for the application may state that users in the Admin
            // role are allowed to have multiple sessions
            if (currentUser == null)
            {
                return;
            }

            var storedToken = currentUser.SessionToken;
            if (sessionToken == storedToken)
            {
                if (currentUser.HasActiveSession)
                {
                    return;
                }

                // Apparently we retarted the application server and we need to update the "HasActiveSession"
                currentUser.HasActiveSession = true;
                userService.Update(currentUser);

                return;
            }

            // Stored session does not match one in Authentication
            // ticket so logout the user
            authentication.SignOut();
            if (!httpHelper.IsAjaxRequest())
            {
                filterContext.Result =
                    new RedirectToRouteResult(controllerHelper.GetRouteValues<AccountController>(a => a.LogIn()));
            }
            else
            {
                // Set status code = 432. Acording this page we can use empty status code http://www.iana.org/assignments/http-status-codes/http-status-codes.xml
                filterContext.Result = new HttpStatusCodeResult(432);
            }
        }
    }
}