﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthTokenRequestDto.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Dto.Account
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The auth token request dto.
    /// </summary>
    public class AuthTokenRequestDto
    {
        /// <summary>
        /// Gets or sets the device id.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        [MaxLength(100)]
        public string UserName { get; set; }
    }
}