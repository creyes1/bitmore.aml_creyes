﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthTokenResponseDto.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Dto.Account
{
    using System;

    /// <summary>
    /// The auth token view model.
    /// </summary>
    public class AuthTokenResponseDto
    {
        /// <summary>
        /// Gets or sets the authorization token.
        /// </summary>
        public string AuthorizationToken { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}