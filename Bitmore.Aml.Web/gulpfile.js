﻿var gulp = require("gulp");
var less = require("gulp-less");
var path = require("path");
var plumber = require("gulp-plumber");
var paths = {
    less: ["./Content/css/**/*.less", "!./Content/css/**/*variables.less"]
};
gulp.task("less", function () {
    return gulp.src(paths.less)
    .pipe(plumber())
      .pipe(less())
      .pipe(gulp.dest("./Content/css/dist/"));
});