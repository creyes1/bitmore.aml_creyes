// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpHelper.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The HttpHelper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Helpers
{
    #region

    using System;
    using System.Linq;
    using System.Web.Routing;

    using Bitmore.Aml.Web.Infrastructure;

    #endregion

    /// <summary>
    /// The HttpHelper interface.
    /// </summary>
    public interface IHttpHelper
    {
        /// <summary>
        /// The get uri.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="Uri"/>.
        /// </returns>
        Uri GetUri(string path = null);

        /// <summary>
        /// The is ajax request.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsAjaxRequest();

        /// <summary>
        /// The get app path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetAppPath();

        /// <summary>
        /// The get url.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetUrl(string action, string controller);
    }

    /// <summary>
    /// The http helper.
    /// </summary>
    public class HttpHelper : IHttpHelper
    {
        /// <summary>
        /// The url format.
        /// </summary>
        private const string UrlFormat = "{0}://{1}{2}";

        /// <summary>
        /// The controller action format.
        /// </summary>
        private const string ControllerActionFormat = @"/{0}/{1}";

        /// <summary>
        /// The controller name.
        /// </summary>
        private const string ControllerName = @"Controller";

        /// <summary>
        /// The action name.
        /// </summary>
        private const string ActionName = @"Action";

        /// <summary>
        /// The Authentication.
        /// </summary>
        private readonly IAuthentication authentication;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpHelper"/> class.
        /// </summary>
        /// <param name="authentication">
        /// The Authentication.
        /// </param>
        public HttpHelper(IAuthentication authentication)
        {
            this.authentication = authentication;
        }

        /// <summary>
        /// The get uri.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="Uri"/>.
        /// </returns>
        public Uri GetUri(string path = null)
        {
            var request = this.authentication.GetOwinContext().Request;
            var fullurl = string.Format(UrlFormat, request.Scheme, request.Host.Value, path ?? request.Path.ToString());
            var builder = new UriBuilder(fullurl);
            return builder.Uri;
        }

        /// <summary>
        /// The get full url.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetUrl(string action, string controller)
        {
            var uri = this.GetUri(string.Format(ControllerActionFormat, controller, action));
            return uri.ToString();
        }

        /// <summary>
        /// The get url.
        /// </summary>
        /// <param name="routes">
        /// The routes.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetUrl(RouteValueDictionary routes)
        {
            var controllerName = routes.FirstOrDefault(r => r.Key.ToLower() == ControllerName.ToLower()).Value.ToString();
            var actionName = routes.FirstOrDefault(r => r.Key.ToLower() == ActionName.ToLower()).Value.ToString();
            return this.GetUrl(controllerName, actionName);
        }

        /// <summary>
        /// The get app path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAppPath()
        {
            var url = string.Empty;
            var uri = this.GetUri();
            if (uri != null)
            {
                url = uri.GetLeftPart(UriPartial.Authority);
            }

            return url;
        }

        /// <summary>
        /// The is ajax request.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsAjaxRequest()
        {
            var request = this.authentication.GetOwinContext().Request;
            var query = request.Query;
            if ((query != null) && (query["X-Requested-With"] == "XMLHttpRequest"))
            {
                return true;
            }

            var headers = request.Headers;
            return (headers != null) && (headers["X-Requested-With"] == "XMLHttpRequest");
        }
    }
}