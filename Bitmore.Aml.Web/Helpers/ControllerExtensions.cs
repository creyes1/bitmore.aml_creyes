﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControllerExtensions.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Helpers
{
    #region

    using System;
    using System.Text;
    using System.Web.Http.ModelBinding;

    #endregion

    /// <summary>
    /// The controller extensions.
    /// </summary>
    public static class ControllerExtensions
    {
        /// <summary>
        /// The get errors.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        public static void GetErrors(this ModelStateDictionary dictionary, Action<ModelError> action)
        {
            if (action == null)
            {
                return;
            }

            foreach (var modelState in dictionary.Values)
            {
                foreach (var error in modelState.Errors)
                {
                    action(error);
                }
            }
        }

        /// <summary>
        /// The get errors as string.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetErrorsAsString(this ModelStateDictionary dictionary)
        {
            var result = new StringBuilder();
            dictionary.GetErrors(e => { result.Append(e.ErrorMessage); });
            return result.ToString();
        }
    }
}