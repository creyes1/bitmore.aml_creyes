// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControllerHelper.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The ControllerHelper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Helpers
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    using AutoMapper;

    using Bitmore.Aml.Domain.Config;
    using Bitmore.Aml.Domain.Utils;

    #endregion

    /// <summary>
    /// The ControllerHelper interface.
    /// </summary>
    public interface IControllerHelper
    {
        /// <summary>
        /// The action.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string Action<TController>(Expression<Action<TController>> action) where TController : Controller;

        /// <summary>
        /// The action.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string Action<TController>(UrlHelper url, Expression<Action<TController>> action) where TController : Controller;

        /// <summary>
        /// The create filter to jq grid.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        IEnumerable<ExpandoObject> CreateFilterToGrid(IEnumerable<string> list);

        /// <summary>
        /// The get controller name.
        /// </summary>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetControllerName<TController>() where TController : Controller;

        /// <summary>
        /// The get route values.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="RouteValueDictionary"/>.
        /// </returns>
        RouteValueDictionary GetRouteValues<TController>(Expression<Action<TController>> action)
            where TController : Controller;

        /// <summary>
        /// The get temporary file.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <param name="fileDownloadName"></param>
        /// <param name="deleteFile"></param>
        /// <param name="createFile"></param>
        /// <returns>
        /// The <see cref="FileContentResult"/>.
        /// </returns>
        FileContentResult GetTemporaryFile(
            string filename,
            string fileDownloadName,
            bool deleteFile = true,
            Func<bool> createFile = null);

        /// <summary>
        /// Get the real temp path 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        string GetRealTempPath(string filename);

        /// <summary>
        /// The get current action.
        /// </summary>
        /// <param name="controllerContext">
        /// The controller context.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetCurrentAction(ControllerContext controllerContext);

        /// <summary>
        /// The get file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="fileDownloadName">
        /// The file download name.
        /// </param>
        /// <param name="deleteFile">
        /// The delete file.
        /// </param>
        /// <param name="createFile">
        /// The create file.
        /// </param>
        /// <returns>
        /// The <see cref="FileContentResult"/>.
        /// </returns>
        FileContentResult GetFile(
            string path,
            string fileDownloadName,
            bool deleteFile = true,
            Func<bool> createFile = null);
    }

    /// <summary>
    /// The controller helpers.
    /// </summary>
    public class ControllerHelper : IControllerHelper
    {
        /// <summary>
        /// The action name.
        /// </summary>
        private const string ActionName = @"Action";

        /// <summary>
        /// The controller name.
        /// </summary>
        private const string ControllerName = @"Controller";

        /// <summary>
        /// The xls content type.
        /// </summary>
        private readonly IDictionary<string, string> contentTypes = 
            new Dictionary<string, string>
                {
                    { ".xlsx", @"application/vnd.ms-excel.12" },
                    { ".jpg", @"image/jpeg" },
                    { ".png", @"image/png" },
                    { ".svg", "image/svg+xml; charset=utf-8" },
                    { ".csv", "text/csv" },
                    { ".txt", "text/plain" }
                };

        /// <summary>
        /// The app configuration.
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerHelper"/> class. 
        /// </summary>
        /// <param name="appConfiguration"></param>
        public ControllerHelper(
            IAppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        /// <summary>
        /// The action.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Action<TController>(Expression<Action<TController>> action) where TController : Controller
        {
            var controllerAndAction = this.GetControllerAndActionName(action);
            var helper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return helper.Action(controllerAndAction.Item2, controllerAndAction.Item1);
        }

        /// <summary>
        /// The action.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Action<TController>(UrlHelper url, Expression<Action<TController>> action) where TController : Controller
        {
            var controllerAndAction = this.GetControllerAndActionName(action);
            return url.Action(controllerAndAction.Item2, controllerAndAction.Item1);
        }

        /// <summary>
        /// The create filter to jq grid.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public IEnumerable<ExpandoObject> CreateFilterToGrid(IEnumerable<string> list)
        {
            return this.CreateFilterToJqGrid(list, x => x, x => x);
        }

        /// <summary>
        /// The create filter to jq grid.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public IEnumerable<ExpandoObject> CreateFilterToJqGrid<T>(
            IEnumerable<T> list,
            Expression<Func<T, object>> value,
            Expression<Func<T, object>> text)
        {
            var enumerable = list as T[] ?? list.ToArray();
            if (!enumerable.Any())
            {
                return null;
            }

            var returnlist = new List<ExpandoObject>();
            if (typeof(T).IsValueType || typeof(T) == typeof(string))
            {
                enumerable.ToList().ForEach(
                    i =>
                        {
                            var element = new ExpandoObject() as dynamic;
                            element.Key = i.ToString();
                            element.Value = i.ToString();
                            returnlist.Add(element);
                        });
            }
            else
            {
                enumerable.ToList().ForEach(
                    i =>
                        {

                            var key = value.Compile().Invoke(i).ToString();
                            var evalue = text.Compile().Invoke(i).ToString();

                            var element = new ExpandoObject() as dynamic;
                            element.Key = key;
                            element.Value = evalue;
                            returnlist.Add(element);
                        });
            }

            return returnlist;
        }

        /// <summary>
        /// The get url.
        /// </summary>
        /// <param name="routes">
        /// The routes.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public Tuple<string, string> GetControllerAndActionName(RouteValueDictionary routes)
        {
            var controllerName =
                routes.FirstOrDefault(r => r.Key.ToLower() == ControllerName.ToLower()).Value.ToString();
            var actionName = routes.FirstOrDefault(r => r.Key.ToLower() == ActionName.ToLower()).Value.ToString();
            return new Tuple<string, string>(controllerName, actionName);
        }

        /// <summary>
        /// The get controller and action name.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        public Tuple<string, string> GetControllerAndActionName<TController>(Expression<Action<TController>> action)
            where TController : Controller
        {
            var routes = this.GetRouteValues(action);
            return this.GetControllerAndActionName(routes);
        }

        /// <summary>
        /// The get controller name.
        /// </summary>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public string GetControllerName<TController>() where TController : Controller
        {
            return this.GetControllerName(typeof(TController));
        }

        /// <summary>
        /// The get controller name.
        /// </summary>
        /// <param name="controllerType">
        /// The controller type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public string GetControllerName(Type controllerType)
        {
            var controllerName = controllerType.Name;
            if (!controllerName.EndsWith("Controller", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("Control name don't ends with 'Controller'");
            }

            controllerName = controllerName.Substring(0, controllerName.Length - "Controller".Length);
            if (controllerName.Length == 0)
            {
                throw new ArgumentException("CannotRouteToController ");
            }

            return controllerName;
        }

        /// <summary>
        /// The get route values.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="TController">
        /// </typeparam>
        /// <returns>
        /// The <see cref="RouteValueDictionary"/>.
        /// </returns>
        public RouteValueDictionary GetRouteValues<TController>(Expression<Action<TController>> action)
            where TController : Controller
        {
            var methodName = ExtendedMethods.GetMemberName(action);
            var controllerName = this.GetControllerName(typeof(TController));
            var routeValues = new RouteValueDictionary
                                  {
                                      { ControllerName, controllerName }, 
                                      { ActionName, methodName }
                                  };
            return routeValues;
        }

        /// <summary>
        /// The get temporary file.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <param name="fileDownloadName"></param>
        /// <param name="deleteFile"></param>
        /// <param name="createFile"></param>
        /// <returns>
        /// The <see cref="FileContentResult"/>.
        /// </returns>
        public FileContentResult GetTemporaryFile(string filename, string fileDownloadName, bool deleteFile = true, Func<bool> createFile = null)
        {
            var path = this.GetRealTempPath(filename);
            return this.GetFile(path, fileDownloadName, deleteFile, createFile);
        }

        /// <summary>
        /// The get file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="fileDownloadName">
        /// The file download name.
        /// </param>
        /// <param name="deleteFile">
        /// The delete file.
        /// </param>
        /// <param name="createFile">
        /// The create file.
        /// </param>
        /// <returns>
        /// The <see cref="FileContentResult"/>.
        /// </returns>
        public FileContentResult GetFile(string path, string fileDownloadName, bool deleteFile = true, Func<bool> createFile = null)
        {
            if (!File.Exists(path) && (createFile == null || !createFile()))
            {
                return null;
            }

            var bytes = File.ReadAllBytes(path);
            var file = new FileContentResult(bytes, this.GetContentType(Path.GetFileName(path)))
            {
                FileDownloadName = fileDownloadName
            };

            if (deleteFile)
            {
                File.Delete(path);
            }

            return file;
        }

        /// <summary>
        /// Get the content file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetContentType(string filename)
        {
            var ext = Path.GetExtension(filename);
            return ext != null ? this.contentTypes[ext.ToLower()] : null;
        }

        /// <summary>
        /// The get real temp path method
        /// </summary>
        /// <returns></returns>
        public string GetRealTempPath(string filename)
        {
            var realpath = $@"{this.appConfiguration.AppEnvironment.ApplicationBasePath}\{this.appConfiguration.AppEnvironment.TemporaryFolderPath}";

            // ReSharper disable once AssignNullToNotNullAttribute
            return Path.Combine(realpath, filename);
        }

        /// <summary>
        /// The get current action.
        /// </summary>
        /// <param name="controllerContext">
        /// The controller Context.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetCurrentAction(ControllerContext controllerContext)
        {
            const string Action = "action";
            const string Controller = "controller";

            var actionName = controllerContext.RouteData.Values[Action].ToString();
            var controllerName = controllerContext.RouteData.Values[Controller].ToString();
            return $"/{controllerName}/{actionName}";
        }
    }
}