// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HtmlHelper.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The HtmlHelper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Helpers
{
    #region

    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Bitmore.Aml.Domain.DataObject;
    using Bitmore.Aml.Services.Services;
    using Bitmore.Aml.Web.Infrastructure;
    using Bitmore.Aml.Web.ViewModel.Account;

    using Newtonsoft.Json;

    #endregion

    /// <summary>
    /// The HtmlHelper interface.
    /// </summary>
    public interface IHtmlHelper
    {
        /// <summary>
        /// The get application path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetApplicationPath();

        /// <summary>
        /// The get notify before days.
        /// </summary>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        Tuple<bool, int> GetNotifyBeforeDays();

        /// <summary>
        /// The set log in class.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string SetCurrentClass();

        /// <summary>
        /// The has children with url.
        /// </summary>
        /// <param name="helper">
        /// The helper.
        /// </param>
        /// <param name="mod">
        /// The mod.
        /// </param>
        /// <param name="urlHelper">
        /// The url helper.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool HasChildrenWithUrl(System.Web.Mvc.HtmlHelper helper, AppModuleMenu mod, UrlHelper urlHelper);

        /// <summary>
        /// The is menu url.
        /// </summary>
        /// <param name="helper">
        /// The helper.
        /// </param>
        /// <param name="mod">
        /// The mod.
        /// </param>
        /// <param name="urlHelper">
        /// The url helper.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsMenuUrl(System.Web.Mvc.HtmlHelper helper, AppModuleMenu mod, UrlHelper urlHelper);

        /// <summary>
        /// The Json Net
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        string JsonNet(object obj);

        /// <summary>
        /// The get global options
        /// </summary>
        /// <returns></returns>
        string GetGlobalOptions();

        /// <summary>
        /// The is log in page.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsLogInPage();

        /// <summary>
        /// The contains app module.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ContainsAppModule();
    }

    /// <summary>
    /// The html helper.
    /// </summary>
    public class HtmlHelper : IHtmlHelper
    {
        /// <summary>
        /// The no app pages.
        /// </summary>
        private static readonly string[] noAppPages = { "/account/login", "/account/requestpassword" };

        /// <summary>
        /// The authentication.
        /// </summary>
        private readonly IAuthentication authentication;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlHelper"/> class.
        /// </summary>
        /// <param name="authentication">
        /// The authentication.
        /// </param>
        /// <param name="userService">
        /// The user Service.
        /// </param>
        public HtmlHelper(IAuthentication authentication, IUserService userService)
        {
            this.authentication = authentication;
            this.userService = userService;
        }

        /// <summary>
        /// The get application path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetApplicationPath()
        {
            var path = HttpContext.Current.Request.ApplicationPath;
            if (path != string.Empty && path != "/")
            {
                path = path + '/';
            }
            else
            {
                path = string.Empty;
            }

            return path;
        }

        /// <summary>
        /// The get notify before days.
        /// </summary>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        public Tuple<bool, int> GetNotifyBeforeDays()
        {
            var userId = this.authentication.GetCurrentUserId();
            return this.userService.GetNotifyBeforeDays(userId);
        }

        /// <summary>
        /// The is log in page.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public string SetCurrentClass()
        {
            return this.IsLogInPage() ? "login" : "default";
        }

        /// <summary>
        /// The is log in page.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsLogInPage()
        {
            var path = HttpContext.Current.Request.Path.ToLower();
            return path.StartsWith("/account/login") || path == @"/";
        }

        /// <summary>
        /// The contains app module.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsAppModule()
        {
            var path = HttpContext.Current.Request.Path.ToLower();
            return !noAppPages.Any(x => path.StartsWith(x));
        }

        /// <summary>
        /// The has children with url.
        /// </summary>
        /// <param name="helper">
        /// The helper.
        /// </param>
        /// <param name="mod">
        /// The mod.
        /// </param>
        /// <param name="urlHelper">
        /// The url helper.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasChildrenWithUrl(System.Web.Mvc.HtmlHelper helper, AppModuleMenu mod, UrlHelper urlHelper)
        {
            if (mod == null)
            {
                return false;
            }

            if (mod.ChildrenAppModules == null || !mod.ChildrenAppModules.Any())
            {
                return this.IsMenuUrl(helper, mod, urlHelper);
            }

            return mod.ChildrenAppModules.Any(x => this.IsMenuUrl(helper, x, urlHelper));
        }

        /// <summary>
        /// The is menu url.
        /// </summary>
        /// <param name="helper">
        /// The helper.
        /// </param>
        /// <param name="mod">
        /// The mod.
        /// </param>
        /// <param name="urlHelper">
        /// The url helper.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsMenuUrl(System.Web.Mvc.HtmlHelper helper, AppModuleMenu mod, UrlHelper urlHelper)
        {
            if (mod?.TypeOrUrl == null)
            {
                return false;
            }

            return helper.ViewContext.HttpContext.Request.Url.AbsolutePath.ToLower() == urlHelper.Content(mod.TypeOrUrl).ToLower();
        }

        /// <summary>
        /// Convert to JsonNet
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string JsonNet(object obj)
        {
            var settings = JsonDotNetResult.Settings;
            return JsonConvert.SerializeObject(obj, settings);
        }

        /// <summary>
        /// Get global options
        /// </summary>
        /// <returns></returns>
        public string GetGlobalOptions()
        {
            var result =
                new
                {
                    Url = this.GetApplicationPath(),
                    CurrentCulture = this.authentication.GetCurrentCultureInfo().Name
                };
            return this.JsonNet(result);
        }
    }
}