// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HtmlExtensions.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Web.Helpers
{
    #region

    using System;
    using System.Diagnostics;
    using System.Text;
    using System.Web.Mvc;

    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The html extensions.
    /// </summary>
    public static class HtmlExtensions
    {
        /// <summary>
        /// Gets or sets the current version.
        /// </summary>
        private static string CurrentVersion { get; set; }

        /// <summary>
        /// The input search.
        /// </summary>
        /// <param name="htmlHelper">
        /// The html helper.
        /// </param>
        /// <param name="stsearch">
        /// The stsearch.
        /// </param>
        /// <param name="classes">
        /// The classes.
        /// </param>
        /// <param name="hide">
        /// The hide.
        /// </param>
        /// <param name="filterOp"></param>
        /// <returns>
        /// The <see cref="MvcHtmlString"/>.
        /// </returns>
        public static MvcHtmlString InputSearch(
            this System.Web.Mvc.HtmlHelper htmlHelper, 
            string stsearch, 
            string classes = null, 
            bool hide = false,
            string filterOp = "cn")
        {
            var thclass = hide.GetHiddenClass();
            return
                Draw($@"<th class='{thclass}'>
                            <input class='form-control input-sm {classes}' st-search='{stsearch}' st-search-filter-op='{ filterOp }' />
                        </th>");
        }

        /// <summary>
        /// The select search.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="stsearch"></param>
        /// <param name="collectionName"></param>
        /// <param name="propertyText"></param>
        /// <param name="classes"></param>
        /// <param name="propertyValue"></param>
        /// <param name="hide"></param>
        /// <param name="filterOp"></param>
        /// <returns></returns>
        public static MvcHtmlString SelectSearch(
            this System.Web.Mvc.HtmlHelper htmlHelper,
            string stsearch,
            string collectionName,
            string propertyText,
            string classes = null,
            string propertyValue = null,
            bool hide = false,
            string filterOp = "eq")
        {
            var thclass = hide.GetHiddenClass();
            propertyValue = propertyValue ?? (char.ToLowerInvariant(stsearch[0]) + stsearch.Substring(1));
            return Draw($@"<th class='{thclass}'>
	                            <select st-search='{ stsearch }' st-search-filter-op='{ filterOp }' class='form-control input-sm { classes }'>
		                            <option value=''>{ Global.ViewSharedAllElements }</option>
		                            <option ng-repeat='row in ctrl.vm.config.{ collectionName }' value='{{{{row.{ propertyValue }}}}}'>{{{{row.{ propertyText }}}}}</option>
	                            </select>
                            </th>");
        }

        /// <summary>
        /// The get hidden class
        /// </summary>
        /// <param name="hide"></param>
        /// <returns></returns>
        private static string GetHiddenClass(this bool hide)
        {
            return hide ? "hidden" : string.Empty;
        }

        /// <summary>
        /// The input search.
        /// </summary>
        /// <param name="htmlHelper">
        /// The html helper.
        /// </param>
        /// <param name="stsearch">
        /// The stsearch.
        /// </param>
        /// <param name="filterOp"></param>
        /// <returns>
        /// The <see cref="MvcHtmlString"/>.
        /// </returns>
        public static MvcHtmlString InputSearch(this System.Web.Mvc.HtmlHelper htmlHelper, string[] stsearch, string filterOp = "cn")
        {
            var builder = new StringBuilder();
            foreach (var value in stsearch)
            {
                builder.Append(
                    string.IsNullOrEmpty(value)
                        ? "<th></th>"
                        : $"<th><input class='form-control input-sm' st-search='{value}' st-search-filter-op='{ filterOp }' /></th>");
            }

            return Draw(builder.ToString());
        }

        /// <summary>
        /// The draw.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="MvcHtmlString"/>.
        /// </returns>
        private static MvcHtmlString Draw(string value)
        {
            return new MvcHtmlString(value);
        }

        public static string GetVersion()
        {
            if (CurrentVersion != null)
            {
                return CurrentVersion;
            }

            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            // ReSharper disable once AssignNullToNotNullAttribute
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            return CurrentVersion = fvi.FileVersion;
        }

        public static string ContentVs(this UrlHelper urlHelper, string contentPath)
        {
            return urlHelper.Content($"{contentPath}?v={GetVersion()}");
        }
    }
}