/* Register module */
(function (ng, undefined) {
    "use strict";
    ng.module("smart-table")
    .directive("stRatio", [function () {
        return {
            require: "^stTable",
            link: function (scope, element, attr, ctrl) {
                var ratio = +(attr.stRatio);
                element.css("width", ratio + "%");
            }
        };
    }]);
})(angular);

(function (ng, undefined) {
    "use strict";
    ng.module("smart-table")
    .directive("stSearchFilterOp", [function () {
      return {
          require: "^stTable",
          link: function (scope, element, attr, ctrl) {
              var tableState = ctrl.tableState();
              if (undefined === tableState.stSearchFilterOp) {
                  tableState.stSearchFilterOp = {};
              }

              var attrName = attr.stSearch;
              if (undefined === attrName) {
                  attrName = attr.stSearchFilterOpField;
              }

              tableState.stSearchFilterOp[attrName] = attr.stSearchFilterOp;
          }
      };
  }]);
})(angular);