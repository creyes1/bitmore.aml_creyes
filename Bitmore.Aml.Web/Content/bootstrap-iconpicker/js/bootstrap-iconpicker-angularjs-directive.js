angular
  .module('bootstrap-iconpicker', [])
  .directive('bootstrapIconpicker', [ function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        options: '=?options'
      },
      link: function(scope, element, attrs, ngModel) {

        var options = {};
        if (scope.options){
          options = scope.$eval(attrs.options);
        }

        // state -> view
        ngModel.$render = function() {
          element.iconpicker('setIcon', ngModel.$viewValue);
        };

        // view -> state
        element
            .on("change", function (evt) {
              ngModel.$setViewValue(evt.icon);
            });

        element.iconpicker(options);
      }
    };
  }]);