(function ($) {
    $(document).ready(function () {
        var $menu = $(".sidenav-menu");
        var $html = $("html");
        var $targetToggled = $(".target-toggled");
        var $overlay = $("<div class='toggled-overlay'></div>");
        $targetToggled.append($overlay);

        $(".sidenav-open").click(function (e) {
            e.preventDefault();
            $targetToggled.toggleClass("toggled");
            $menu.width(250);
            $html.css("overflow", "hidden");
        });

        var fnclose = function(e)
        {
            e.preventDefault();
            $menu.width(0);
            $targetToggled.removeClass("toggled");
            $html.css("overflow", "initial");
        }

        $(".sidenav-close").click(fnclose);
        $overlay.click(fnclose);
    });
}(jQuery));