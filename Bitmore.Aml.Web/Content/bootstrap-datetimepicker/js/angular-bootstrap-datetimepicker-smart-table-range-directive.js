(function (ng, undefined) {
    "use strict";
    ng.module("smart-table")
  .directive("stSearchDateTimePickerRange", ["stConfig", "$timeout", "$parse", function (stConfig, $timeout, $parse) {
      return {
          require: "^stTable",
          link: function (scope, element, attr, ctrl) {
              var tableCtrl = ctrl;
              var promise = null;
              var throttle = attr.stDelay || stConfig.search.delay;

              var input = element.find("input");
              var $firstInput = $(input[0]);
              var $secondInput = $(input[1]);
              var setDateVale = function () {
                  if (($firstInput.val() && !$secondInput.val()) || !$firstInput.val() && $secondInput.val()) {
                      return;
                  }

                  var finalValue = "";
                  if ($firstInput.val() && $secondInput.val()) {
                      finalValue = `${$firstInput.val()} - ${$secondInput.val()}`;
                  }

                  if (promise !== null) {
                      $timeout.cancel(promise);
                  }

                  promise = $timeout(function () {
                      tableCtrl.search(finalValue, attr.stSearchDateTimePickerRange || "");
                      promise = null;
                  }, throttle);
              };

              $secondInput.data("DateTimePicker").useCurrent(false);
              $firstInput.on("dp.change", function (evt) {
                  $secondInput.data("DateTimePicker").minDate(evt.date);
                  setDateVale();
              });
              $secondInput.on("dp.change", function (evt) {
                  $firstInput.data("DateTimePicker").maxDate(evt.date);
                  setDateVale();
              });
          }
      };
  }]);
})(angular);