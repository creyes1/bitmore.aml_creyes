(function (ng, undefined) {
    "use strict";
    ng.module("smart-table")
  .directive("stSearchDateTimePicker", ["stConfig", "$timeout", "$parse", function (stConfig, $timeout, $parse) {
      return {
          require: "^stTable",
          link: function (scope, element, attr, ctrl) {
              var tableCtrl = ctrl;
              var promise = null;
              var throttle = attr.stDelay || stConfig.search.delay;
              var event = attr.stInputEvent || stConfig.search.inputEvent;

              attr.$observe("stSearchDateTimePicker", function (newValue, oldValue) {
                  var input = element[0].value;
                  if (newValue !== oldValue && input) {
                      ctrl.tableState().search = {};
                      tableCtrl.search(input, newValue);
                  }
              });

              // table state -> view
              scope.$watch(function () {
                  return ctrl.tableState().search;
              }, function (newValue, oldValue) {
                  var predicateExpression = attr.stSearchDateTimePicker || "$";
                  if (newValue.predicateObject && $parse(predicateExpression)(newValue.predicateObject) !== element[0].value) {
                      var value = $parse(predicateExpression)(newValue.predicateObject) || "";
                      element.data("DateTimePicker").date(value);
                  }
              }, true);

              // view -> table state
              element
                  .on("dp.change", function (evt) {
                      evt = evt.originalEvent || evt;
                      if (promise !== null) {
                          $timeout.cancel(promise);
                      }

                      promise = $timeout(function () {
                          //new Date(evt.target.value)
                          tableCtrl.search(evt.target.value, attr.stSearchDateTimePicker || "");
                          promise = null;
                      }, throttle);
                  });
          }
      };
  }]);
})(angular);