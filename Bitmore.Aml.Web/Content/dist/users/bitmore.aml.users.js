/// <reference path="../bitmore.aml.controllers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Users;
(function (Users) {
    var ControllerBase = Controllers.ControllerBase;
    var UserTokensController = UserTokens.UserTokensController;
    var UserDetailModalController = (function (_super) {
        __extends(UserDetailModalController, _super);
        function UserDetailModalController() {
            _super.apply(this, arguments);
        }
        UserDetailModalController.prototype.init = function (config) {
            _super.prototype.init.call(this, config);
            if (this.vm.data && this.vm.data.userId) {
                this.fillCatalogByStr("currentUserStatus", "userStatusId");
                this.fillCatalogByStr("currentLanguage", "defaultLanguage", "name");
                this.fillCatalogByStr("currentRole", "currentRoleId", "roleId");
            }
            else {
                if (!this.vm.data.defaultLanguage && this.vm.config.currentLanguage.length === 1) {
                    this.vm.data.defaultLanguage = this.vm.config.currentLanguage[0].name;
                }
            }
        };
        UserDetailModalController.prototype.onLinkCompany = function () {
            var _this = this;
            UserCompany.ModalCompanyAssignamentController.addModalCompany(this.modalEngineService, this.vm.data)
                .result.then(function (result) {
                if (result) {
                    angular.merge(_this.vm.data, result);
                }
            });
        };
        return UserDetailModalController;
    }(Controllers.DetailModalController));
    Users.UserDetailModalController = UserDetailModalController;
    var UserController = (function (_super) {
        __extends(UserController, _super);
        function UserController() {
            _super.apply(this, arguments);
        }
        UserController.prototype.getUrlEdit = function () { return "/Account/GetUser"; };
        ;
        UserController.prototype.getUrlGrid = function () { return "/Account/GetUsers"; };
        ;
        UserController.prototype.getUrlSave = function () { return "/Account/Save"; };
        ;
        UserController.prototype.getUrlExportToXlsx = function () { return "/App/ExportToXlsx?name=UsersGrid"; };
        ;
        UserController.prototype.gridPostParam = function (row) {
            return { UserId: row.userId };
        };
        UserController.prototype.parseModalSettings = function (settings) {
            settings.windowClass = "modal-wide";
        };
        UserController.prototype.onSendPass = function (index, row) {
            this.confirmAndPost(this.vm.config.lblMsgTableChngPass, "/Account/SendPasswordById", null, JSON.stringify({ userId: row.userId }));
        };
        UserController.prototype.onEditTokens = function (index, row) {
            this.showTokensModal(row);
        };
        UserController.prototype.showTokensModal = function (data) {
            var _this = this;
            return UserTokensController.addModalTokens(this.modalEngineService, data).result.then(function (result) {
                if (result) {
                    // Reload data
                    _this.loadData(_this.tableState);
                }
            });
        };
        return UserController;
    }(ControllerBase));
    Users.UserController = UserController;
    // ***************
    // * ViewModels  *
    // ***************
    var UserRowViewModel = (function () {
        function UserRowViewModel() {
            this.roles = new Array();
        }
        return UserRowViewModel;
    }());
    Users.UserRowViewModel = UserRowViewModel;
    // ***************
    // *  Register   *
    // ***************
    angular
        .module("app", ["jcs-autoValidate", "ui.bootstrap", "smart-table", "datetimepicker", "checklist-model", "toastr", "signlRMessages", "selectize"])
        .controller("userTokensController", UserTokens.UserTokensController)
        .controller("userController", UserController)
        .controller("detailInstanceCtrl", UserDetailModalController)
        .controller("modalCompanyAssignamentCtrl", UserCompany.ModalCompanyAssignamentController)
        .run(["defaultErrorMessageResolver", Common.defaultErrorMessageResolver])
        .service("smartGridService", SmartGrid.SmartGridService)
        .service("modalEngineService", Common.ModalService)
        .service("commonService", Common.CommonService)
        .config(["toastrConfig", function (toastrConfig) { return Common.toastrConfig(toastrConfig); }]);
})(Users || (Users = {}));
//# sourceMappingURL=bitmore.aml.users.js.map