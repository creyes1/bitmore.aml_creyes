/// <reference path="../bitmore.aml.controllers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var UserCompany;
(function (UserCompany) {
    // ***************
    // * Controllers *
    // ***************
    var ModalCompanyAssignamentController = (function (_super) {
        __extends(ModalCompanyAssignamentController, _super);
        function ModalCompanyAssignamentController() {
            _super.apply(this, arguments);
        }
        ModalCompanyAssignamentController.prototype.init = function (config) {
            var _this = this;
            _super.prototype.init.call(this, config);
            this.vm.config.companySelectizeConfig = {
                maxItems: 1,
                valueField: 'companyId',
                labelField: 'name',
                searchField: 'name',
                placeholder: this.vm.config.lblViewAccountUsersCompany,
                load: function (query, callback) {
                    if (!query.length || query.length < 3) {
                        return callback();
                    }
                    _this.$http.post(_this.commonService.getUrl("/Account/GetCompaniesByFilter"), JSON.stringify({ term: query }))
                        .success(function (jsonResult) {
                        callback(jsonResult);
                    });
                    return callback();
                },
                onItemAdd: function (value, $item) {
                    _this.vm.data.companyName = $item["0"].innerText;
                },
                onItemRemove: function (value) {
                    _this.vm.data.companyId = null;
                    _this.vm.data.companyName = null;
                }
            };
        };
        ModalCompanyAssignamentController.prototype.onClose = function () {
            this.$uibModalInstance.close();
        };
        ModalCompanyAssignamentController.prototype.onSaveCompanySelected = function () {
            this.$uibModalInstance.close(this.vm.data);
        };
        ModalCompanyAssignamentController.addModalCompany = function (modalEngineService, data) {
            var modal = {
                windowClass: "right fade modal-sidebar-xs",
                templateUrl: "modalCompanyAssignament.html",
                controller: "modalCompanyAssignamentCtrl as ctrl",
                backdrop: true,
                resolve: {
                    data: angular.copy(data, {}),
                    urlSave: function () { return " "; }
                }
            };
            return modalEngineService.createAndOpen(modal);
        };
        return ModalCompanyAssignamentController;
    }(Controllers.DetailModalController));
    UserCompany.ModalCompanyAssignamentController = ModalCompanyAssignamentController;
})(UserCompany || (UserCompany = {}));
//# sourceMappingURL=bitmore.aml.users.company.js.map