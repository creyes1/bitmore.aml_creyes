/// <reference path="../bitmore.aml.controllers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var UserTokens;
(function (UserTokens) {
    var ControllerBase = Controllers.ControllerBase;
    // ***************
    // * Controllers *
    // ***************
    var UserTokensController = (function (_super) {
        __extends(UserTokensController, _super);
        function UserTokensController($http, $filter, smartGridService, commonService, modalEngineService, data, $uibModalInstance) {
            _super.call(this, $http, $filter, smartGridService, commonService, modalEngineService);
            this.$uibModalInstance = $uibModalInstance;
            this.vm.data = data;
            this.$uibModalInstance = $uibModalInstance;
        }
        UserTokensController.prototype.getUrlEdit = function () { return "/Account/GetUserToken"; };
        ;
        UserTokensController.prototype.getUrlGrid = function () { return "/Account/GetUserTokens?userId=" + this.vm.data.userId; };
        ;
        UserTokensController.prototype.getUrlSave = function () { return "/Account/SaveUserToken"; };
        ;
        UserTokensController.prototype.getUrlExportToXlsx = function () { return "/App/ExportToXlsx?name=UserTokenGrid"; };
        ;
        UserTokensController.prototype.gridPostParam = function (row) {
            return { AccessTokenId: row.accessTokenId };
        };
        UserTokensController.prototype.parseModalSettings = function (settings) {
            settings.windowClass = "modal-wide";
        };
        UserTokensController.prototype.onRevokeToken = function (index, row) {
            var _this = this;
            this.modalEngineService.confirm(this.vm.config.confirmRevokeTokenMsg)
                .result.then(function (result) {
                _this.$http.post(_this.commonService.getUrl("/Account/RevokeToken"), JSON.stringify({ accessTokenId: row.accessTokenId }))
                    .success(function (jsonResult) {
                    if (jsonResult.success) {
                        _this.modalEngineService.success(jsonResult.message);
                        _this.reloadGrid();
                    }
                    else {
                        _this.modalEngineService.alert(jsonResult.message);
                    }
                });
            });
        };
        UserTokensController.prototype.onCreateToken = function () {
            var _this = this;
            var userId = this.vm.data.userId;
            this.modalEngineService.confirm(this.vm.config.confirmGenerateTokenMsg)
                .result.then(function (result) {
                _this.$http.post(_this.commonService.getUrl("/Account/GenerateToken"), JSON.stringify({ userId: userId }))
                    .success(function (jsonResult) {
                    if (jsonResult.success) {
                        _this.modalEngineService.success(jsonResult.message);
                        _this.reloadGrid();
                    }
                    else {
                        _this.modalEngineService.alert(jsonResult.message);
                    }
                });
            });
        };
        UserTokensController.prototype.onCloseTokenModal = function () {
            this.$uibModalInstance.close();
        };
        UserTokensController.addModalTokens = function (modalEngineService, data) {
            var modal = {
                windowClass: "modal-wide",
                templateUrl: "modalTokensEdit.html",
                controller: "userTokensController as ctrl",
                resolve: {
                    data: function () {
                        return data;
                    }
                }
            };
            return modalEngineService.createAndOpen(modal);
        };
        // To solve the minification problem
        UserTokensController.$inject = ["$http", "$filter", "smartGridService", "commonService", "modalEngineService", "data", "$uibModalInstance"];
        return UserTokensController;
    }(ControllerBase));
    UserTokens.UserTokensController = UserTokensController;
    // ***************
    // * ViewModels  *
    // ***************
    var UserTokenRowViewModel = (function () {
        function UserTokenRowViewModel() {
        }
        return UserTokenRowViewModel;
    }());
    UserTokens.UserTokenRowViewModel = UserTokenRowViewModel;
})(UserTokens || (UserTokens = {}));
//# sourceMappingURL=bitmore.aml.users.tokens.js.map