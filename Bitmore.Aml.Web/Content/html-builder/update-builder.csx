#r "$NuGet\HtmlAgilityPack\1.4.9.5\lib\Net45\HtmlAgilityPack.dll"
#r "System.Xml"

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

const string htmlpath = @"C:\Ricardo\Proyectos\Bit.Aml\src-mvc5\Bitmore.Aml.Web\Content\html-builder\builder\index.html";
const string cshtmlpath = @"C:\Ricardo\Proyectos\Bit.Aml\src-mvc5\Bitmore.Aml.Web\Views\Cms\HtmlBuilder.cshtml";
const string builderjspath = @"C:\Ricardo\Proyectos\Bit.Aml\src-mvc5\Bitmore.Aml.Web\Content\html-builder\builder\js\builder.js";

// File content
var html = File.ReadAllText(htmlpath);
var builderjs = File.ReadAllText(builderjspath);

// HtmlAgilityPack Drops Option End Tags (Fix bug)
HtmlAgilityPack.HtmlNode.ElementsFlags.Remove("option");

// Get the html
var parse = new HtmlAgilityPack.HtmlDocument();
parse.LoadHtml(html);

Action writeHtml = () =>
{
    const string HeaderCshtml = @"@model Bitmore.Aml.Web.ViewModel.Cms.HtmlBuilderViewModel
@{
    this.Layout = null;
}";
    parse.Save(cshtmlpath);
    var lines = new List<string>(File.ReadAllLines(cshtmlpath));
    lines.Insert(0, HeaderCshtml);
    File.WriteAllLines(cshtmlpath, lines);
};

Func<HtmlAgilityPack.HtmlNode, string, HtmlAgilityPack.HtmlAttribute> selectorAttr = (n, name) => {
    return n.Attributes.FirstOrDefault(a => a.Name == name && !string.IsNullOrEmpty(a.Value));
};

Action<string, Func<HtmlAgilityPack.HtmlAttribute, bool>> changeAttr = (name, validate) => {
    // Change attr
    var nodes = parse.DocumentNode.SelectNodes($"//*[@{ name }]").Select(n => selectorAttr(n, name)).Where(a => a != null).ToList();
    nodes.ForEach(a => {
        if (validate(a))
        {
            a.Value = $@"@Url.Content(""~/Content/html-builder/builder/{ a.Value }"")";
        }
    });
};

Action<string, string> addClass = (id, newclass) => {
    var allattr = parse.GetElementbyId(id).Attributes;
    var attr = allattr.FirstOrDefault(a => a.Name == "class");
    if (attr == null)
    {
        allattr.Add("class", newclass);
    }
    else
    {
        attr.Value = attr.Value + $" { newclass }";
    }
};

// Change href
changeAttr("href", (a) => !a.Value.Contains("#"));

// Change src
changeAttr("src", (a) => true);

// Change the action path of upload image
parse.GetElementbyId("imageUploadForm").Attributes.FirstOrDefault(a => a.Name == "action").Value = @"@Url.Action(""Upload"", ""Cms"")";

// Change icon
var icon = parse.DocumentNode.SelectSingleNode("//link[@rel='shortcut icon']");
icon.Attributes.FirstOrDefault(a => a.Name == "href").Value = @"@Url.Content(""~/favicon.ico"")";

// Hide buttons and extra elements
addClass("exportPage", "hidden");
addClass("buttonPreview", "hidden");
addClass("pages", "hidden");
addClass("addPage", "hidden");
parse.GetElementbyId("main").SelectNodes("h3").Skip(1).FirstOrDefault().Attributes.Add("class", "hidden");

// Add builder-custom.css
var buildercss = HtmlAgilityPack.HtmlNode.CreateNode("<link href='@Url.Content(\"~/Content/html-builder/builder-custom.css\")' rel='stylesheet'>");
parse.DocumentNode.SelectSingleNode("//head").AppendChild(buildercss);

// Add urlUnique
var urlUnique = HtmlAgilityPack.HtmlNode.CreateNode("<input type='hidden' name='urlUnique' value='@Model.UrlUnique' id='urlUnique' />");
var siteidinput = parse.DocumentNode.SelectSingleNode("//input[@name='siteID']");
siteidinput.ParentNode.InsertBefore(urlUnique, siteidinput);

// Add new buttons
var clearScreen = parse.GetElementbyId("clearScreen");
var closebtn = HtmlAgilityPack.HtmlNode.CreateNode(@"<a href='@Url.Action(""WebPage"", ""Cms"")' class='btn btn-inverse pull-right nohover'><i class='fa fa-times-circle'></i><span>Close</span></a>");
clearScreen.ParentNode.InsertBefore(closebtn, clearScreen);

// Replace elements
builderjs = builderjs.Replace(
    @"$.getJSON('elements.json?v=12345678', function(data){ builderUI.allBlocks = data; builderUI.implementBlocks(); });",
    @"$.getJSON('/Content/html-builder/builder/elements.json?v=12345678', function(data){ builderUI.allBlocks = data; builderUI.implementBlocks(); });");

// Replace site.json
builderjs = builderjs.Replace(
    "$.getJSON(\"site.json\", function (data) {",
    @"var urlUnique = $('#urlUnique').val();
			$.getJSON('/Cms/GetSiteForEdit?urlUnique=' + urlUnique, function(data) {
    ");

// Replace the page title
builderjs = builderjs.Replace(
    @"site.pageTitle.innerHTML = this.name;",
    @"site.pageTitle.innerHTML = typeof this.pageSettings.title !== 'undefined' ? this.pageSettings.title : this.name;");

// Replace the medium-bootstrap path
builderjs = builderjs.Replace(
    @"../css/medium-bootstrap.css",
    @"/Content/html-builder/builder/css/medium-bootstrap.css");

// Replace the url save
builderjs = builderjs.Replace(
    @"_save.php",
    @"/Cms/SaveContent");

// Comment the line of code where inject external scripts
builderjs = builderjs.Replace(
    @"this.frameDocument.querySelector('body').appendChild(newScript);",
    @"//this.frameDocument.querySelector('body').appendChild(newScript);");

// Save files
writeHtml();
File.WriteAllText(builderjspath, builderjs);
