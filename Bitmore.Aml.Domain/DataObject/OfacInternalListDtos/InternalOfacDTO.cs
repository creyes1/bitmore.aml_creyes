﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InternalOfacDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacInternalListDtos
{
    /// <summary>
    /// The internal ofac dto.
    /// </summary>
    public class InternalOfacDTO
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the add_address.
        /// </summary>
        public string add_address { get; set; }

        /// <summary>
        /// Gets or sets the add_city_name.
        /// </summary>
        public string add_city_name { get; set; }

        /// <summary>
        /// Gets or sets the add_country.
        /// </summary>
        public string add_country { get; set; }

        /// <summary>
        /// Gets or sets the add_remarks.
        /// </summary>
        public string add_remarks { get; set; }

        /// <summary>
        /// Gets or sets the alt_name.
        /// </summary>
        public string alt_name { get; set; }

        /// <summary>
        /// Gets or sets the alt_remarks.
        /// </summary>
        public string alt_remarks { get; set; }

        /// <summary>
        /// Gets or sets the alt_type.
        /// </summary>
        public string alt_type { get; set; }

        /// <summary>
        /// Gets or sets the sdn_name.
        /// </summary>
        public string sdn_name { get; set; }

        /// <summary>
        /// Gets or sets the sdn_remarks.
        /// </summary>
        public string sdn_remarks { get; set; }

        #endregion
    }
}