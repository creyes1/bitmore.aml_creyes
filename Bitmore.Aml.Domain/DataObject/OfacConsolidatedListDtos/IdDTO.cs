﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos
{
    /// <summary>
    /// The id dto.
    /// </summary>
    public class IdDTO
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        public string expiration_date { get; set; }

        /// <summary>
        /// Gets or sets the issue_date.
        /// </summary>
        public string issue_date { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        public string number { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string type { get; set; }

        #endregion
    }
}