﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchedFieldsDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The matched fields dto.
    /// </summary>
    public class MatchedFieldsDTO
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchedFieldsDTO"/> class.
        /// </summary>
        public MatchedFieldsDTO()
        {
            this.alt_idx = new List<string>();
            this.name_idx = new List<string>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the alt_idx.
        /// </summary>
        public List<string> alt_idx { get; set; }

        /// <summary>
        /// Gets or sets the name_idx.
        /// </summary>
        public List<string> name_idx { get; set; }

        #endregion
    }
}