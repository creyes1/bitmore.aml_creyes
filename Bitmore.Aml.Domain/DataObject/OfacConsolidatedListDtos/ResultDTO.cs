﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResultDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The result dto.
    /// </summary>
    public class ResultDTO
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultDTO"/> class.
        /// </summary>
        public ResultDTO()
        {
            this.addresses = new List<AddressDTO>();
            this.alt_names = new List<string>();
            this.ids = new List<IdDTO>();
            this.matched_fields = new MatchedFieldsDTO();
            this.programs = new List<string>();
            this.citizenships = new List<string>();
            this.dates_of_birth = new List<string>();
            this.places_of_birth = new List<string>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the addresses.
        /// </summary>
        public List<AddressDTO> addresses { get; set; }

        /// <summary>
        /// Gets or sets the alt_names.
        /// </summary>
        public List<string> alt_names { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// Gets or sets the entity_number.
        /// </summary>
        public string entity_number { get; set; }

        /// <summary>
        /// Gets or sets the federal_register_notice.
        /// </summary>
        public string federal_register_notice { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the ids.
        /// </summary>
        public List<IdDTO> ids { get; set; }

        /// <summary>
        /// Gets or sets the matched_fields.
        /// </summary>
        public MatchedFieldsDTO matched_fields { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the programs.
        /// </summary>
        public List<string> programs { get; set; }

        /// <summary>
        /// Gets or sets the citizenships.
        /// </summary>
        public List<string> citizenships { get; set; }

        /// <summary>
        /// Gets or sets the dates_of_birth.
        /// </summary>
        public List<string> dates_of_birth { get; set; }

        /// <summary>
        /// Gets or sets the places_of_birth.
        /// </summary>
        public List<string> places_of_birth { get; set; }

        /// <summary>
        /// Gets or sets the remarks.
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public string score { get; set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        public string source { get; set; }

        /// <summary>
        /// Gets or sets the source_information_url.
        /// </summary>
        public string source_information_url { get; set; }

        /// <summary>
        /// Gets or sets the source_list_url.
        /// </summary>
        public string source_list_url { get; set; }

        /// <summary>
        /// Gets or sets the start_date.
        /// </summary>
        public string start_date { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string type { get; set; }

        #endregion
    }
}