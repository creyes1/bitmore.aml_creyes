﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddressDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos
{
    /// <summary>
    /// The address dto.
    /// </summary>
    public class AddressDTO
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// Gets or sets the postal_code.
        /// </summary>
        public string postal_code { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string state { get; set; }

        #endregion
    }
}