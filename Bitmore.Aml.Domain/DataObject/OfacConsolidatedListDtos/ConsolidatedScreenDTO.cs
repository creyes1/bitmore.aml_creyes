﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsolidatedScreenDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos
{
    #region

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The consolidated screen dto.
    /// </summary>
    public class ConsolidatedScreenDTO
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsolidatedScreenDTO"/> class.
        /// </summary>
        public ConsolidatedScreenDTO()
        {
            this.results = new List<ResultDTO>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        public int offset { get; set; }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        public List<ResultDTO> results { get; set; }

        /// <summary>
        /// Gets or sets the search_performed_at.
        /// </summary>
        public string search_performed_at { get; set; }

        /// <summary>
        /// Gets or sets the sources_used.
        /// </summary>
        public List<SourcesUsedDTO> sources_used { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public int total { get; set; }

        #endregion
    }
}