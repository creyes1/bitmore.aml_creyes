﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourcesUsedDTO.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject.OfacConsolidatedListDtos
{
    /// <summary>
    /// The sources used dto.
    /// </summary>
    public class SourcesUsedDTO
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the import_rate.
        /// </summary>
        public string import_rate { get; set; }

        /// <summary>
        /// Gets or sets the last_imported.
        /// </summary>
        public string last_imported { get; set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        public string source { get; set; }

        /// <summary>
        /// Gets or sets the source_last_updated.
        /// </summary>
        public string source_last_updated { get; set; }

        #endregion
    }
}