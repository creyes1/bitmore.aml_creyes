﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleMenu.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.DataObject
{
    #region

    using System.Collections.Generic;

    using Bitmore.Aml.Resources.Language;

    #endregion

    /// <summary>
    /// The app module menu.
    /// </summary>
    public class AppModuleMenu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleMenu"/> class.
        /// </summary>
        public AppModuleMenu()
        {
            this.ChildrenAppModules = new List<AppModuleMenu>();
        }

        /// <summary>
        /// Gets or sets the children app modules.
        /// </summary>
        public ICollection<AppModuleMenu> ChildrenAppModules { get; set; }

        /// <summary>
        /// Gets or sets the web icon.
        /// </summary>
        public string CssOrDesktopIcon { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string NameTranslated => Global.ResourceManager.GetString($"ViewSharedMenu{this.Name}");

        /// <summary>
        /// Gets or sets the type or url.
        /// </summary>
        public string TypeOrUrl { get; set; }
    }
}