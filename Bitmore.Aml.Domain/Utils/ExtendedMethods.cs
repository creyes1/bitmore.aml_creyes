// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtendedMethods.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The extended methods.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Utils
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    #endregion

    /// <summary>
    /// The extended methods.
    /// </summary>
    public static class ExtendedMethods
    {
        #region Constants

        /// <summary>
        /// The int 32 string.
        /// </summary>
        private const string Int32String = "Int32";

        /// <summary>
        /// The int string.
        /// </summary>
        private const string IntString = "int";

        /// <summary>
        /// The system format.
        /// </summary>
        private const string SystemFormat = "System.{0}";

        #endregion

        #region Static Fields

        /// <summary>
        /// The primitive.
        /// </summary>
        private static readonly string[] Primitive = { "int", "string", "bool", "boolean" };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The deserialize from xml.
        /// </summary>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="dynamic"/>.
        /// </returns>
        public static dynamic DeserializeFromXml(this string xml, Type type = null)
        {
            if (xml == null || xml.Length <= 0)
            {
                return null;
            }

            var doc = new XmlDocument();
            doc.LoadXml(xml);
            if (doc.DocumentElement != null)
            {
                var name = doc.DocumentElement.Name;
                if (type == null && Primitive.Any(p => p == name))
                {
                    if (name == IntString)
                    {
                        name = Int32String;
                    }

                    type = Type.GetType(string.Format(SystemFormat, char.ToUpper(name[0]) + name.Substring(1)));
                }
            }

            object obj;
            var xmlSerialixer = new XmlSerializer(type);
            using (TextReader tr = new StringReader(xml))
            {
                obj = xmlSerialixer.Deserialize(tr);
            }

            return obj;
        }

        /// <summary>
        /// The deserialize from xml.
        /// </summary>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T DeserializeFromXml<T>(this string xml)
        {
            return (T)xml.DeserializeFromXml(typeof(T));
        }

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="toSerialize">
        /// The to serialize.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string SerializeObject(this object toSerialize)
        {
            var xmlSerializer = new XmlSerializer(toSerialize.GetType());
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// The to expando object.
        /// </summary>
        /// <param name="object">
        /// The object.
        /// </param>
        /// <returns>
        /// The <see cref="ExpandoObject"/>.
        /// </returns>
        public static ExpandoObject ToExpandoObject(this object @object)
        {
            if (@object == null)
            {
                return null;
            }

            var expando = new ExpandoObject();
            var dic = expando as IDictionary<string, object>;
            var properties = @object.GetType().GetProperties();
            properties.ToList().ForEach(p => { dic[p.Name] = p.GetValue(@object, null); });
            return expando;
        }

        /// <summary>
        /// The to object.
        /// </summary>
        /// <param name="object">
        /// The object.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T ToObject<T>(this ExpandoObject @object)
        {
            if (@object == null)
            {
                return default(T);
            }

            var obj = Activator.CreateInstance<T>();
            var dic = @object as IDictionary<string, object>;
            var properties = typeof(T).GetProperties();
            properties.ToList().ForEach(
                p =>
                {
                    if (dic.ContainsKey(p.Name))
                    {
                        p.SetValue(obj, dic[p.Name], null);
                    }
                });
            return obj;
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public static string GetMemberName(Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            var memberExpression = expression as MemberExpression;
            if (memberExpression != null)
            {
                // Reference type property or field
                return memberExpression.Member.Name;
            }

            var callExpression = expression as MethodCallExpression;
            if (callExpression != null)
            {
                // Reference type method
                return callExpression.Method.Name;
            }

            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                // Property, field of method returning value type
                return GetMemberName(unaryExpression);
            }

            throw new ArgumentException("Invalid expression");
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="unaryExpression">
        /// The unary expression.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetMemberName(UnaryExpression unaryExpression)
        {
            if (!(unaryExpression.Operand is MethodCallExpression))
            {
                return ((MemberExpression)unaryExpression.Operand).Member.Name;
            }

            var methodExpression = (MethodCallExpression)unaryExpression.Operand;
            return methodExpression.Method.Name;
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public static string GetMemberName<T>(Expression<Func<T, object>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            return GetMemberName(expression.Body);
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public static string GetMemberName(Expression<Func<object>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            return GetMemberName(expression.Body);
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public static string GetMemberName<T>(Expression<Action<T>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            return GetMemberName(expression.Body);
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetMemberName<T>(this T instance, Expression<Func<T, object>> expression)
        {
            return GetMemberName(expression);
        }

        /// <summary>
        /// The get member name.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetMemberName<T>(this T instance, Expression<Action<T>> expression)
        {
            return GetMemberName(expression);
        }

        /// <summary>
        /// The get property value.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object GetPropertyValue(this object instance, string propertyName)
        {
            var nameParts = propertyName.Split('.');
            if (nameParts.Length == 1)
            {
                return instance.GetType().GetProperty(propertyName).GetValue(instance, null);
            }

            foreach (var part in nameParts)
            {
                if (instance == null)
                {
                    return null;
                }

                var type = instance.GetType();
                var info = type.GetProperty(part);
                if (info == null)
                {
                    return null;
                }

                instance = info.GetValue(instance, null);
            }

            return instance;
        }

        /// <summary>
        /// The is simple type.
        /// </summary>
        /// <param name="typeToCheck">
        /// The type to check.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsSimpleType(this Type typeToCheck)
        {
            var underlyingType = typeToCheck.GetUnderlyingType();
            var typeCode = Type.GetTypeCode(underlyingType);
            switch (typeCode)
            {
                case TypeCode.Boolean:
                case TypeCode.Byte:
                case TypeCode.Char:
                case TypeCode.DateTime:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.String:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (underlyingType == typeof(Guid))
                    {
                        return true;
                    }

                    break;
                default:
                    return false;
            }

            return false;
        }

        /// <summary>
        /// The get underlying type.
        /// </summary>
        /// <param name="typeToCheck">
        /// The type to check.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/>.
        /// </returns>
        public static Type GetUnderlyingType(this Type typeToCheck)
        {
            return typeToCheck.IsNullable() ? Nullable.GetUnderlyingType(typeToCheck) : typeToCheck;
        }

        /// <summary>
        /// The is i enumerable.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsIEnumerable(this Type type)
        {
            return type != null && type.IsGenericType
                   && (type.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                       || type.GetGenericTypeDefinition() == typeof(ICollection<>));
        }

        /// <summary>
        /// The is nullable.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsNullable(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        /// <summary>
        /// The clone.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Clone<T>(this T obj, PropertyInfo[] properties = null) where T : class
        {
            if (obj == null)
            {
                return default(T);
            }

            if (properties == null)
            {
                properties = obj.GetType().GetNotVirtualProperties();
            }

            var newobj = Activator.CreateInstance<T>();
            foreach (var prop in properties)
            {
                var value = prop.GetValue(obj);
                prop.SetValue(newobj, value);
            }

            return newobj;
        }

        /// <summary>
        /// The get not virtual properties.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="PropertyInfo[]"/>.
        /// </returns>
        public static PropertyInfo[] GetNotVirtualProperties(this Type type)
        {
            return type.GetProperties().Where(p => !p.GetGetMethod().IsVirtual).ToArray();
        }

        /// <summary>
        /// The for each.
        /// </summary>
        /// <param name="sequence">
        /// The sequence.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        public static void ForEach<T>(this IEnumerable<T> sequence, Action<int, T> action)
        {
            // argument null checking omitted
            var i = 0;
            foreach (var item in sequence)
            {
                action(i, item);
                i++;
            }
        }

        #endregion
    }
}