// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   Types of logs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Utils
{
    #region

    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;

    using Config;

    using log4net;
    using log4net.Appender;
    using log4net.Config;
    using log4net.Core;
    using log4net.Layout;
    using log4net.Repository.Hierarchy;

    using Microsoft.Extensions.Configuration;

    #endregion

    /// <summary>
    /// Types of logs
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// Actions that users makes, for example update a entity
        /// </summary>
        Activity,

        /// <summary>
        /// The activity unhandled.
        /// </summary>
        ActivityUnhandled,

        /// <summary>
        /// Errors that the application throws
        /// </summary>
        Errors,

        /// <summary>
        /// The info.
        /// </summary>
        Info
    }

    /// <summary>
    /// The LogEngine interface.
    /// </summary>
    public interface ILogEngine
    {
        /// <summary>
        /// The write activity.
        /// </summary>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void WriteActivity(string activity, string username, string data);

        /// <summary>
        /// The write info.
        /// </summary>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void WriteInfo(string activity, string data);

        /// <summary>
        /// The write activity unhandled.
        /// </summary>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void WriteActivityUnhandled(string activity, string username, string data);

        /// <summary>
        /// The write error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        void WriteError(Exception exception);

        /// <summary>
        /// The write error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="username"></param>
        void WriteError(string message, string data, string username);

        /// <summary>
        /// The write error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        void WriteError(Exception exception, string userId);

        /// <summary>
        /// The app types.
        /// </summary>
        string[] AppTypes { get; }

        /// <summary>
        /// The message types.
        /// </summary>
        string[] MessageTypes { get; set; }
    }

    /// <summary>
    /// The log.
    /// </summary>
    public class Log : ILogEngine
    {
        #region Constants

        /// <summary>
        /// The default message.
        /// </summary>
        private const string DefaultMessage = "[unknown]";

        /// <summary>
        /// The app log type string.
        /// </summary>
        private const string AppLogTypeString = "AppLogType";

        /// <summary>
        /// The asp net.
        /// </summary>
        private const string AspNet = "ASP.NET";

        /// <summary>
        /// The desktop.
        /// </summary>
        private const string Desktop = "Desktop";

        /// <summary>
        /// The sql ado net logger.
        /// </summary>
        private const string SqlAdoNetLogger = @"INSERT INTO App.AppLog (
		        [Date],
		        [Thread],
		        [Level],
		        [Logger],
		        [Message],
		        [Data],
		        [UserName],
		        [AppLogType])
	        VALUES (
		        @log_date,
		        @thread,
		        @log_level,
		        @logger,
		        @message,
		        @Data,
		        @UserName,
		        @AppLogType)";

        /// <summary>
        /// The user id.
        /// </summary>
        private const string UserId = "username";

        /// <summary>
        /// The web.
        /// </summary>
        private const string Web = "Web";

        #endregion

        #region Static Fields

        /// <summary>
        /// The loaded Config
        /// </summary>
        private static bool LoadedConfig { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// The app types.
        /// </summary>
        public string[] AppTypes => new[] { Web, Desktop };

        /// <summary>
        /// The message types.
        /// </summary>
        public string[] MessageTypes { get; set; }

        /// <summary>
        /// The config
        /// </summary>
        private readonly IConfiguration config;

        /// <summary>
        /// The app configuration
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Prevents a default instance of the <see cref="Log"/> class from being created.
        /// </summary>
        public Log(IConfiguration config, IAppConfiguration appConfiguration)
        {
            this.config = config;
            this.appConfiguration = appConfiguration;
            this.MessageTypes = Enum.GetNames(typeof(LogType));
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The write activity.
        /// </summary>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void WriteActivity(string activity, string username, string data)
        {
            this.WriteActivityOrUnhandled(LogType.Activity, activity, username, data);
        }

        /// <summary>
        /// The write info.
        /// </summary>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void WriteInfo(string activity, string data)
        {
            var userName = DefaultMessage;
            if (!string.IsNullOrEmpty(Thread.CurrentPrincipal?.Identity.Name))
            {
                userName = Thread.CurrentPrincipal.Identity.Name;
            }

            this.WriteActivityOrUnhandled(LogType.Info, activity, userName, data);
        }

        /// <summary>
        /// The write activity unhandled.
        /// </summary>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void WriteActivityUnhandled(string activity, string username, string data)
        {
            this.WriteActivityOrUnhandled(LogType.ActivityUnhandled, activity, username, data);
        }

        /// <summary>
        /// The write error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public void WriteError(Exception exception)
        {
            this.WriteLog(LogType.Errors, exception.Message, exception);
        }

        /// <summary>
        /// The write error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="username"></param>
        public void WriteError(string message, string data, string username)
        {
            ThreadContext.Properties[UserId] = username;
            this.WriteLog(LogType.Errors, message, new Exception(data));
        }

        /// <summary>
        /// The write error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        public void WriteError(Exception exception, string userId)
        {
            ThreadContext.Properties[UserId] = userId;
            this.WriteLog(LogType.Errors, exception.Message, exception);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get current app type.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetCurrentAppType()
        {
            return AppDomain.CurrentDomain.DynamicDirectory.Contains(AspNet) ? Web : Desktop;
        }

        /// <summary>
        /// The get logger.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="ILog"/>.
        /// </returns>
        private ILog GetLogger(LogType type)
        {
            const string Defaultfile = "log4net.config";
            const string DefaultConnection = "DefaultConnection";

            var currentAppType = GetCurrentAppType();
            ThreadContext.Properties[AppLogTypeString] = currentAppType;

            if (LoadedConfig)
            {
                return LogManager.GetLogger(type.ToString());
            }

            var fileInfo = new FileInfo(string.Concat(this.appConfiguration.AppEnvironment.ApplicationBasePath, Defaultfile));
            if (fileInfo.Exists)
            {
                XmlConfigurator.ConfigureAndWatch(fileInfo);
            }

            // Get the Hierarchy object that organizes the loggers
            var hier = LogManager.GetRepository() as Hierarchy;

            if (hier == null)
            {
                return LogManager.GetLogger(type.ToString());
            }

            foreach (
                var adoNetAppender in
                    hier.GetAppenders()
                        .OfType<AdoNetAppender>()
                        .Where(adoNetAppender => string.IsNullOrEmpty(adoNetAppender.CommandText)))
            {
                var conn = adoNetAppender.ConnectionStringName ?? DefaultConnection;
                adoNetAppender.ConnectionString = this.config.GetConnectionString(conn);
                adoNetAppender.CommandText = SqlAdoNetLogger;
                adoNetAppender.ActivateOptions(); // Refresh settings of appender
            }

            LoadedConfig = true;
            return LogManager.GetLogger(type.ToString());
        }

        /// <summary>
        /// The write activity or unhandled.
        /// </summary>
        /// <param name="logtype">
        /// The logtype.
        /// </param>
        /// <param name="activity">
        /// The activity.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        private void WriteActivityOrUnhandled(LogType logtype, string activity, string username, string data)
        {
            ThreadContext.Properties[UserId] = username;
            this.WriteLog(logtype, activity, data: data);
        }

        /// <summary>
        /// The write log.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        private void WriteLog(LogType type, string message = null, Exception exception = null, string data = null)
        {
            var log = this.GetLogger(type);

            switch (type)
            {
                case LogType.ActivityUnhandled:
                case LogType.Activity:
                case LogType.Info:
                    var excdata = (data != null) ? new Exception(Environment.NewLine + data) : null;
                    log.Info(message, excdata);
                    break;
                case LogType.Errors:
                    log.Error(message, exception);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        #endregion
    }

    /// <summary>
    /// The data layout extended.
    /// </summary>
    public class DataLayoutExtended : LayoutSkeleton
    {
        #region Constants

        /// <summary>
        /// The loggin event.
        /// </summary>
        private const string LogginEvent = "loggingEvent";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The activate options.
        /// </summary>
        public override void ActivateOptions()
        {
        }

        /// <summary>
        /// The format.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="loggingEvent">
        /// The logging event.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public override void Format(TextWriter writer, LoggingEvent loggingEvent)
        {
            if (loggingEvent == null)
            {
                throw new ArgumentNullException(LogginEvent);
            }

            if (loggingEvent.ExceptionObject != null)
            {
                writer.Write(loggingEvent.Level == Level.Error ? loggingEvent.ExceptionObject.ToString() : loggingEvent.ExceptionObject.Message);
            }
        }

        #endregion
    }
}