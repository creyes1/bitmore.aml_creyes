// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppEnvironment.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app environment.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Config.Extensions
{
    /// <summary>
    /// The app environment.
    /// </summary>
    public class AppEnvironment
    {
        /// <summary>
        /// Gets or sets a value indicating whether log javascript errors.
        /// </summary>
        public bool LogJavascriptErrors { get; set; }

        /// <summary>
        /// Gets or sets the path logo.
        /// </summary>
        public string EmailFolderPath { get; set; }

        /// <summary>
        /// Gets or sets the path logo.
        /// </summary>
        public string TemporaryFolderPath { get; set; }

        /// <summary>
        /// Gets or sets the application base path.
        /// </summary>
        public string ApplicationBasePath { get; set; }
    }
}