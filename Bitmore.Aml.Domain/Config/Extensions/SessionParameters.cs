// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SingleSession.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Config.Extensions
{
    /// <summary>
    /// The single session.
    /// </summary>
    public class SessionParameters
    {
        /// <summary>
        /// Gets or sets a value indicating whether single session activated.
        /// </summary>
        public bool SingleSessionActivated { get; set; }

        /// <summary>
        /// Gets or sets the expire time span.
        /// </summary>
        public int ExpireTimeSpan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether SingleSessionShowAlert.
        /// </summary>
        public bool SingleSessionShowAlert { get; set; }
    }
}