// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvalidPasswordAttempt.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Config.Extensions
{
    /// <summary>
    /// The invalid password attempt.
    /// </summary>
    public class InvalidPasswordAttempt
    {
        /// <summary>
        /// Gets or sets a value indicating whether activated.
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// Gets or sets the attempt.
        /// </summary>
        public int Attempt { get; set; }
    }
}