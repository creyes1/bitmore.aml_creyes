// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpirationPassword.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Config.Extensions
{
    /// <summary>
    /// The expiration password.
    /// </summary>
    public class ExpirationPassword
    {
        /// <summary>
        /// Gets or sets a value indicating whether activated.
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// Gets or sets the days.
        /// </summary>
        public int Days { get; set; }

        /// <summary>
        /// Gets or sets the notify before days.
        /// </summary>
        public int NotifyBeforeDays { get; set; }
    }
}