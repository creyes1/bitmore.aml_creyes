// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfiguration.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Config
{
    #region

    using Bitmore.Aml.Domain.Config.Extensions;
    
    #endregion

    /// <summary>
    /// The AppConfigurationExtensions interface.
    /// </summary>
    public interface IAppConfiguration
    {
        /// <summary>
        /// Gets or sets the expiration password.
        /// </summary>
        ExpirationPassword ExpirationPassword { get; set; }

        /// <summary>
        /// Gets or sets the session parameters.
        /// </summary>
        SessionParameters SessionParameters { get; set; }

        /// <summary>
        /// Gets or sets the invalid password attempt.
        /// </summary>
        InvalidPasswordAttempt InvalidPasswordAttempt { get; set; }

        /// <summary>
        /// Gets or sets the app environment.
        /// </summary>
        AppEnvironment AppEnvironment { get; set; }
    }

    /// <summary>
    /// The app configuration extensions.
    /// </summary>
    public class AppConfiguration : IAppConfiguration
    {
        /// <summary>
        /// Gets or sets the expiration password.
        /// </summary>
        public ExpirationPassword ExpirationPassword { get; set; }

        /// <summary>
        /// Gets or sets the session parameters.
        /// </summary>
        public SessionParameters SessionParameters { get; set; }

        /// <summary>
        /// Gets or sets the invalid password attempt.
        /// </summary>
        public InvalidPasswordAttempt InvalidPasswordAttempt { get; set; }

        /// <summary>
        /// Gets or sets the app environment.
        /// </summary>
        public AppEnvironment AppEnvironment { get; set; }
    }
}