// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Company.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The company.
    /// </summary>
    public class Company
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Company"/> class.
        /// </summary>
        public Company()
        {
            this.Users = new List<User>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        public DateTime? CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public virtual ICollection<User> Users { get; set; }

        #endregion
    }
}