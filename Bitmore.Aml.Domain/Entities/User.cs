// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The user.
    /// </summary>
    [GeneratedCode("EF.Reverse.POCO.Generator", "2.15.2.0")]
    public class User
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
            this.AccessTokens = new List<AccessToken>();
            this.HasActiveSession = false;
            this.Roles = new List<Role>();
        }

        /// <summary>
        /// Gets or sets the access tokens.
        /// </summary>
        public virtual ICollection<AccessToken> AccessTokens { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        public Guid? CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the current log in.
        /// </summary>
        public DateTime? CurrentLogIn { get; set; }

        /// <summary>
        /// Gets or sets the current role id.
        /// </summary>
        public Guid CurrentRoleId { get; set; }

        /// <summary>
        /// Gets or sets the default language.
        /// </summary>
        public string DefaultLanguage { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has active session.
        /// </summary>
        public bool HasActiveSession { get; set; }

        /// <summary>
        /// Gets or sets the last log in.
        /// </summary>
        public DateTime? LastLogIn { get; set; }

        /// <summary>
        /// Gets or sets the maternal last name.
        /// </summary>
        public string MaternalLastName { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the password attempt.
        /// </summary>
        public byte PasswordAttempt { get; set; }

        /// <summary>
        /// Gets or sets the password updated on date.
        /// </summary>
        public DateTime? PasswordUpdatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the paternal last name.
        /// </summary>
        public string PaternalLastName { get; set; }

        /// <summary>
        /// Gets or sets the profile image.
        /// </summary>
        public string ProfileImage { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        public virtual Role Role { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        public virtual ICollection<Role> Roles { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        public byte[] RowVersion { get; set; }

        /// <summary>
        /// Gets or sets the salt.
        /// </summary>
        public string Salt { get; set; }

        /// <summary>
        /// Gets or sets the session token.
        /// </summary>
        public Guid SessionToken { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user status.
        /// </summary>
        public virtual UserStatus UserStatus { get; set; }

        /// <summary>
        /// Gets or sets the user status id.
        /// </summary>
        public Guid UserStatusId { get; set; }
    }
}