// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserStatu.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The user status.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The user status.
    /// </summary>
    [GeneratedCode("EF.Reverse.POCO.Generator", "2.15.2.0")]
    public class UserStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserStatus"/> class.
        /// </summary>
        public UserStatus()
        {
            this.Users = new List<User>();
        }

        /// <summary>
        /// Gets or sets the user status id.
        /// </summary>
        public Guid UserStatusId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public virtual ICollection<User> Users { get; set; }
    }
}