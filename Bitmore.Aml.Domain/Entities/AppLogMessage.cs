// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLogMessage.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app log message.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The app log message.
    /// </summary>
    public class AppLogMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogMessage"/> class.
        /// </summary>
        public AppLogMessage()
        {
        }

        /// <summary>
        /// Gets or sets the app log message id.
        /// </summary>
        public Guid AppLogMessageId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is regex.
        /// </summary>
        public bool IsRegex { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the regex stack trace or class.
        /// </summary>
        public string RegexStackTraceOrClass { get; set; }
    }
}