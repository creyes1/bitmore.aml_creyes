// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLog.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app log.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The app log.
    /// </summary>
    public class AppLog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppLog"/> class.
        /// </summary>
        public AppLog()
        {
        }

        /// <summary>
        /// Gets or sets the app log id.
        /// </summary>
        public Guid AppLogId { get; set; }

        /// <summary>
        /// Gets or sets the app log type.
        /// </summary>
        public string AppLogType { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        public string Logger { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the thread.
        /// </summary>
        public string Thread { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }
    }
}