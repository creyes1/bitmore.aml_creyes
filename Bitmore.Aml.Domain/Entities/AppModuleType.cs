// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleType.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app module type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The app module type.
    /// </summary>
    [GeneratedCode("EF.Reverse.POCO.Generator", "2.15.2.0")]
    public class AppModuleType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleType"/> class.
        /// </summary>
        public AppModuleType()
        {
            this.AppModules = new List<AppModule>();
        }

        /// <summary>
        /// Gets or sets the app module type id.
        /// </summary>
        public Guid AppModuleTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the app modules.
        /// </summary>
        public virtual ICollection<AppModule> AppModules { get; set; }
    }
}