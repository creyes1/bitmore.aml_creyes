// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAttribute.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The global attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The global attribute.
    /// </summary>
    public class GlobalAttribute
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is editable.
        /// </summary>
        public bool IsEditable { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}