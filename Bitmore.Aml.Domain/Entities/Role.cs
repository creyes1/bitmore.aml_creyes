// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Role.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The role.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The role.
    /// </summary>
    [GeneratedCode("EF.Reverse.POCO.Generator", "2.15.2.0")]
    public class Role
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Role"/> class.
        /// </summary>
        public Role()
        {
            this.CreatedOnDate = DateTime.Now;
            this.Users = new List<User>();
            this.UsersInRole = new List<User>();
            this.AppModuleActions = new List<AppModuleAction>();
        }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        public byte[] RowVersion { get; set; }

        /// <summary>
        /// Gets or sets the app module actions.
        /// </summary>
        public virtual ICollection<AppModuleAction> AppModuleActions { get; set; }

        /// <summary>
        /// Gets or sets the users_ current role id.
        /// </summary>
        public virtual ICollection<User> Users { get; set; }

        /// <summary>
        /// Gets or sets the users_ user id.
        /// </summary>
        public virtual ICollection<User> UsersInRole { get; set; }
    }
}