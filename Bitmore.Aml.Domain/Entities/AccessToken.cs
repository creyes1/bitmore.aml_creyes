// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccessToken.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The access token.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The access token.
    /// </summary>
    public class AccessToken
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the access token id.
        /// </summary>
        public Guid AccessTokenId { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        public DateTimeOffset CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the expiration on date.
        /// </summary>
        public DateTimeOffset ExpirationOnDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is revoked.
        /// </summary>
        public bool IsRevoked { get; set; }

        /// <summary>
        /// Gets or sets the signature.
        /// </summary>
        public string Signature { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        #endregion
    }
}