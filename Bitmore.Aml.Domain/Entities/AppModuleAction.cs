// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleAction.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app module action.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The app module action.
    /// </summary>
    [GeneratedCode("EF.Reverse.POCO.Generator", "2.15.2.0")]
    public class AppModuleAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleAction"/> class.
        /// </summary>
        public AppModuleAction()
        {
            this.Roles = new List<Role>();
        }

        /// <summary>
        /// Gets or sets the app module action id.
        /// </summary>
        public Guid AppModuleActionId { get; set; }

        /// <summary>
        /// Gets or sets the app module id.
        /// </summary>
        public Guid AppModuleId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        public virtual ICollection<Role> Roles { get; set; }

        /// <summary>
        /// Gets or sets the app module.
        /// </summary>
        public virtual AppModule AppModule { get; set; }
    }
}