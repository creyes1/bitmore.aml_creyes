// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModule.cs" company="Bitmore Technologies">
//   Copyright Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The app module.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.Domain.Entities
{
    #region

    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The app module.
    /// </summary>
    [GeneratedCode("EF.Reverse.POCO.Generator", "2.15.2.0")]
    public class AppModule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModule"/> class.
        /// </summary>
        public AppModule()
        {
            this.ChildrenAppModules = new List<AppModule>();
            this.AppModuleActions = new List<AppModuleAction>();
        }

        /// <summary>
        /// Gets or sets the app module id.
        /// </summary>
        public Guid AppModuleId { get; set; }

        /// <summary>
        /// Gets or sets the app module type id.
        /// </summary>
        public Guid AppModuleTypeId { get; set; }

        /// <summary>
        /// Gets or sets the css or desktop icon.
        /// </summary>
        public string CssOrDesktopIcon { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the parent app module id.
        /// </summary>
        public Guid? ParentAppModuleId { get; set; }

        /// <summary>
        /// Gets or sets the type or url.
        /// </summary>
        public string TypeOrUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is private.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        /// Gets or sets the app modules.
        /// </summary>
        public virtual ICollection<AppModule> ChildrenAppModules { get; set; }

        /// <summary>
        /// Gets or sets the app module actions.
        /// </summary>
        public virtual ICollection<AppModuleAction> AppModuleActions { get; set; }

        /// <summary>
        /// Gets or sets the app module parent app module id.
        /// </summary>
        public virtual AppModule ParentAppModule { get; set; }

        /// <summary>
        /// Gets or sets the app module type.
        /// </summary>
        public virtual AppModuleType AppModuleType { get; set; }
    }
}