// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AmlContext.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The bit seed context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess
{
    #region

    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.DataAccess.Mapping;

    #endregion

    /// <summary>
    /// The bit seed context.
    /// </summary>
    public class AmlContext : DbContext, IDbContext
    {
        /// <summary>
        /// The database log.
        /// </summary>
        private readonly IDatabaseLog databaseLog;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="AmlContext"/> class.
        /// </summary>
        static AmlContext()
        {
            Database.SetInitializer<AmlContext>(null);
            
            // If we need to create a DataBase:
            // Database.SetInitializer(new MigrateDatabaseToLatestVersion<AmlContext, Configuration>());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AmlContext"/> class.
        /// </summary>
        /// <param name="databaseLog">
        /// The database Log.
        /// </param>
        /// <param name="connectionString">
        /// The connection String.
        /// </param>
        public AmlContext(IDatabaseLog databaseLog, string connectionString) : base(connectionString)
        {
            this.databaseLog = databaseLog;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AmlContext"/> class.
        /// </summary>
        /// <param name="databaseLog"></param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="model">
        /// The model.
        /// </param>
        public AmlContext(IDatabaseLog databaseLog, string connectionString, DbCompiledModel model) : base(connectionString, model)
        {
            this.databaseLog = databaseLog;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AmlContext"/> class.
        /// </summary>
        /// <param name="databaseLog">
        /// </param>
        /// <param name="connectionString">
        /// The connection String.
        /// </param>
        /// <param name="proxyCreationEnabled">
        /// The proxy creation enabled.
        /// </param>
        /// <param name="lazyLoadingEnabled">
        /// The lazy loading enabled.
        /// </param>
        public AmlContext(IDatabaseLog databaseLog, string connectionString, bool proxyCreationEnabled = true, bool lazyLoadingEnabled = true) : base(connectionString)
        {
            this.databaseLog = databaseLog;
            this.Configuration.ProxyCreationEnabled = proxyCreationEnabled;
            this.Configuration.LazyLoadingEnabled = lazyLoadingEnabled;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create model.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        /// <param name="schema">
        /// The schema.
        /// </param>
        /// <returns>
        /// The <see cref="DbModelBuilder"/>.
        /// </returns>
        public static DbModelBuilder CreateModel(DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new AppLogMap(schema));
            modelBuilder.Configurations.Add(new AppLogMessageMap(schema));
            modelBuilder.Configurations.Add(new AppModuleMap(schema));
            modelBuilder.Configurations.Add(new AppModuleActionMap(schema));
            modelBuilder.Configurations.Add(new AppModuleTypeMap(schema));
            modelBuilder.Configurations.Add(new GlobalAttributeMap(schema));
            modelBuilder.Configurations.Add(new RoleMap(schema));
            modelBuilder.Configurations.Add(new UserMap(schema));
            modelBuilder.Configurations.Add(new UserStatusMap(schema));
            modelBuilder.Configurations.Add(new AccessTokenMap(schema));
            modelBuilder.Configurations.Add(new CompanyMap(schema));
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            return modelBuilder;
        }

        /// <summary>
        /// The commit.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Commit()
        {
            this.databaseLog.InitLog(this);
            try
            {
                this.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationError in ex.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors))
                {
                    Trace.TraceInformation($"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage}");
                }

                throw;
            }
            catch (DbUpdateConcurrencyException cex)
            {
                return false;
            }

            this.databaseLog.WriteLog();
            return true;
        }

        /// <summary>
        /// The commit async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<int> CommitAsync()
        {
            return this.SaveChangesAsync();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new AppLogMap());
            modelBuilder.Configurations.Add(new AppLogMessageMap());
            modelBuilder.Configurations.Add(new AppModuleMap());
            modelBuilder.Configurations.Add(new AppModuleActionMap());
            modelBuilder.Configurations.Add(new AppModuleTypeMap());
            modelBuilder.Configurations.Add(new GlobalAttributeMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserStatusMap());
            modelBuilder.Configurations.Add(new AccessTokenMap());
            modelBuilder.Configurations.Add(new CompanyMap());
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        #endregion
    }
}