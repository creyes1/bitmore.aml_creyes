// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleActionRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The AppModuleActionRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The AppModuleActionRepository interface.
    /// </summary>
    public interface IAppModuleActionRepository : IRepository<AppModuleAction>
    {
    }

    /// <summary>
    /// The app module action repository.
    /// </summary>
    public class AppModuleActionRepository : RepositoryBase<AppModuleAction>, IAppModuleActionRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleActionRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AppModuleActionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion
    }
}