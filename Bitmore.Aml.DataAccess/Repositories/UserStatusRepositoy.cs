// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserStatusRepositoy.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The UserStatusRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The UserStatusRepository interface.
    /// </summary>
    public interface IUserStatusRepository : IRepository<UserStatus>
    {
    }

    /// <summary>
    /// The user status repositoy.
    /// </summary>
    public class UserStatusRepositoy : RepositoryBase<UserStatus>, IUserStatusRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStatusRepositoy"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public UserStatusRepositoy(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion
    }
}