// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLogRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The AppLogRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using System.Linq;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    using EntityFramework.Extensions;

    #endregion

    /// <summary>
    /// The AppLogRepository interface.
    /// </summary>
    public interface IAppLogRepository : IRepository<AppLog>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The clear app log errors.
        /// </summary>
        void ClearAppLogErrors();

        #endregion
    }

    /// <summary>
    /// The app log repository.
    /// </summary>
    public class AppLogRepository : RepositoryBase<AppLog>, IAppLogRepository
    {
        #region Constants

        /// <summary>
        /// The command clear app log errors.
        /// </summary>
        private const string LoggerTypeError = "Errors";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AppLogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The clear app log errors.
        /// </summary>
        public void ClearAppLogErrors()
        {
            this.Dbset.Where(log => log.Logger == LoggerTypeError).Delete();
        }

        #endregion
    }
}