// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAttributeRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The GlobalAttributeRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The GlobalAttributeRepository interface.
    /// </summary>
    public interface IGlobalAttributeRepository : IRepository<GlobalAttribute>
    {
    }

    /// <summary>
    /// The global attribute repository.
    /// </summary>
    public class GlobalAttributeRepository : RepositoryBase<GlobalAttribute>, IGlobalAttributeRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalAttributeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public GlobalAttributeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion
    }
}