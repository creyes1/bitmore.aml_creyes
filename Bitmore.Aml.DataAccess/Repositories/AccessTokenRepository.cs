// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccessTokenRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The AccessTokenRepository interface.
    /// </summary>
    public interface IAccessTokenRepository : IRepository<AccessToken>
    {
    }

    /// <summary>
    /// The access token repository.
    /// </summary>
    public class AccessTokenRepository : RepositoryBase<AccessToken>, IAccessTokenRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessTokenRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AccessTokenRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion
    }
}