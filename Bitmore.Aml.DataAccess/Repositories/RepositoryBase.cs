// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryBase.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The repository base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Dynamic;
    using System.Linq.Expressions;
    using System.Runtime.Caching;
    using System.Threading.Tasks;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Utils;

    using EntityFramework.Caching;
    using EntityFramework.Extensions;

    using LinqKit;

    #endregion

    /// <summary>
    /// The repository base.
    /// </summary>
    /// <typeparam name="TEntity">
    /// </typeparam>
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        #region Constants

        /// <summary>
        /// The row version.
        /// </summary>
        private const string RowVersion = "RowVersion";

        /// <summary>
        /// The cache query.
        /// </summary>
        private const string CacheQueryKey = "CacheQuery";

        /// <summary>
        /// The default duration expiration.
        /// </summary>
        private const int DefaultDurationExpiration = 900;

        #endregion

        #region Fields

        /// <summary>
        /// The Dbset.
        /// </summary>
        protected readonly IDbSet<TEntity> Dbset;

        /// <summary>
        /// The data context.
        /// </summary>
        private DbContext dbContext;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="RepositoryBase"/> class.
        /// </summary>
        static RepositoryBase()
        {
            CachePolicy.Default.Duration = TimeSpan.FromSeconds(DefaultDurationExpiration);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{TEntity}"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            this.DatabaseFactory = databaseFactory;
            this.Dbset = this.DataContext.Set<TEntity>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the data context.
        /// </summary>
        protected DbContext DataContext
        {
            get
            {
                return this.dbContext ?? (this.dbContext = this.DatabaseFactory.Get());
            }

            private set
            {
                this.dbContext = value;
            }
        }

        /// <summary>
        /// Gets the database factory.
        /// </summary>
        protected IDatabaseFactory DatabaseFactory { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Add(TEntity entity)
        {
            this.Dbset.Add(entity);
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        public virtual void Add(IEnumerable<TEntity> entities, bool autoDetectChangesEnabled = false)
        {
            this.ExecuteBulk(entities, this.Dbset.Add, autoDetectChangesEnabled);
        }

        /// <summary>
        /// The attach.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Attach(TEntity entity)
        {
            if (this.DataContext.Entry(entity).State == EntityState.Detached)
            {
                this.Dbset.Attach(entity);
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Delete(TEntity entity)
        {
            this.Dbset.Remove(entity);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        public virtual void Delete(params object[] primaryKeys)
        {
            var entity = this.Get(primaryKeys);
            if (entity != null)
            {
                this.Dbset.Remove(entity);
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        public virtual void Delete(IEnumerable<TEntity> entities, bool autoDetectChangesEnabled = false)
        {
            this.ExecuteBulk(entities, this.Dbset.Remove, autoDetectChangesEnabled);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto Detect Changes Enabled.
        /// </param>
        public virtual void Delete(Expression<Func<TEntity, bool>> where, bool autoDetectChangesEnabled = true)
        {
            var objects = this.Dbset.Where(where).AsEnumerable();
            if (objects == null)
            {
                return;
            }

            this.ExecuteBulk(objects, this.Dbset.Remove, autoDetectChangesEnabled);
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity Get(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Where(where).SingleOrDefault();
        }

        /// <summary>
        /// The get from cache.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity GetFromCache(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Where(where).FromCacheFirstOrDefault(tags: new[] { CacheQueryKey });
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity Get(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Dbset.IncludeMultiple(includes).Where(where).SingleOrDefault();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary Keys.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public virtual TEntity Get(params object[] primaryKeys)
        {
            return this.Dbset.Find(primaryKeys);
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual Task<TEntity> GetAsync(params object[] primaryKeys)
        {
            var entitydbset = (DbSet<TEntity>)this.Dbset;
            return entitydbset.FindAsync(primaryKeys);
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Dbset.IncludeMultiple(includes).ToList();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.Dbset.ToList();
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<List<TEntity>> GetAllAsync()
        {
            return this.Dbset.ToListAsync();
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<List<TEntity>> GetAllAsync(params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Dbset.IncludeMultiple(includes).ToListAsync();
        }

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetAllFromCache()
        {
            return this.Dbset.FromCache(tags: new[] { CacheQueryKey });
        }

        /// <summary>
        /// the get all from cache
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAllFromCache(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Where(where).FromCache(tags: new[] { CacheQueryKey });
        }

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<T> GetAllFromCache<T>(IQueryable<T> query) where T : class
        {
            return query.FromCache(tags: new[] { CacheQueryKey });
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Where(where).SingleOrDefaultAsync();
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Dbset.IncludeMultiple(includes).Where(where).SingleOrDefaultAsync();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="primaryKeys">
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetCollection(params object[] primaryKeys)
        {
            return this.Dbset.Where(this.DataContext, primaryKeys.ToList()).ToList();
        }

        /// <summary>
        /// The get collection.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetCollection(params object[][] primaryKeys)
        {
            return this.Dbset.Where(this.DataContext, primaryKeys.ToList()).ToList();
        }

        /// <summary>
        /// The get many.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetMany(
            Expression<Func<TEntity, bool>> where,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return this.Dbset.IncludeMultiple(includes).Where(where).ToList();
        }

        /// <summary>
        /// The get many.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public virtual IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Where(where).ToList();
        }

        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool Any(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Any(where);
        }

        /// <summary>
        /// The get many async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> where)
        {
            return this.Dbset.Where(where).ToListAsync();
        }

        /// <summary>
        /// The get many async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<List<TEntity>> GetManyAsync(
            Expression<Func<TEntity, bool>> where,
            params Expression<Func<TEntity, object>>[] includes)
        {
            return this.GetQuery().IncludeMultiple(includes).Where(where).ToListAsync();
        }

        /// <summary>
        /// The get paged query.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="sort">
        /// The sort.
        /// </param>
        /// <param name="maximumRows">
        /// The maximum rows.
        /// </param>
        /// <param name="startRowIndex">
        /// The start row index.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<T> GetPagedQuery<T>(
            IQueryable<T> query,
            string sort,
            int maximumRows,
            int startRowIndex,
            string where = null)
        {
            return this.GetDynamicQuery(query, sort, where).Skip(startRowIndex).Take(maximumRows);
        }

        /// <summary>
        /// The get dynamic query.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="sort">
        /// The sort.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<T> GetDynamicQuery<T>(
            IQueryable<T> query,
            string sort,
            string where = null)
        {
            if (string.IsNullOrEmpty(where))
            {
                where = "true";
            }

            query = query.AsExpandable().Where(where);
            return query.OrderBy(sort);
        }

        /// <summary>
        /// The get query.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<TEntity> GetQuery()
        {
            return this.Dbset;
        }

        /// <summary>
        /// The get query as no tracking.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public virtual IQueryable<TEntity> GetQueryAsNoTracking()
        {
            return this.Dbset.AsNoTracking();
        }

        /// <summary>
        /// The map keys.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <typeparam name="TSource">
        /// </typeparam>
        /// <returns>
        /// The <see cref="object[]"/>.
        /// </returns>
        public virtual object[] MapKeys<TSource>(TSource source) where TSource : class
        {
            var keysOfEntity = this.DataContext.GetKeyNames<TEntity>();
            var keysProperties = keysOfEntity.GetProperties<TSource>();
            return keysProperties?.GetKeysValues(source);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="checkOptimisticConcurrency">
        /// The check optimistic concurrency.
        /// </param>
        public virtual void Update(TEntity entity, bool checkOptimisticConcurrency = false)
        {
            Action<TEntity> actionBeforeModified = null;
            if (checkOptimisticConcurrency)
            {
                actionBeforeModified = obj =>
                {
                    var property = this.DataContext.Entry(obj).Property(RowVersion);
                    if (property != null)
                    {
                        property.OriginalValue = property.CurrentValue;
                    }
                };
            }

            this.UpdateEntity(entity, actionBeforeModified);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        public virtual void Update(IEnumerable<TEntity> entities, bool autoDetectChangesEnabled = false)
        {
            this.ExecuteBulk(entities, obj => this.UpdateEntity(obj), autoDetectChangesEnabled);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (this.DataContext == null)
            {
                return;
            }

            this.DataContext.Dispose();
            this.DataContext = null;
        }

        /// <summary>
        /// The execute bulk.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="bulkFunction">
        /// The bulk function.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        private void ExecuteBulk(
            IEnumerable<TEntity> entities,
            Func<TEntity, TEntity> bulkFunction,
            bool autoDetectChangesEnabled = false)
        {
            this.DataContext.Configuration.AutoDetectChangesEnabled = autoDetectChangesEnabled;
            if (bulkFunction != null)
            {
                entities.ToList().ForEach(e => bulkFunction(e));
            }

            this.DataContext.Configuration.AutoDetectChangesEnabled = true;
        }

        /// <summary>
        /// The update entity.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="actionBeforeModified">
        /// The action before modified.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        private TEntity UpdateEntity(TEntity entity, Action<TEntity> actionBeforeModified = null)
        {
            this.Attach(entity);
            actionBeforeModified?.Invoke(entity);
            this.DataContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        /// <summary>
        /// The purge cache.
        /// </summary>
        public void PurgeCache()
        {
            CacheManager.Current.Expire(CacheQueryKey);
        }

        #endregion
    }
}