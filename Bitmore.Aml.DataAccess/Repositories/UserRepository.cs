// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The UserRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using System;
    using System.Linq;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    using EntityFramework.Extensions;

    #endregion

    /// <summary>
    /// The UserRepository interface.
    /// </summary>
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// The clear has active session.
        /// </summary>
        /// <param name="userName">
        /// The user Name.
        /// </param>
        void SignOut(string userName);

        /// <summary>
        /// The clear has active session.
        /// </summary>
        void ClearHasActiveSession();
    }

    /// <summary>
    /// The user repository.
    /// </summary>
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion

        /// <summary>
        /// The change has active session.
        /// </summary>
        /// <param name="userName">
        /// The user id.
        /// </param>
        public void SignOut(string userName)
        {
            var newSessionToken = Guid.NewGuid();
            this.Dbset
                .Where(t => t.UserName == userName)
                .Update(t => new User { HasActiveSession = false, SessionToken = newSessionToken });
        }

        /// <summary>
        /// The clear has active session.
        /// </summary>
        public void ClearHasActiveSession()
        {
            this.Dbset
                .Where(t => t.HasActiveSession)
                .Update(t => new User { HasActiveSession = false });
        }
    }
}