// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The AppModuleRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The AppModuleRepository interface.
    /// </summary>
    public interface IAppModuleRepository : IRepository<AppModule>
    {
    }

    /// <summary>
    /// The app module repository.
    /// </summary>
    public class AppModuleRepository : RepositoryBase<AppModule>, IAppModuleRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AppModuleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion
    }
}