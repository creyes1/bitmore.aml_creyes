// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompanyRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.DataObject.OfacInternalListDtos;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The CompanyRepository interface.
    /// </summary>
    public interface ICompanyRepository : IRepository<Company>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get local ofac search details.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastNameFirst">
        /// The last name first.
        /// </param>
        /// <param name="lastNameSecond">
        /// The last name second.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<InternalOfacDTO> GetLocalOfacSearchDetails(string firstName, string lastNameFirst, string lastNameSecond);

        #endregion
    }

    /// <summary>
    /// The company repository.
    /// </summary>
    public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
    {
        #region Constants

        /// <summary>
        /// The store name st ofac search details.
        /// </summary>
        private const string StoreNameStOfacSearchDetails = "Ofac.ST_OFAC_SEARCH_DETAILS @f_name, @l_name_1, @l_name_2";

        /// <summary>
        /// The store param first name.
        /// </summary>
        private const string StoreParamFirstName = "f_name";

        /// <summary>
        /// The store param last name first.
        /// </summary>
        private const string StoreParamLastNameFirst = "l_name_1";

        /// <summary>
        /// The store param last name second.
        /// </summary>
        private const string StoreParamLastNameSecond = "l_name_2";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public CompanyRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get local ofac search details.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastNameFirst">
        /// The last name first.
        /// </param>
        /// <param name="lastNameSecond">
        /// The last name second.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<InternalOfacDTO> GetLocalOfacSearchDetails(
            string firstName,
            string lastNameFirst,
            string lastNameSecond)
        {
            var paramFirstName = new SqlParameter(StoreParamFirstName, firstName);
            var paramLastNameFirst = new SqlParameter(StoreParamLastNameFirst, lastNameFirst);
            var paramLastNameSecond = new SqlParameter(StoreParamLastNameSecond, lastNameSecond);
            var internalOfacResult =
                this.DataContext.Database.SqlQuery<InternalOfacDTO>(
                    StoreNameStOfacSearchDetails,
                    paramFirstName,
                    paramLastNameFirst,
                    paramLastNameSecond).ToList();
            return internalOfacResult;
        }

        #endregion
    }
}