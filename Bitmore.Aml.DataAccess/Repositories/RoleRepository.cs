// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The RoleRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The RoleRepository interface.
    /// </summary>
    public interface IRoleRepository : IRepository<Role>
    {
    }

    /// <summary>
    /// The role repository.
    /// </summary>
    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion
    }
}