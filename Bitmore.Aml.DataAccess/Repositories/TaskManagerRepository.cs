// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskManagerRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The TaskManagerRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using System;

    #endregion

    /// <summary>
    /// The TaskManagerRepository interface.
    /// </summary>
    public interface ITaskManagerRepository
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create back up and delete log.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CreateBackUpAndDeleteLog();

        #endregion
    }

    /// <summary>
    /// The task manager repository.
    /// </summary>
    public class TaskManagerRepository : ITaskManagerRepository
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create back up and delete log.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool CreateBackUpAndDeleteLog()
        {
            return true;
        }

        #endregion
    }
}