// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtensionMethods.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The extension methods.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region


    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServer;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text.RegularExpressions;

    using LinqKit;

    using DynamicExpression = System.Linq.Dynamic.DynamicExpression;

    #endregion

    /// <summary>
    /// The extension methods.
    /// </summary>
    public static class ExtensionMethods
    {
        #region Constants

        /// <summary>
        /// The pattern number to string.
        /// </summary>
        public const string PatternNumberToString = @"([a-zA-Z0-9\-_\-.]*)\.(StartsWith|Contains)\((Int32|Int64|Double|Float|Decimal)\((.*)\)\)";

        /// <summary>
        /// The string true.
        /// </summary>
        public const string StringTrue = "true";

        /// <summary>
        /// The correct and.
        /// </summary>
        private const string CorrectAnd = @"&&";

        /// <summary>
        /// The operators.
        /// </summary>
        private static readonly string[] Operators = { @" && ", @" || " };

        /// <summary>
        /// The data context message.
        /// </summary>
        private const string DataContextMessage = "The DataContext can not be null";

        /// <summary>
        /// The format false.
        /// </summary>
        private const string FormatFalse = "false";

        /// <summary>
        /// The max value of bigint.
        /// </summary>
        private const int MaxBigintLength = 64;

        /// <summary>
        /// The paramater lambda.
        /// </summary>
        private const string ParamaterLambda = "e";

        /// <summary>
        /// The pattern duplicated and.
        /// </summary>
        private const string PatternDuplicatedAnd = @"&&\s*&&";

        /// <summary>
        /// The pattern unnecesary and.
        /// </summary>
        private const string PatternUnnecesaryAnd = @"^\s*&&|&&\s*$";

        /// <summary>
        /// The string converter.
        /// </summary>
        private const string StringConverter = "StringConvert";

        /// <summary>
        /// The string trim.
        /// </summary>
        private const string StringTrim = "Trim";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get default value.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object GetDefaultValue(this object obj)
        {
            if (obj == null)
            {
                return null;
            }

            var type = obj.GetType();
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        /// <summary>
        /// The get keys values.
        /// </summary>
        /// <param name="keysProperties">
        /// The keys properties.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <returns>
        /// The <see cref="object[]"/>.
        /// </returns>
        public static object[] GetKeysValues(this PropertyInfo[] keysProperties, object source)
        {
            var listOgKeys = new List<object>();
            keysProperties.ToList().ForEach(
                k =>
                {
                    var value = k.GetValue(source, new object[] { });
                    listOgKeys.Add(value);
                });
            return listOgKeys.ToArray();
        }

        /// <summary>
        /// The getp properties.
        /// </summary>
        /// <param name="keysName">
        /// The keys name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="PropertyInfo[]"/>.
        /// </returns>
        public static PropertyInfo[] GetProperties<T>(this string[] keysName) where T : class
        {
            if (keysName == null)
            {
                return default(PropertyInfo[]);
            }

            var objectType = typeof(T);
            var listOgKeys = new List<PropertyInfo>();
            keysName.ToList().ForEach(
                k =>
                {
                    var propertyInfo = objectType.GetProperty(k);
                    listOgKeys.Add(propertyInfo);
                });
            return listOgKeys.ToArray();
        }

        /// <summary>
        /// The get where numeric string.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="methodName">
        /// The method name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public static Expression<Func<T, bool>> GetWhereNumericString<T>(
            string propertyName,
            string value,
            string methodName)
        {
            double tryDouble;
            if (!double.TryParse(value, out tryDouble))
            {
                return DynamicExpression.ParseLambda<T, bool>(FormatFalse);
            }

            var parameterExpression = Expression.Parameter(typeof(T), ParamaterLambda);
            var stringConvertMethodInfo = typeof(SqlFunctions).GetMethod(
                StringConverter,
                new[] { typeof(double?), typeof(int?) });
            var stringContainsMethodInfo = typeof(string).GetMethods().First(m => m.Name == methodName);
            var stringTrimMethodInfo = typeof(string).GetMethod(StringTrim, new Type[0]);

            var convertProperty = Expression.Convert(parameterExpression.GetDeepProperty(propertyName), typeof(double?));
            var maxLength = Expression.Constant(MaxBigintLength, typeof(int?));
            var convertMethod = Expression.Call(stringConvertMethodInfo, convertProperty, maxLength);
            var trimMethod = Expression.Call(convertMethod, stringTrimMethodInfo);
            var convertString = Expression.Call(trimMethod, stringContainsMethodInfo, Expression.Constant(value));
            var lambda = Expression.Lambda<Func<T, bool>>(convertString, parameterExpression);
            return lambda;
        }

        /// <summary>
        /// The include multiple.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public static IQueryable<T> IncludeMultiple<T>(
            this IQueryable<T> query,
            params Expression<Func<T, object>>[] includes) where T : class
        {
            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }

            return query;
        }

        /// <summary>
        /// The remove unnecesary tokens.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string RemoveUnnecesaryTokens(this string where)
        {
            where = Regex.Replace(where, PatternUnnecesaryAnd, string.Empty);
            where = Regex.Replace(where, PatternDuplicatedAnd, CorrectAnd);
            return where;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get entity key names.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="dataContext">
        /// The data Context.
        /// </param>
        /// <returns>
        /// The <see cref="string[]"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        internal static string[] GetKeyNames<T>(this DbContext dataContext) where T : class
        {
            if (dataContext == null)
            {
                throw new ArgumentNullException(DataContextMessage);
            }

            var set = ((IObjectContextAdapter)dataContext).ObjectContext.CreateObjectSet<T>();
            var entitySet = set.EntitySet;
            return entitySet.ElementType.KeyMembers.Select(k => k.Name).ToArray();
        }

        /// <summary>
        /// The get keys.
        /// </summary>
        /// <param name="dataContext">
        /// The data context.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="PropertyInfo[]"/>.
        /// </returns>
        internal static PropertyInfo[] GetKeys<T>(this DbContext dataContext) where T : class
        {
            var listOgKeys = dataContext.GetKeyNames<T>().GetProperties<T>();
            return listOgKeys;
        }

        /// <summary>
        /// The where.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="secondWhere">
        /// The second where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        internal static IQueryable<T> Where<T>(
            this IQueryable<T> query,
            string where,
            Expression<Func<T, bool>> secondWhere = null)
        {
            var whereIsEmpty = string.IsNullOrEmpty(where) || where.Trim() == string.Empty;
            if (whereIsEmpty && secondWhere == null)
            {
                return query;
            }

            if (secondWhere != null && whereIsEmpty)
            {
                return query.Where(secondWhere);
            }

            var resultWhere = default(Expression<Func<T, bool>>);
            @where.Split(Operators, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(w =>
            {
                var match = Regex.Match(w, PatternNumberToString);
                Expression<Func<T, bool>> parseWhere;
                if (match.Groups.Count >= 3)
                {
                    parseWhere =
                        GetWhereNumericString<T>(
                        match.Groups[1].ToString(),
                        match.Groups[match.Groups.Count - 1].ToString(),
                            match.Groups[2].ToString()).Expand();
                }
                else
                {
                    parseWhere = DynamicExpression.ParseLambda<T, bool>(w);
                }

                if (parseWhere != null)
                {
                    // ReSharper disable AccessToModifiedClosure
                    resultWhere = resultWhere == null ? parseWhere : resultWhere.And(parseWhere);
                }
            });

            if (secondWhere != null)
            {
                resultWhere = resultWhere.And(secondWhere);
            }

            return query.Where(resultWhere.Expand());
        }

        /// <summary>
        /// The where.
        /// </summary>
        /// <param name="self">
        /// The self.
        /// </param>
        /// <param name="dataContext">
        /// The data Context.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <typeparam name="TValue">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        internal static IQueryable<T> Where<T, TValue>(
            this IQueryable<T> self,
            DbContext dataContext,
            List<TValue> values) where TValue : class where T : class
        {
            var primaryKeysNames = dataContext.GetKeyNames<T>();
            if (primaryKeysNames == null)
            {
                return self;
            }

            var finalpredicate = PredicateBuilder.False<T>();
            for (var i = 0; i < values.Count(); i++)
            {
                var value = values.ElementAt(i);
                var propertyType = value.GetType();
                if (propertyType.GetInterface(typeof(IEnumerable<>).FullName) == null)
                {
                    var pkname = primaryKeysNames.First();
                    finalpredicate = finalpredicate.Or(Equal<T>(pkname, value));
                }
                else
                {
                    var enumerable = value as IEnumerable;
                    if (enumerable == null)
                    {
                        continue;
                    }


                    var vidx = 0;
                    var predicate = default(Expression<Func<T, bool>>);
                    foreach (var v in enumerable)
                    {
                        var equal = Equal<T>(primaryKeysNames[vidx++], v);
                        predicate = predicate != null ? predicate.And(equal) : equal;
                    }

                    finalpredicate = finalpredicate.Or(predicate);
                }
            }

            return self.Where(finalpredicate.Expand());
        }

        /// <summary>
        /// The equal.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        private static Expression<Func<T, bool>> Equal<T>(string property, object value)
        {
            var predicate = PredicateBuilder.False<T>();
            var param = predicate.Parameters.Single();
            var prop = Expression.Property(param, property);
            var constant = Expression.Constant(value);
            var equal = Expression.Equal(prop, constant);
            return Expression.Lambda<Func<T, bool>>(equal, param);
        }

        /// <summary>
        /// The get deep property.
        /// </summary>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        private static Expression GetDeepProperty(this Expression parameter, string property)
        {
            var props = property.Split('.');
            var type = parameter.Type;

            var expr = parameter;

            // ReSharper disable AccessToModifiedClosure
            foreach (var pi in props.Select(prop => type?.GetProperty(prop)))
            {
                // ReSharper restore AccessToModifiedClosure
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }

            return expr;
        }

        /// <summary>
        /// The PropertyExists method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool PropertyExists<T>(string propertyName)
        {
            return typeof(T).GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) != null;
        }

        #endregion
    }
}