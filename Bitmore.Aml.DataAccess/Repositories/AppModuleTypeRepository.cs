// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleTypeRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Repositories
{
    #region

    using Bitmore.Aml.DataAccess.Infrastructure;
    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The AppModuleTypeRepository interface.
    /// </summary>
    public interface IAppModuleTypeRepository : IRepository<AppModuleType>
    {
    }

    /// <summary>
    /// The app module type repository.
    /// </summary>
    public class AppModuleTypeRepository : RepositoryBase<AppModuleType>, IAppModuleTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleTypeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AppModuleTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}