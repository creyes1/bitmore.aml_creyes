// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessorUpdate.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The update parameter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure.Log
{
    #region

    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    #endregion

    /// <summary>
    /// The update parameter.
    /// </summary>
    internal class ProcessorUpdate : ProcessorBase
    {
        #region Constants

        /// <summary>
        /// The name string.
        /// </summary>
        private const string NameString = "Name";

        /// <summary>
        /// The regex get parameters.
        /// </summary>
        private const string RegexGetParameters = @"(?<=\[)(?<Name>\w*)(?=\]\s\=\s@)";

        /// <summary>
        /// The regex groups of queries.
        /// </summary>
        private const string RegexGroupsOfQueries = @"UPDATE\s((.(?!Executing))*\n)*";

        /// <summary>
        /// The regex sql query.
        /// </summary>
        private const string RegexSqlQuery = @"UPDATE\s.*[\r\n]?SET\s.*[\r\n]?WHERE\s.*[\r\n]?";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get parameters from sql statment.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public override IEnumerable<string> GetParametersFromSqlStatment(string sqlStatment)
        {
            var query = Regex.Match(sqlStatment, RegexSqlQuery).Value;
            return this.GetValuesFromRegex(query, RegexGetParameters, NameString);
        }

        /// <summary>
        /// The get regex groups of queries.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetRegexGroupsOfQueries()
        {
            return RegexGroupsOfQueries;
        }

        #endregion
    }
}