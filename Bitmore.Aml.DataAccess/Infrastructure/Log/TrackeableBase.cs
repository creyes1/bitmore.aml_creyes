// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrackeableBase.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The trackeable base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure.Log
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    #endregion

    /// <summary>
    /// The trackeable base.
    /// </summary>
    public class TrackeableBase
    {
        #region Constants

        /// <summary>
        /// The regex parameter message.
        /// </summary>
        private const string RegexParameterMessage = @"(@\w*)";

        /// <summary>
        /// The regex parameter replace.
        /// </summary>
        private const string RegexParameterReplace = @"(@{0}*)";

        #endregion

        #region Fields

        /// <summary>
        /// The current parameter statments.
        /// </summary>
        private readonly IList<ProcessorBase> currentParameterStatments = new ProcessorBase[]
                                                                              {
                                                                                  new ProcessorUpdate(), 
                                                                                  new ProcessorInsert()
                                                                              };

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the regex stack trace.
        /// </summary>
        public string RegexStackTraceOrClass { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get message.
        /// </summary>
        /// <param name="stackTrace">
        /// The stack trace.
        /// </param>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public virtual string GetMessage(string stackTrace, string sqlStatment)
        {
            if (!Regex.IsMatch(stackTrace, this.RegexStackTraceOrClass))
            {
                return null;
            }

            var parameters = this.GetParametersFromMessage(this.Message);
            var enumerable = parameters as string[] ?? parameters.ToArray();
            if (!enumerable.Any())
            {
                return this.Message;
            }

            var values = this.GetValues(sqlStatment, enumerable.ToArray());
            var result = this.Message;
            foreach (var param in values)
            {
                var paramPattern = string.Format(RegexParameterReplace, param.Key);
                result = Regex.Replace(result, paramPattern, param.Value);
            }

            return result;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get parameters from message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        protected IEnumerable<string> GetParametersFromMessage(string message)
        {
            return
                (from Match match in Regex.Matches(message, RegexParameterMessage)
                 select match.Value.Replace("@", string.Empty)).ToList();
        }

        /// <summary>
        /// The get values.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        protected Dictionary<string, string> GetValues(string sqlStatment, string[] parameters)
        {
            var result = new Dictionary<string, string>();
            this.currentParameterStatments.Any(
                c =>
                    {
                        if (c.IsTypeOfStatment(sqlStatment))
                        {
                            result = c.GetValues(sqlStatment, parameters);
                            return true;
                        }

                        return false;
                    });

            return result;
        }

        #endregion
    }
}