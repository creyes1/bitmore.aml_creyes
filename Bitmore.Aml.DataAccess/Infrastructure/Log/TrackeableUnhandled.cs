// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrackeableUnhandled.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The unhandled trackeable.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure.Log
{
    #region

    using System.Linq;
    using System.Text.RegularExpressions;

    #endregion

    /// <summary>
    /// The unhandled trackeable.
    /// </summary>
    internal class TrackeableUnhandled : TrackeableBase
    {
        #region Constants

        /// <summary>
        /// The regex get stack.
        /// </summary>
        private const string RegexGetStack = @"Bitmore*\n*.*";

        /// <summary>
        /// The return empty.
        /// </summary>
        private const string ReturnEmpty = "Unhandled activity";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get message.
        /// </summary>
        /// <param name="stackTrace">
        /// The stack trace.
        /// </param>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetMessage(string stackTrace, string sqlStatment)
        {
            var matched = Regex.Matches(stackTrace, RegexGetStack);
            return matched.Count > 0
                       ? (from Match match in matched select match.Value).Aggregate((current, next) => current + next)
                       : ReturnEmpty;
        }

        #endregion
    }
}