// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessorInsert.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The update parameter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure.Log
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    #endregion

    /// <summary>
    /// The update parameter.
    /// </summary>
    internal class ProcessorInsert : ProcessorBase
    {
        #region Constants

        /// <summary>
        /// The get parameters from values.
        /// </summary>
        private const string GetParametersFromValues = @"(?<=VALUES\s*\()(.*(?=\)))";

        /// <summary>
        /// The get parameters name.
        /// </summary>
        private const string GetParametersName = @"(?<=\(\[|\,\s\[{1})(?<Name>\w*)(?=\])";

        /// <summary>
        /// The name string.
        /// </summary>
        private const string NameString = "Name";

        /// <summary>
        /// The regex groups of queries.
        /// </summary>
        private const string RegexGroupsOfQueries = @"INSERT\s((.(?!Executing))*\n)*";

        /// <summary>
        /// The regex sql query.
        /// </summary>
        private const string RegexSqlQuery = @"INSERT\s.*[\r\n]?(OUTPUT\s.*[\r\n]?)?VALUES\s.*[\r\n]?";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get parameters from sql statment.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public override IEnumerable<string> GetParametersFromSqlStatment(string sqlStatment)
        {
            var query = Regex.Match(sqlStatment, RegexSqlQuery).Value;
            var parameters = Regex.Match(query, GetParametersFromValues).Value;
            var parametersIndex = new List<int>();
            var index = 0;
            parameters.Split(',').ToList().ForEach(
                p =>
                    {
                        if (p.Trim().StartsWith("@"))
                        {
                            parametersIndex.Add(index);
                        }

                        index++;
                    });
            var parametersName = this.GetValuesFromRegex(query, GetParametersName, NameString);

            // Remove the null values
            var returnParameters = new List<string>();
            parametersIndex.ForEach(ix => returnParameters.Add(parametersName.ElementAt(ix)));
            return returnParameters;
        }

        /// <summary>
        /// The get regex groups of queries.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetRegexGroupsOfQueries()
        {
            return RegexGroupsOfQueries;
        }

        #endregion
    }
}