// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessorBase.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The processor base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure.Log
{
    #region

    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    using LinqKit;

    #endregion

    /// <summary>
    /// The processor base.
    /// </summary>
    public abstract class ProcessorBase
    {
        #region Constants

        /// <summary>
        /// The regex get values.
        /// </summary>
        private const string RegexGetValues = @"@\d{1,}\:\s?\'(?<Value>[\s\S]*?)\'\s?\(Type";

        /// <summary>
        /// The value string.
        /// </summary>
        private const string ValueString = "Value";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get parameters from sql statment.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public abstract IEnumerable<string> GetParametersFromSqlStatment(string sqlStatment);

        /// <summary>
        /// The get regex groups of queries.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public abstract string GetRegexGroupsOfQueries();

        /// <summary>
        /// The get values.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public virtual Dictionary<string, string> GetValues(string sqlStatment, string[] parameters)
        {
            var parameterValue = new Dictionary<string, string>();
            var matches = Regex.Matches(sqlStatment, this.GetRegexGroupsOfQueries());
            if (matches.Count == 0)
            {
                return null;
            }

            foreach (Match match in matches)
            {
                var values = this.GetValuesFromSqlStatment(match.Value);
                var paramatersFromSql = this.GetParametersFromSqlStatment(match.Value);
                var index = 0;
                paramatersFromSql.ToList().ForEach(
                    p =>
                    {
                        if (!parameterValue.ContainsKey(p) && parameters.Contains(p))
                        {
                            parameterValue.Add(p, values.ElementAt(index));
                        }

                        index++;
                    });
            }

            return !parameterValue.Any() ? null : parameterValue;
        }

        /// <summary>
        /// The is type of statment.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsTypeOfStatment(string sqlStatment)
        {
            return Regex.IsMatch(sqlStatment, this.GetRegexGroupsOfQueries());
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get values from sql statment.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <param name="regex">
        /// The regex.
        /// </param>
        /// <param name="groupNameToExtract">
        /// The group Name To Extract.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        protected virtual IEnumerable<string> GetValuesFromRegex(
            string sqlStatment, 
            string regex, 
            string groupNameToExtract)
        {
            IList<string> values = new List<string>();
            foreach (Match match in Regex.Matches(sqlStatment, regex))
            {
                var value = match.Groups[groupNameToExtract].Value.Trim();
                values.Add(value);
            }

            return values;
        }

        /// <summary>
        /// The get values from sql statment.
        /// </summary>
        /// <param name="sqlStatment">
        /// The sql statment.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/>.
        /// </returns>
        protected virtual IEnumerable<string> GetValuesFromSqlStatment(string sqlStatment)
        {
            return this.GetValuesFromRegex(sqlStatment, RegexGetValues, ValueString);
        }

        #endregion
    }
}