// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseLog.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The database log.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading;

    using Bitmore.Aml.DataAccess.Infrastructure.Log;
    using Bitmore.Aml.DataAccess.Repositories;
    using Bitmore.Aml.Domain.Entities;
    using Bitmore.Aml.Domain.Utils;

    using CacheManager.Core;

    using EntityFramework.Extensions;

    using LinqKit;

    #endregion

    /// <summary>
    /// The DatabaseLog interface.
    /// </summary>
    public interface IDatabaseLog
    {
        /// <summary>
        /// The init log.
        /// </summary>
        /// <param name="dbContext">
        /// The db context.
        /// </param>
        void InitLog(DbContext dbContext);

        /// <summary>
        /// The write log.
        /// </summary>
        void WriteLog();
    }

    /// <summary>
    /// The database log.
    /// </summary>
    public class DatabaseLog : IDatabaseLog
    {
        #region Constants

        /// <summary>
        /// The default message.
        /// </summary>
        private const string DefaultMessage = "[unknown]";

        #endregion

        #region Static Fields

        /// <summary>
        /// The contexts.
        /// </summary>
        private DbContext context;

        /// <summary>
        /// The log result.
        /// </summary>
        private StringBuilder logResult;

        /// <summary>
        /// The Log engine
        /// </summary>
        private readonly ILogEngine logEngine;

        /// <summary>
        /// The cache trackeable base.
        /// </summary>
        private readonly ICacheManager<List<TrackeableBase>> cacheTrackeableBase;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseLog"/> class.
        /// </summary>
        /// <param name="logEngine">
        /// The log engine.
        /// </param>
        /// <param name="cacheTrackeableBase">
        /// The cache trackeable base.
        /// </param>
        public DatabaseLog(ILogEngine logEngine, ICacheManager<List<TrackeableBase>> cacheTrackeableBase)
        {
            this.logEngine = logEngine;
            this.cacheTrackeableBase = cacheTrackeableBase;
        }

        #region Public Methods and Operators

        /// <summary>
        /// The get business message.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="stackTrace"></param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetBusinessMessage(string data, string stackTrace = null)
        {
            if (stackTrace == null)
            {
                stackTrace = Environment.StackTrace;
            }
            
            string message = null;
            this.GetTrackeable().Any(
                track =>
                {
                    message = track.GetMessage(stackTrace, data);
                    return message != null;
                });

            return message;
        }

        /// <summary>
        /// The get trackeable base.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<TrackeableBase> GetTrackeable()
        {
            const string FormatKeyCacheAppLogMessage = "AppLogMessages";
            Func<string, List<TrackeableBase>> getData = x =>
            {
                var trackeableBase = new List<TrackeableBase>();
                this.context.Set<AppLogMessage>().FromCache().ToList().ForEach(
                    app =>
                    {
                        if (!app.IsRegex)
                        {
                            return;
                        }

                        var trackeable = new TrackeableBase
                        {
                            Message = app.Message,
                            RegexStackTraceOrClass = app.RegexStackTraceOrClass
                        };
                        trackeableBase.Add(trackeable);
                    });

                return trackeableBase;
            };

            return this.cacheTrackeableBase.GetOrAdd(FormatKeyCacheAppLogMessage, getData);
        }

        /// <summary>
        /// The get unhandler message.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetUnhandlerMessage(string data)
        {
            var stack = Environment.StackTrace;
            var unhandled = new TrackeableUnhandled();
            return unhandled.GetMessage(stack, data);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The init log.
        /// </summary>
        /// <param name="dbContext">
        /// The db context.
        /// </param>
        public void InitLog(DbContext dbContext)
        {
            this.logResult = new StringBuilder();
            this.context = dbContext;
            this.context.Database.Log = s => this.logResult.Append(s);
        }

        /// <summary>
        /// The write log.
        /// </summary>
        public void WriteLog()
        {
            var principal = Thread.CurrentPrincipal;
            var userName = DefaultMessage;
            if (!string.IsNullOrEmpty(principal?.Identity.Name))
            {
                userName = principal.Identity.Name;
            }

            var data = this.logResult.ToString();

            // Add activities
            var message = this.GetBusinessMessage(data);
            Action<string, string, string> methodWrite;
            if (message == null)
            {
                message = this.GetUnhandlerMessage(data);
                methodWrite = this.logEngine.WriteActivityUnhandled;
            }
            else
            {
                methodWrite = this.logEngine.WriteActivity;
            }

            methodWrite(message, userName, data);
        }

        #endregion
    }
}