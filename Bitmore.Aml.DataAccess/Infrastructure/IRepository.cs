// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    #endregion

    /// <summary>
    /// The Repository interface.
    /// </summary>
    /// <typeparam name="TEntity">
    /// </typeparam>
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Add(TEntity entity);

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        void Add(IEnumerable<TEntity> entities, bool autoDetectChangesEnabled = false);

        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Any(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// The attach.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Attach(TEntity entity);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Delete(TEntity entity);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        void Delete(params object[] primaryKeys);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        void Delete(IEnumerable<TEntity> entities, bool autoDetectChangesEnabled = false);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto Detect Changes Enabled.
        /// </param>
        void Delete(Expression<Func<TEntity, bool>> where, bool autoDetectChangesEnabled = true);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity Get(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// The get from cache.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity GetFromCache(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary Keys.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity Get(params object[] primaryKeys);

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<TEntity> GetAsync(params object[] primaryKeys);

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetAllFromCache();

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetAllFromCache(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// The get all from cache.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<T> GetAllFromCache<T>(IQueryable<T> query) where T : class;

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TEntity>> GetAllAsync();

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TEntity>> GetAllAsync(params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<TEntity> GetAsync(
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// The get collection.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetCollection(params object[] primaryKeys);

        /// <summary>
        /// The get collection.
        /// </summary>
        /// <param name="primaryKeys">
        /// The primary keys.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetCollection(params object[][] primaryKeys);

        /// <summary>
        /// The get dynamic query.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="sort">
        /// The sort.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<T> GetDynamicQuery<T>(
            IQueryable<T> query,
            string sort,
            string where = null);

        /// <summary>
        /// The get many.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// The get many.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> GetMany(
            Expression<Func<TEntity, bool>> where,
            params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// The get many async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        /// The get many async.
        /// </summary>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <param name="includes">
        /// The includes.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TEntity>> GetManyAsync(
            Expression<Func<TEntity, bool>> where,
            params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// The get paged query.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="sort">
        /// The sort.
        /// </param>
        /// <param name="maximumRows">
        /// The maximum rows.
        /// </param>
        /// <param name="startRowIndex">
        /// The start row index.
        /// </param>
        /// <param name="where">
        /// The where.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<T> GetPagedQuery<T>(
            IQueryable<T> query,
            string sort,
            int maximumRows,
            int startRowIndex,
            string where = null);

        /// <summary>
        /// The get query.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<TEntity> GetQuery();

        /// <summary>
        /// The get query as no tracking.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<TEntity> GetQueryAsNoTracking();

        /// <summary>
        /// The map keys.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <typeparam name="TSource">
        /// </typeparam>
        /// <returns>
        /// The <see cref="object[]"/>.
        /// </returns>
        object[] MapKeys<TSource>(TSource source) where TSource : class;

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="checkOptimisticConcurrency">
        /// The check optimistic concurrency.
        /// </param>
        void Update(TEntity entity, bool checkOptimisticConcurrency = false);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <param name="autoDetectChangesEnabled">
        /// The auto detect changes enabled.
        /// </param>
        void Update(IEnumerable<TEntity> entities, bool autoDetectChangesEnabled = false);

        /// <summary>
        /// The purge cache.
        /// </summary>
        void PurgeCache();

        #endregion
    }
}