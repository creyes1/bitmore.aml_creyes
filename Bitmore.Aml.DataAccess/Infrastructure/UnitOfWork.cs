// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitOfWork.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The unit of work.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure
{
    #region

    using System.Data.Entity;
    using System.Threading.Tasks;

    #endregion

    /// <summary>
    /// The unit of work.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields

        /// <summary>
        /// The database factory.
        /// </summary>
        private readonly IDatabaseFactory databaseFactory;

        /// <summary>
        /// The data context.
        /// </summary>
        private DbContext dataContext;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the i db context.
        /// </summary>
        internal IDbContext DbContext => this.DataContext as IDbContext;

        /// <summary>
        /// Gets the data context.
        /// </summary>
        protected DbContext DataContext => this.dataContext ?? (this.dataContext = this.databaseFactory.Get());

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The commit.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Commit()
        {
            return this.DbContext.Commit();
        }

        /// <summary>
        /// The commit async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<int> CommitAsync()
        {
            return this.DbContext.CommitAsync();
        }

        #endregion
    }
}