// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDbContext.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The DbContext interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure
{
    #region

    using System.Threading.Tasks;

    #endregion

    /// <summary>
    /// The DbContext interface.
    /// </summary>
    internal interface IDbContext
    {
        #region Public Methods and Operators

        /// <summary>
        /// The commit.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Commit();

        /// <summary>
        /// The commit async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<int> CommitAsync();

        #endregion
    }
}