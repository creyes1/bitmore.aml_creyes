// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseFactory.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// <summary>
//   The database factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bitmore.Aml.DataAccess.Infrastructure
{
    #region

    using System.Data.Entity;

    using Microsoft.Extensions.Configuration;

    #endregion

    /// <summary>
    /// The database factory.
    /// </summary>
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        #region Fields

        /// <summary>
        /// The lazy loading enabled.
        /// </summary>
        private readonly bool lazyLoadingEnabled;

        /// <summary>
        /// The proxy creation enabled.
        /// </summary>
        private readonly bool proxyCreationEnabled;

        /// <summary>
        /// The data base log.
        /// </summary>
        private readonly IDatabaseLog databaseLog;

        /// <summary>
        /// The connection string
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// The data context.
        /// </summary>
        private DbContext dataContext;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseFactory"/> class.
        /// </summary>
        /// <param name="databaseLog"></param>
        /// <param name="config"></param>
        public DatabaseFactory(IDatabaseLog databaseLog, IConfiguration config)
        {
            this.databaseLog = databaseLog;
            this.proxyCreationEnabled = true;
            this.lazyLoadingEnabled = true;
            this.connectionString = config.GetConnectionString("DefaultConnection");
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="AmlContext"/>.
        /// </returns>
        public DbContext Get()
        {
            return this.dataContext
                   ?? (this.dataContext = new AmlContext(this.databaseLog, this.connectionString, this.proxyCreationEnabled, this.lazyLoadingEnabled));
        }

        #endregion

        #region Methods

        /// <summary>
        /// The dispose core.
        /// </summary>
        protected override void DisposeCore()
        {
            this.dataContext?.Dispose();
        }

        #endregion
    }
}