// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAttributeMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The global attribute map.
    /// </summary>
    public class GlobalAttributeMap : EntityTypeConfiguration<GlobalAttribute>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalAttributeMap"/> class.
        /// </summary>
        public GlobalAttributeMap()
            : this("Config")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalAttributeMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public GlobalAttributeMap(string schema)
        {
            this.ToTable(schema + ".GlobalAttributes");
            this.HasKey(x => x.Name);

            this.Property(x => x.Name)
                .HasColumnName("Name")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(50)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.Property(x => x.CreatedOnDate).HasColumnName("CreatedOnDate").IsRequired().HasColumnType("datetime");
            this.Property(x => x.Description)
                .HasColumnName("Description")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(255);
            this.Property(x => x.IsEditable).HasColumnName("IsEditable").IsRequired().HasColumnType("bit");
            this.Property(x => x.Value).HasColumnName("Value").IsRequired().HasColumnType("nvarchar");
        }
    }
}