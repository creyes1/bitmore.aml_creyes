// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLogMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The app log map.
    /// </summary>
    public class AppLogMap : EntityTypeConfiguration<AppLog>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogMap"/> class.
        /// </summary>
        public AppLogMap()
            : this("App")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public AppLogMap(string schema)
        {
            this.ToTable(schema + ".AppLog");
            this.HasKey(x => x.AppLogId);

            this.Property(x => x.AppLogId)
                .HasColumnName("AppLogID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.AppLogType)
                .HasColumnName("AppLogType")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);
            this.Property(x => x.Data).HasColumnName("Data").IsOptional().HasColumnType("nvarchar");
            this.Property(x => x.Date).HasColumnName("Date").IsRequired().HasColumnType("datetime");
            this.Property(x => x.Level).HasColumnName("Level").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            this.Property(x => x.Logger)
                .HasColumnName("Logger")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(255);
            this.Property(x => x.Message)
                .HasColumnName("Message")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(4000);
            this.Property(x => x.Thread)
                .HasColumnName("Thread")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(255);
            this.Property(x => x.UserName)
                .HasColumnName("UserName")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);
        }
    }
}