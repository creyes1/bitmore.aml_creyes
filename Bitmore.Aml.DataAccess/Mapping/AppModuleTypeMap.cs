// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleTypeMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The app module type map.
    /// </summary>
    public class AppModuleTypeMap : EntityTypeConfiguration<AppModuleType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleTypeMap"/> class.
        /// </summary>
        public AppModuleTypeMap()
            : this("App")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleTypeMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public AppModuleTypeMap(string schema)
        {
            this.ToTable(schema + ".AppModuleType");
            this.HasKey(x => x.AppModuleTypeId);

            this.Property(x => x.AppModuleTypeId)
                .HasColumnName("AppModuleTypeID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
        }
    }
}