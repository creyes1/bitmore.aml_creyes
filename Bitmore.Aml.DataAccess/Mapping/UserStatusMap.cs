// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserStatusMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The user status map.
    /// </summary>
    public class UserStatusMap : EntityTypeConfiguration<UserStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserStatusMap"/> class.
        /// </summary>
        public UserStatusMap()
            : this("Person")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStatusMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public UserStatusMap(string schema)
        {
            this.ToTable(schema + ".UserStatus");
            this.HasKey(x => x.UserStatusId);

            this.Property(x => x.UserStatusId)
                .HasColumnName("UserStatusID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
        }
    }
}