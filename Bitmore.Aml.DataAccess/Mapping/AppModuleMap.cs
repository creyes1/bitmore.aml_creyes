// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The app module map.
    /// </summary>
    public class AppModuleMap : EntityTypeConfiguration<AppModule>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleMap"/> class.
        /// </summary>
        public AppModuleMap()
            : this("App")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public AppModuleMap(string schema)
        {
            this.ToTable(schema + ".AppModule");
            this.HasKey(x => x.AppModuleId);

            this.Property(x => x.AppModuleId)
                .HasColumnName("AppModuleID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.AppModuleTypeId)
                .HasColumnName("AppModuleTypeID")
                .IsRequired()
                .HasColumnType("uniqueidentifier");
            this.Property(x => x.CssOrDesktopIcon)
                .HasColumnName("CssOrDesktopIcon")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(50);
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            this.Property(x => x.ParentAppModuleId)
                .HasColumnName("ParentAppModuleID")
                .IsOptional()
                .HasColumnType("uniqueidentifier");
            this.Property(x => x.TypeOrUrl)
                .HasColumnName("TypeOrUrl")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);
            this.Property(x => x.IsPrivate)
                .HasColumnName("IsPrivate")
                .IsRequired()
                .HasColumnType("bit");

            this.HasOptional(a => a.ParentAppModule)
                .WithMany(b => b.ChildrenAppModules)
                .HasForeignKey(c => c.ParentAppModuleId);
            this.HasRequired(a => a.AppModuleType).WithMany(b => b.AppModules).HasForeignKey(c => c.AppModuleTypeId);
        }
    }
}