// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompanyMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The company map.
    /// </summary>
    public class CompanyMap : EntityTypeConfiguration<Company>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyMap"/> class.
        /// </summary>
        public CompanyMap()
            : this("Company")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public CompanyMap(string schema)
        {
            this.ToTable(schema + ".Company");
            this.HasKey(x => x.CompanyId);

            this.Property(x => x.CompanyId)
                .HasColumnName("CompanyID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(150);
            this.Property(x => x.CreatedOnDate).HasColumnName("CreatedOnDate").IsOptional().HasColumnType("datetime");
        }

        #endregion
    }
}