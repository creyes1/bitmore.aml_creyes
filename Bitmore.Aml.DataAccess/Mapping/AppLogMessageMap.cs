// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppLogMessageMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The app log message map.
    /// </summary>
    public class AppLogMessageMap : EntityTypeConfiguration<AppLogMessage>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogMessageMap"/> class.
        /// </summary>
        public AppLogMessageMap()
            : this("App")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogMessageMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public AppLogMessageMap(string schema)
        {
            this.ToTable(schema + ".AppLogMessage");
            this.HasKey(x => x.AppLogMessageId);

            this.Property(x => x.AppLogMessageId)
                .HasColumnName("AppLogMessageID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.IsRegex).HasColumnName("IsRegex").IsRequired().HasColumnType("bit");
            this.Property(x => x.Message).HasColumnName("Message").IsOptional().HasColumnType("nvarchar");
            this.Property(x => x.RegexStackTraceOrClass)
                .HasColumnName("RegexStackTraceOrClass")
                .IsRequired()
                .HasColumnType("nvarchar");
        }
    }
}