// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppModuleActionMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The app module action map.
    /// </summary>
    public class AppModuleActionMap : EntityTypeConfiguration<AppModuleAction>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleActionMap"/> class.
        /// </summary>
        public AppModuleActionMap()
            : this("App")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppModuleActionMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public AppModuleActionMap(string schema)
        {
            this.ToTable(schema + ".AppModuleAction");
            this.HasKey(x => x.AppModuleActionId);

            this.Property(x => x.AppModuleActionId)
                .HasColumnName("AppModuleActionID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.AppModuleId)
                .HasColumnName("AppModuleID")
                .IsRequired()
                .HasColumnType("uniqueidentifier");
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);

            this.HasRequired(a => a.AppModule).WithMany(b => b.AppModuleActions).HasForeignKey(c => c.AppModuleId);
            this.HasMany(t => t.Roles).WithMany(t => t.AppModuleActions).Map(
                m =>
                    {
                        m.ToTable("RoleAppModuleAction", "Person");
                        m.MapLeftKey("AppModuleActionID");
                        m.MapRightKey("RoleID");
                    });
        }
    }
}