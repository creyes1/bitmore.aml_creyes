// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The user map.
    /// </summary>
    public class UserMap : EntityTypeConfiguration<User>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserMap"/> class.
        /// </summary>
        public UserMap()
            : this("Person")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public UserMap(string schema)
        {
            this.ToTable(schema + ".User");
            this.HasKey(x => x.UserId);

            this.Property(x => x.UserId)
                .HasColumnName("UserID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.CreatedOnDate)
                .HasColumnName("CreatedOnDate")
                .IsRequired()
                .HasColumnType("datetime")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(x => x.CurrentLogIn).HasColumnName("CurrentLogIn").IsOptional().HasColumnType("datetime");
            this.Property(x => x.CurrentRoleId)
                .HasColumnName("CurrentRoleID")
                .IsRequired()
                .HasColumnType("uniqueidentifier");
            this.Property(x => x.DefaultLanguage)
                .HasColumnName("DefaultLanguage")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(5);
            this.Property(x => x.Email).HasColumnName("Email").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            this.Property(x => x.LastLogIn).HasColumnName("LastLogIn").IsOptional().HasColumnType("datetime");
            this.Property(x => x.MaternalLastName)
                .HasColumnName("MaternalLastName")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(50);
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            this.Property(x => x.Password)
                .HasColumnName("Password")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(200);
            this.Property(x => x.PasswordAttempt).HasColumnName("PasswordAttempt").IsRequired().HasColumnType("tinyint");
            this.Property(x => x.PasswordUpdatedOnDate)
                .HasColumnName("PasswordUpdatedOnDate")
                .IsOptional()
                .HasColumnType("datetime");
            this.Property(x => x.PaternalLastName)
                .HasColumnName("PaternalLastName")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(50);
            this.Property(x => x.RowVersion)
                .HasColumnName("RowVersion")
                .IsRequired()
                .IsFixedLength()
                .HasColumnType("timestamp")
                .HasMaxLength(8)
                .IsRowVersion()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(x => x.Salt).HasColumnName("Salt").IsRequired().HasColumnType("nvarchar").HasMaxLength(100);
            this.Property(x => x.SessionToken)
                .HasColumnName("SessionToken")
                .IsRequired()
                .HasColumnType("uniqueidentifier");
            this.Property(x => x.UserName)
                .HasColumnName("UserName")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);
            this.Property(x => x.UserStatusId)
                .HasColumnName("UserStatusID")
                .IsRequired()
                .HasColumnType("uniqueidentifier");
            this.Property(x => x.DisplayName)
                .HasColumnName("DisplayName")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(152)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(x => x.HasActiveSession).HasColumnName("HasActiveSession").IsRequired();
            this.Property(x => x.ProfileImage)
                .HasColumnName("ProfileImage")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);
            this.Property(x => x.CompanyId).HasColumnName("CompanyID").IsOptional().HasColumnType("uniqueidentifier");

            this.HasOptional(a => a.Company).WithMany(b => b.Users).HasForeignKey(c => c.CompanyId);
            this.HasRequired(a => a.Role).WithMany(b => b.UsersInRole).HasForeignKey(c => c.CurrentRoleId);
            this.HasRequired(a => a.UserStatus).WithMany(b => b.Users).HasForeignKey(c => c.UserStatusId);
        }
    }
}