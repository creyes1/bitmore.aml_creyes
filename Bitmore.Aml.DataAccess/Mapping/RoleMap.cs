// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleMap.cs" company="Bitmore Technologies">
//   Copyright (c) Bitmore Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Bitmore.Aml.DataAccess.Mapping
{
    #region

    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Bitmore.Aml.Domain.Entities;

    #endregion

    /// <summary>
    /// The role map.
    /// </summary>
    public class RoleMap : EntityTypeConfiguration<Role>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleMap"/> class.
        /// </summary>
        public RoleMap()
            : this("Person")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleMap"/> class.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        public RoleMap(string schema)
        {
            this.ToTable(schema + ".Role");
            this.HasKey(x => x.RoleId);

            this.Property(x => x.RoleId)
                .HasColumnName("RoleID")
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.CreatedOnDate)
                .HasColumnName("CreatedOnDate")
                .IsRequired()
                .HasColumnType("datetime")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.Property(x => x.Description)
                .HasColumnName("Description")
                .IsOptional()
                .HasColumnType("nvarchar")
                .HasMaxLength(200);
            this.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            this.Property(x => x.RowVersion)
                .HasColumnName("RowVersion")
                .IsRequired()
                .IsFixedLength()
                .HasColumnType("timestamp")
                .HasMaxLength(8)
                .IsRowVersion()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            this.HasMany(t => t.Users).WithMany(t => t.Roles).Map(
                m =>
                    {
                        m.ToTable("UserRole", "Person");
                        m.MapLeftKey("RoleID");
                        m.MapRightKey("UserID");
                    });
        }
    }
}